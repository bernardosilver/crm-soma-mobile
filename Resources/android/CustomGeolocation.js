var httpClient = null;
var warnning = false;

function getCurrentCoordinatesiOS(action) {

    Ti.Geolocation.purpose = "Localização Atual via GPS";
    Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
    Ti.Geolocation.distanceFilter = 0;
    Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;

    var locationCallBack = function (e) {
        if (!e.success || e.error) {
            if (warnning === false) {
                warnning = true;
            }
        }

        if (action) action(e.coords);

        Ti.Geolocation.removeEventListener('location', locationCallBack);
    };
    Ti.Geolocation.addEventListener('location', locationCallBack);
}

function getCurrentCoordinatesAndroid(action) {
    if (Ti.Geolocation.locationServicesEnabled) {
        Titanium.Geolocation.getCurrentPosition(function (e) {
            if (e.error) {
                Ti.API.error('Error: ' + e.error);
            } else {
                action(e.coords);
                Ti.API.info(e.coords);
            }
        });
    } else {
        console.log('DESATIVADO');
        alert('Please enable location services');
    }
}

function translateErrorCode(code) {
    if (code === null) {
        return null;
    }
    switch (code) {
        case Ti.Geolocation.ERROR_LOCATION_UNKNOWN:
            return "Localização desconhecida";
            break;
        case Ti.Geolocation.ERROR_DENIED:
            return "Acesso negado";
            break;
        case Ti.Geolocation.ERROR_NETWORK:
            return "Erro de conexão";
            break;
        case Ti.Geolocation.ERROR_HEADING_FAILURE:
            return "Falha em detectar posição";
            break;
        case Ti.Geolocation.ERROR_REGION_MONITORING_DENIED:
            return "Acesso negado ao monitoramento de região";
            break;
        case Ti.Geolocation.ERROR_REGION_MONITORING_FAILURE:
            return "Falha no acesso ao monitoramento de região";
            break;
        case Ti.Geolocation.ERROR_REGION_MONITORING_DELAYED:
            return "Instalação de monitoramento região adiada";
            break;
        default:
            return "Erro desconhecido";
            break;
    }
}

function getCurrentCoordinates(action) {
    if (true) {
        getCurrentCoordinatesAndroid(action);
    } else {
        getCurrentCoordinatesiOS(action);
    }
}

exports.hasPermission = function () {
    return Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE);
};

exports.getCoordinates = function (action) {
    var warnning = false;
    if (Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
        getCurrentCoordinates(action);
    } else {
        Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function (e) {
            Ti.API.info(JSON.stringify(e));
            if (e.success === true) {
                getCurrentCoordinates(action);
            } else {
                Ti.API.error("Access denied, error: " + e.error);
                action(null);
            }
        });
    }
};

exports.getDistance = function (lat1, lon1, lat2, lon2) {
    var R = 6371;

    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    return d;
};

function checkComponent(components) {
    var types = [{ input: "route", result: "logradouro" }, { input: "street_number", result: "numero" }, { input: "administrative_area_level_2", result: "cidade" }, { input: "administrative_area_level_1", result: "estado" }, { input: "locality", result: "cidade" }, { input: "postal_code", result: "cep" }, { input: "sublocality_level_1", result: "bairro" }];

    for (var j in types) {
        if (components.types.indexOf(types[j].input) > -1) {
            return { key: types[j].result, value: components.long_name };
        }
    }
}

function getAddressComponets(components) {
    var address = {};
    for (var i in components) {
        var result = checkComponent(components[i]);
        if (result) address[result.key] = result.value;
    }
    return address;
}

exports.getCoordinatesByAddress = function (params) {
    var address = params.address || "";
    var input = params.input;
    var latitude = params.latitude;
    var longitude = params.longitude;
    var successCallBack = params.successCallBack;
    var errorCallBack = params.errorCallBack;

    if (httpClient) {
        Ti.API.info("ABORT");
        httpClient.abort();
        httpClient = null;
    }
    Ti.API.info("GET ADDRESSES");
    httpClient = Titanium.Network.createHTTPClient({
        timeout: 20000
    });

    var key = "AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE";

    var body = "address=" + encodeURI(address);
    body = body + "&key=" + key;
    var url = "https://maps.googleapis.com/maps/api/geocode/json?" + body;

    httpClient.open("GET", url);
    httpClient.setRequestHeader("Cache-Control", "no-cache, no-store, must-revalidate");

    httpClient.onload = function () {
        Ti.API.info('onload called, readyState = ' + this.readyState + " Response: " + this.responseText + " Length: " + this.responseData.length);
        try {
            var json = JSON.parse(this.responseText);
            if (json) {
                var address = {};
                if (json.results && json.results.length) {
                    address = getAddressComponets(json.results[0].address_components);
                    address.latitude = json.results[0].geometry.location.lat;
                    address.longitude = json.results[0].geometry.location.lng;
                }

                successCallBack(address);
            } else {
                errorCallBack();
            }
        } catch (e) {
            Ti.API.error("Erro: " + e.message + " Response: " + this.responseText);
        }
    };

    httpClient.onerror = function (e) {
        Ti.API.error('onerror called, readyState = ' + this.readyState + ' error = ' + e.error);
    };

    httpClient.send();
};

function getAddressesList(predictions) {
    var addresses = [];
    for (var i in predictions) {
        if (addresses.length < 10) addresses.push({ address: predictions[i].description });else return addresses;
    }
    return addresses;
}

exports.getAddresses = function (params) {
    var input = params.input;
    var latitude = params.latitude;
    var longitude = params.longitude;
    var successCallBack = params.successCallBack;
    var errorCallBack = params.errorCallBack;

    if (httpClient) {
        Ti.API.info("ABORT");
        httpClient.abort();
        httpClient = null;
    }
    Ti.API.info("GET ADDRESSES");
    httpClient = Titanium.Network.createHTTPClient({
        timeout: 20000
    });

    var key = "AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE";

    var body = "input=" + encodeURI(input);
    body = body + "&language=pt_BR";

    if (latitude && longitude) body = body + "&location=" + latitude + "," + longitude;

    body = body + "&key=" + key;

    httpClient.open("GET", "https://maps.googleapis.com/maps/api/place/autocomplete/json?" + body);
    httpClient.setRequestHeader("Cache-Control", "no-cache, no-store, must-revalidate");

    httpClient.onload = function () {
        Ti.API.info('onload called, readyState = ' + this.readyState + " Response: " + this.responseText + " Length: " + this.responseData.length);
        try {
            var json = JSON.parse(this.responseText);
            successCallBack(getAddressesList(json.predictions));
        } catch (e) {
            Ti.API.error("Erro: " + e.message + " Response: " + this.responseText);
        }
    };

    httpClient.onerror = function (e) {
        Ti.API.error('onerror called, readyState = ' + this.readyState + ' error = ' + e.error);
    };

    httpClient.send();
};