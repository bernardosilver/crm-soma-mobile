var moment = require('alloy/moment');

var languages = ['pt-br'];

require('alloy/moment/lang/pt-br');

function selectLanguage(lang) {
    [lang.split('-')[0], lang.toLowerCase()].forEach(function (lang) {
        if (languages.indexOf(lang) !== -1) {
            moment.lang(lang);
        }
    });
}

exports.selectLanguage = selectLanguage;