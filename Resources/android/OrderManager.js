let moment = require('alloy/moment');

const formatData = dia => {
    console.log('DATA SPLIT: ', dia);
    let arr = dia.split('/');
    let dt = arr[2] + arr[1] + arr[0];
    return dt;
};

let pedido = {
    desconto: 0,
    cabecalho: {},
    items: []
};

let produtos = [];

pedido.cabecalho.CJ_FRTBAST = 0;

let total = 0;
let totalProd = 0;
let pesoTotal = 0;
let valorFret = 0;

var CLIENTE = false;
var TABELA = false;
var PAGAMENTO = false;
var PARCELA = false;
var FRETE = false;

const getStatus = () => {
    if (!CLIENTE) return 1;else if (!TABELA) return 2;else if (!PAGAMENTO) return 3;else if (!PARCELA) return 4;else if (!FRETE) return 5;else return 0;
};

const deletePedido = () => {
    pedido = {
        cabecalho: {},
        items: []
    };
};

const isActive = () => {
    const status = getStatus();
    if (status !== 0) return true;else return false;
};

const setCliente = cliente => {
    if (cliente) {
        pedido.cabecalho.CJ_CLIENTE = cliente.A1_COD;
        pedido.cabecalho.CJ_CLIENT = cliente.A1_COD;
        pedido.cabecalho.CJ_LOJA = cliente.A1_LOJA, pedido.cabecalho.CJ_LOJAENT = cliente.CJ_LOJAENT;
        pedido.cabecalho.SLD = cliente.SLD;
        pedido.cabecalho.CJ_PESSOA = cliente.A1_PESSOA;
        pedido.cabecalho.CJ_CGC = cliente.A1_CGC;
        pedido.cabecalho.CJ_NOME = cliente.A1_NOME;
        pedido.cabecalho.CJ_NREDUZ = cliente.A1_NREDUZ;
        CLIENTE = true;
    } else {
        pedido.cabecalho.CJ_CLIENTE = null;
        pedido.cabecalho.CJ_CLIENT = null;
        pedido.cabecalho.CJ_LOJA = null;
        pedido.cabecalho.CJ_LOJAENT = null;
        pedido.cabecalho.SLD = null;
        pedido.cabecalho.CJ_PESSOA = null;
        pedido.cabecalho.CJ_CGC = null;
        pedido.cabecalho.CJ_NOME = null;
        pedido.cabecalho.CJ_NREDUZ = null;
        CLIENTE = false;
    }
    setTabela();
    setPagamento();
    setParcela();
    removeProdutos();
};

const setTabela = tabela => {
    console.log('TABELA: ', tabela);
    pedido.cabecalho.CJ_TABELA = tabela ? tabela.CodTab : '';
    pedido.cabecalho.CJ_DTTAB = tabela ? tabela.DtTab : '';
    pedido.cabecalho.CJ_FILIAL = tabela ? tabela.Filial : '';
    TABELA = true;
    if (!tabela) TABELA = false;

    setPagamento();
    setParcela();
    removeProdutos();
};

const setPagamento = pagamento => {
    pedido.cabecalho.CJ_CONDPAG = pagamento ? pagamento.CONDPG : '';
    pedido.cabecalho.ACRSVEN = pagamento ? pagamento.ACRSVEN : '';
    pedido.cabecalho.DESCVEN = pagamento ? pagamento.DESCVEN : '';
    PAGAMENTO = true;

    if (pagamento && pagamento.CONDPG == '003') {
        PARCELA = false;
    } else if (pagamento && pagamento.CONDPG != '003') {
        PARCELA = true;
    } else if (!pagamento) {
        PAGAMENTO = false;
    }
    setParcela();
};

const setParcela = parcela => {
    pedido.cabecalho.CJ_DATA1 = parcela && parcela[0] ? formatData(parcela[0]) : '';

    pedido.cabecalho.CJ_DATA2 = parcela && parcela[1] ? formatData(parcela[1]) : '';

    pedido.cabecalho.CJ_DATA3 = parcela && parcela[2] ? formatData(parcela[2]) : '';

    pedido.cabecalho.CJ_DATA4 = parcela && parcela[3] ? formatData(parcela[3]) : '';

    pedido.cabecalho.CJ_DATA5 = parcela && parcela[4] ? formatData(parcela[4]) : '';

    pedido.cabecalho.CJ_DATA6 = parcela && parcela[5] ? formatData(parcela[5]) : '';

    pedido.cabecalho.CJ_DATA7 = parcela && parcela[6] ? formatData(parcela[6]) : '';

    pedido.cabecalho.CJ_DATA8 = parcela && parcela[7] ? formatData(parcela[7]) : '';

    pedido.cabecalho.CJ_DATA9 = parcela && parcela[8] ? formatData(parcela[8]) : '';

    PARCELA = true;
    if (!parcela && pedido.cabecalho.CJ_CONDPAG == '003') PARCELA = false;
};

const setFrete = frete => {
    const value = frete.CJ_FRTBAST.replace(".", "").replace(",", ".");
    console.log('SET FRETE: ', value, parseFloat(value));
    pedido.cabecalho.CJ_TPFRETE = frete ? frete.CJ_TPFRETE : '';
    pedido.cabecalho.CJ_FRTBAST = frete ? parseFloat(value) : 0;
    FRETE = true;
    calcTotalPedido();
};

const setDesconto = desc => {
    pedido.desconto = desc ? desc : 0;
};

const setObs = obs => {
    pedido.cabecalho.CJ_OBS = obs;
};

const removeProdutos = () => {
    produtos = [];
    calcTotalPedido();
};

const insertItems = (values, produto) => {
    let encontrou = _.find(produtos, prod => {
        return prod.CODPRO == produto.CODPRO;
    });
    if (encontrou) console.log('ENCONTROU O PRODUTO: ', encontrou);
    produto.CK_QTDVEN = values.qnt;
    produto.CK_DESCONT = values.desc;
    produto.CK_VALOR = values.val * values.qnt;
    produto.CK_PRUNIT = values.val;
    produto.TPESO = parseFloat(produto.PESO) * parseFloat(values.qnt);
    produtos.push(produto);
    calcTotalPedido();
};

const updateItem = (values, produto) => {
    _.find(produtos, prod => {
        if (prod.CODPRO == produto.CODPRO) {
            prod.CK_QTDVEN = values.qnt;
            prod.CK_DESCONT = values.desc;
            prod.CK_VALOR = values.val * values.qnt;
            prod.CK_PRUNIT = values.val;
            prod.TPESO = parseFloat(produto.PESO) * parseFloat(values.qnt);
        }
    });
    calcTotalPedido();
};

const setItem = produto => {
    const prod = {
        CK_NUM: '',
        CK_FILIAL: pedido.cabecalho.CJ_FILIAL,
        CK_ITEM: pedido.items.length + 1,
        CK_PRODUTO: produto.CODPRO,
        CK_DESCRI: produto.DESCR,
        CK_UM: produto.UN,
        CK_LOCAL: produto.ARMZ,
        PRCTAB: produto.PRVEN,
        CK_PRCVEN: '',
        CK_PRUNIT: produto.CK_PRUNIT,
        CK_COMIS1: produto.COMIS,
        CALCIPI: '',
        CALCST: '',
        CK_QTDVEN: produto.CK_QTDVEN,
        CK_DESCONT: produto.CK_DESCONT,
        CK_VALOR: produto.CK_VALOR,
        CK_CLIENTE: pedido.cabecalho.CJ_CLIENTE,
        CK_LOJA: pedido.cabecalho.CJ_LOJA,
        CK_ENTREG: '',
        CK_FILVEN: '',
        CK_FILENT: '',
        CK_DT1VEN: '',
        CK_YOPER: '',
        CK_DTALT: '',
        GRUPO: produto.GRUPO,
        PESOUNI: parseFloat(produto.PESO).toFixed(2),
        TPESO: produto.TPESO,
        TVLRFRT: '',
        FRTPROD: '',
        ID: '',
        CJID: '',
        RECNO: 0,
        DTSYNC: '',
        CK_COTCLI: ''
    };

    pedido.items.push(prod);
};

const calcTotalPedido = () => {
    console.log('SOMADO: ', pedido.cabecalho.CJ_FRTBAST);
    const reducerValor = (accumulator, currentValue) => accumulator + parseFloat(currentValue.CK_VALOR);
    const reducerPeso = (accumulator, currentValue) => accumulator + parseFloat(currentValue.TPESO);
    const valorProds = produtos.reduce(reducerValor, 0);
    valorFret = produtos.reduce(reducerPeso, 0) / 1000 * pedido.cabecalho.CJ_FRTBAST;
    pesoTotal = produtos.reduce(reducerPeso, 0);

    total = valorProds + valorFret;
    totalProd = valorProds;
};

const getCliente = () => {
    return {
        A1_COD: pedido.cabecalho.CJ_CLIENTE,
        A1_LOJA: pedido.cabecalho.CJ_LOJA,
        CJ_LOJAENT: pedido.cabecalho.CJ_LOJAENT,
        SLD: pedido.cabecalho.SLD,
        A1_PESSOA: pedido.cabecalho.CJ_PESSOA,
        A1_CGC: pedido.cabecalho.CJ_CGC,
        A1_NOME: pedido.cabecalho.CJ_NOME,
        A1_NREDUZ: pedido.cabecalho.CJ_NREDUZ
    };
};

const getSaldo = () => {
    return pedido.cabecalho.SLD;
};

const getTabela = () => {
    return {
        CJ_TABELA: pedido.cabecalho.CJ_TABELA,
        CJ_DTTAB: pedido.cabecalho.CJ_DTTAB
    };
};

const getPagamento = () => {
    return {
        CJ_CONDPAG: pedido.cabecalho.CJ_CONDPAG,
        ACRSVEN: pedido.cabecalho.ACRSVEN,
        DESCVEN: pedido.cabecalho.DESCVEN
    };
};

const getParcela = () => {
    return {
        CJ_PARC1: pedido.cabecalho.CJ_PARC1,
        CJ_DATA1: pedido.cabecalho.CJ_DATA1,

        CJ_PARC2: pedido.cabecalho.CJ_PARC2,
        CJ_DATA2: pedido.cabecalho.CJ_DATA2,

        CJ_PARC3: pedido.cabecalho.CJ_PARC3,
        CJ_DATA3: pedido.cabecalho.CJ_DATA3,

        CJ_PARC4: pedido.cabecalho.CJ_PARC4,
        CJ_DATA4: pedido.cabecalho.CJ_DATA4,

        CJ_PARC5: pedido.cabecalho.CJ_PARC5,
        CJ_DATA5: pedido.cabecalho.CJ_DATA5,

        CJ_PARC6: pedido.cabecalho.CJ_PARC6,
        CJ_DATA6: pedido.cabecalho.CJ_DATA6,

        CJ_PARC7: pedido.cabecalho.CJ_PARC7,
        CJ_DATA7: pedido.cabecalho.CJ_DATA7,

        CJ_PARC8: pedido.cabecalho.CJ_PARC8,
        CJ_DATA8: pedido.cabecalho.CJ_DATA8,

        CJ_PARC9: pedido.cabecalho.CJ_PARC9,
        CJ_DATA9: pedido.cabecalho.CJ_DATA9
    };
};

const getFrete = () => {
    return {
        CJ_TPFRETE: pedido.cabecalho.CJ_TPFRETE,
        CJ_FRTBAST: pedido.cabecalho.CJ_FRTBAST
    };
};

const getDesconto = () => {
    return pedido.desconto;
};

const getObs = () => {
    return pedido.cabecalho.CJ_OBS;
};

const getItems = () => {
    return produtos;
};

const getPedido = () => {
    return pedido;
};

const getTotalPedido = () => {
    return { valor: total, totalProd: totalProd, qntItens: produtos.length, pesoTotal: pesoTotal, valorFret: valorFret };
};

const orderGenerate = () => {
    var ped = {
        CJ_FILIAL: '',
        CJ_NUM: '',
        CJ_EMISSAO: moment(),
        CJ_CLIENTE: '',
        CJ_LOJA: '01',
        CJ_CLIENT: '',
        CJ_LOJAENT: '01',
        CJ_CONDPAG: '',
        CJ_TABELA: '',
        CJ_DTTAB: '',
        CJ_STATUS: 'A',
        CJ_VALIDA: '',
        CJ_MOEDA: '1',
        CJ_TIPLIB: '2',
        CJ_TPCARGA: '1',
        CJ_TXMOEDA: '1',
        CJ_ROTEIRO: '',
        CJ_TPFRETE: '',
        CJ_FRTBAST: 0,
        CJ_VEND: '',
        CJ_OBS: '',
        CJ_PARC1: '',
        CJ_DATA1: '',
        CJ_PARC2: '',
        CJ_DATA2: '',
        CJ_PARC3: '',
        CJ_DATA3: '',
        CJ_PARC4: '',
        CJ_DATA4: '',
        CJ_PARC5: '',
        CJ_DATA5: '',
        CJ_PARC6: '',
        CJ_DATA6: '',
        CJ_PARC7: '',
        CJ_DATA7: '',
        CJ_PARC8: '',
        CJ_DATA8: '',
        CJ_PARC9: '',
        CJ_DATA9: '',
        CJ_DTALT: '',
        GRPVEN: '',
        FATORCONDPG: '',
        DTPREV: '',
        ID: '',
        ORC_ID: '',
        RECNO: '',
        DTSYNC: '1900-01-01 00:00:00',
        DEL: ''
    };
};

exports.getStatus = getStatus;
exports.isActive = isActive;
exports.delete = deletePedido;
exports.setCliente = setCliente;
exports.setTabela = setTabela;
exports.setPagamento = setPagamento;
exports.setParcela = setParcela;
exports.setFrete = setFrete;
exports.setDesconto = setDesconto;
exports.setObs = setObs;
exports.setItem = setItem;
exports.insertItems = insertItems;
exports.updateItem = updateItem;
exports.getCliente = getCliente;
exports.getSaldo = getSaldo;
exports.getTabela = getTabela;
exports.getPagamento = getPagamento;
exports.getParcela = getParcela;
exports.getFrete = getFrete;
exports.getDesconto = getDesconto;
exports.getObs = getObs;
exports.getItems = getItems;
exports.getPedido = getPedido;
exports.getTotalPedido = getTotalPedido;
exports.orderGenerate = orderGenerate;