
exports.removeCaracter = function (txt) {
    var tmp = txt.replace(/[ÁÀÂÃÄáàãâä]/g, 'A');
    tmp = tmp.replace(/[ÉÈÊËéèêë]/g, 'E');
    tmp = tmp.replace(/[ÍÌÎÏíìîï]/g, 'I');
    tmp = tmp.replace(/[ÓÓÕÔÖóòõôö]/g, 'O');
    tmp = tmp.replace(/[ÚÚÛÜúùûü]/g, 'U');
    tmp = tmp.replace(/[çÇ]/g, 'C');
    tmp = tmp.replace(/[^A-Za-z0-9-, ]{1,}/g, '');

    return tmp;
};

exports.removeAcentos = function (txt) {
    var tmp = txt.replace(/[ÁÀÂÃÄáàãâä]/g, 'A');
    tmp = tmp.replace(/[ÉÈÊËéèêë]/g, 'E');
    tmp = tmp.replace(/[ÍÌÎÏíìîï]/g, 'I');
    tmp = tmp.replace(/[ÓÓÕÔÖóòõôö]/g, 'O');
    tmp = tmp.replace(/[ÚÚÛÜúùûü]/g, 'U');
    tmp = tmp.replace(/[çÇ]/g, 'C');

    return tmp;
};

exports.removeTelMask = function (tel) {
    return tel.replace('(', '').replace(')', '').replace('-', '');
};

exports.removeCPFCNPJMask = function (value) {
    return value.replace(".", "").replace(".", "").replace("/", "").replace("-", "");
};

exports.real = function (int) {
    var tmp = int.replace(",", "").replace(".", "") + '';
    tmp = tmp.replace(/(\d)(\d{1,2})$/g, "$1,$2");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    if (tmp.length > 6) tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    return tmp;
};

exports.float = function (int) {
    var tmp = int.replace(",", "").replace(".", "") + '';
    tmp = tmp.replace(/(\d)(\d{1,2})$/g, "$1.$2");

    if (!tmp) tmp = '';
    if (tmp.length > 6) tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ",$1.$2");

    return tmp;
};

exports.int = function (int) {
    var tmp = int.replace(",", "").replace(".", "") + '';
    tmp = tmp.replace(/(\D{1,})$/g, "");
    return tmp;
};

exports.cel = function (int) {
    var tmp = int.replace("(", "").replace(")", "").replace("-", "") + '';
    if (tmp.length > 11) tmp = tmp.slice(0, 11);

    tmp = tmp.replace(/(\d{0,2})(\d{0,5})(\d{0,4})$/g, "($1)$2-$3");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.tel = function (int) {
    var tmp = int.replace("(", "").replace(")", "").replace("-", "") + '';
    if (tmp.length > 10) tmp = tmp.slice(0, 10);

    tmp = tmp.replace(/(\d{0,2})(\d{0,4})(\d{0,4})$/g, "($1)$2-$3");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.telefone = function (int) {
    var tmp = int.replace("(", "").replace(")", "").replace("-", "") + '';
    if (tmp.length > 11) tmp = tmp.slice(0, 11);

    tmp = tmp.replace(/(\d{0,2})(\d{0,4}\d{0,1}?)(\d{0,4})$/g, "($1)$2-$3");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.cep = function (int) {
    var tmp = int.replace("-", "") + '';
    if (tmp.length > 8) tmp = tmp.slice(0, 8);
    tmp = tmp.replace(/(\d{0,5})(\d{0,3})$/g, "$1-$2");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.date = function (int) {
    var tmp = int.replace("/", "").replace("/", "") + '';
    if (tmp.length > 8) tmp = tmp.slice(0, 8);

    tmp = tmp.replace(/(\d{0,1}\d{0,1})(\d{0,1}\d{0,1})(\d{0,3}\d{0,3})$/g, "$1/$2/$3");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.cpf = function (int) {
    var tmp = int.replace(".", "").replace(".", "").replace(".", "").replace("-", "") + '';
    if (tmp.length > 11) tmp = tmp.slice(0, 11);
    tmp = tmp.replace(/(\d{0,3})(\d{0,3})(\d{0,3})(\d{0,2})$/g, "$1.$2.$3-$4");

    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};

exports.cnpj = function (int) {
    var tmp = int.replace(".", "").replace("/", "").replace("-", "") + '';
    if (tmp.length > 14) tmp = tmp.slice(0, 14);

    tmp = tmp.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/g, "$1.$2.$3\/$4-$5");
    tmp = tmp.replace(/(\D{1,})$/g, "");

    return tmp;
};