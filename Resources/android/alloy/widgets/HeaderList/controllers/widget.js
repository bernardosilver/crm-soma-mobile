var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
	var index = s.lastIndexOf('/');
	var path = index === -1 ?
	'HeaderList/' + s :
	s.substring(0, index) + '/HeaderList/' + s.substring(index + 1);

	return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {
	var Widget = new (require('/alloy/widget'))('HeaderList');this.__widgetId = 'HeaderList';
	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'widget';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.mainView = Ti.UI.createView(
	{ backgroundColor: Alloy.Globals.DARK_MAIN_COLOR, id: "mainView", height: "40dp", width: Ti.UI.FILL });

	$.__views.mainView && $.addTopLevelView($.__views.mainView);
	$.__views.__alloyId0 = Ti.UI.createView(
	{ left: "10dp", right: "10dp", bottom: "10dp", width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "__alloyId0" });

	$.__views.mainView.add($.__views.__alloyId0);
	$.__views.lbTitle = Ti.UI.createLabel(
	{ color: Alloy.Globals.WHITE_COLOR, font: { fontSize: "18dp", fontWeight: "bold" }, height: Ti.UI.SIZE, width: Ti.UI.FILL, id: "lbTitle" });

	$.__views.__alloyId0.add($.__views.lbTitle);
	exports.destroy = function () {};




	_.extend($, $.__views);


	var args = arguments[0] || {};
	var title = args.title || "";
	var bgColor = args.bgColor || "white";

	$.lbTitle.setText(title);










	_.extend($, exports);
}

module.exports = Controller;