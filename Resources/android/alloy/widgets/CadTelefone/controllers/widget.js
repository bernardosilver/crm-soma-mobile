var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
	var index = s.lastIndexOf('/');
	var path = index === -1 ?
	'CadTelefone/' + s :
	s.substring(0, index) + '/CadTelefone/' + s.substring(index + 1);

	return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {
	var Widget = new (require('/alloy/widget'))('CadTelefone');this.__widgetId = 'CadTelefone';
	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'widget';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.viewMain = Ti.UI.createView(
	{ id: "viewMain", width: Ti.UI.FILL, height: "45dp" });

	$.__views.viewMain && $.addTopLevelView($.__views.viewMain);
	$.__views.viewBtApagar = Ti.UI.createView(
	{ id: "viewBtApagar", height: Ti.UI.FILL, width: "80dp", right: 0 });

	$.__views.viewMain.add($.__views.viewBtApagar);
	$.__views.btApagar = Ti.UI.createButton(
	{ backgroundColor: Alloy.Globals.RED_COLOR, id: "btApagar", height: Ti.UI.FILL, width: Ti.UI.FILL, title: "Apagar", color: "white" });

	$.__views.viewBtApagar.add($.__views.btApagar);
	$.__views.viewContents = Ti.UI.createView(
	{ id: "viewContents", height: Ti.UI.FILL, width: "100%", backgroundColor: "white" });

	$.__views.viewMain.add($.__views.viewContents);
	$.__views.__alloyId1 = Ti.UI.createImageView(
	{ touchEnabled: false, image: "/images/exit.png", height: "25dp", width: "25dp", left: "130dp", id: "__alloyId1" });

	$.__views.viewContents.add($.__views.__alloyId1);
	$.__views.tfTelefone1 = Ti.UI.createTextField(
	{ font: { fontSize: "17dp" }, color: "black", height: "45dp", backgroundColor: "white", right: 0, borderRadius: 5, paddingLeft: 10, paddingRight: 0, hintTextColor: "gray", bubbleParent: true, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfTelefone1", left: "50dp", bottom: 0, hintText: "Telefone alternativo", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

	$.__views.viewContents.add($.__views.tfTelefone1);
	$.__views.picMenos = Ti.UI.createView(
	{ id: "picMenos", width: "50dp", left: 0, bubbleParent: false, height: Ti.UI.FILL });

	$.__views.viewContents.add($.__views.picMenos);
	$.__views.__alloyId2 = Ti.UI.createImageView(
	{ width: "30dp", height: "30dp", left: "10dp", image: "/images/removeRed.png", id: "__alloyId2" });

	$.__views.picMenos.add($.__views.__alloyId2);
	$.__views.__alloyId3 = Ti.UI.createView(
	{ width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId3" });

	$.__views.viewMain.add($.__views.__alloyId3);
	exports.destroy = function () {};




	_.extend($, $.__views);


	var args = arguments[0] || {};
	var tipo = args.tipo || '';
	var tel = args.tel || '';
	var editable = args.editable || false;
	var mask = require('Mask');
	var actionApagar;

	if (tipo == 'fixo')
	$.tfTelefone1.setHintText('Telefone alternativo');else
	if (tipo == 'cel')
	$.tfTelefone1.setHintText('Telefone alternativo');
	if (tel) $.tfTelefone1.setValue(tel);

	$.tfTelefone1.setEditable(editable);

	exports.setFocus = function () {

		if (false)
		setTimeout(function () {$.tfTelefone1.focus();}, 200);

		$.viewMain.setHeight("1dp");
		$.viewMain.animate({ height: "45dp", duration: 200 }, function () {});
	};

	exports.getValues = function () {
		return {
			telefone: $.tfTelefone1.getValue() };

	};

	exports.setActionApagar = function (action) {
		actionApagar = action;
	};

	$.picMenos.addEventListener("click", function (e) {
		if (!editable) return true;
		$.viewContents.animate({ left: "-80dp", duration: 200 }, function () {});
	});

	$.btApagar.addEventListener("click", function (e) {
		if (!editable) return true;
		setTimeout(function () {actionApagar();}, 200);
		if (true) $.viewMain.animate({ height: "1dp", duration: 200 }, function () {});else
		if (false) {
			$.viewBtApagar.animate({ height: "0", duration: 200 }, function () {});
			$.viewContents.animate({ height: "0", duration: 200 }, function () {});
		}
	});

	$.tfTelefone1.addEventListener("change", function (e) {
		var v = tipo == 'fixo' ? mask.tel(e.value) : mask.telefone(e.value);
		if ($.tfTelefone1.getValue() != v) $.tfTelefone1.setValue(v);
		$.tfTelefone1.setSelection($.tfTelefone1.getValue().length, $.tfTelefone1.getValue().length);
	});









	_.extend($, exports);
}

module.exports = Controller;