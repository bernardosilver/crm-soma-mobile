var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
	var index = s.lastIndexOf('/');
	var path = index === -1 ?
	'CadRefComercial/' + s :
	s.substring(0, index) + '/CadRefComercial/' + s.substring(index + 1);

	return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {
	var Widget = new (require('/alloy/widget'))('CadRefComercial');this.__widgetId = 'CadRefComercial';
	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'widget';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.viewMain = Ti.UI.createView(
	{ id: "viewMain", width: Ti.UI.FILL, height: "200dp", top: "20dp" });

	$.__views.viewMain && $.addTopLevelView($.__views.viewMain);
	$.__views.viewBtApagar = Ti.UI.createView(
	{ id: "viewBtApagar", height: Ti.UI.FILL, width: "80dp", right: 0, backgroundColor: "yellow" });

	$.__views.viewMain.add($.__views.viewBtApagar);
	$.__views.btApagar = Ti.UI.createButton(
	{ backgroundColor: Alloy.Globals.RED_COLOR, id: "btApagar", height: Ti.UI.FILL, width: Ti.UI.FILL, title: "Apagar", color: "white" });

	$.__views.viewBtApagar.add($.__views.btApagar);
	$.__views.viewContents = Ti.UI.createView(
	{ id: "viewContents", height: Ti.UI.FILL, width: "100%", backgroundColor: "white" });

	$.__views.viewMain.add($.__views.viewContents);
	$.__views.__alloyId9 = Ti.UI.createView(
	{ width: "100%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, top: 0, id: "__alloyId9" });

	$.__views.viewContents.add($.__views.__alloyId9);
	$.__views.__alloyId10 = Ti.UI.createView(
	{ width: Ti.UI.FILL, height: "45dp", top: "2dp", left: "50dp", id: "__alloyId10" });

	$.__views.viewContents.add($.__views.__alloyId10);
	$.__views.tfEmpresa = Ti.UI.createTextField(
	{ font: { fontSize: "17dp" }, color: "black", height: "45dp", backgroundColor: "white", right: "5%", borderRadius: 5, paddingLeft: 5, paddingRight: 5, hintTextColor: "gray", bubbleParent: true, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfEmpresa", maxLength: 30, width: Ti.UI.FILL, hintText: "Nome da empresa", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

	$.__views.__alloyId10.add($.__views.tfEmpresa);
	$.__views.lbObgt1 = Ti.UI.createLabel(
	{ font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "lbObgt1", top: "25dp" });

	$.__views.__alloyId10.add($.__views.lbObgt1);
	$.__views.__alloyId11 = Ti.UI.createView(
	{ width: "100%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId11" });

	$.__views.__alloyId10.add($.__views.__alloyId11);
	$.__views.__alloyId12 = Ti.UI.createView(
	{ width: Ti.UI.FILL, height: Ti.UI.SIZE, top: "50dp", left: "50dp", id: "__alloyId12" });

	$.__views.viewContents.add($.__views.__alloyId12);
	$.__views.tfTelefone = Ti.UI.createTextField(
	{ font: { fontSize: "17dp" }, color: "black", height: "45dp", backgroundColor: "white", right: "5%", borderRadius: 5, paddingLeft: 5, paddingRight: 5, hintTextColor: "gray", bubbleParent: true, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfTelefone", width: Ti.UI.FILL, hintText: "Telefone - apenas números", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

	$.__views.__alloyId12.add($.__views.tfTelefone);
	$.__views.lbObgt2 = Ti.UI.createLabel(
	{ font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "lbObgt2", top: "25dp" });

	$.__views.__alloyId12.add($.__views.lbObgt2);
	$.__views.__alloyId13 = Ti.UI.createView(
	{ width: "100%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId13" });

	$.__views.__alloyId12.add($.__views.__alloyId13);
	$.__views.__alloyId14 = Ti.UI.createView(
	{ width: Ti.UI.FILL, height: "45dp", top: "100dp", left: "50dp", id: "__alloyId14" });

	$.__views.viewContents.add($.__views.__alloyId14);
	$.__views.tfCidadeUF = Ti.UI.createTextField(
	{ font: { fontSize: "17dp" }, color: "black", height: "45dp", backgroundColor: "white", right: "5%", borderRadius: 5, paddingLeft: 5, paddingRight: 5, hintTextColor: "gray", bubbleParent: true, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfCidadeUF", maxLength: 40, width: Ti.UI.FILL, hintText: "Cidade/UF", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

	$.__views.__alloyId14.add($.__views.tfCidadeUF);
	$.__views.lbObgt3 = Ti.UI.createLabel(
	{ font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "lbObgt3", top: "25dp" });

	$.__views.__alloyId14.add($.__views.lbObgt3);
	$.__views.__alloyId15 = Ti.UI.createView(
	{ width: "100%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId15" });

	$.__views.__alloyId14.add($.__views.__alloyId15);
	$.__views.__alloyId16 = Ti.UI.createView(
	{ width: Ti.UI.FILL, height: "45dp", top: "145dp", left: "50dp", id: "__alloyId16" });

	$.__views.viewContents.add($.__views.__alloyId16);
	$.__views.tfContato = Ti.UI.createTextField(
	{ font: { fontSize: "17dp" }, color: "black", height: "45dp", backgroundColor: "white", right: "5%", borderRadius: 5, paddingLeft: 5, paddingRight: 5, hintTextColor: "gray", bubbleParent: true, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfContato", maxLength: 30, width: Ti.UI.FILL, hintText: "Contato", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

	$.__views.__alloyId16.add($.__views.tfContato);
	$.__views.lbObgt4 = Ti.UI.createLabel(
	{ font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "lbObgt4", top: "25dp" });

	$.__views.__alloyId16.add($.__views.lbObgt4);
	$.__views.__alloyId17 = Ti.UI.createView(
	{ width: "100%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId17" });

	$.__views.__alloyId16.add($.__views.__alloyId17);
	$.__views.lbId = Ti.UI.createLabel(
	{ id: "lbId", visible: false });

	$.__views.viewContents.add($.__views.lbId);
	$.__views.picMenos = Ti.UI.createView(
	{ id: "picMenos", width: "40dp", left: 0, bubbleParent: false, height: Ti.UI.FILL });

	$.__views.viewContents.add($.__views.picMenos);
	$.__views.__alloyId18 = Ti.UI.createImageView(
	{ width: "30dp", height: "30dp", left: "10dp", image: "/images/removeRed.png", id: "__alloyId18" });

	$.__views.picMenos.add($.__views.__alloyId18);
	exports.destroy = function () {};




	_.extend($, $.__views);


	var args = arguments[0] || {};
	var obgt = args.obgt;
	var ref = args.ref || '';
	var mask = require('Mask');
	var editable = args.editable || false;
	var actionApagar;

	if (ref) {
		$.tfEmpresa.setValue(ref.AO_NOMINS);
		$.tfTelefone.setValue(mask.tel(ref.AO_TELEFON));
		$.tfCidadeUF.setValue(ref.AO_OBSERV);
		$.tfContato.setValue(ref.AO_CONTATO);
		$.lbId.setText(ref.ID);
	}

	$.tfEmpresa.setEditable(editable);
	$.tfTelefone.setEditable(editable);
	$.tfCidadeUF.setEditable(editable);
	$.tfContato.setEditable(editable);

	exports.setFocus = function () {
		if (false)
		setTimeout(function () {$.tfEmpresa.focus();}, 200);

		$.viewMain.setHeight("1dp");
		$.viewMain.animate({ height: "200dp", duration: 200 }, function () {});
	};

	exports.getValues = function () {
		return {
			nome: $.tfEmpresa.getValue(),
			telefone: $.tfTelefone.getValue(),
			cidadeUF: $.tfCidadeUF.getValue(),
			contato: $.tfContato.getValue(),
			ID: $.lbId.getText() };

	};

	exports.setActionApagar = function (action) {
		actionApagar = action;
	};

	$.picMenos.addEventListener("click", function (e) {
		if (!editable) return true;
		$.viewContents.animate({ left: "-80dp", duration: 200 }, function () {});
	});

	$.btApagar.addEventListener("click", function (e) {
		if (!editable) return true;
		setTimeout(function () {actionApagar();}, 200);
		if (true) $.viewMain.animate({ height: "1dp", duration: 200 }, function () {});else
		if (false) {
			$.viewBtApagar.animate({ height: "0", duration: 200 }, function () {});
			$.viewContents.animate({ height: "0", duration: 200 }, function () {});
		}
	});

	$.tfTelefone.addEventListener("change", function (e) {
		var v = mask.tel(e.value);
		if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
		$.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
	});

	$.lbObgt1.setVisible(obgt);
	$.lbObgt2.setVisible(obgt);
	$.lbObgt3.setVisible(obgt);
	$.lbObgt4.setVisible(obgt);

	var arraytf = [$.tfCpf];










	_.extend($, exports);
}

module.exports = Controller;