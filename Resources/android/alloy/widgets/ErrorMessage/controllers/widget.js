var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
		var index = s.lastIndexOf('/');
		var path = index === -1 ?
		'ErrorMessage/' + s :
		s.substring(0, index) + '/ErrorMessage/' + s.substring(index + 1);

		return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
		}
		return arg;
}

function Controller() {
		var Widget = new (require('/alloy/widget'))('ErrorMessage');this.__widgetId = 'ErrorMessage';
		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'widget';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.view = Ti.UI.createScrollView(
		{ id: "view", backgroundColor: "white", height: Ti.UI.FILL, width: Ti.UI.FILL, visible: false });

		$.__views.view && $.addTopLevelView($.__views.view);
		$.__views.__alloyId19 = Ti.UI.createView(
		{ height: Ti.UI.SIZE, width: Ti.UI.SIZE, layout: "vertical", top: "100dp", id: "__alloyId19" });

		$.__views.view.add($.__views.__alloyId19);
		$.__views.label = Ti.UI.createLabel(
		{ color: Alloy.Globals.DARK_GRAY_COLOR, font: { fontSize: "17dp" }, id: "label", width: "90%", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT });

		$.__views.__alloyId19.add($.__views.label);
		$.__views.button = Ti.UI.createButton(
		{ backgroundColor: Alloy.Globals.MAIN_COLOR, id: "button", height: "50dp", top: "30dp", color: "white", width: "60%", borderRadius: 3 });

		$.__views.__alloyId19.add($.__views.button);
		click ? $.addListener($.__views.button, 'click', click) : __defers['$.__views.button!click!click'] = true;exports.destroy = function () {};




		_.extend($, $.__views);


		var args = arguments[0] || {};

		var _class = args.class;

		for (prop in args) {
				if (prop != "class") {
						$.view[prop] = args[prop];
				} else {
						var style = $.createStyle({
								classes: args[prop] });

						$.view.applyProperties(style);
				}
		}

		var actionButton = function () {};
		$.button.setVisible(true);

		exports.setVisible = function (bool) {
				$.view.setVisible(bool);
				$.view.setTouchEnabled(bool);
		};

		exports.setText = function (text) {
				text = text || defaultMessage;
				$.label.setText(text);
		};

		exports.setTop = function (size) {
				$.view.setTop(size);
		};

		exports.setTextAlign = function (align) {
				$.label.setTextAlign(align);
		};

		exports.setBtTitle = function (title) {
				title = title || defaultTitle;
				$.button.setTitle(title);
		};

		exports.setBtAction = function (action) {
				actionButton = action || function () {};
				if (action)
				$.button.setVisible(true);else

				$.button.setVisible(false);
		};

		function click(e) {
				actionButton();
		}





		__defers['$.__views.button!click!click'] && $.addListener($.__views.button, 'click', click);



		_.extend($, exports);
}

module.exports = Controller;