var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
  var index = s.lastIndexOf('/');
  var path = index === -1 ?
  'PopUp/' + s :
  s.substring(0, index) + '/PopUp/' + s.substring(index + 1);

  return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {
  var Widget = new (require('/alloy/widget'))('PopUp');this.__widgetId = 'PopUp';
  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'widget';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.main = Ti.UI.createView(
  { touchEnabled: false, id: "main", width: Ti.UI.FILL, height: Ti.UI.FILL, opacity: 0, visible: false });

  $.__views.main && $.addTopLevelView($.__views.main);
  onClick ? $.addListener($.__views.main, 'click', onClick) : __defers['$.__views.main!click!onClick'] = true;$.__views.background = Ti.UI.createView(
  { id: "background", backgroundColor: "black", opacity: 0.3, width: Ti.UI.FILL, height: Ti.UI.FILL });

  $.__views.main.add($.__views.background);
  $.__views.content = Ti.UI.createScrollView(
  { id: "content", top: "50dp", bottom: "50dp", width: "85%", height: Ti.UI.SIZE, borderRadius: 4, backgroundColor: "white", elevation: 10, contentWidth: Ti.UI.FILL, showVerticalScrollIndicator: true });

  $.__views.main.add($.__views.content);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var Mask = require("Mask");
  var isDisplay = false;
  var lbColor = Alloy.Globals.DARK_GRAY_COLOR;
  var dialog = null;
  var OrderManager = require('OrderManager');

  function calcPreco(desc, prcIni) {
    if (desc) {
      return (parseFloat(prcIni).toFixed(2) * parseFloat(desc).toFixed(2) / 100 + parseFloat(prcIni)).toFixed(2);
    } else
    return prcIni;
  }

  function createViewLabelWithTag(options, callback, title, callBackPedir) {
    var view = Ti.UI.createScrollView({
      layout: "vertical",
      backgroundColor: "white",
      bottom: "25dp",
      top: "25dp",
      height: Ti.UI.SIZE });

    var lbTitle = Ti.UI.createLabel({
      color: "gray",
      top: false ? callBackPedir ? "15dp" : "0dp" : "15dp",
      bottom: "15dp",
      font: {
        fontSize: 14 },

      textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
      height: Ti.UI.SIZE,
      text: title });

    var viewLine = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "15dp",
      right: "15dp",
      backgroundColor: Alloy.Globals.BACKGROUND_COLOR });






    view.add(lbTitle);
    view.add(viewLine);

    for (var i in options) {
      view.add(itemLabelWithTag(options[i], callback, i));
    }

    if (true) view.add(Ti.UI.createView({ height: "25dp" }));

    return view;
  }


  function itemLabelWithTag(option, callback, i) {

    var valor;
    var tag;
    var array = option.split(": ");

    if (array.length == 2) {
      valor = array[1];
      tag = array[0];
    } else {
      valor = option;
      tag = "";
    }

    var view = Ti.UI.createView({
      height: "65dp",
      width: Ti.UI.FILL,
      left: 0,
      bubbleParent: callback ? false : true });


    var viewLine = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      bottom: 0,
      left: "15dp",
      right: "15dp",
      backgroundColor: Alloy.Globals.BACKGROUND_COLOR });


    var view2 = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.SIZE,
      layout: "vertical",
      left: "10dp" });


    var etiqueta = Ti.UI.createLabel({
      text: tag,
      color: Alloy.Globals.BLUE_COLOR,
      font: {
        fontSize: 12 },

      left: "10dp",
      top: "5dp",
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT });


    var label = Ti.UI.createLabel({
      text: valor,
      color: lbColor,
      height: "32dp",
      font: {
        fontSize: 14,
        fontWeight: 'bold' },

      left: "10dp",
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT });


    var imgCTA = Ti.UI.createImageView({
      image: "/images/disclosure.png",
      height: "20dp",
      width: "20dp",
      right: "15dp" });


    view.addEventListener("click", function (e) {
      if (callback) {
        callback(valor, i);
        hide();
      }
    });

    view.add(viewLine);
    view.add(view2);
    view.add(imgCTA);
    if (array.length == 2) view2.add(etiqueta);
    view2.add(label);
    return view;
  }

  function createPopUpAddItem(prod, info, callback) {
    var desconto = OrderManager.getDesconto();
    var view = Ti.UI.createScrollView({
      layout: "vertical",
      backgroundColor: "white",
      bottom: "25dp",
      top: "25dp",
      height: Ti.UI.SIZE });


    var lbTitle = Ti.UI.createLabel({
      color: "#205D33",
      top: "15dp",
      bottom: "15dp",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
      height: Ti.UI.SIZE,
      text: prod.DESCR + ' - R$' + parseFloat(info.prcVnd).toFixed(2) });


    var viewLine = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "15dp",
      right: "15dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#CCC" });


    var viewSeparator1 = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "25dp",
      right: "25dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#EEE" });


    var viewSeparator2 = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "25dp",
      right: "25dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#4C5454" });


    var viewSeparator3 = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "25dp",
      right: "25dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#4C5454" });


    var lbQnt = Ti.UI.createLabel({
      color: "#28a745",
      top: "5dp",
      bottom: "9dp",
      left: '10dp',
      font: {
        fontSize: 17,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      height: Ti.UI.SIZE,
      text: "Quantidade: " });


    var lbDesc = Ti.UI.createLabel({
      color: "#28a745",
      top: "5dp",
      bottom: "9dp",
      left: '10dp',
      font: {
        fontSize: 17,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      height: Ti.UI.SIZE,
      text: "Desconto (%): " });


    var lbValor = Ti.UI.createLabel({
      color: "#28a745",
      top: "5dp",
      bottom: "9dp",
      left: '10dp',
      font: {
        fontSize: 17,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      height: Ti.UI.SIZE,
      text: "Preço (R$): " });


    var viewQnt = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "0dp",
      bottom: "15dp" });


    var viewDesc = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "0dp",
      bottom: "15dp" });


    var viewValor = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "0dp",
      bottom: "15dp" });


    var imgRemovQnt = Ti.UI.createImageView({
      image: "/images/removeRed.png",
      height: "31dp",
      width: "31dp",
      right: "20dp" });


    var imgRemovDesc = Ti.UI.createImageView({
      image: "/images/removeRed.png",
      height: "31dp",
      width: "31dp",
      right: "20dp" });


    var txtQnt = Ti.UI.createTextField({
      height: "40dp",
      width: "100dp",
      color: "#205D33",
      backgroundColor: "#B2B2B2",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      hintText: '0',
      value: prod.CK_QTDVEN ? prod.CK_QTDVEN : 0,
      borderRadius: 4,
      keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });


    var txtDesc = Ti.UI.createTextField({
      height: "40dp",
      width: "100dp",
      color: "#205D33",
      backgroundColor: "#B2B2B2",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      hintText: '0',
      value: prod.CK_DESCONT ? prod.CK_DESCONT : desconto,
      borderRadius: 4,
      keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });


    var txtValor = Ti.UI.createTextField({
      height: "40dp",
      width: "100dp",

      color: "#205D33",
      backgroundColor: "#B2B2B2",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      hintText: '0',
      value: prod.CK_PRUNIT ? parseFloat(prod.CK_PRUNIT).toFixed(2) : calcPreco(desconto, info.prcVnd),
      borderRadius: 4,
      keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });


    var imgAddQnt = Ti.UI.createImageView({
      image: "/images/addGreen.png",
      height: "40dp",
      width: "40dp",
      right: "65dp" });


    var imgAddDesc = Ti.UI.createImageView({
      image: "/images/addGreen.png",
      height: "40dp",
      width: "40dp",
      right: "65dp" });


    var viewBtns = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "15dp",
      bottom: "15dp" });



    var btnConfirm = Ti.UI.createButton({
      title: "Confirmar",
      left: "35dp",
      backgroundColor: "#28a745",
      borderRadius: 4,
      font: {
        fontSize: 17 } });



    var btnCancel = Ti.UI.createButton({
      title: "Cancelar",
      right: "35dp",
      backgroundColor: "#FF4B4B",
      borderRadius: 4,
      font: {
        fontSize: 17 } });



    btnConfirm.addEventListener("click", function () {
      if (txtQnt.getValue() == 0 || txtValor.getValue() == 0) return Alloy.Globals.showAlert('Ops!', 'É necessário inserir a quantidade e o valor do item!');
      let prcUni = parseFloat(txtValor.getValue()).toFixed(2);
      callback({ qnt: parseInt(txtQnt.getValue()), val: prcUni, desc: parseFloat(txtDesc.getValue()).toFixed(2) }, prod);
    });

    btnCancel.addEventListener("click", function () {
      if (false)
      hide();else
      if (dialog)
      dialog.hide();
    });

    imgRemovQnt.addEventListener("click", function () {
      var value = txtQnt.getValue() ? parseFloat(txtQnt.getValue()).toFixed(2) : 0;
      value = value <= 0 ? 0 : value - 1;
      txtQnt.setValue(value);
    });

    imgRemovDesc.addEventListener("click", function () {
      var value = txtDesc.getValue() ? parseFloat(txtDesc.getValue()).toFixed(2) : 0;
      value = value - 1;
      txtDesc.setValue(value);
      let prcUni = calcPreco(parseFloat(txtDesc.getValue()), info.prcVnd);
      txtValor.setValue(prcUni);
    });

    imgAddQnt.addEventListener("click", function () {
      var value = txtQnt.getValue() ? parseFloat(txtQnt.getValue()).toFixed(2) : 0;
      value = value + 1;
      txtQnt.setValue(value);
    });

    imgAddDesc.addEventListener("click", function () {
      var value = txtDesc.getValue() ? parseFloat(txtDesc.getValue()).toFixed(2) : 0;
      value = value + 1;
      txtDesc.setValue(value);
      let prcUni = calcPreco(parseFloat(txtDesc.getValue()), info.prcVnd);
      txtValor.setValue(prcUni);
    });

    txtQnt.addEventListener("change", function (e) {
      txtQnt.setValue(Mask.int(e.source.value));
    });

    txtDesc.addEventListener("change", function (e) {
      if (!txtDesc.getValue()) return txtValor.setValue(parseFloat(info.prcVnd));
      txtDesc.setValue(Mask.float(e.source.value));
      let prcUni = calcPreco(parseFloat(txtDesc.getValue()), info.prcVnd);
      txtValor.setValue(prcUni);
    });

    txtValor.addEventListener("change", function (e) {
      txtValor.setValue(Mask.float(e.source.value));
      let desc = ((parseFloat(txtValor.getValue()).toFixed(2) - parseFloat(info.prcVnd).toFixed(2)) / (parseFloat(info.prcVnd).toFixed(2) / 100)).toFixed(2);
      txtDesc.setValue(desc);
    });

    view.add(lbTitle);
    view.add(viewLine);



    viewQnt.add(lbQnt);
    viewQnt.add(imgRemovQnt);
    viewQnt.add(txtQnt);
    viewQnt.add(imgAddQnt);
    view.add(viewQnt);



    viewValor.add(lbValor);
    viewValor.add(txtValor);
    view.add(viewValor);




    viewDesc.add(lbDesc);
    viewDesc.add(imgRemovDesc);
    viewDesc.add(txtDesc);
    viewDesc.add(imgAddDesc);
    view.add(viewDesc);

    view.add(viewSeparator3);

    view.add(viewBtns);
    viewBtns.add(btnConfirm);
    viewBtns.add(btnCancel);

    return view;
  }

  function createPopUpDesconto(callback) {
    var view = Ti.UI.createScrollView({
      layout: "vertical",
      backgroundColor: "white",
      bottom: "25dp",
      top: "25dp",
      height: Ti.UI.SIZE });


    var lbTitle = Ti.UI.createLabel({
      color: "#205D33",
      top: "15dp",
      bottom: "15dp",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
      height: Ti.UI.SIZE,
      text: 'DESCONTO GERAL' });


    var viewLine = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "15dp",
      right: "15dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#CCC" });


    var viewSeparator3 = Ti.UI.createView({
      height: 1,
      width: Ti.UI.FILL,
      left: "25dp",
      right: "25dp",
      top: "15dp",
      bottom: "15dp",
      backgroundColor: "#4C5454" });


    var lbDesc = Ti.UI.createLabel({
      color: "#28a745",
      top: "5dp",
      bottom: "9dp",
      left: '10dp',
      font: {
        fontSize: 17,
        fontWeight: "bold" },

      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      height: Ti.UI.SIZE,
      text: "Desconto (%): " });


    var viewDesc = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "0dp",
      bottom: "15dp" });


    var imgRemovDesc = Ti.UI.createImageView({
      image: "/images/removeRed.png",
      height: "31dp",
      width: "31dp",
      right: "20dp" });


    var txtDesc = Ti.UI.createTextField({
      height: "40dp",
      width: "100dp",
      color: "#205D33",
      backgroundColor: "#B2B2B2",
      font: {
        fontSize: 18,
        fontWeight: "bold" },

      hintText: '0',
      value: 0,
      borderRadius: 4,
      keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });


    var imgAddDesc = Ti.UI.createImageView({
      image: "/images/addGreen.png",
      height: "40dp",
      width: "40dp",
      right: "65dp" });


    var viewBtns = Ti.UI.createView({
      height: Ti.UI.SIZE,
      width: Ti.UI.FILL,
      top: "15dp",
      bottom: "15dp" });



    var btnConfirm = Ti.UI.createButton({
      title: "Confirmar",
      left: "35dp",
      backgroundColor: "#28a745",
      borderRadius: 4,
      font: {
        fontSize: 17 } });



    var btnCancel = Ti.UI.createButton({
      title: "Cancelar",
      right: "35dp",
      backgroundColor: "#FF4B4B",
      borderRadius: 4,
      font: {
        fontSize: 17 } });



    btnConfirm.addEventListener("click", function () {
      callback(parseFloat(txtDesc.getValue()).toFixed(2));
    });

    btnCancel.addEventListener("click", function () {
      if (false)
      hide();else
      if (dialog)
      dialog.hide();
    });

    imgRemovDesc.addEventListener("click", function () {
      var value = txtDesc.getValue() ? parseFloat(txtDesc.getValue()).toFixed(2) : 0;
      value = value - 1;
      txtDesc.setValue(value);
    });

    imgAddDesc.addEventListener("click", function () {
      var value = txtDesc.getValue() ? parseFloat(txtDesc.getValue()).toFixed(2) : 0;
      value = value + 1;
      txtDesc.setValue(value);
    });

    txtDesc.addEventListener("change", function (e) {
      txtDesc.setValue(Mask.float(e.source.value));

    });

    view.add(lbTitle);
    view.add(viewLine);

    viewDesc.add(lbDesc);
    viewDesc.add(imgRemovDesc);
    viewDesc.add(txtDesc);
    viewDesc.add(imgAddDesc);
    view.add(viewDesc);

    view.add(viewSeparator3);

    view.add(viewBtns);
    viewBtns.add(btnConfirm);
    viewBtns.add(btnCancel);

    return view;
  }

  exports.showLabelWithTag = function (options, callback, title, callBackPedir) {
    if (false) {
      $.content.removeAllChildren();
      $.content.add(createViewLabelWithTag(options, callback, title, callBackPedir));
      show();
    } else {
      dialog = Ti.UI.createOptionDialog();
      dialog.setAndroidView(createViewLabelWithTag(options, callback, title, callBackPedir));
      dialog.show();
    }
  };

  exports.showPopUpAddItem = function (prod, info, callback) {
    if (false) {
      $.content.removeAllChildren();
      $.content.add(createPopUpAddItem(prod, info, callback));
      show();
    } else {
      dialog = Ti.UI.createOptionDialog();
      dialog.setAndroidView(createPopUpAddItem(prod, info, callback));
      dialog.show();
    }
  };

  exports.showPopUpDesconto = function (callback) {
    if (false) {
      $.content.removeAllChildren();
      $.content.add(createPopUpDesconto(callback));
      show();
    } else {
      dialog = Ti.UI.createOptionDialog();
      dialog.setAndroidView(createPopUpDesconto(callback));
      dialog.show();
    }
  };

  exports.hide = function () {
    if (false)
    hide();else
    if (dialog)
    dialog.hide();
  };



  function onClick() {
    if (isDisplay)
    hide();
  }

  function show() {
    $.main.touchEnabled = true;
    $.main.visible = true;
    $.main.animate({
      duration: 120,
      opacity: 1 },
    function () {
      isDisplay = true;
    });
  }

  function hide() {
    $.main.touchEnabled = false;
    isDisplay = false;
    $.main.animate({
      duration: 120,
      opacity: 0 },
    function () {
      $.main.visible = false;
    });
  }





  __defers['$.__views.main!click!onClick'] && $.addListener($.__views.main, 'click', onClick);



  _.extend($, exports);
}

module.exports = Controller;