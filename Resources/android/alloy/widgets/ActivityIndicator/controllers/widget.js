var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
	var index = s.lastIndexOf('/');
	var path = index === -1 ?
	'ActivityIndicator/' + s :
	s.substring(0, index) + '/ActivityIndicator/' + s.substring(index + 1);

	return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {
	var Widget = new (require('/alloy/widget'))('ActivityIndicator');this.__widgetId = 'ActivityIndicator';
	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'widget';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.viewMainActivityIndicator = Ti.UI.createView(
	{ zIndex: 1, width: Ti.UI.SIZE, height: Ti.UI.SIZE, visible: false, touchEnabled: false, id: "viewMainActivityIndicator" });

	$.__views.viewMainActivityIndicator && $.addTopLevelView($.__views.viewMainActivityIndicator);
	$.__views.viewBackground = Ti.UI.createView(
	{ width: Ti.UI.FILL, height: Ti.UI.FILL, backgroundColor: "#cecece", opacity: 0.6, touchEnabled: false, id: "viewBackground" });

	$.__views.viewMainActivityIndicator.add($.__views.viewBackground);
	$.__views.activityIndicator = Ti.UI.createActivityIndicator(
	{ style: Titanium.UI.ActivityIndicatorStyle.BIG, indicatorColor: "#205D33", id: "activityIndicator" });

	$.__views.viewMainActivityIndicator.add($.__views.activityIndicator);
	$.__views.lbText = Ti.UI.createLabel(
	{ font: { fontSize: "17dp" }, id: "lbText", textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER, bottom: "15dp", color: "black" });

	$.__views.viewMainActivityIndicator.add($.__views.lbText);
	exports.destroy = function () {};




	_.extend($, $.__views);


	exports.show = function () {
		$.viewMainActivityIndicator.setTop(0);
		$.viewMainActivityIndicator.setLeft(0);
		$.activityIndicator.show();
		$.viewMainActivityIndicator.setVisible(true);
	};

	exports.hide = function () {
		$.activityIndicator.hide();
		$.viewMainActivityIndicator.setVisible(false);
	};

	exports.setTouch = function (value) {
		if (value) {
			$.viewMainActivityIndicator.setTouchEnabled(true);
			$.viewBackground.setOpacity(0.3);
		} else {
			$.viewMainActivityIndicator.setTouchEnabled(false);
			$.viewBackground.setOpacity(0);
		}
	};

	exports.setText = function (txt) {
		$.lbText.setText(txt);
	};

	exports.setBackground = function (color) {
		$.viewMainActivityIndicator.setBackgroundColor(color);
	};









	_.extend($, exports);
}

module.exports = Controller;