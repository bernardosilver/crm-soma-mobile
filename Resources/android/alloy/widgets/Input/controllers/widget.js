var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;


function WPATH(s) {
		var index = s.lastIndexOf('/');
		var path = index === -1 ?
		'Input/' + s :
		s.substring(0, index) + '/Input/' + s.substring(index + 1);

		return path.indexOf('/') !== 0 ? '/' + path : path;
}

function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
		}
		return arg;
}

function Controller() {
		var Widget = new (require('/alloy/widget'))('Input');this.__widgetId = 'Input';
		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'widget';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.viewMain = Ti.UI.createView(
		{ id: "viewMain", top: "5dp", bottom: "5dp", height: "50dp", width: Ti.UI.FILL });

		$.__views.viewMain && $.addTopLevelView($.__views.viewMain);
		click ? $.addListener($.__views.viewMain, 'click', click) : __defers['$.__views.viewMain!click!click'] = true;$.__views.viewTf = Ti.UI.createView(
		{ id: "viewTf", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, borderWidth: 1, borderColor: "transparent" });

		$.__views.viewMain.add($.__views.viewTf);
		click ? $.addListener($.__views.viewTf, 'click', click) : __defers['$.__views.viewTf!click!click'] = true;$.__views.textField = Ti.UI.createTextField(
		{ hintTextColor: Alloy.Globals.GRAY_COLOR, id: "textField", height: Ti.UI.FILL, width: Ti.UI.FILL, left: "5dp", right: "5dp", backgroundColor: "transparent" });

		$.__views.viewTf.add($.__views.textField);
		exports.destroy = function () {};




		_.extend($, $.__views);


		var args = arguments[0] || {};

		if (args.hintText)
		$.textField.setHintText(args.hintText);

		if (args.maxLength > 0) {
				$.textField.setMaxLength(args.maxLength);
		}

		exports.setHintText = function (text) {
				$.textField.setHintText(text);
		};

		exports.setBottom = function (value) {
				$.viewMain.setBottom(value);
		};

		exports.setPasswordMask = function (bool) {
				$.textField.setPasswordMask(bool);
		};

		exports.setReturnKeyType = function (text) {
				var type = Titanium.UI.RETURNKEY_DEFAULT;
				if (text == 'next') type = Titanium.UI.RETURNKEY_NEXT;else
				if (text == 'go') type = Titanium.UI.RETURNKEY_GO;else
				if (text == 'send') type = Titanium.UI.RETURNKEY_SEND;else
				if (text == 'continue') type = Titanium.UI.RETURNKEY_CONTINUE;else
				if (text == 'done') type = Titanium.UI.RETURNKEY_DONE;

				$.textField.setReturnKeyType(type);
		};

		exports.setKeyboardType = function (text) {
				var type = Titanium.UI.KEYBOARD_TYPE_DEFAULT;
				if (text == 'email') type = Titanium.UI.KEYBOARD_TYPE_EMAIL;else
				if (text == 'number') type = Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION;else
				if (text == 'user') type = Titanium.UI.KEYBOARD_TYPE_ASCII;else
				if (text == 'phone') type = Titanium.UI.KEYBOARD_TYPE_PHONE_PAD;else
				if (text == 'decimal') type = Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD;

				$.textField.setKeyboardType(type);
		};

		exports.setValue = function (text) {
				$.textField.setValue(text);
		};

		exports.getValue = function () {
				return $.textField.getValue();
		};

		exports.setColorLabel = function (value) {
				$.lbHeader.setColor(value);
		};

		exports.setColorBorder = function (value) {
				$.viewTf.setBorderColor(value);
		};

		exports.setBackgroundColor = function (value) {
				$.viewTf.setBackgroundColor(value);
		};

		exports.setEditable = function (bool) {
				$.textField.setEditable(bool);
		};

		exports.blur = function () {
				$.textField.blur();
		};

		exports.hide = function () {
				$.viewMain.hide();
		};

		exports.show = function () {
				$.viewMain.show();
		};

		function click(e) {
				$.textField.focus();
		}

		$.textField.addEventListener('change', function (e) {
				var v = e.value;
				v = v.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");

				if (args.maxLength) {
						if ($.textField.getValue() != v) $.textField.setValue(v);
						$.textField.setSelection($.textField.getValue().length, $.textField.getValue().length);
				}
		});





		__defers['$.__views.viewMain!click!click'] && $.addListener($.__views.viewMain, 'click', click);__defers['$.__views.viewTf!click!click'] && $.addListener($.__views.viewTf, 'click', click);



		_.extend($, exports);
}

module.exports = Controller;