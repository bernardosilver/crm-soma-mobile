var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'FormPedido';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Novo Pedido", tabBarHidden: true });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId274() {
      $.__views.mainWindow.removeEventListener('open', __alloyId274);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Novo Pedido";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId274);
  }
  $.__views.__alloyId275 = Ti.UI.createView(
  { id: "__alloyId275" });

  $.__views.mainWindow.add($.__views.__alloyId275);
  var __alloyId276 = {};var __alloyId279 = [];var __alloyId281 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId279.push(__alloyId281);var __alloyId283 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId279.push(__alloyId283);var __alloyId278 = { properties: { name: "templateCliente", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId279 };__alloyId276["templateCliente"] = __alloyId278;var __alloyId286 = [];var __alloyId288 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId286.push(__alloyId288);var __alloyId290 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId286.push(__alloyId290);var __alloyId285 = { properties: { name: "templateTabela", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId286 };__alloyId276["templateTabela"] = __alloyId285;var __alloyId293 = [];var __alloyId295 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId293.push(__alloyId295);var __alloyId297 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId293.push(__alloyId297);var __alloyId292 = { properties: { name: "templatePagamento", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId293 };__alloyId276["templatePagamento"] = __alloyId292;var __alloyId300 = [];var __alloyId302 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId300.push(__alloyId302);var __alloyId304 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId300.push(__alloyId304);var __alloyId299 = { properties: { name: "templateParcela", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId300 };__alloyId276["templateParcela"] = __alloyId299;var __alloyId307 = [];var __alloyId309 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId307.push(__alloyId309);var __alloyId311 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId307.push(__alloyId311);var __alloyId306 = { properties: { name: "templateFrete", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId307 };__alloyId276["templateFrete"] = __alloyId306;var __alloyId314 = [];var __alloyId316 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId314.push(__alloyId316);var __alloyId318 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId314.push(__alloyId318);var __alloyId313 = { properties: { name: "templateDesc", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId314 };__alloyId276["templateDesc"] = __alloyId313;var __alloyId321 = [];var __alloyId323 = { type: 'Ti.UI.Label', bindId: 'lbObservacao', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbObservacao", height: Ti.UI.SIZE, width: Ti.UI.SIZE, left: "15dp", right: "75dp", maxLines: 3 } };__alloyId321.push(__alloyId323);var __alloyId325 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId321.push(__alloyId325);var __alloyId320 = { properties: { name: "templateObservacao", height: "60dp", backgroundColor: "white", accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId321 };__alloyId276["templateObservacao"] = __alloyId320;var __alloyId328 = [];var __alloyId330 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId328.push(__alloyId330);var __alloyId332 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId328.push(__alloyId332);var __alloyId327 = { properties: { name: "templateProdutos", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId328 };__alloyId276["templateProdutos"] = __alloyId327;var __alloyId335 = [];var __alloyId337 = { type: 'Ti.UI.Label', bindId: 'label', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "label", height: Ti.UI.SIZE, width: Ti.UI.SIZE, left: "15dp" } };__alloyId335.push(__alloyId337);var __alloyId339 = { type: 'Ti.UI.Label', bindId: 'labelEscolher', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "labelEscolher", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId335.push(__alloyId339);var __alloyId334 = { properties: { name: "templateEscolher", height: "50dp", backgroundColor: "white", accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId335 };__alloyId276["templateEscolher"] = __alloyId334;var __alloyId342 = [];var __alloyId344 = { type: 'Ti.UI.ImageView', properties: { touchEnabled: false, image: "/images/removeRed.png", top: "17dp", left: "10dp", width: "18dp", height: "18dp" } };__alloyId342.push(__alloyId344);var __alloyId346 = { type: 'Ti.UI.Label', bindId: 'lbQtd', properties: { color: Alloy.Globals.BLUE_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbQtd", height: "40dp", top: "5dp", left: "40dp" } };__alloyId342.push(__alloyId346);var __alloyId348 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "15dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbNome", height: "40dp", width: Ti.UI.FILL, top: "5dp", left: "80dp", right: "85dp" } };__alloyId342.push(__alloyId348);var __alloyId350 = { type: 'Ti.UI.Label', bindId: 'lbDescricao', properties: { color: Alloy.Globals.LIGHT_GRAY_COLOR2, font: { fontSize: "13dp" }, touchEnabled: false, bindId: "lbDescricao", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "45dp", left: "80dp", right: "15dp", bottom: "15dp" } };__alloyId342.push(__alloyId350);var __alloyId352 = { type: 'Ti.UI.Label', bindId: 'lbPreco', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "15dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbPreco", top: "15dp", right: "15dp", height: Ti.UI.SIZE } };__alloyId342.push(__alloyId352);var __alloyId341 = { properties: { name: "templateCarrinho", height: Ti.UI.SIZE, backgroundColor: "white" }, childTemplates: __alloyId342 };__alloyId276["templateCarrinho"] = __alloyId341;var __alloyId355 = [];var __alloyId357 = { type: 'Ti.UI.Label', bindId: 'label', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp" }, text: 'Adicionar mais itens', touchEnabled: false, bindId: "label", height: Ti.UI.SIZE, width: Ti.UI.FILL, left: "12dp", right: "12dp", textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER } };__alloyId355.push(__alloyId357);var __alloyId354 = { properties: { name: "templateAddItem", height: "45dp", backgroundColor: "white", accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId355 };__alloyId276["templateAddItem"] = __alloyId354;var __alloyId360 = [];var __alloyId362 = { type: 'Ti.UI.Label', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Subtotal:', touchEnabled: false, left: "15dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE } };__alloyId360.push(__alloyId362);var __alloyId364 = { type: 'Ti.UI.Label', bindId: 'subtotal', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "subtotal", right: "15dp", height: Ti.UI.SIZE } };__alloyId360.push(__alloyId364);var __alloyId359 = { properties: { name: "templateSubtotal", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, touchEnabled: false }, childTemplates: __alloyId360 };__alloyId276["templateSubtotal"] = __alloyId359;var __alloyId367 = [];var __alloyId369 = { type: 'Ti.UI.Label', bindId: 'tempoEntrega', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "tempoEntrega", left: "15dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE } };__alloyId367.push(__alloyId369);var __alloyId371 = { type: 'Ti.UI.Label', bindId: 'taxaEntrega', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "taxaEntrega", right: "15dp", height: Ti.UI.SIZE } };__alloyId367.push(__alloyId371);var __alloyId366 = { properties: { name: "templateTaxaEntrega", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, touchEnabled: false }, childTemplates: __alloyId367 };__alloyId276["templateTaxaEntrega"] = __alloyId366;var __alloyId374 = [];var __alloyId376 = { type: 'Ti.UI.Label', properties: { font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Total:', touchEnabled: false, left: "15dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE } };__alloyId374.push(__alloyId376);var __alloyId378 = { type: 'Ti.UI.Label', bindId: 'total', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "total", right: "15dp", height: Ti.UI.SIZE } };__alloyId374.push(__alloyId378);var __alloyId373 = { properties: { name: "templateTotal", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL, touchEnabled: false }, childTemplates: __alloyId374 };__alloyId276["templateTotal"] = __alloyId373;$.__views.listView = Ti.UI.createListView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId276, id: "listView", top: "-2dp", bottom: "60dp" });

  $.__views.__alloyId275.add($.__views.listView);
  $.__views.popup = Alloy.createWidget('PopUp', 'widget', { id: "popup", __parentSymbol: $.__views.__alloyId275 });
  $.__views.popup.setParent($.__views.__alloyId275);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.__alloyId275 });
  $.__views.activityIndicator.setParent($.__views.__alloyId275);
  $.__views.viewPanel = Ti.UI.createView(
  { backgroundColor: Alloy.Globals.RED_COLOR, id: "viewPanel", height: "40dp", bottom: "10dp", left: "15dp", right: "15dp", width: Ti.UI.FILL, borderRadius: 4, elevation: 11 });

  $.__views.__alloyId275.add($.__views.viewPanel);
  $.__views.btCheckOut = Ti.UI.createLabel(
  { color: Alloy.Globals.WHITE_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, id: "btCheckOut", textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER });

  $.__views.viewPanel.add($.__views.btCheckOut);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var click_pedido = args.pedido || null;
  var OrderManager = require('OrderManager');
  var ClientManager = require('ClientManager');
  var LocalData = require('LocalData');
  var Mask = require("Mask");
  var actionItem = {};
  var pedido = {};
  var sections = [];
  var status = null;
  var listManager = null;
  var sectionParcela = null;


  function doClose() {
    $.mainWindow.close();
  }

  function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
      var menuItemPin = e.menu.add({
        icon: "/images/save.png",
        width: "40dp",
        height: "40dp",
        showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

      menuItemPin.addEventListener("click", function (e) {
        console.log('SALVAR PEDIDO!');

      });
    };
    activity.invalidateOptionsMenu();
  }

  function createListView() {
    sections = [];

    var sectionCliente = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Cliente" }).getView() });

    sectionCliente.setItems(refreshCliente());
    sections.push(sectionCliente);

    var sectionTabela = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Tabela de preços" }).getView() });

    sectionTabela.setItems(refreshTabela());
    sections.push(sectionTabela);

    var sectionPagamento = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Forma de pagamento" }).getView() });

    sectionPagamento.setItems(refreshPagamento());
    sections.push(sectionPagamento);


    sections.push(sectionParcela);

    var sectionFrete = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Frete" }).getView() });

    sectionFrete.setItems(refreshFrete());
    sections.push(sectionFrete);

    var sectionDesconto = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Desconto" }).getView() });

    sectionDesconto.setItems(refreshDesconto());
    sections.push(sectionDesconto);

    var sectionObservacao = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Observação" }).getView() });

    sectionObservacao.setItems(refreshObservacao());
    sections.push(sectionObservacao);

    var sectionProdutos = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Produtos" }).getView() });

    sectionProdutos.setItems(refreshProdutos());
    sections.push(sectionProdutos);

    $.listView.setSections(sections);

    return {
      setCliente: function (cliente) {
        sectionCliente.setItems(refreshCliente(cliente));
      },
      setTabela: function (tabela) {
        sectionTabela.setItems(refreshTabela(tabela));
      },
      setPagamento: function (condicao) {
        sectionPagamento.setItems(refreshPagamento(condicao));
      },
      setParcela: function (parcelas) {
        sectionParcela.setItems(refreshParcela(parcelas));
      },
      setFrete: function (frete) {
        sectionFrete.setItems(refreshFrete(frete));
      },
      setDesconto: function (desc) {
        sectionDesconto.setItems(refreshDesconto(desc));
      },
      setObservacao: function (obs) {
        sectionObservacao.setItems(refreshObservacao(obs));
      },
      setProdutos: function (cart) {
        console.log('SET PRODUTOS');
        sectionProdutos.setItems(refreshProdutos(cart));
      } };

  }

  function refreshListView(obj) {
    var cliente = obj.cliente || null;
    var tabela = obj.tabela || null;
    var pagamento = obj.pagamento || null;
    var parcela = obj.parcela || null;
    var frete = obj.frete || null;
    var obs = obj.desconto || null;
    var prods = obj.obs || null;
    listManager.setCliente(cliente);
    listManager.setTabela(tabela);
    listManager.setPagamento(pagamento);
    listManager.setParcela(parcela);
    listManager.setFrete(frete);
    listManager.setDesconto(frete);
    listManager.setObservacao(obs);
    listManager.setProdutos(prods);
    $.activityIndicator.hide();
  }

  function refreshCliente(cliente) {
    var itemCliente = cliente ? createItemCliente(Alloy.Globals.createTextCliente(cliente)) : createItemListEscolher('cliente', 'Clique para selecionar');
    return [itemCliente];
  }

  function refreshTabela(tabela) {
    var itemTabela = tabela ? createItemTabela(tabela) : createItemListEscolher('tabela', 'Clique para selecionar');
    return [itemTabela];
  }

  function refreshPagamento(pagamento) {
    var itemPagamento = pagamento ? createItemPagamento(pagamento) : createItemListEscolher('pagamento', 'Clique para selecionar');
    return [itemPagamento];
  }

  function refreshParcela(parcela) {
    var itemParcela = parcela ? createItemParcela(parcela) : createItemListEscolher('parcela', 'Clique para selecionar');
    return [itemParcela];
  }

  function refreshFrete(frete) {
    var itemFrete = frete ? createItemFrete(frete) : createItemListEscolher('frete', 'Clique para adicionar');
    return [itemFrete];
  }

  function refreshDesconto(desc) {
    var itemDesconto = desc ? createItemDesconto(desc) : createItemListEscolher('desconto', 'Clique para adicionar');
    return [itemDesconto];
  }

  function refreshObservacao(obs) {
    var itemObs = obs ? createItemObs(obs) : createItemListEscolher('obs', 'Observações para entrega');
    return [itemObs];
  }

  function refreshProdutos(cart) {
    var itemProds = cart ? createItemProds(cart) : createItemListEscolher('prods', 'Produtos do pedido');
    return [itemProds];
  }

  function createItemListEscolher(type, label) {
    return {
      template: 'templateEscolher',
      label: {
        text: label || 'Escolher' },

      labelEscolher: {
        text: type == 'obs' || type == 'frete' ? 'Adicionar' : 'Escolher' },

      properties: {
        type: type,
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemCliente(cliente) {
    return {
      template: "templateCliente",
      lbNome: {
        text: cliente },

      properties: {
        type: 'cliente',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemTabela(tabela) {
    return {
      template: "templateTabela",
      lbNome: {
        text: tabela.Descr + ' - Filial: ' + tabela.Filial },

      properties: {
        type: 'tabela',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemPagamento(pagamento) {
    return {
      template: "templatePagamento",
      lbNome: {
        text: pagamento.DESCRI },

      properties: {
        type: 'pagamento',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemParcela(parcela) {
    return {
      template: "templateParcela",
      lbNome: {
        text: parcela.length + ' Parcela(s)' },

      properties: {
        type: 'parcela',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemFrete(frete) {
    return {
      template: "templateFrete",
      lbNome: {
        text: frete.CJ_TPFRETE == 'F' ? 'FOB - R$' + frete.CJ_FRTBAST : 'CIF - R$' + frete.CJ_FRTBAST },

      properties: {
        type: 'frete',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemDesconto(desc) {
    return {
      template: "templateDesc",
      lbNome: {
        text: desc + '%' },

      properties: {
        type: 'desconto',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemObs(obs) {
    return {
      template: "templateObservacao",
      lbObservacao: {
        text: obs },

      properties: {
        type: 'obs',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemProds(cart) {
    return {
      template: "templateProdutos",
      lbNome: {
        text: `${cart.qntItens} Produto(s) no pedido. Valor total s/ frete: R$${Mask.real(parseFloat(cart.totalProd).toFixed(2))}` },

      properties: {
        type: 'prods',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function checkStatus() {
    status = OrderManager.getStatus();
    var text = '';
    if (status == 1) $.btCheckOut.setText('Selecionar cliente');else
    if (status == 2) $.btCheckOut.setText('Selecionar tabela de preços');else
    if (status == 3) $.btCheckOut.setText('Selecionar forma de pagamento');else
    if (status == 4) $.btCheckOut.setText('Selecionar parcela');else
    if (status == 5) $.btCheckOut.setText('Inserir frete');else
    $.btCheckOut.setText('Selecionar produtos');
    $.activityIndicator.hide();
  }


  async function insertPedido() {
    $.activityIndicator.show();
    var pedido_id = 0;
    const new_dados = await OrderManager.generateCabecalho();

    if (click_pedido && click_pedido.ID) {
      await LocalData.updateCadPedido({
        data: {
          id: click_pedido.ID,
          pedido: new_dados },

        success: function (id) {
          pedido_id = id;
        },
        error: function (err) {
          console.log('DEU ERRO AO ATUALIZAR PEDIDO: ', err);
        } });

      const new_items = await OrderManager.generateItems(click_pedido.ID);
      console.log('ITEMS PEDIDO> ', new_items);
      for (const new_item of new_items) {
        await LocalData.updateCadRef({
          data: {
            id: new_item.ID,
            ref: new_item },

          success: function (ref) {
            console.log('REFERENCIA ATUALIZADA: ', ref);
          },
          error: function (err) {
            console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
          } });

      }
      $.activityIndicator.hide();
    } else
    {
      await LocalData.insertCadPedido({
        pedido: new_dados,
        success: function (id) {
          pedido_id = id;
        },
        error: function (err) {
          console.log('DEU ERRADO AO CADASTRAR PEDIDO: ', err);
        } });

      const new_refs = await ClientManager.generateRefs(pedido_id);
      await LocalData.createTableCadRef();
      for (const new_ref of new_refs) {
        await LocalData.insertCadRef({
          ref: new_ref,
          success: function (ref) {
            console.log('REFERENCIA CADASTRADA: ', ref);
          },
          error: function (err) {
            console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
          } });

      }
      doClose();
      $.activityIndicator.hide();
    }
  }

  actionItem.cliente = function (e) {
    Alloy.Globals.openWindow('Clientes', { pedido: true, callback: function (cliente) {
        var status = ClientManager.getStatusCli(cliente.A1_MSBLQL, cliente.A1_TITVENC, cliente.A1_RISCO, cliente.A1_OBSERV, cliente.A1_TLC, cliente.SLD);
        console.log('CLIENTE: ', cliente);
        listManager.setCliente(cliente);
        OrderManager.setCliente(cliente);
        listManager.setTabela();
        listManager.setPagamento();
        if (sectionParcela) {
          listManager.setParcela();
          sectionParcela = null;
          $.listView.replaceSectionAt(3, sectionParcela);
        }
      } });
  };

  actionItem.tabela = function (e) {
    if (status != 0 && status < 2) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar um cliente primeiro!');
    Alloy.Globals.openWindow('CadPedido/TabelaPreco', { pedido: true, callback: function (tabela) {
        listManager.setTabela(tabela);
        listManager.setPagamento();
        if (sectionParcela) {
          listManager.setParcela();
          sectionParcela = null;
          $.listView.replaceSectionAt(3, sectionParcela);
        }
        OrderManager.setTabela(tabela);
      } });
  };

  actionItem.pagamento = function (e) {
    if (status != 0 && status < 3) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a tabela de preço primeiro!');
    Alloy.Globals.openWindow('CadPedido/CondPagamento', { pedido: true, callback: function (cond) {
        console.log('PAG: ', cond);
        listManager.setPagamento(cond);
        if (sectionParcela) {
          listManager.setParcela();
          sectionParcela = null;
          $.listView.replaceSectionAt(3, sectionParcela);
        }
        OrderManager.setPagamento(cond);
        if (cond.CONDPG == '003') {
          sectionParcela = Ti.UI.createListSection({
            headerView: Alloy.createWidget("HeaderList", { title: "Parcela(s)" }).getView() });

          sectionParcela.setItems(refreshParcela());
          $.listView.insertSectionAt(3, sectionParcela);
        } else
        if (sectionParcela) {
          sectionParcela = null;
          $.listView.replaceSectionAt(3, sectionParcela);
        }
      } });
  };

  actionItem.parcela = function (e) {
    if (status != 0 && status < 4) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a forma de pagamento primeiro!');
    Alloy.Globals.openWindow('CadPedido/Parcela', { callback: function (parcelas) {
        console.log('PARCELAS CADASTRADAS> ', parcelas);
        listManager.setParcela(parcelas);
        OrderManager.setParcela(parcelas);
      } });
  };

  actionItem.frete = function (e) {
    Alloy.Globals.openWindow('CadPedido/Frete', { callback: function (frete) {
        listManager.setFrete(frete);
        OrderManager.setFrete(frete);
      } });
  };

  actionItem.desconto = function (e) {
    $.popup.showPopUpDesconto(function (value) {
      console.log('DESCONTO ADICIONADO: ', value);
      $.popup.hide();
      listManager.setDesconto(value);
      OrderManager.setDesconto(value);
    });
  };

  actionItem.obs = function (e) {
    Alloy.Globals.openWindow('CadPedido/Observacao', { callback: function (obs) {
        listManager.setObservacao(obs);
        OrderManager.setObs(obs);
      } });
  };

  actionItem.prods = function (e) {
    if (sectionParcela && status <= 4) return Alloy.Globals.showAlert('Atenção!', 'É necessário inserir as parcelas primeiro!');else
    if (status != 0 && status < 4) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a forma de pagamento primeiro!');







    Alloy.Globals.openWindow('Produtos', { callback: function (prod) {



      } });
  };


  listManager = createListView();

  $.listView.addEventListener('itemclick', function (e) {
    var item = e.section.getItemAt(e.itemIndex);
    var type = item.properties.type;
    if (actionItem[type])
    actionItem[type](e);
  });

  $.viewPanel.addEventListener('click', function (e) {
    if (status == 0) {

      console.log('CADASTRAR PEDIDO!');
    } else
    if (status == 1) actionItem.cliente();else
    if (status == 2) actionItem.tabela();else
    if (status == 3) actionItem.pagamento();else
    if (status == 4) actionItem.parcela();else
    if (status == 5) actionItem.frete();
  });
























  $.mainWindow.addEventListener('focus', function (e) {
    if (listManager) {
      console.log('TEM LIST MANAGER');
      var cart = OrderManager.getTotalPedido();
      listManager.setProdutos(cart);
    }
    checkStatus();
  });

  $.mainWindow.addEventListener('open', function (e) {
    $.activityIndicator.show();
    refreshMenuActionBar();
    if (click_pedido && click_pedido.A1_MOBILE) {
      OrderManager.init();
      getDados();
    } else
    if (OrderManager.isActive()) {
      var obj = {};
      obj.cliente = OrderManager.getCliente();
      obj.tabela = OrderManager.getTabela();
      obj.pagamento = OrderManager.getPagamento();
      obj.parcela = OrderManager.getParcela();
      obj.frete = OrderManager.getFrete();
      obj.desconto = OrderManager.getDesconto();
      obj.obs = OrderManager.getObs();
      refreshListView(obj);
    } else
    OrderManager.init();
    checkStatus();
  });









  _.extend($, exports);
}

module.exports = Controller;