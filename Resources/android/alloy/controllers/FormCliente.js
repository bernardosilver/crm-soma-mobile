var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'FormCliente';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Custom", id: "mainWindow", title: "Cliente", tabBarHidden: true });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId197() {
      $.__views.mainWindow.removeEventListener('open', __alloyId197);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Cliente";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId197);
  }
  var __alloyId198 = {};var __alloyId201 = [];var __alloyId203 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId201.push(__alloyId203);var __alloyId205 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId201.push(__alloyId205);var __alloyId200 = { properties: { name: "templateDados", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId201 };__alloyId198["templateDados"] = __alloyId200;var __alloyId208 = [];var __alloyId210 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId208.push(__alloyId210);var __alloyId212 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId208.push(__alloyId212);var __alloyId207 = { properties: { name: "templateEndereco", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId208 };__alloyId198["templateEndereco"] = __alloyId207;var __alloyId215 = [];var __alloyId217 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId215.push(__alloyId217);var __alloyId219 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId215.push(__alloyId219);var __alloyId214 = { properties: { name: "templateCobranca", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId215 };__alloyId198["templateCobranca"] = __alloyId214;var __alloyId222 = [];var __alloyId224 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId222.push(__alloyId224);var __alloyId226 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId222.push(__alloyId226);var __alloyId221 = { properties: { name: "templateSocios", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId222 };__alloyId198["templateSocios"] = __alloyId221;var __alloyId229 = [];var __alloyId231 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId229.push(__alloyId231);var __alloyId233 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId229.push(__alloyId233);var __alloyId228 = { properties: { name: "templateComerciais", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId229 };__alloyId198["templateComerciais"] = __alloyId228;var __alloyId236 = [];var __alloyId238 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId236.push(__alloyId238);var __alloyId240 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId236.push(__alloyId240);var __alloyId235 = { properties: { name: "templateBancarias", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId236 };__alloyId198["templateBancarias"] = __alloyId235;var __alloyId243 = [];var __alloyId245 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "lbNome", left: "15dp", right: "65dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId243.push(__alloyId245);var __alloyId247 = { type: 'Ti.UI.Label', bindId: 'labelAlterar', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, text: 'Alterar', touchEnabled: false, bindId: "labelAlterar", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE, top: "15dp", bottom: "15dp" } };__alloyId243.push(__alloyId247);var __alloyId242 = { properties: { name: "templateCredito", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId243 };__alloyId198["templateCredito"] = __alloyId242;var __alloyId250 = [];var __alloyId252 = { type: 'Ti.UI.Label', bindId: 'label', properties: { color: Alloy.Globals.GRAY_COLOR, font: { fontSize: "14dp" }, touchEnabled: false, bindId: "label", height: Ti.UI.SIZE, width: Ti.UI.SIZE, left: "15dp" } };__alloyId250.push(__alloyId252);var __alloyId254 = { type: 'Ti.UI.Label', bindId: 'labelEscolher', properties: { color: Alloy.Globals.RED_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "labelEscolher", height: Ti.UI.SIZE, right: "15dp", width: Ti.UI.SIZE } };__alloyId250.push(__alloyId254);var __alloyId249 = { properties: { name: "templateEscolher", height: "50dp", backgroundColor: "white", accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId250 };__alloyId198["templateEscolher"] = __alloyId249;$.__views.listView = Ti.UI.createListView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId198, id: "listView", top: "-2dp", bottom: "60dp" });

  $.__views.mainWindow.add($.__views.listView);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  $.__views.viewPanel = Ti.UI.createView(
  { backgroundColor: Alloy.Globals.RED_COLOR, id: "viewPanel", height: "40dp", bottom: "10dp", left: "15dp", right: "15dp", width: Ti.UI.FILL, borderRadius: 4, elevation: 11 });

  $.__views.mainWindow.add($.__views.viewPanel);
  $.__views.btCheckOut = Ti.UI.createLabel(
  { color: Alloy.Globals.WHITE_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, id: "btCheckOut", textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER });

  $.__views.viewPanel.add($.__views.btCheckOut);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var click_cliente = args.cliente || null;
  var Mask = require("Mask");
  var actionItem = {};
  var cliente = {};
  var status = null;
  var ClientManager = require('ClientManager');
  var LocalData = require('LocalData');
  var listManager = null;

  function doClose() {
    $.mainWindow.close();
  }

  function createListView(obj) {
    var sections = [];

    var sectionDadosPessoais = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Dados Pessoais" }).getView() });

    sectionDadosPessoais.setItems(refreshDadosPessoais());
    sections.push(sectionDadosPessoais);

    var sectionEndereco = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Endereço de entrega" }).getView() });

    sectionEndereco.setItems(refreshEndereco());
    sections.push(sectionEndereco);

    var sectionCobranca = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Endereço de cobrança" }).getView() });

    sectionCobranca.setItems(refreshCobranca());
    sections.push(sectionCobranca);
    if (!click_cliente || click_cliente.A1_MOBILE) {
      var sectionSocios = Ti.UI.createListSection({
        headerView: Alloy.createWidget("HeaderList", { title: "Sócios" }).getView() });

      sectionSocios.setItems(refreshSocios());
      sections.push(sectionSocios);

      var sectionComerciais = Ti.UI.createListSection({
        headerView: Alloy.createWidget("HeaderList", { title: "Referências Comerciais" }).getView() });

      sectionComerciais.setItems(refreshComerciais());
      sections.push(sectionComerciais);

      var sectionBancarias = Ti.UI.createListSection({
        headerView: Alloy.createWidget("HeaderList", { title: "Referências Bancárias" }).getView() });

      sectionBancarias.setItems(refreshBancarias());
      sections.push(sectionBancarias);
    }

    var sectionCredito = Ti.UI.createListSection({
      headerView: Alloy.createWidget("HeaderList", { title: "Limite de Crédito" }).getView() });

    sectionCredito.setItems(refreshCredito());
    sections.push(sectionCredito);

    $.listView.setSections(sections);

    return {
      setDadosPessoais: function (dados) {
        sectionDadosPessoais.setItems(refreshDadosPessoais(dados));
      },
      setEndereco: function (end) {
        sectionEndereco.setItems(refreshEndereco(end));
      },
      setCobranca: function (end) {
        sectionCobranca.setItems(refreshCobranca(end));
      },
      setSocios: function (socios) {
        sectionSocios.setItems(refreshSocios(socios));
      },
      setComerciais: function (referencias) {
        sectionComerciais.setItems(refreshComerciais(referencias));
      },
      setBancarias: function (referencias) {
        sectionBancarias.setItems(refreshBancarias(referencias));
      },
      setCredito: function (credito) {
        sectionCredito.setItems(refreshCredito(credito));
      } };

  }

  function refreshListView(obj) {
    var dados = obj.dados || null;
    var entrega = obj.entrega || null;
    var cobranca = obj.cobranca || null;
    var socios = obj.socios || null;
    var refCom = obj.refCom || null;
    var refBan = obj.refBan || null;
    var cred = obj.cred || null;
    listManager.setDadosPessoais(dados);
    listManager.setEndereco(entrega);
    listManager.setCobranca(cobranca);
    listManager.setCredito(cred);
    if (!click_cliente || click_cliente.A1_MOBILE) {
      listManager.setSocios(socios);
      listManager.setComerciais(refCom);
      listManager.setBancarias(refBan);
    }
    $.activityIndicator.hide();
  }

  function refreshDadosPessoais(dados) {
    var itemDadosPessoais = dados && dados.A1_NOME ? createItemDadosPessoais(Alloy.Globals.createTextCliente(dados)) : createItemListEscolher('dados', 'Clique para inserir as informações.');
    return [itemDadosPessoais];
  }

  function refreshEndereco(endereco) {
    var itemEndereco = endereco && endereco.A1_END ? createItemEndereco(Alloy.Globals.createTextEndereco(endereco)) : createItemListEscolher('endereco', 'Clique para inserir as informações.');
    return [itemEndereco];
  }

  function refreshCobranca(endereco) {
    var itemCobranca = endereco && endereco.A1_ENDCOB ? createItemCobranca(Alloy.Globals.createTextEnderecoCob(endereco)) : createItemListEscolher('cobranca', 'Clique para inserir as informações.');
    return [itemCobranca];
  }

  function refreshSocios(socios) {
    var itemSocios = socios && socios.length ? createItemSocios(Alloy.Globals.createTextSocios(socios)) : createItemListEscolher('socios', 'Clique para inserir as informações.');
    return [itemSocios];
  }

  function refreshComerciais(referencias) {
    var itemComerciais = referencias && referencias.length ? createItemComerciais(Alloy.Globals.createTextRefCom(referencias)) : createItemListEscolher('comerciais', 'Clique para inserir as informações.');
    return [itemComerciais];
  }

  function refreshBancarias(bancos) {
    var itemBancarias = bancos && bancos.length ? createItemBancarias(Alloy.Globals.createTextRefBanc(bancos)) : createItemListEscolher('bancarias', 'Clique para inserir as informações.');
    return [itemBancarias];
  }

  function refreshCredito(credito) {
    var itemCredito = credito ? createItemCredito(credito) : createItemListEscolher('credito', 'Clique para inserir.');
    return [itemCredito];
  }

  function createItemListEscolher(type, label) {
    return {
      template: 'templateEscolher',
      label: {
        text: label || 'Escolher' },

      labelEscolher: {
        text: 'Inserir' },

      properties: {
        type: type,
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemDadosPessoais(dados) {
    return {
      template: "templateDados",
      lbNome: {
        text: dados },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: dados,
        type: 'dados',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemEndereco(endereco) {
    return {
      template: "templateEndereco",
      lbNome: {
        text: endereco },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: endereco,
        type: 'endereco',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemCobranca(endereco) {
    return {
      template: "templateCobranca",
      lbNome: {
        text: endereco },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: endereco,
        type: 'cobranca',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemSocios(socios) {
    return {
      template: "templateSocios",
      lbNome: {
        text: socios },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: socios,
        type: 'socios',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemComerciais(text) {
    return {
      template: "templateComerciais",
      lbNome: {
        text: text },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: text,
        type: 'comerciais',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemBancarias(text) {
    return {
      template: "templateBancarias",
      lbNome: {
        text: text },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: text,
        type: 'bancarias',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  function createItemCredito(valor) {
    return {
      template: "templateCredito",
      lbNome: {
        text: 'R$ ' + Mask.real(valor) },

      labelAlterar: {
        visible: click_cliente ? false : true },

      properties: {
        data: valor,
        type: 'credito',
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  async function getDados() {
    var cli = {};
    var refs = [];
    $.activityIndicator.show();
    if (click_cliente.A1_COD) {
      cli = await getCliente({ A1_COD: click_cliente.A1_COD });
      await createCliente(cli, refs);
      checkStatus();
    } else
    if (click_cliente.A1_MOBILE) {
      cli = await getCliente({ ID: click_cliente.ID });
      refs = await getReferencias(click_cliente.ID);
      console.log('CLIENTE: ', cli);
      console.log('CLIENTE: ', refs);

      createManager(cli, refs);
    }
  }

  const getCliente = id => new Promise((resolve, reject) => {
    LocalData.getClienteDetail({
      id,
      success: function (response) {
        resolve(response[0]);
      },
      error: function (err) {
        console.log('ERRO AO BUSCAR CLIENTE DETAIL: ', err);
        reject(err);
      } });

  });

  const getReferencias = cod_cli => new Promise((resolve, reject) => {
    LocalData.getCadRef({
      cliente_id: cod_cli,
      success: function (response) {
        resolve(response);
      },
      error: function (err) {
        console.log('ERRO AO BUSCAR REFS: ', err);
        reject(err);
      } });

  });

  function createCliente(cli, refs) {
    cliente.socios = [];
    cliente.refCom = [];
    cliente.refBan = [];


    cliente.dados = {
      A1_NOME: cli.A1_NOME,
      A1_NREDUZ: cli.A1_NREDUZ,
      A1_DDD: cli.A1_DDD.trim(),
      A1_TEL: cli.A1_TEL,
      A1_TEL2: cli.A1_TEL2,
      A1_TEL3: cli.A1_TEL3,
      A1_TEL4: cli.A1_TEL4,
      A1_EMAIL: cli.A1_EMAIL,
      A1_PESSOA: cli.A1_PESSOA,
      A1_CGC: cli.A1_CGC,
      A1_INSCR: cli.A1_INSCR,
      A1_PFISICA: cli.A1_PFISICA,
      A1_DTNASC: cli.A1_DTNASC,
      A1_ME: cli.A1_ME,
      A1_OBSRC: cli.A1_OBSRC };



    cliente.entrega = {
      A1_END: cli.A1_END,
      A1_COMPLEM: cli.A1_COMPLEM,
      A1_BAIRRO: cli.A1_BAIRRO,
      A1_EST: cli.A1_EST,
      A1_MUN: cli.A1_MUN,
      A1_CODMUN: cli.A1_CODMUN,
      A1_CEP: cli.A1_CEP,
      A1_INFOROT: cli.A1_INFOROT,
      A1_LAT: cli.A1_LAT,
      A1_LONG: cli.A1_LONG };



    cliente.cred = parseFloat(cli.A1_LC).toFixed(2);


    cliente.cobranca = {
      A1_ENDCOB: cli.A1_ENDCOB,
      A1_BAIRROC: cli.A1_BAIRROC,
      A1_ESTC: cli.A1_ESTC,
      A1_MUNC: cli.A1_MUNC,
      A1_CEPC: cli.A1_CEPC };

    if (refs.length) {
      for (const ref of refs) {
        if (ref.AO_TIPO == '1') cliente.socios.push(ref);
        if (ref.AO_TIPO == '2') cliente.refCom.push(ref);
        if (ref.AO_TIPO == '3') cliente.refBan.push(ref);
      }
    }

    refreshListView(cliente);
  }

  function checkStatus() {
    status = ClientManager.getStatus();
    var text = '';
    if (status == 1) $.btCheckOut.setText('Inserir dados pessoais');else
    if (status == 2) $.btCheckOut.setText('Inserir endereço de entrega');else
    if (status == 3) $.btCheckOut.setText('Inserir endereço de cobrança');else
    if (status == 4) $.btCheckOut.setText('Inserir sócio');else
    if (status == 5) $.btCheckOut.setText('Inserir referências comereciais');else
    if (status == 6) $.btCheckOut.setText('Inserir referências bancárias');else
    if (status == 7) $.btCheckOut.setText('Inserir limite de crédito');else
    $.btCheckOut.setText('Salvar cliente');
    $.activityIndicator.hide();
  }

  async function insertCliente() {
    $.activityIndicator.show();
    var cliente_id = 0;
    const new_dados = await ClientManager.generateCliente();

    if (click_cliente && click_cliente.ID) {
      await LocalData.updateCadCliente({
        data: {
          id: click_cliente.ID,
          cliente: new_dados },

        success: function (id) {
          cliente_id = id;
        },
        error: function (err) {
          console.log('DEU ERRO AO ATUALIZAR CLIENTE: ', err);
        } });

      const new_refs = await ClientManager.generateRefs(click_cliente.ID);
      console.log('REFERENCIAS> ', new_refs);
      for (const new_ref of new_refs) {
        await LocalData.updateCadRef({
          data: {
            id: new_ref.ID,
            ref: new_ref },

          success: function (ref) {
            console.log('REFERENCIA ATUALIZADA: ', ref);
          },
          error: function (err) {
            console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
          } });

      }
      $.activityIndicator.hide();
    } else
    {
      await LocalData.insertCadCliente({
        cliente: new_dados,
        success: function (id) {
          cliente_id = id;
        },
        error: function (err) {
          console.log('DEU ERRADO AO CADASTRAR CLIENTE: ', err);
        } });

      const new_refs = await ClientManager.generateRefs(cliente_id);
      await LocalData.createTableCadRef();
      for (const new_ref of new_refs) {
        await LocalData.insertCadRef({
          ref: new_ref,
          success: function (ref) {
            console.log('REFERENCIA CADASTRADA: ', ref);
          },
          error: function (err) {
            console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
          } });

      }
      doClose();
      $.activityIndicator.hide();
    }
  }

  function createManager(cli, refs) {
    var com = [];
    var banc = [];
    var soc = [];
    ClientManager.setDadosPessoais(cli);
    ClientManager.setEndereco({
      logradouro: cli.A1_END,
      complemento: cli.A1_COMPLEM,
      bairro: cli.A1_BAIRRO,
      uf: cli.A1_EST,
      COD_UF: cli.COD_UF,
      municipio: cli.A1_MUN,
      CODMUN: cli.A1_CODMUN,
      cep: cli.A1_CEP,
      referencia: cli.A1_INFOROT,
      latitude: cli.A1_LAT,
      longitude: cli.A1_LONG });

    ClientManager.setEnderecoCobranca({
      logradouro: cli.A1_ENDCOB,
      bairro: cli.A1_BAIRROC,
      uf: cli.A1_ESTC,
      CODC_UF: cli.CODC_UF,
      municipio: cli.A1_MUNC,
      COD_MC: cli.COD_MC,
      cep: cli.A1_CEPC });

    ClientManager.setLimiteCredito(parseFloat(cli.A1_LC).toFixed(2));
    for (var i in refs) {
      if (refs[i].AO_TIPO == '1') {
        soc.push(refs[i]);
      } else
      if (refs[i].AO_TIPO == '2') {
        com.push(refs[i]);
      } else
      if (refs[i].AO_TIPO == '3') {
        banc.push(refs[i]);
      }
    }
    ClientManager.setSocios(soc);
    ClientManager.setReferenciasCom(com);
    ClientManager.setReferenciasBanc(banc);

    var obj = {};
    obj.dados = ClientManager.getDadosPessoais();
    obj.entrega = ClientManager.getEndereco();
    obj.cobranca = ClientManager.getEnderecoCobranca();
    obj.socios = ClientManager.getSocios();
    obj.refCom = ClientManager.getReferenciasCom();
    obj.refBan = ClientManager.getReferenciasBanc();
    obj.cred = parseFloat(ClientManager.getLimiteCredito()).toFixed(2);

    refreshListView(obj);

    ClientManager.setDadosCompleto();
    ClientManager.setEntregaCompleto();
    ClientManager.setCobrancaCompleto();
    ClientManager.setSociosCompleto();
    ClientManager.setComercialCompleto();
    ClientManager.setBancariaCompleto();
    ClientManager.setCreditoCompleto();
  }

  actionItem.dados = function (e) {
    Alloy.Globals.openWindow('CadCliente/DadosPessoais', { dados: cliente.dados, callback: function (dados) {
        listManager.setDadosPessoais(dados);
        ClientManager.setDadosCompleto();
      } });
  };

  actionItem.endereco = function (e) {
    Alloy.Globals.openWindow('FormEndereco', { end_entrega: cliente.entrega, callback: function (endereco) {
        listManager.setEndereco(endereco);
        ClientManager.setEntregaCompleto();
      } });
  };

  actionItem.cobranca = function (e) {
    Alloy.Globals.openWindow('FormEndereco', { end_cobranca: cliente.cobranca, cobranca: true, callback: function (endereco) {
        listManager.setCobranca(endereco);
        ClientManager.setCobrancaCompleto();
      } });
  };

  actionItem.socios = function (e) {
    Alloy.Globals.openWindow('CadCliente/Socios', { socios: cliente.socios, callback: function (socios) {
        listManager.setSocios(socios);
        ClientManager.setSociosCompleto();
      } });
  };

  actionItem.comerciais = function (e) {
    Alloy.Globals.openWindow('CadCliente/RefComercial', { refs: cliente.refCom, callback: function (referencias) {
        listManager.setComerciais(referencias);
        ClientManager.setComercialCompleto();
      } });
  };

  actionItem.bancarias = function (e) {
    Alloy.Globals.openWindow('CadCliente/RefBancaria', { refs: cliente.refBan, callback: function (bancos) {
        listManager.setBancarias(bancos);
        ClientManager.setBancariaCompleto();
      } });
  };

  actionItem.credito = function (e) {
    Alloy.Globals.openWindow('CadCliente/Credito', { cred: cliente.cred, callback: function (valor) {
        listManager.setCredito(valor);
        ClientManager.setCreditoCompleto();
      } });
  };

  listManager = createListView();

  $.listView.addEventListener('itemclick', function (e) {
    var item = e.section.getItemAt(e.itemIndex);
    var type = item.properties.type;
    if (actionItem[type])
    actionItem[type](e);
  });

  $.viewPanel.addEventListener('click', function (e) {
    if (status == 0) {
      insertCliente();
    } else
    if (status == 1) actionItem.dados();else
    if (status == 2) actionItem.endereco();else
    if (status == 3) actionItem.cobranca();else
    if (status == 4) actionItem.socios();else
    if (status == 5) actionItem.comerciais();else
    if (status == 6) actionItem.bancarias();else
    if (status == 7) actionItem.credito();
  });

  $.mainWindow.addEventListener('focus', function (e) {
    checkStatus();
  });
  $.mainWindow.addEventListener('open', function (e) {
    $.activityIndicator.show();
    if (click_cliente && click_cliente.A1_COD) {
      $.listView.setBottom('0dp');
      $.viewPanel.setHeight('0dp');
      $.viewPanel.setVisible('false');
      $.viewPanel.setBottom('0dp');
      getDados();
    } else
    if (click_cliente && click_cliente.A1_MOBILE) {
      ClientManager.init();
      getDados();
    } else
    if (ClientManager.isActive()) {
      var obj = {};
      obj.dados = ClientManager.getDadosPessoais();
      obj.entrega = ClientManager.getEndereco();
      obj.cobranca = ClientManager.getEnderecoCobranca();
      obj.socios = ClientManager.getSocios();
      obj.refCom = ClientManager.getReferenciasCom();
      obj.refBan = ClientManager.getReferenciasBanc();
      obj.cred = ClientManager.getLimiteCredito();
      refreshListView(obj);
    } else
    ClientManager.init();
    checkStatus();
  });









  _.extend($, exports);
}

module.exports = Controller;