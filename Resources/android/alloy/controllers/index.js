var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'index';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  exports.destroy = function () {};




  _.extend($, $.__views);


  var refreshData = require("RefreshData");
  var LocalData = require("LocalData");

  Alloy.Globals.openWindowTab = function (screen, params) {
    var controller = Alloy.createController(screen, params);
    if (false)
    Alloy.Globals.tabGroup.activeTab.open(controller.getView());else

    controller.getView().open({ modal: true });
  };

  Alloy.Globals.openWindow = function (screen, params) {
    var controller = Alloy.createController(screen, params);
    var currentWindow = null;
    var navigations = Alloy.Globals.Navigations || [];

    var windowNav = params && params.windowNav ? params.windowNav : null;
    var navigation = params && params.navigation ? params.navigation : null;
    var modal = params && params.modal ? params.modal : false;

    if (true || !windowNav && !navigation)
    currentWindow = controller.getView();

    if (false) {
      if (windowNav) {
        for (var i in navigations) {
          if (navigations[i].controller == windowNav) {
            navigations[i].navigation.openWindow(controller.getView());
            return true;
          }
        }
      } else if (navigation) {
        currentWindow = Titanium.UI.iOS.createNavigationWindow({
          window: controller.getView() });

        navigations.push({ navigation: currentWindow, controller: screen });
        Alloy.Globals.Navigations = navigations;
      }
    }

    currentWindow.open({ modal: modal });
  };

  Alloy.Globals.openWindowModal = function (screen, params) {
    var controller = Alloy.createController(screen, params);
    var currentWindow = null;
    if (params.navigation == true && false) {
      currentWindow = Titanium.UI.iOS.createNavigationWindow({
        window: controller.getView() });

      var navigations = Alloy.Globals.Navigations || [];
      navigations.push({ navigation: currentWindow, controller: screen });
      Alloy.Globals.Navigations = navigations;
    } else
    currentWindow = controller.getView();

    currentWindow.open({ modal: true });
  };

  Alloy.Globals.refreshToken = function () {
    Ti.API.error("[refreshToken]");
    if (Ti.App.Properties.getString("token")) {
      refreshData.sendToken({
        successCallBack: function (data) {
          Ti.API.info("Token cadastrado");
        },
        errorCallBack: function (error) {
          Ti.API.error("Erro ao enviar o token");
        },
        data: {
          usuario_id: Alloy.Globals.isLogged() ? Alloy.Globals.getLocalDataUsuario().id : null,
          token: Ti.App.Properties.getString("token"),
          tipo: true ? "android" : "ios" } });


    }
  };

  function openMainWindow() {
    var MainWindow = Alloy.createController("MainWindow").getView();
    if (false)
    MainWindow.open();else

    MainWindow.open({ modal: true });
  }

  Ti.App.addEventListener("uncaughtException", function (e) {
    Ti.API.info(JSON.stringify(e));
  });

  openMainWindow();
  var usuario = Alloy.Globals.getLocalDataUsuario();
  if (usuario) {
    console.log("TEM USUÁRIO: ", usuario);
    openMainWindow();
  } else
  {
    console.log("NÃO TEM USUÁRIO: ", usuario);
    Alloy.Globals.openWindow("Login", { navigation: true });
  }









  _.extend($, exports);
}

module.exports = Controller;