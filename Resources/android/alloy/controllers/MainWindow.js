var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'MainWindow';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Custom", id: "mainWindow" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  $.__views.label = Ti.UI.createLabel(
  { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "18dp", fontWeight: "bold" }, id: "label", left: "15dp", top: "10dp", right: "15dp" });

  $.__views.mainWindow.add($.__views.label);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: __parentSymbol });
  $.__views.activityIndicator && $.addTopLevelView($.__views.activityIndicator);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var LocalData = require('LocalData');
  var moment = require('alloy/moment');
  var usuario = Alloy.Globals.getLocalDataUsuario();;
  var menu = null;
  var actionBar = null;

  function createMenu() {
    var leftView = Alloy.createController('Menu').getView();



    var drawer = Ti.UI.Android.createDrawerLayout({
      leftView: leftView });










    return drawer;
  }

  function doClose() {
    $.mainWindow.close();
  }

  function checkSync() {
    var dtSync = Alloy.Globals.getSync();
    var dtAtual = moment().hours(0).minutes(0).seconds(0).milliseconds(0);
    var diffDay = dtSync ? dtAtual.diff(moment(dtSync), 'days') : null;
    var ultSync = moment(dtSync);
    console.log('DT SYNC: ', dtSync);
    console.log('DT HJ: ', dtAtual);
    console.log('DIFF: ', diffDay);
    if (!dtSync || dtSync && diffDay >= 1) {
      Alloy.Globals.openWindow("Sync", { download: true });
    }
    countCadCli();
  }

  function countCadCli() {
    LocalData.countCadCli({
      success: function (count) {
        if (count == 0) {
          $.label.setText('');
          $.label.setVisible(false);
        }
        if (count > 0) {
          Alloy.Globals.setCountCadCli(count);
          $.label.setText(`Atenção! Existe(m) ${count} cliente(s) para ser(em) enviado(s)!`);
        }
      },
      error: function (err) {
        console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
      } });

  }

  $.mainWindow.open();

  $.mainWindow.addEventListener('focus', function () {
    usuario = Alloy.Globals.getLocalDataUsuario();
    if (!usuario) {
      doClose();
      Alloy.Globals.openWindow("Login", { navigation: true });
    }
    var activity = $.mainWindow.getActivity(),
    actionBar = activity.getActionBar();

    if (actionBar) {
      actionBar.displayHomeAsUp = true;

    }
    if (menu) {
      $.mainWindow.remove(menu);
    }
    if (usuario) {
      menu = createMenu();
      $.mainWindow.add(menu);
      actionBar.onHomeIconItemSelected = function () {
        menu.toggleLeft();
      };
    }
    checkSync();
  });

  $.mainWindow.addEventListener('open', function () {

  });









  _.extend($, exports);
}

module.exports = Controller;