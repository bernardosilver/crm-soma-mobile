var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'Sync';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Login", backgroundGradient: { type: "linear", startPoint: { x: "0%", y: "0%" }, endPoint: { x: "100%", y: "100%" }, colors: [{ color: Alloy.Globals.MAIN_COLOR, offset: 0 }, { color: Alloy.Globals.DARK_MAIN_COLOR, offset: 1 }] }, id: "mainWindow", title: "Soma Nutrição Animal" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  $.__views.__alloyId522 = Ti.UI.createView(
  { height: Ti.UI.SIZE, width: "90%", layout: "vertical", top: "100dp", id: "__alloyId522" });

  $.__views.mainWindow.add($.__views.__alloyId522);
  $.__views.__alloyId523 = Ti.UI.createLabel(
  { font: { fontSize: "23dp", fontWeight: "bold" }, text: 'Bem vindo ao Soma CRM!', color: "white", textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER, top: 0, id: "__alloyId523" });

  $.__views.__alloyId522.add($.__views.__alloyId523);
  $.__views.__alloyId524 = Ti.UI.createLabel(
  { font: { fontSize: "18dp", fontWeight: "bold" }, text: 'Aguarde... estamos carregando todas as informações para você!', color: "white", textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER, top: "20dp", id: "__alloyId524" });

  $.__views.__alloyId522.add($.__views.__alloyId524);
  $.__views.__alloyId525 = Ti.UI.createImageView(
  { image: "/images/logosomaBRANCO.png", height: "120dp", width: "339dp", top: "90dp", bottom: "50dp", id: "__alloyId525" });

  $.__views.__alloyId522.add($.__views.__alloyId525);
  $.__views.message = Alloy.createWidget('ErrorMessage', 'widget', { id: "message", top: 0, __parentSymbol: $.__views.mainWindow });
  $.__views.message.setParent($.__views.mainWindow);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var args = $.args;
  var download = args.download || false;
  var upload = args.upload || false;
  var moment = require('alloy/moment');
  var RefreshData = require("RefreshData");
  var LocalData = require("LocalData");
  var usuario = Alloy.Globals.getLocalDataUsuario().data;

  function doClose() {
    $.mainWindow.close();
  }

  function showErrorMessage() {
    $.message.setVisible(true);
    $.message.setTextAlign(Ti.UI.TEXT_ALIGNMENT_CENTER);
    $.message.setText("Ocorreu um erro ao sincronizar os dados! Verifique a conexão com a internet e tente novamente!");
    $.message.setBtTitle("Tentar Novamente");
    $.message.setBtAction(function () {
      downloadDados();
      $.message.setVisible(false);
    });
    $.activityIndicator.hide();
  }

  async function uploadDados() {
    let insertCli = {};
    let id_cliLocal = 0;
    let refs = [];
    const qnt = await countCadCli();

    $.activityIndicator.show();
    $.activityIndicator.setText(`... fazendo upload dos clientess. 1 de ${qnt} ...`);

    var clientes = await getCadCli();
    for (var i = 0; i < clientes.length; i++) {
      $.activityIndicator.setText(`... fazendo upload dos clientes. '${i + 1} de ${qnt}' ...`);
      id_cliLocal = clientes[i].ID;
      insertCli = await uploadCli(clientes[i]);
      insertCli.id_cliLocal = id_cliLocal;
      await updateReferencias(insertCli);
      refs = await getCadRef(insertCli.ID);
      await uploadReferencias(refs);
    }
    $.activityIndicator.hide();
    await downloadDados();
  }

  async function downloadDados() {
    var clientes = [];
    var tabelas = [];
    $.activityIndicator.show();
    $.activityIndicator.setText('... fazendo download da lista de estados ...');
    await downloadEstados();
    $.activityIndicator.setText('... concluído! ...');
    $.activityIndicator.setText('... fazendo download da lista de cidades ...');
    await downloadMunicipios();
    $.activityIndicator.setText('... concluído! ...');
    $.activityIndicator.setText('... fazendo download da lista de clientes ...');
    clientes = await downloadClientes();
    $.activityIndicator.setText('... concluído! ...');
    $.activityIndicator.setText('... fazendo download das condições de pagamento ...');
    await downloadCondPag(clientes);
    $.activityIndicator.setText('... concluído! ...');
    $.activityIndicator.setText('... fazendo download das tabelas de preços ...');
    tabelas = await downloadTabelas();
    $.activityIndicator.setText('... concluído! ...');
    $.activityIndicator.setText('... fazendo download dos produtos ...');
    await downloadProdutos(tabelas);
    Alloy.Globals.setSync(moment().hours(0).minutes(0).seconds(0).milliseconds(0));
    $.activityIndicator.hide();
    doClose();
  }

  async function downloadEstados() {
    try {
      let estados = [];
      console.log('1');
      await LocalData.createTableUF();
      console.log('2');
      await LocalData.clearTable({ table: 'uf' });
      console.log('3');
      estados = await getUf();
      console.log('4');
      await LocalData.insertUF(estados);
      console.log('5');
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD ESTADOS: ', err);
    }
  }

  const getUf = () => new Promise((resolve, reject) => {
    RefreshData.getEstado({
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO UF: ', err);
        if (err.code == 401) {
          Alloy.Globals.openWindow("Login");
          doClose();
        } else

        showErrorMessage();
      } });

  });

  async function downloadMunicipios() {
    try {
      let mnc = [];
      await LocalData.createTableMnc();
      await LocalData.clearTable({ table: 'mnc' });
      mnc = await getMnc();
      await LocalData.insertMnc(mnc);
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD CIDADES: ', err);
    }
  }

  const getMnc = () => new Promise((resolve, reject) => {
    RefreshData.getMnc({
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO MNC: ', err);
        if (err.code == 401) {
          Alloy.Globals.openWindow("Login");
          doClose();
        } else

        showErrorMessage();
      } });

  });

  async function downloadClientes() {
    try {
      let clientes = [];
      await LocalData.createTableCliente();
      await LocalData.clearTable({ table: 'cliente', where: ' WHERE A1_MOBILE !=1 ' });

      clientes = await getClientes();
      await LocalData.insertClientes(clientes);
      return clientes;
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD CLIENTES: ', err);
    }
  }

  const getClientes = () => new Promise((resolve, reject) => {
    var data = {};
    data.cod = usuario.USR_LOGIN;
    RefreshData.getCliente({
      data: data,
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO CLI: ', err);
        if (err.code == 401) {
          Alloy.Globals.openWindow("Login");
          doClose();
        } else

        showErrorMessage();
      } });

  });

  async function downloadCondPag(clientes) {
    try {
      let cond = [];
      await LocalData.clearTable({ table: 'condPag' });
      await LocalData.createTableCondPag();
      for (var i in clientes) {
        cond = await getCondPag({ cod: clientes[i].A1_COD, grupo: clientes[i].A1_GRPVEN });
        await LocalData.insertCondPag(cond);
      }
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD CONDIÇÃO DE PAGAMENTO: ', err);
    }
  }

  const getCondPag = data => new Promise((resolve, reject) => {

    RefreshData.getCondPag({
      data: data,
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO COND PAG: ', err);
        if (err.code == 401) {
          Alloy.Globals.openWindow("Login");
          doClose();
        } else

        showErrorMessage();
      } });

  });

  async function downloadReferencias() {
    try {
      let refs = [];
      await LocalData.createTableReferencia();
      await LocalData.clearTable({ table: 'referencia' });
      refs = await getReferencias();
      await LocalData.insertReferencia(refs);
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD REFERENCIAS: ', err);
    }
  }

  const getReferencias = () => new Promise((resolve, reject) => {
    RefreshData.getReferencia({
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO REFS: ', err);
        if (err.code == 401) {
          Alloy.Globals.openWindow("Login");
          doClose();
        } else
        {
          showErrorMessage();
        }
      } });

  });

  async function downloadTabelas() {
    try {
      let tabelas = [];
      await LocalData.createTableTabelas();
      await LocalData.clearTable({ table: 'tabelaPreco' });
      await LocalData.createTableProd();
      await LocalData.clearTable({ table: 'produto' });
      tabelas = await getTabelas();
      await LocalData.insertTabelas(tabelas, usuario.USR_LOGIN);
      return tabelas;
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD TABELAS: ', err);
    }
  }

  async function downloadProdutos(tabelas) {
    try {
      let prods = [];
      for (var i in tabelas) {
        prods = await getProduto(tabelas[i]);
        await LocalData.insertProds(prods);
      }
    }
    catch (err) {
      console.log('DEU ERRO DOWNLOAD PRODUTOS: ', err);
    }
  }

  const getTabelas = () => new Promise((resolve, reject) => {
    RefreshData.getTabelas({
      data: { user: usuario.USR_LOGIN },
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO TABELAS: ', err);
      } });

  });

  const getProduto = tabela => new Promise((resolve, reject) => {
    RefreshData.getProduto({
      data: tabela,
      successCallBack: function (response) {
        resolve(response.data);
      },
      errorCallBack: function (err) {
        console.log('DEU ERRO TABELAS: ', err);
      } });

  });

  const countCadCli = () => new Promise((resolve, reject) => {
    LocalData.countCadCli({
      success: function (count) {
        resolve(count);
      },
      error: function (err) {
        console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
        reject(err);
      } });

  });

  const getCadCli = () => new Promise((resolve, reject) => {
    LocalData.getClienteDetail({
      id: { A1_MOBILE: '1' },
      success: function (clientes) {
        resolve(clientes);
      },
      error: function (err) {
        reject(err);
        console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
      } });

  });

  const uploadCli = cli => new Promise((resolve, reject) => {
    var id = cli.ID;
    delete cli.ID;
    delete cli.A1_MOBILE;
    delete cli.A1_LAT;
    delete cli.A1_LONG;
    delete cli.SLD;
    delete cli.E4_DESCRI;
    delete cli.E4_DESCRI2;
    cli.A1_MSALDO = 0;
    cli.A1_MCOMPRA = 0;
    cli.A1_METR = 0;
    cli.A1_MATR = 0;
    cli.A1_TITPROT = 0;
    cli.A1_CHQDEVO = 0;
    cli.A1_DTULCHQ = '';
    cli.A1_PRICOM = '';
    RefreshData.cadCliente({
      successCallBack: function (response) {
        LocalData.clearTable({
          table: 'cliente',
          where: `WHERE ID = '${id}'` });

        resolve(response.data);
      },
      errorCallBack: function (response) {
        console.log("ERRO!!: ", response);
        Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cliente. " + response.error);
      },
      data: cli });

  });

  const updateReferencias = obj => new Promise((resolve, reject) => {
    LocalData.updateRef({
      data: obj,
      success: function (response) {
        resolve();
      },
      error: function (err) {
        console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
        reject(err);
      } });

  });

  const getCadRef = id => new Promise((resolve, reject) => {
    LocalData.getCadRef({
      cod_cli: id,
      success: function (response) {
        resolve(response);
      },
      error: function (err) {
        console.log('ERRO AO BUSCAR REFS: ', err);
        reject(err);
      } });

  });

  async function uploadReferencias(refs) {
    let id_refLocal = 0;
    var success = false;
    for (var i in refs) {
      id_refLocal = refs[i].ID;
      delete refs[i].ID;
      await RefreshData.cadReferencia({
        successCallBack: function (response) {
          LocalData.clearTable({
            table: 'cadRef',
            where: `WHERE ID = '${id_refLocal}'` });

        },
        errorCallBack: function (response) {
          console.log("ERRO!!: ", response);
          Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cliente. " + response.error);
          return false;
        },
        data: refs[i] });

    }
  }


  $.mainWindow.addEventListener('open', function () {
    if (download) downloadDados();else
    if (upload) uploadDados();
  });









  _.extend($, exports);
}

module.exports = Controller;