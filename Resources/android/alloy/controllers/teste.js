var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'teste';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainView = Ti.UI.createView(
  { id: "mainView", backgroundColor: "white" });

  $.__views.mainView && $.addTopLevelView($.__views.mainView);
  var __alloyId526 = {};var __alloyId529 = [];var __alloyId531 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId532 = [];var __alloyId534 = { type: 'Ti.UI.ImageView', bindId: 'viewProfile', properties: { bindId: "viewProfile", width: "68dp", height: "68dp", left: "15dp", top: "50dp", defaultImage: "/images/perfil_green.png" } };__alloyId532.push(__alloyId534);var __alloyId536 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbNome", left: "15dp", top: "7dp" } };__alloyId532.push(__alloyId536);var __alloyId538 = { type: 'Ti.UI.Label', bindId: 'lbEmail', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, bindId: "lbEmail", left: "15dp" } };__alloyId532.push(__alloyId538);return __alloyId532;}(), properties: { layout: "vertical" } };__alloyId529.push(__alloyId531);var __alloyId540 = { type: 'Ti.UI.View', properties: { bottom: 0, width: Ti.UI.FILL, height: 1, backgroundColor: "#ddd" } };__alloyId529.push(__alloyId540);var __alloyId528 = { properties: { name: "templateUsuario", layout: "vertical", backgroundImage: "/images/wallpaper.png", height: "180dp", width: Ti.UI.FILL, separatorColor: "none" }, childTemplates: __alloyId529 };__alloyId526["templateUsuario"] = __alloyId528;var __alloyId542 = { properties: { name: "templateSeparator", backgroundColor: "#d0d3d4", height: "1dp", width: Titanium.UI.FILL, touchEnabled: false } };__alloyId526["templateSeparator"] = __alloyId542;var __alloyId545 = [];var __alloyId547 = { type: 'Ti.UI.View', bindId: 'menu', childTemplates: function () {var __alloyId548 = [];var __alloyId550 = { type: 'Ti.UI.ImageView', bindId: 'imgOpcao', properties: { bindId: "imgOpcao", height: "25dp", width: "25dp", left: "0dp" } };__alloyId548.push(__alloyId550);var __alloyId552 = { type: 'Ti.UI.Label', bindId: 'lbOpcao', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbOpcao", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: "12dp" } };__alloyId548.push(__alloyId552);return __alloyId548;}(), properties: { bindId: "menu", layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.FILL, left: "15dp", right: "15dp", top: "12dp", bottom: "12dp" } };__alloyId545.push(__alloyId547);var __alloyId544 = { properties: { name: "templateMenu", backgroundColor: "white", height: Ti.UI.SIZE, top: "6dp", bottom: 0, separatorColor: "none" }, childTemplates: __alloyId545 };__alloyId526["templateMenu"] = __alloyId544;$.__views.listView = Ti.UI.createListView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: "none", backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId526, id: "listView", top: "-2dp" });

  $.__views.mainView.add($.__views.listView);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;

  function createMenu() {
    console.log('CRIOU MENU');
    var sections = [];

    var serctionUsuario = Ti.UI.createListSection();
    var sectionNovos = Ti.UI.createListSection();
    var sectionSeparator = Ti.UI.createListSection();
    var sectionConsultas = Ti.UI.createListSection();

    serctionUsuario.setItems([createOptionUsuario()]);
    sectionNovos.setItems([
    createOption({ opc: 'Novo Pedido', img: '/images/novo_pedido_green.png' }),
    createOption({ opc: 'Novo Cliente', img: '/images/novo_cliente_green.png' }),
    createOption({ opc: 'Nova Reclamação', img: '/images/nova_reclamacao_green.png' })]);

    sectionSeparator.setItems([createSeparator()]);
    sectionConsultas.setItems([
    createOption({ opc: 'Ver Pedidos', img: '/images/pedidos_green.png' }),
    createOption({ opc: 'Ver Clientes', img: '/images/clientes_green.png' }),
    createOption({ opc: 'Ver Reclamações', img: '/images/reclamacoes_green.png' })]);


    sections.push(serctionUsuario);
    sections.push(sectionNovos);
    sections.push(sectionSeparator);
    sections.push(sectionConsultas);


    $.listView.setSections(sections);
  }


































  function createOptionUsuario(opcao) {
    return {
      template: 'templateUsuario',
      lbNome: {
        text: 'Bernardo Silveira Martins' },

      lbEmail: {
        text: 'silveiramartins.bernardo@gmail.com' },

      properties: {
        data: null,
        type: 'perfil' },

      events: {
        "itemClick": function () {
          console.log('CLICOU!');
        },
        itemClick: function () {
          console.log('CLICOU!');
        } } };


  }

  function createOption(obj) {
    return {
      template: 'templateMenu',
      lbOpcao: {
        text: obj.opc },

      imgOpcao: {
        image: obj.img },

      properties: {
        data: obj,
        type: 'menu' } };


  }

  function createSeparator() {
    return {
      template: 'templateSeparator' };

  }

  function openWindowTabGroup(tab) {
    var WindowTabGroup = Alloy.createController(tab).getView();
    if (false)
    WindowTabGroup.open();else

    WindowTabGroup.open({ modal: false });
  }

  createMenu();

  $.listView.addEventListener("itemclick", function (e) {

    var item = e.section.getItemAt(e.itemIndex);
    var data = item ? item.properties.data : null;
    var type = item ? item.properties.type : null;
    console.log('ITEM: ', item);
    console.log('DATA: ', data);
    console.log('TYPE: ', type);

    if (data && data.opc == 'Novo Pedido') {
      console.log('CRIAR PEDIDO');
    } else
    if (data && data.opc == 'Novo Cliente') {
      console.log('CADASTRAR CLIENTE');
      openWindowTabGroup('FormCliente');
    } else
    if (data && data.opc == 'Nova Reclamação') {
      console.log('CADASTRAR RECLAMACAO');
    } else
    if (data && data.opc == 'Ver Pedidos') {
      console.log('VER PEDIDOS');
    } else
    if (data && data.opc == 'Ver Clientes') {
      console.log('VER CLIENTES');
    } else
    if (data && data.opc == 'Ver Reclamações') {
      console.log('VER RECLAMACOES');
    } else
    if (!data && type == 'perfil') {
      console.log('EXIBIR PERFIL');
    }
  });









  _.extend($, exports);
}

module.exports = Controller;