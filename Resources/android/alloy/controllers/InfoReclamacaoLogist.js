var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'InfoReclamacaoLogist';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.InfoReclamacaoLogist = Ti.UI.createView(
	{ id: "InfoReclamacaoLogist" });

	$.__views.InfoReclamacaoLogist && $.addTopLevelView($.__views.InfoReclamacaoLogist);
	exports.destroy = function () {};




	_.extend($, $.__views);



	var args = $.args;









	_.extend($, exports);
}

module.exports = Controller;