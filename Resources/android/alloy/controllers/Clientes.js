var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'Clientes';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Clientes", tabBarHidden: true, modal: true });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId166() {
      $.__views.mainWindow.removeEventListener('open', __alloyId166);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId166);
  }
  $.__views.__alloyId167 = Ti.UI.createView(
  { id: "__alloyId167" });

  $.__views.mainWindow.add($.__views.__alloyId167);
  var __alloyId168 = {};var __alloyId171 = [];var __alloyId173 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId174 = [];var __alloyId176 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.DARK_GRAY_COLOR, font: { fontSize: "14dp" }, bindId: "lbNome", maxLines: 2, left: "0dp" } };__alloyId174.push(__alloyId176);var __alloyId178 = { type: 'Ti.UI.Label', bindId: 'lbMunicipio', properties: { color: Alloy.Globals.DARK_GRAY_COLOR, font: { fontSize: "14dp" }, bindId: "lbMunicipio", maxLines: 1, left: "0dp" } };__alloyId174.push(__alloyId178);var __alloyId180 = { type: 'Ti.UI.Label', bindId: 'lbFone', properties: { color: Alloy.Globals.DARK_GRAY_COLOR, font: { fontSize: "14dp" }, bindId: "lbFone", maxLines: 1, left: "0dp" } };__alloyId174.push(__alloyId180);var __alloyId182 = { type: 'Ti.UI.Label', bindId: 'lbDtCompra', properties: { color: Alloy.Globals.DARK_GRAY_COLOR, font: { fontSize: "14dp" }, bindId: "lbDtCompra", maxLines: 1, left: "0dp" } };__alloyId174.push(__alloyId182);return __alloyId174;}(), properties: { layout: "vertical", width: Ti.UI.FILL, right: "55dp", left: 0, top: "5dp" } };__alloyId171.push(__alloyId173);var __alloyId184 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId185 = [];var __alloyId187 = { type: 'Ti.UI.View', bindId: 'status', childTemplates: function () {var __alloyId188 = [];var __alloyId190 = { type: 'Ti.UI.Label', bindId: 'lbBlock', properties: { color: Alloy.Globals.WHITE_COLOR, font: { fontSize: "20dp", fontWeight: "bold" }, bindId: "lbBlock" } };__alloyId188.push(__alloyId190);return __alloyId188;}(), properties: { height: "25dp", bindId: "status", width: "25dp", borderRadius: 15, backgroundColor: "red", right: 0 } };__alloyId185.push(__alloyId187);return __alloyId185;}(), properties: { right: "5dp" } };__alloyId171.push(__alloyId184);var __alloyId170 = { properties: { name: "templateCliente", backgroundColor: "white", width: Ti.UI.FILL, height: "100dp", left: "15dp", right: "20dp" }, childTemplates: __alloyId171 };__alloyId168["templateCliente"] = __alloyId170;var __alloyId192 = { properties: { name: "templateEmpty", backgroundColor: "white", height: "60dp", width: Titanium.UI.FILL, touchEnabled: false } };__alloyId168["templateEmpty"] = __alloyId192;$.__views.listView = Ti.UI.createListView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId168, id: "listView" });

  $.__views.__alloyId167.add($.__views.listView);
  $.__views.btFilter = Ti.UI.createView(
  { backgroundColor: Alloy.Globals.GRAY_COLOR, id: "btFilter", width: Ti.UI.SIZE, height: "55dp", bottom: "10dp", borderRadius: 18, elevation: 11 });

  $.__views.__alloyId167.add($.__views.btFilter);
  onFilter ? $.addListener($.__views.btFilter, 'click', onFilter) : __defers['$.__views.btFilter!click!onFilter'] = true;$.__views.__alloyId193 = Ti.UI.createLabel(
  { font: { fontSize: "17dp" }, text: 'Todas opções', color: "white", width: Ti.UI.SIZE, left: "18dp", right: "18dp", id: "__alloyId193" });

  $.__views.btFilter.add($.__views.__alloyId193);
  $.__views.popup = Alloy.createWidget('PopUp', 'widget', { id: "popup", __parentSymbol: $.__views.__alloyId167 });
  $.__views.popup.setParent($.__views.__alloyId167);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.__alloyId167 });
  $.__views.activityIndicator.setParent($.__views.__alloyId167);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var pedido = args.pedido || false;
  var callback = args.callback || (() => {});
  var optionsFilter = [];
  var firstVisibleSectionIndex = 0;
  var ClientManager = require('ClientManager');
  var LocalData = require('LocalData');
  var moment = require("alloy/moment");

  if (false) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
      hintText: "Procurar cliente",
      barColor: "#e4e8e9",
      borderColor: "#e4e8e9",
      height: "45dp" }));

  } else if (true) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
      hintText: "Procurar cliente",
      backgroundColor: "#a5a5a5",
      color: "#205D33" }));

  }

  function doClose() {
    $.mainWindow.close();
  }

  function onFilter() {
    $.popup.showLabelWithTag(optionsFilter, function (categoria, index) {
      var itemIndex = true ? firstVisibleSectionIndex < index ? 0 : 0 : 0;
      var sectionIndex = parseInt(index);
      scrollTo(sectionIndex, itemIndex);
      $.popup.hide();
    }, "Toque para filtrar");
  }

  function scrollTo(sectionIndex, itemIndex) {
    var options = false ? { position: Titanium.UI.iOS.ListViewScrollPosition.TOP, animated: false } : { animated: false };
    $.listView.scrollToItem(sectionIndex, itemIndex, options);

  }

  function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
      var menuItemPin = e.menu.add({
        icon: "/images/add.png",
        width: "40dp",
        height: "40dp",
        showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

      menuItemPin.addEventListener("click", function (e) {
        ClientManager.delete();
        var WindowTabGroup = Alloy.createController('FormCliente').getView();
        if (false)
        WindowTabGroup.open();else

        WindowTabGroup.open({ modal: false });
      });
    };
    activity.invalidateOptionsMenu();
  }

  function createListView(array) {
    var sections = [];
    var cadastrados = [];
    var liberados = [];
    var bloqueados = [];
    var aguardando = [];
    var insuficiente = [];
    var titulos = [];
    optionsFilter = [];

    var clientes = _.sortBy(array, function (cli) {return cli.A1_NOME;});
    for (var i in clientes) {
      var status = ClientManager.getStatusCli(clientes[i].A1_MSBLQL, clientes[i].A1_TITVENC, clientes[i].A1_RISCO, clientes[i].A1_OBSERV, clientes[i].A1_TLC, clientes[i].SLD);
      if (clientes[i].A1_MOBILE) {
        cadastrados.push(createItem(clientes[i], status));
      } else
      if (status.status == 1) {
        liberados.push(createItem(clientes[i], status));
      } else
      if (status.status == 2) {
        bloqueados.push(createItem(clientes[i], status));
      } else
      if (status.status == 3) {
        aguardando.push(createItem(clientes[i], status));
      } else
      if (status.status == 4) {
        insuficiente.push(createItem(clientes[i], status));
      } else
      if (status.status == 5) {
        titulos.push(createItem(clientes[i], status));
      }
    }

    if (liberados.length) {
      optionsFilter.push('LIBERADOS');
      var sectionLiberados = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Liberados') });

      sectionLiberados.setItems(liberados);
      sections.push(sectionLiberados);
    }
    if (bloqueados.length) {
      optionsFilter.push('BLOQUEADOS');
      var sectionBloqueados = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Bloqueados') });

      sectionBloqueados.setItems(bloqueados);
      sections.push(sectionBloqueados);
    }
    if (aguardando.length) {
      optionsFilter.push('AGUARD. LIB.');
      var sectionAguardando = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Aguardando liberação') });

      sectionAguardando.setItems(aguardando);
      sections.push(sectionAguardando);
    }
    if (insuficiente.length) {
      optionsFilter.push('CRED. INSUF.');
      var sectionInsuficiente = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Crédito insuficiente') });

      sectionInsuficiente.setItems(insuficiente);
      sections.push(sectionInsuficiente);
    }
    if (titulos.length) {
      optionsFilter.push('TÍTULOS VENC.');
      var sectionVencidos = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Títulos Vencidos') });

      sectionVencidos.setItems(titulos);
      sections.push(sectionVencidos);
    }
    if (cadastrados.length) {
      optionsFilter.push('CADASTRADOS');
      var sectionCad = Ti.UI.createListSection({
        headerView: Alloy.Globals.createSectionHeader('Cadastrados') });

      sectionCad.setItems(cadastrados);
      sections.push(sectionCad);
    }
    if (sections && sections.length) sections[sections.length - 1].appendItems([setEmptyBottom()]);
    $.listView.setSections(sections);
    $.activityIndicator.hide();
  }

  function createItem(cliente, status) {
    var searchableText = cliente.A1_NOME + cliente.A1_COD + cliente.A1_MUN + cliente.A1_CGC + cliente.A1_DDD + cliente.A1_TEL;

    var data = cliente.A1_ULTCOM && cliente.A1_ULTCOM.trim() ? moment(cliente.A1_ULTCOM).format('DD/MM/YYYY') : null;
    return {
      template: 'templateCliente',
      lbNome: {
        attributedString: Alloy.Globals.setNameWithCode(cliente.A1_NOME, cliente.A1_COD) },

      lbMunicipio: {
        text: cliente.A1_MUN },

      lbFone: {
        text: mask.telefone(cliente.A1_DDD.trim() + cliente.A1_TEL.trim()) },

      lbDtCompra: {
        attributedString: Alloy.Globals.setNameWithCode(data, 'Ult. compra') },

      status: {
        backgroundColor: status.color },

      lbBlock: {
        text: status.block },

      properties: {
        data: cliente,
        accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
        selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
        searchableText: searchableText } };


  }

  function setEmptyBottom() {
    return {
      template: "templateEmpty",
      properties: {
        accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
        selectedBackgroundColor: Alloy.Globals.BACKGROUND_COLOR } };


  }

  async function getClientes() {
    var clientes = [];
    await LocalData.getClientes({
      cliente_id: null,
      success: function (array) {
        clientes = array;
      },
      error: function (err) {
        console.log('DEU ERRADO AO CADASTRAR CLIENTE: ', err);
      } });

    await createListView(clientes);
  }

  async function excluirCliente(cli) {
    var codCli = cli.ID;
    $.activityIndicator.show();
    await LocalData.clearTable({ table: 'cliente', where: ` WHERE ID = '${codCli}'` });
    await LocalData.clearTable({ table: 'cadRef', where: ` WHERE ID_CLIENTE = '${codCli}'` });
    $.activityIndicator.hide();
  }

  $.listView.addEventListener('itemclick', function (e) {
    var click = e.section.getItemAt(e.itemIndex);
    var cliente = click.properties.data;
    if (cliente.A1_MOBILE) {
      Alloy.Globals.showAlert({
        title: 'ATENÇÃO',
        message: 'Qual opção deseja executar?',

        buttons: ['Excluir', 'Cancelar', 'Editar'],

        listener: function (e) {
          if (e.index == 0) {

            excluirCliente(cliente);
          } else
          if (e.index == 2) {

            Alloy.Globals.openWindow('FormCliente', { cliente: cliente });
          }
          console.log('Index click: ', e.index);
        } });


    } else
    if (pedido) {
      var status = ClientManager.getStatusCli(cliente.A1_MSBLQL, cliente.A1_TITVENC, cliente.A1_RISCO, cliente.A1_OBSERV, cliente.A1_TLC, cliente.SLD);
      if (status.status == 1) {
        callback(cliente);
        doClose();
      } else
      {
        Alloy.Globals.showAlert({
          title: 'ATENÇÃO',
          message: `Este cliente está bloqueado para pedidos!\nMotivo: ${status.desc}.\nDeseja fazer um orçamento?`,

          buttons: ['Sim', 'Não'],

          listener: function (e) {
            console.log('Index click: ', e.index);
            if (e.index == 0) {

              callback(cliente);
              doClose();
            } else
            {

            }
          } });


      }
    } else
    {
      Alloy.Globals.openWindow('FormCliente', { cliente: cliente });
    }
  });

  $.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
  });

  $.mainWindow.addEventListener("focus", function () {
    $.activityIndicator.show();
    getClientes();
  });





  __defers['$.__views.btFilter!click!onFilter'] && $.addListener($.__views.btFilter, 'click', onFilter);



  _.extend($, exports);
}

module.exports = Controller;