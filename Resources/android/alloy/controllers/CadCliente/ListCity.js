var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
		}
		return arg;
}

function Controller() {

		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'CadCliente/ListCity';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.mainWindow = Ti.UI.createWindow(
		{ navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Cidades", tabBarHidden: true, modal: true });

		$.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
		if (true) {
				function __alloyId55() {
						$.__views.mainWindow.removeEventListener('open', __alloyId55);
						if ($.__views.mainWindow.activity) {
								$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
						} else {
								Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
								Ti.API.warn('UI component which does not have an Android activity. Android Activities');
								Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
						}
				}
				$.__views.mainWindow.addEventListener('open', __alloyId55);
		}
		$.__views.__alloyId56 = Ti.UI.createView(
		{ id: "__alloyId56" });

		$.__views.mainWindow.add($.__views.__alloyId56);
		var __alloyId57 = {};var __alloyId60 = [];var __alloyId62 = { type: 'Ti.UI.Label', bindId: 'lbCidade', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, bindId: "lbCidade", left: "15dp", height: Ti.UI.SIZE, width: Ti.UI.FILL } };__alloyId60.push(__alloyId62);var __alloyId59 = { properties: { name: "templateCidades", backgroundColor: "white", height: "50dp", width: Ti.UI.FILL }, childTemplates: __alloyId60 };__alloyId57["templateCidades"] = __alloyId59;$.__views.listView = Ti.UI.createListView(
		{ width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId57, id: "listView" });

		$.__views.__alloyId56.add($.__views.listView);
		$.__views.popup = Alloy.createWidget('PopUp', 'widget', { id: "popup", __parentSymbol: $.__views.__alloyId56 });
		$.__views.popup.setParent($.__views.__alloyId56);
		$.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.__alloyId56 });
		$.__views.activityIndicator.setParent($.__views.__alloyId56);
		exports.destroy = function () {};




		_.extend($, $.__views);



		var args = $.args;
		var uf_id = args.uf_id;
		var callback = args.callback || function () {};
		var mask = require('Mask');
		var LocalData = require('LocalData');
		var ClientManager = require('ClientManager');

		if (false) {
				$.listView.setSearchView(Titanium.UI.createSearchBar({
						hintText: "Procurar cidade",
						barColor: "#e4e8e9",
						borderColor: "#e4e8e9",
						height: "45dp" }));

		} else if (true) {
				$.listView.setSearchView(Ti.UI.Android.createSearchView({
						hintText: "Procurar cidade",
						backgroundColor: "#a5a5a5",
						color: "#205D33" }));

		}

		var ufs = [];
		function doClose(e) {
				$.mainWindow.close();
		}

		function createListView(array) {
				var section = Ti.UI.createListSection();
				var listCidades = [];
				var cidades = _.sortBy(array, function (uf) {
						return uf.MNC_MUN;
				});
				for (var i in cidades) {
						listCidades.push(createItemList(cidades[i]));
				}
				section.setItems(listCidades);
				$.listView.setSections([section]);
		}

		function createItemList(cidade) {
				var searchableText = cidade.MNC_CODMUN + mask.removeCaracter(cidade.MNC_MUN) + cidade.MNC_CODMC;
				return {
						template: 'templateCidades',
						lbCidade: {
								text: mask.removeCaracter(cidade.MNC_MUN).toUpperCase() },

						properties: {
								data: cidade,
								accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
								selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
								searchableText: searchableText } };


		}

		function selectCidade(cidade) {
				callback(cidade);
				doClose();
		}

		async function getMnc() {
				ufs = await LocalData.getMnc({ coduf: uf_id });
				await createListView(ufs);
		}

		$.mainWindow.addEventListener("open", function (e) {
				getMnc();
		});

		$.listView.addEventListener('itemclick', function (e) {
				var item = e.section.getItemAt(e.itemIndex);
				var data = item.properties.data;
				selectCidade(data);
		});









		_.extend($, exports);
}

module.exports = Controller;