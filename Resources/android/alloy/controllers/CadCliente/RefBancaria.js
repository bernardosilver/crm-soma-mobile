var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'CadCliente/RefBancaria';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Ref. Bancárias" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId70() {
      $.__views.mainWindow.removeEventListener('open', __alloyId70);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Ref. Bancárias";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId70);
  }
  $.__views.__alloyId71 = Ti.UI.createScrollView(
  { top: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "__alloyId71" });

  $.__views.mainWindow.add($.__views.__alloyId71);
  $.__views.__alloyId72 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId72" });

  $.__views.__alloyId71.add($.__views.__alloyId72);
  $.__views.tfEmpresa = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfEmpresa", maxLength: 30, hintText: "Nome do banco", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId72.add($.__views.tfEmpresa);
  $.__views.__alloyId73 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId73" });

  $.__views.__alloyId72.add($.__views.__alloyId73);
  $.__views.__alloyId74 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId74" });

  $.__views.__alloyId72.add($.__views.__alloyId74);
  $.__views.__alloyId75 = Ti.UI.createView(
  { width: Ti.UI.FILL, top: "10dp", height: Ti.UI.SIZE, id: "__alloyId75" });

  $.__views.__alloyId71.add($.__views.__alloyId75);
  $.__views.tfTelefone = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfTelefone", hintText: "Telefone - somente números", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

  $.__views.__alloyId75.add($.__views.tfTelefone);
  $.__views.__alloyId76 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', top: "25dp", id: "__alloyId76" });

  $.__views.__alloyId75.add($.__views.__alloyId76);
  $.__views.__alloyId77 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", top: "45dp", id: "__alloyId77" });

  $.__views.__alloyId75.add($.__views.__alloyId77);
  $.__views.__alloyId78 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId78" });

  $.__views.__alloyId71.add($.__views.__alloyId78);
  $.__views.tfCidadeUF = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfCidadeUF", maxLength: 40, hintText: "Cidade/UF", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId78.add($.__views.tfCidadeUF);
  $.__views.__alloyId79 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId79" });

  $.__views.__alloyId78.add($.__views.__alloyId79);
  $.__views.__alloyId80 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId80" });

  $.__views.__alloyId78.add($.__views.__alloyId80);
  $.__views.__alloyId81 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId81" });

  $.__views.__alloyId71.add($.__views.__alloyId81);
  $.__views.tfContato = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfContato", maxLength: 30, hintText: "Contato", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId81.add($.__views.tfContato);
  $.__views.__alloyId82 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId82" });

  $.__views.__alloyId81.add($.__views.__alloyId82);
  $.__views.__alloyId83 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId83" });

  $.__views.__alloyId81.add($.__views.__alloyId83);
  $.__views.lbId = Ti.UI.createLabel(
  { id: "lbId", visible: false });

  $.__views.__alloyId71.add($.__views.lbId);
  $.__views.viewAddReferencia = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "viewAddReferencia", top: "10dp" });

  $.__views.__alloyId71.add($.__views.viewAddReferencia);
  $.__views.viewReferencia = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "40dp", backgroundSelectedColor: Alloy.Globals.BACKGROUND_COLOR, id: "viewReferencia", top: "15dp", bottom: "40dp" });

  $.__views.__alloyId71.add($.__views.viewReferencia);
  $.__views.__alloyId84 = Ti.UI.createImageView(
  { font: { fontSize: "15dp" }, width: "40dp", height: "40dp", left: "5dp", image: "/images/addGreen.png", touchEnabled: false, id: "__alloyId84" });

  $.__views.viewReferencia.add($.__views.__alloyId84);
  $.__views.__alloyId85 = Ti.UI.createLabel(
  { color: Alloy.Globals.BLUE_COLOR, left: "50dp", touchEnabled: false, text: 'Adicionar outra referência', id: "__alloyId85" });

  $.__views.viewReferencia.add($.__views.__alloyId85);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var refs = args.refs;
  var callback = args.callback || function () {};
  var mask = require('Mask');
  var referencias = [];
  var formularios = [];
  var ClientManager = require('ClientManager');


  function doClose(e) {
    $.mainWindow.close();
  }

  function doSave(e) {
    $.activityIndicator.show();
    var message = validate();
    if (message) {
      Alloy.Globals.showAlert("Ops!", "É necessário informar o(a) " + message.name + "!");
      if (message.field) message.field.focus();
      $.activityIndicator.hide();
      return false;
    }
    getValues();
    ClientManager.setReferenciasBanc(referencias);
    callback(referencias);
    doClose();
  }

  function validate() {
    var nome = mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase();
    var telefone = mask.removeTelMask($.tfTelefone.getValue());
    var cidadeUF = mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase();
    var contato = mask.removeCaracter($.tfContato.getValue()).toUpperCase();
    if (!!!nome.match(/[aA-zZ]*/) || nome.length < 4) {
      return { field: $.tfEmpresa, name: 'nome da empresa' };
    }
    if (telefone.trim() == '' || telefone.length < 10) {
      return { field: $.tfTelefone, name: 'Telefone' };
    }
    if (cidadeUF.trim() == '' || cidadeUF.length < 5) {
      return { field: $.tfCidadeUF, name: 'Cidade e UF' };
    }
    if (contato.trim() == '' || contato.length < 4) {
      return { field: $.tfContato, name: 'Contato' };
    }
    if (formularios.length >= 1) {
      var count = 0;
      var obj = {};
      var nome = '';
      var telefone = '';
      var cidadeUF = '';
      var contato = '';
      for (var i = 0; i < formularios.length; i++) {
        var count = parseInt(i);
        var obj = formularios[count].getValues();
        var nome = mask.removeCaracter(obj.nome).toUpperCase();
        var telefone = mask.removeTelMask(obj.telefone);
        var cidadeUF = mask.removeCaracter(obj.cidadeUF).toUpperCase();
        var contato = mask.removeCaracter(obj.contato).toUpperCase();
        if (!!!nome.match(/[aA-zZ]*/) || nome.length < 4) {
          return { name: `nome da empresa da ${count + 2}ª referência` };
        }
        if (telefone.trim() == '' || telefone.length < 10) {
          return { name: `Telefone da ${count + 2}ª referência` };
        }
        if (cidadeUF.trim() == '' || cidadeUF.length < 5) {
          return { name: `Cidade e UF da ${count + 2}ª referência` };
        }
        if (contato.trim() == '' || contato.length < 4) {
          return { name: `Contato da ${count + 2}ª referência` };
        }
      }
    }
  }

  function getValues() {
    referencias = [];
    referencias.push({
      AO_NOMINS: mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase(),
      AO_TELEFON: mask.removeTelMask($.tfTelefone.getValue()),
      AO_CONTATO: mask.removeCaracter($.tfContato.getValue()).toUpperCase(),
      AO_OBSERV: mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase(),
      ID: $.lbId.getText() });

    for (var i = 0; i < formularios.length; i++) {
      var count = parseInt(i);
      var obj = formularios[count].getValues();
      referencias.push({
        AO_NOMINS: mask.removeCaracter(obj.nome).toUpperCase(),
        AO_TELEFON: mask.removeTelMask(obj.telefone),
        AO_CONTATO: mask.removeCaracter(obj.contato).toUpperCase(),
        AO_OBSERV: mask.removeCaracter(obj.cidadeUF).toUpperCase(),
        ID: obj.ID });

    }
  }

  function setValues(obj) {
    referencias = obj ? obj : ClientManager.getReferenciasBanc();
    if (referencias.length) {
      $.tfEmpresa.setValue(referencias[0].AO_NOMINS);
      $.tfTelefone.setValue(mask.tel(referencias[0].AO_TELEFON));
      $.tfContato.setValue(referencias[0].AO_CONTATO);
      $.tfCidadeUF.setValue(referencias[0].AO_OBSERV);
      $.lbId.setText(referencias[0].ID);
    }
    if (referencias.length > 1) {
      for (var i = 1; i < referencias.length; i++) {
        var count = parseInt(i);
        clickViewRefBancaria(referencias[count]);
      }
    }
  }

  function createMenuAndroid() {
    if (true) {
      var activity = $.mainWindow.activity;
      activity.onCreateOptionsMenu = function (e) {
        var btDone = e.menu.add({
          title: "Salvar",
          showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

        btDone.addEventListener("click", doSave);
      };
      activity.invalidateOptionsMenu();
    }
  }

  function clickViewRefBancaria(obj) {
    if (formularios.length >= 1) {
      Alloy.Globals.showAlert("Ops", "É possível adicionar apenas 2 referencias!");
      return false;
    }
    var referencia = Alloy.createWidget('CadRefComercial', 'widget', { activityIndicator: $.activityIndicator, ref: obj, editable: refs ? false : true });
    $.viewAddReferencia.add(referencia.getView());

    formularios.push(referencia);

    referencia.setFocus();

    referencia.setActionApagar(function () {
      $.viewAddReferencia.remove(referencia.getView());
      for (var i in formularios) {
        if (formularios[i] == referencia) {
          formularios.splice(i, 1);
          break;
        }
      }
    });
  }

  function setEditable(bool) {
    $.tfEmpresa.setEditable(bool);
    $.tfTelefone.setEditable(bool);
    $.tfContato.setEditable(bool);
    $.tfCidadeUF.setEditable(bool);
  }

  $.mainWindow.addEventListener("open", function (e) {



    if (!refs) {
      setValues();
      setEditable(true);
      createMenuAndroid();
    } else
    {
      setValues(refs);
      setEditable(false);
    }
  });

  $.viewReferencia.addEventListener("click", function () {
    if (refs) return true;
    clickViewRefBancaria();
  });

  $.tfTelefone.addEventListener("change", function (e) {
    var v = mask.tel(e.value);
    if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
    $.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
  });









  _.extend($, exports);
}

module.exports = Controller;