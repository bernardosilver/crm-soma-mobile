var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'CadCliente/Credito';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Limite de crédito" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId23() {
      $.__views.mainWindow.removeEventListener('open', __alloyId23);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Limite de crédito";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId23);
  }
  $.__views.viewMain = Ti.UI.createView(
  { id: "viewMain", width: Ti.UI.FILL, height: Ti.UI.FILL });

  $.__views.mainWindow.add($.__views.viewMain);
  $.__views.__alloyId24 = Ti.UI.createLabel(
  { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "18dp", fontWeight: "bold" }, top: "10dp", width: Ti.UI.SIZE, textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER, text: "Qual limite deseja?", id: "__alloyId24" });

  $.__views.viewMain.add($.__views.__alloyId24);
  $.__views.credito = Ti.UI.createTextField(
  { font: { fontSize: "19dp", fontWeight: "semibold" }, id: "credito", top: "50dp", hintTextColor: "gray", maxLength: 10, backgroundColor: "#EBEBEB", color: "black", height: "45dp", width: "90%", left: "5%", right: "5%", suppressReturn: false, textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER, keyboardType: Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD });

  $.__views.viewMain.add($.__views.credito);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  exports.destroy = function () {};




  _.extend($, $.__views);


  var args = arguments[0] || {};
  var callback = args.callback || function () {};
  var cred = args.cred || null;
  var mask = require("Mask");
  var credito = '';
  var ClientManager = require('ClientManager');

  $.credito.value = credito ? 'R$ ' + mask.real(credito) : "R$ 0,00";

  if (false) {
    var btDone = Titanium.UI.createButton({
      title: "Salvar",
      color: "white",
      font: {
        fontSize: "13",
        fontWeight: "bold" } });


    $.credito.addEventListener('focus', function (e) {
      $.mainWindow.setRightNavButton(btDone);
    });
    $.credito.addEventListener('blur', function (e) {
      $.mainWindow.setRightNavButton(null);
    });
    btDone.addEventListener('click', function (e) {
      $.credito.blur();
      doSave();
    });
  }

  function doClose(e) {
    $.mainWindow.close();
  }

  function doSave(e) {
    $.activityIndicator.show();
    getValues();
    console.log('PEGOU VALOR: ', credito);
    ClientManager.setLimiteCredito(credito);
    callback(credito);
    doClose();
  }

  function getValues() {
    credito = $.credito.getValue();
    credito = credito.replace(".", "").replace(",", ".");
    if (credito.indexOf("R$") >= 0) credito = credito.slice(3);
  }

  function setValues(value) {
    credito = value ? value : ClientManager.getLimiteCredito();
    console.log('SET VALUE: ', credito);
    if (credito)
    $.credito.setValue(mask.real(credito));
  }

  function createMenuAndroid() {
    if (true) {
      var activity = $.mainWindow.activity;
      activity.onCreateOptionsMenu = function (e) {
        var btDone = e.menu.add({
          title: "Salvar",
          showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

        btDone.addEventListener("click", doSave);
      };
      activity.invalidateOptionsMenu();
    }
  }

  function setEditable(bool) {
    $.credito.setEditable(bool);
  }

  $.credito.addEventListener("change", function (e) {
    $.credito.setValue(mask.real(e.source.value));
  });

  $.credito.addEventListener("focus", function () {
    if (cred) return true;
    $.credito.setValue("");
  });

  $.mainWindow.addEventListener("open", function (e) {



    if (!cred) {
      setValues();
      setEditable(true);
      createMenuAndroid();
    } else
    {
      setValues(cred);
      setEditable(false);
    }
  });









  _.extend($, exports);
}

module.exports = Controller;