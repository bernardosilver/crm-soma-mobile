var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
		}
		return arg;
}

function Controller() {

		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'CadCliente/ListUF';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.mainWindow = Ti.UI.createWindow(
		{ navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Estados" });

		$.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
		if (true) {
				function __alloyId63() {
						$.__views.mainWindow.removeEventListener('open', __alloyId63);
						if ($.__views.mainWindow.activity) {
								$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
						} else {
								Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
								Ti.API.warn('UI component which does not have an Android activity. Android Activities');
								Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
						}
				}
				$.__views.mainWindow.addEventListener('open', __alloyId63);
		}
		var __alloyId64 = {};var __alloyId67 = [];var __alloyId69 = { type: 'Ti.UI.Label', bindId: 'lbUF', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "16dp", fontWeight: "bold" }, touchEnabled: false, bindId: "lbUF", left: "15dp", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp", bottom: "15dp" } };__alloyId67.push(__alloyId69);var __alloyId66 = { properties: { name: "templateEstado", backgroundColor: "white", height: Ti.UI.SIZE, width: Ti.UI.FILL, accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE }, childTemplates: __alloyId67 };__alloyId64["templateEstado"] = __alloyId66;$.__views.listView = Ti.UI.createListView(
		{ width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId64, id: "listView", top: "-2dp", bottom: "5dp" });

		$.__views.mainWindow.add($.__views.listView);
		$.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
		$.__views.activityIndicator.setParent($.__views.mainWindow);
		exports.destroy = function () {};




		_.extend($, $.__views);



		var args = $.args;
		var callback = args.callback || function () {};
		var LocalData = require('LocalData');
		var ClientManager = require('ClientManager');

		if (false) {
				$.listView.setSearchView(Titanium.UI.createSearchBar({
						hintText: "Procurar estado",
						barColor: "#e4e8e9",
						borderColor: "#e4e8e9",
						height: "45dp" }));

		} else if (true) {
				$.listView.setSearchView(Ti.UI.Android.createSearchView({
						hintText: "Procurar estado",
						backgroundColor: "#a5a5a5",
						color: "#205D33" }));

		}

		var ufs = [];
		function doClose(e) {
				$.mainWindow.close();
		}

		function createListView(array) {
				var section = Ti.UI.createListSection();
				var listEstados = [];
				var estados = _.sortBy(array, function (uf) {
						return uf.UF_NOME;
				});
				for (var i in estados) {
						listEstados.push(createItemList(estados[i]));
				}
				section.setItems(listEstados);
				$.listView.setSections([section]);
		}

		function createItemList(estado) {
				var searchableText = estado.UF_COD + estado.UF_NOME + estado.UF_SIGLA;
				return {
						template: 'templateEstado',
						lbUF: {
								text: estado.UF_SIGLA + ' - ' + estado.UF_NOME },

						properties: {
								data: estado,
								accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
								selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
								searchableText: searchableText } };


		}

		function selectUF(estado) {
				callback(estado);
				doClose();
		}

		async function getUF() {
				ufs = await LocalData.getUF();
				await createListView(ufs);
		}

		$.mainWindow.addEventListener("open", function (e) {
				getUF();
		});

		$.listView.addEventListener('itemclick', function (e) {
				var item = e.section.getItemAt(e.itemIndex);
				var data = item.properties.data;
				selectUF(data);
		});









		_.extend($, exports);
}

module.exports = Controller;