var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'CadCliente/DadosPessoais';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Dados Pessoais" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId25() {
      $.__views.mainWindow.removeEventListener('open', __alloyId25);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Dados Pessoais";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId25);
  }
  $.__views.__alloyId26 = Ti.UI.createScrollView(
  { top: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "__alloyId26" });

  $.__views.mainWindow.add($.__views.__alloyId26);
  $.__views.__alloyId27 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId27" });

  $.__views.__alloyId26.add($.__views.__alloyId27);
  $.__views.tfNome = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfNome", maxLength: 40, hintText: "Nome completo ou Razão Social", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId27.add($.__views.tfNome);
  $.__views.__alloyId28 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId28" });

  $.__views.__alloyId27.add($.__views.__alloyId28);
  $.__views.__alloyId29 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId29" });

  $.__views.__alloyId27.add($.__views.__alloyId29);
  $.__views.__alloyId30 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId30" });

  $.__views.__alloyId26.add($.__views.__alloyId30);
  $.__views.tfFantasia = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfFantasia", maxLength: 20, hintText: "Nome fantasia, apelido ou nome reduzido", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId30.add($.__views.tfFantasia);
  $.__views.__alloyId31 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId31" });

  $.__views.__alloyId30.add($.__views.__alloyId31);
  $.__views.__alloyId32 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId32" });

  $.__views.__alloyId30.add($.__views.__alloyId32);
  $.__views.__alloyId33 = Ti.UI.createView(
  { width: Ti.UI.FILL, top: "10dp", height: Ti.UI.SIZE, id: "__alloyId33" });

  $.__views.__alloyId26.add($.__views.__alloyId33);
  $.__views.tfTelefone = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfTelefone", top: 0, hintText: "Telefone principal (fixo ou celular)", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

  $.__views.__alloyId33.add($.__views.tfTelefone);
  $.__views.__alloyId34 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', top: "25dp", id: "__alloyId34" });

  $.__views.__alloyId33.add($.__views.__alloyId34);
  $.__views.__alloyId35 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", top: "45dp", id: "__alloyId35" });

  $.__views.__alloyId33.add($.__views.__alloyId35);
  $.__views.viewAddTelefone = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "viewAddTelefone", top: "5dp" });

  $.__views.__alloyId26.add($.__views.viewAddTelefone);
  $.__views.viewTelefone = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "40dp", backgroundSelectedColor: Alloy.Globals.BACKGROUND_COLOR, id: "viewTelefone", top: "5dp" });

  $.__views.__alloyId26.add($.__views.viewTelefone);
  $.__views.__alloyId36 = Ti.UI.createImageView(
  { font: { fontSize: "15dp" }, width: "40dp", height: "40dp", left: "5dp", image: "/images/addGreen.png", touchEnabled: false, id: "__alloyId36" });

  $.__views.viewTelefone.add($.__views.__alloyId36);
  $.__views.__alloyId37 = Ti.UI.createLabel(
  { color: Alloy.Globals.BLUE_COLOR, left: "50dp", touchEnabled: false, text: 'Adicionar outro telefone', id: "__alloyId37" });

  $.__views.viewTelefone.add($.__views.__alloyId37);
  $.__views.__alloyId38 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", id: "__alloyId38" });

  $.__views.__alloyId26.add($.__views.__alloyId38);
  $.__views.__alloyId39 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId39" });

  $.__views.__alloyId26.add($.__views.__alloyId39);
  $.__views.tfEmail = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfEmail", maxLength: 40, hintText: "e-mail", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_EMAIL });

  $.__views.__alloyId39.add($.__views.tfEmail);
  $.__views.__alloyId40 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId40" });

  $.__views.__alloyId39.add($.__views.__alloyId40);
  $.__views.__alloyId41 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId41" });

  $.__views.__alloyId39.add($.__views.__alloyId41);
  $.__views.viewFisica = Ti.UI.createView(
  { id: "viewFisica", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp" });

  $.__views.__alloyId26.add($.__views.viewFisica);
  $.__views.checkOnFisica = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOnFisica", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "15dp", visible: true });

  $.__views.viewFisica.add($.__views.checkOnFisica);
  $.__views.checkOffFisica = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOffFisica", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "15dp", visible: false });

  $.__views.viewFisica.add($.__views.checkOffFisica);
  $.__views.labelFisica = Ti.UI.createLabel(
  { font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Pessoa Física', touchEnabled: false, id: "labelFisica", left: "55dp", height: Ti.UI.SIZE, color: "gray" });

  $.__views.viewFisica.add($.__views.labelFisica);
  $.__views.viewJuridica = Ti.UI.createView(
  { id: "viewJuridica", height: Ti.UI.SIZE, width: Ti.UI.FILL });

  $.__views.__alloyId26.add($.__views.viewJuridica);
  $.__views.checkOnJuridica = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOnJuridica", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "15dp", visible: false });

  $.__views.viewJuridica.add($.__views.checkOnJuridica);
  $.__views.checkOffJuridica = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOffJuridica", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "15dp", visible: true });

  $.__views.viewJuridica.add($.__views.checkOffJuridica);
  $.__views.labelJuridica = Ti.UI.createLabel(
  { font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Pessoa Jurídica', touchEnabled: false, id: "labelJuridica", left: "55dp", height: Ti.UI.SIZE, color: "gray" });

  $.__views.viewJuridica.add($.__views.labelJuridica);
  $.__views.__alloyId42 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "0dp", id: "__alloyId42" });

  $.__views.__alloyId26.add($.__views.__alloyId42);
  $.__views.tfCpfCnpj = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfCpfCnpj", hintText: "CPF - somente numeros", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

  $.__views.__alloyId42.add($.__views.tfCpfCnpj);
  $.__views.__alloyId43 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId43" });

  $.__views.__alloyId42.add($.__views.__alloyId43);
  $.__views.__alloyId44 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId44" });

  $.__views.__alloyId42.add($.__views.__alloyId44);
  $.__views.__alloyId45 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId45" });

  $.__views.__alloyId26.add($.__views.__alloyId45);
  $.__views.tfIpr = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfIpr", maxLength: 18, hintText: "IPR/Insc. Est.", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId45.add($.__views.tfIpr);
  $.__views.__alloyId46 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', id: "__alloyId46" });

  $.__views.__alloyId45.add($.__views.__alloyId46);
  $.__views.__alloyId47 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId47" });

  $.__views.__alloyId45.add($.__views.__alloyId47);
  $.__views.__alloyId48 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId48" });

  $.__views.__alloyId26.add($.__views.__alloyId48);
  $.__views.tfRG = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfRG", maxLength: 18, hintText: "RG/Identidade", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS });

  $.__views.__alloyId48.add($.__views.tfRG);
  $.__views.__alloyId49 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId49" });

  $.__views.__alloyId48.add($.__views.__alloyId49);
  $.__views.__alloyId50 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "45dp", top: "10dp", id: "__alloyId50" });

  $.__views.__alloyId26.add($.__views.__alloyId50);
  $.__views.tfNasc = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfNasc", hintText: "Dt. Nasc", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

  $.__views.__alloyId50.add($.__views.tfNasc);
  $.__views.__alloyId51 = Ti.UI.createLabel(
  { font: { fontSize: "11dp", fontWeight: "bold" }, color: Alloy.Globals.RED_COLOR, right: "10dp", bottom: "5dp", height: Ti.UI.SIZE, width: Ti.UI.SIZE, touchEnabled: false, text: 'Obrigatório', top: "25dp", id: "__alloyId51" });

  $.__views.__alloyId50.add($.__views.__alloyId51);
  $.__views.__alloyId52 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", bottom: 0, id: "__alloyId52" });

  $.__views.__alloyId50.add($.__views.__alloyId52);
  $.__views.labelME = Ti.UI.createLabel(
  { font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Micro Empresa (M.E.):', touchEnabled: false, id: "labelME", left: "20dp", top: "15dp", height: Ti.UI.SIZE, color: "gray" });

  $.__views.__alloyId26.add($.__views.labelME);
  $.__views.viewME = Ti.UI.createView(
  { id: "viewME", height: Ti.UI.SIZE, width: Ti.UI.FILL });

  $.__views.__alloyId26.add($.__views.viewME);
  $.__views.simME = Ti.UI.createView(
  { id: "simME", height: Ti.UI.SIZE, left: "0dp", width: "100dp" });

  $.__views.viewME.add($.__views.simME);
  $.__views.checkOnSimME = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOnSimME", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "20dp", visible: true });

  $.__views.simME.add($.__views.checkOnSimME);
  $.__views.checkOffSimME = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOffSimME", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "20dp", visible: false });

  $.__views.simME.add($.__views.checkOffSimME);
  $.__views.labelSimME = Ti.UI.createLabel(
  { font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Sim', touchEnabled: false, id: "labelSimME", left: "60dp", height: Ti.UI.SIZE, color: "gray" });

  $.__views.simME.add($.__views.labelSimME);
  $.__views.naoME = Ti.UI.createView(
  { id: "naoME", height: Ti.UI.SIZE, left: "115dp", width: "100dp" });

  $.__views.viewME.add($.__views.naoME);
  $.__views.checkOnNaoME = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOnNaoME", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "0dp", visible: false });

  $.__views.naoME.add($.__views.checkOnNaoME);
  $.__views.checkOffNaoME = Ti.UI.createImageView(
  { touchEnabled: false, id: "checkOffNaoME", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "0dp", visible: true });

  $.__views.naoME.add($.__views.checkOffNaoME);
  $.__views.labelNaoME = Ti.UI.createLabel(
  { font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Não', touchEnabled: false, id: "labelNaoME", left: "40dp", height: Ti.UI.SIZE, color: "gray" });

  $.__views.naoME.add($.__views.labelNaoME);
  $.__views.__alloyId53 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", id: "__alloyId53" });

  $.__views.__alloyId26.add($.__views.__alloyId53);
  $.__views.__alloyId54 = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "200dp", top: "10dp", id: "__alloyId54" });

  $.__views.__alloyId26.add($.__views.__alloyId54);
  $.__views.tfObs = Ti.UI.createTextArea(
  { id: "tfObs", height: "180dp", width: "90%", color: "#205D33", hintText: "Observações", borderWidth: 1, borderRadius: 4, borderColor: "#205D33", hintTextColor: "#B2B2B2", keyboardType: Titanium.UI.KEYBOARD_TYPE_ASCII });

  $.__views.__alloyId54.add($.__views.tfObs);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var dados = args.dados || null;
  var cliente = null;
  var callback = args.callback || function () {};
  var mask = require('Mask');
  var moment = require("alloy/moment");
  var telefones = [];
  var celulares = [];
  var fields = [];
  var tipo_pessoa = 'F';
  var me = '1';

  var ClientManager = require('ClientManager');

  fields.push({ field: $.tfNome, name: 'Nome completo' });
  fields.push({ field: $.tfFantasia, name: 'Nome fantasia' });
  fields.push({ field: $.tfTelefone, name: 'Telefone' });
  fields.push({ field: $.tfEmail, name: 'E-mail' });
  fields.push({ field: $.tfCpfCnpj, name: 'CPF ou CNPJ' });
  fields.push({ field: $.tfIpr, name: 'Inscrição Estadual' });
  fields.push({ field: $.tfNasc, name: 'Data de nascimento' });


  function doClose(e) {
    $.mainWindow.close();
  }

  function doSave(e) {
    $.activityIndicator.show();
    if (ClientManager.validateCGC(mask.removeCPFCNPJMask($.tfCpfCnpj.getValue()), 'dado')) {
      Alloy.Globals.showAlert("Ops!", "Você não pode cadastrar o mesmo CPF para o cliente e o sócio!");
      $.activityIndicator.hide();
      return false;
    }
    var message = validate();
    if (message) {
      Alloy.Globals.showAlert("Ops!", "É necessário informar o(a) " + message.name + "!");
      if (message.field) message.field.focus();
      $.activityIndicator.hide();
      return false;
    }
    getValues();
    ClientManager.setDadosPessoais(cliente);
    callback(cliente);
    doClose();
  }

  function validate() {
    var nome = mask.removeCaracter($.tfNome.getValue());
    var razao = mask.removeCaracter($.tfFantasia.getValue());
    var telefone = mask.removeTelMask($.tfTelefone.getValue());
    if (!!!nome.match(/[A-Z][a-z]* [A-Z][a-z]*/) || nome.length < 4) {
      return { field: $.tfNome, name: 'Nome' };
    }
    if (!!!razao.match(/[A-Z][a-z]*/) || razao.length < 4) {
      return { field: $.tfFantasia, name: 'Nome fantasia' };
    }
    if (telefone.trim() == '' || telefone.length < 10) {
      return { field: $.tfTelefone, name: 'Telefone' };
    }
    if (telefones[0] && mask.removeTelMask(telefones[0].getValues().telefone).length > 0 && mask.removeTelMask(telefones[0].getValues().telefone).length < 10) {
      return { name: 'segundo telefone' };
    }
    if (telefones[1] && mask.removeTelMask(telefones[1].getValues().telefone).length > 0 && mask.removeTelMask(telefones[1].getValues().telefone).length < 10) {
      return { name: 'terceiro telefone' };
    }
    if (telefones[2] && mask.removeTelMask(telefones[2].getValues().telefone).length > 0 && mask.removeTelMask(telefones[2].getValues().telefone).length < 10) {
      return { name: 'quarto telefone' };
    }
    if (!validaEmail($.tfEmail.getValue())) {
      return { field: $.tfEmail, name: 'Email' };
    }
    if (tipo_pessoa == 'F' && !validaCPF($.tfCpfCnpj.getValue())) {
      return { field: $.tfCpfCnpj, name: 'CPF' };
    }
    if (tipo_pessoa == 'J' && !validaCNPJ($.tfCpfCnpj.getValue())) {
      return { field: $.tfCpfCnpj, name: 'CNPJ' };
    }
    if (mask.removeCaracter($.tfIpr.getValue()).trim() == '') {
      return { field: $.tfIpr, name: 'Inscrição Estadual' };
    }
    if (!moment($.tfNasc.getValue(), 'DDMMYYYY').utc().isValid()) {
      return { field: $.tfNasc, name: 'Data de nascimento' };
    }

    return null;
  }

  function getValues() {
    cliente = {};
    cliente.A1_NOME = mask.removeCaracter($.tfNome.getValue()).toUpperCase();
    cliente.A1_NREDUZ = mask.removeCaracter($.tfFantasia.getValue()).toUpperCase();
    cliente.A1_DDD = $.tfTelefone.getValue().slice(1, 3);
    cliente.A1_TEL = $.tfTelefone.getValue().slice(4).replace('-', '');
    cliente.A1_TEL2 = telefones.length >= 1 ? mask.removeTelMask(telefones[0].getValues().telefone) : null;
    cliente.A1_TEL3 = telefones.length >= 2 ? mask.removeTelMask(telefones[1].getValues().telefone) : null;
    cliente.A1_TEL4 = telefones.length >= 3 ? mask.removeTelMask(telefones[2].getValues().telefone) : null;
    cliente.A1_EMAIL = $.tfEmail.getValue().toLowerCase();
    cliente.A1_PESSOA = tipo_pessoa;
    cliente.A1_CGC = mask.removeCPFCNPJMask($.tfCpfCnpj.getValue());
    cliente.A1_INSCR = $.tfIpr.getValue().toUpperCase();
    cliente.A1_PFISICA = $.tfRG.getValue().toUpperCase();
    cliente.A1_DTNASC = moment($.tfNasc.getValue(), 'DD/MM/YYYY').format('YYYYMMDD');
    cliente.A1_ME = me;
    cliente.A1_OBSRC = mask.removeAcentos($.tfObs.getValue().toUpperCase());
  }

  function setValues(obj) {
    cliente = {};
    cliente = obj ? obj : ClientManager.getDadosPessoais();
    me = cliente.A1_ME;
    tipo_pessoa = cliente.A1_PESSOA;

    checkME(me);
    checkPessoa(tipo_pessoa);

    $.tfNome.setValue(cliente.A1_NOME);
    $.tfFantasia.setValue(cliente.A1_NREDUZ);
    $.tfEmail.setValue(cliente.A1_EMAIL);
    $.tfIpr.setValue(cliente.A1_INSCR);
    $.tfRG.setValue(cliente.A1_PFISICA);
    cliente.A1_DTNASC ? $.tfNasc.setValue(moment(cliente.A1_DTNASC, 'YYYYMMDD').format('DD/MM/YYYY')) : '';
    $.tfObs.setValue(cliente.A1_OBSRC);

    if (cliente.A1_DDD && cliente.A1_TEL) {
      $.tfTelefone.setValue(mask.telefone(cliente.A1_DDD + cliente.A1_TEL));
    }
    if (tipo_pessoa == 'F') {
      $.tfCpfCnpj.setValue(mask.cpf(cliente.A1_CGC));
    } else
    if (tipo_pessoa == 'J') {
      $.tfCpfCnpj.setValue(mask.cnpj(cliente.A1_CGC));
    }
    if (cliente.A1_TEL2 && cliente.A1_TEL2.trim()) clickViewTelefone(cliente.A1_TEL2);
    if (cliente.A1_TEL3 && cliente.A1_TEL3.trim()) clickViewTelefone(cliente.A1_TEL3);
    if (cliente.A1_TEL4 && cliente.A1_TEL4.trim()) clickViewTelefone(cliente.A1_TEL4);
  }

  function createMenuAndroid() {
    if (true) {
      var activity = $.mainWindow.activity;
      activity.onCreateOptionsMenu = function (e) {
        var btDone = e.menu.add({
          title: "Salvar",
          showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

        btDone.addEventListener("click", doSave);
      };
      activity.invalidateOptionsMenu();
    }
  }

  function clickViewTelefone(tel) {
    if (telefones.length >= 3) {
      Alloy.Globals.showAlert("Ops", "É possível adicionar apenas 4 telefones!");
      return false;
    }
    var telefone = Alloy.createWidget('CadTelefone', 'widget', { activityIndicator: $.activityIndicator, tipo: 'cel', tel: tel, editable: dados ? false : true });
    $.viewAddTelefone.add(telefone.getView());

    telefones.push(telefone);

    telefone.setFocus();

    telefone.setActionApagar(function () {
      $.viewAddTelefone.remove(telefone.getView());
      for (var i in telefones) {
        if (telefones[i] == telefone) {
          telefones.splice(i, 1);
          break;
        }
      }
    });
  }
























  function validaEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  function validaCPF(value) {
    var copy = value.replace(".", "").replace(".", "").replace("-", "");
    var num1 = parseInt(copy[0]);
    var num2 = parseInt(copy[1]);
    var num3 = parseInt(copy[2]);
    var num4 = parseInt(copy[3]);
    var num5 = parseInt(copy[4]);
    var num6 = parseInt(copy[5]);
    var num7 = parseInt(copy[6]);
    var num8 = parseInt(copy[7]);
    var num9 = parseInt(copy[8]);
    var num10 = parseInt(copy[9]);
    var num11 = parseInt(copy[10]);

    if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11) return false;
    var soma1 = num1 * 10 + num2 * 9 + num3 * 8 + num4 * 7 + num5 * 6 + num6 * 5 + num7 * 4 + num8 * 3 + num9 * 2;
    var resto1 = soma1 * 10 % 11;
    if (resto1 == 10 || resto1 == 11) resto1 = 0;else
    if (resto1 != num10) return false;

    var soma2 = num1 * 11 + num2 * 10 + num3 * 9 + num4 * 8 + num5 * 7 + num6 * 6 + num7 * 5 + num8 * 4 + num9 * 3 + num10 * 2;
    var resto2 = soma2 * 10 % 11;
    if (resto2 == 10 || resto2 == 11) resto2 = 0;else
    if (resto2 != num11) return false;

    return true;
  }

  function validaCNPJ(value) {
    var copy = value.replace(".", "").replace(".", "").replace("/", "").replace("-", "");
    var num1 = parseInt(copy[0]);
    var num2 = parseInt(copy[1]);
    var num3 = parseInt(copy[2]);
    var num4 = parseInt(copy[3]);
    var num5 = parseInt(copy[4]);
    var num6 = parseInt(copy[5]);
    var num7 = parseInt(copy[6]);
    var num8 = parseInt(copy[7]);
    var num9 = parseInt(copy[8]);
    var num10 = parseInt(copy[9]);
    var num11 = parseInt(copy[10]);
    var num12 = parseInt(copy[11]);
    var num13 = parseInt(copy[12]);
    var num14 = parseInt(copy[13]);

    if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11 && num11 == num12 && num12 == num13 && num13 == num14) return false;

    var soma1 = num1 * 5 + num2 * 4 + num3 * 3 + num4 * 2 + num5 * 9 + num6 * 8 + num7 * 7 + num8 * 6 + num9 * 5 + num10 * 4 + num11 * 3 + num12 * 2;
    var soma2 = num1 * 6 + num2 * 5 + num3 * 4 + num4 * 3 + num5 * 2 + num6 * 9 + num7 * 8 + num8 * 7 + num9 * 6 + num10 * 5 + num11 * 4 + num12 * 3 + num13 * 2;
    var dig1 = soma1 % 11 < 2 ? 0 : 11 - soma1 % 11;
    var dig2 = soma2 % 11 < 2 ? 0 : 11 - soma2 % 11;
    if (dig1 != num13 || dig2 != num14) return false;
    return true;
  }

  function checkPessoa(value) {
    $.checkOnFisica.setVisible(value == 'F' ? true : false);
    $.checkOffFisica.setVisible(value == 'F' ? false : true);
    $.checkOnJuridica.setVisible(value == 'F' ? false : true);
    $.checkOffJuridica.setVisible(value == 'F' ? true : false);

    $.tfCpfCnpj.setHintText(value == 'F' ? 'CPF - somente numeros' : 'CNPJ - somente numeros');
    $.tfCpfCnpj.setValue('');
  }

  function checkME(value) {
    $.checkOnSimME.setVisible(value == '1' ? true : false);
    $.checkOffSimME.setVisible(value == '1' ? false : true);
    $.checkOnNaoME.setVisible(value == '1' ? false : true);
    $.checkOffNaoME.setVisible(value == '1' ? true : false);
  }

  function setEditable(bool) {
    $.tfNome.setEditable(bool);
    $.tfFantasia.setEditable(bool);
    $.tfTelefone.setEditable(bool);
    $.tfEmail.setEditable(bool);
    $.tfCpfCnpj.setEditable(bool);
    $.tfIpr.setEditable(bool);
    $.tfRG.setEditable(bool);
    $.tfNasc.setEditable(bool);
    $.tfObs.setEditable(bool);
  }

  $.mainWindow.addEventListener("open", function (e) {

    if (!dados) {
      setValues();
      setEditable(true);
      createMenuAndroid();
    } else
    {
      setValues(dados);
      setEditable(false);
    }

  });

  $.viewTelefone.addEventListener("click", function (e) {
    if (dados) return true;
    clickViewTelefone();
  });

  $.tfNome.addEventListener('change', function (e) {
    var v = mask.removeCaracter(e.value);
    if ($.tfNome.getValue() != v) $.tfNome.setValue(v);
    $.tfFantasia.setValue(v.slice(0, v.indexOf(' ')));
  });

  $.tfTelefone.addEventListener("change", function (e) {
    var v = mask.telefone(e.value);
    if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
    $.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
  });

  $.tfNasc.addEventListener("change", function (e) {
    var v = mask.date(e.value);
    if ($.tfNasc.getValue() != v) $.tfNasc.setValue(v);
    $.tfNasc.setSelection($.tfNasc.getValue().length, $.tfNasc.getValue().length);
  });

  $.tfCpfCnpj.addEventListener("change", function (e) {
    var v = '';
    if (tipo_pessoa == 'F') {
      v = mask.cpf(e.value);
      $.tfCpfCnpj.setColor(validaCPF(v) ? 'blue' : 'red');
    } else
    {
      v = mask.cnpj(e.value);
      $.tfCpfCnpj.setColor(validaCNPJ(v) ? 'blue' : 'red');
    }
    if ($.tfCpfCnpj.getValue() != v) $.tfCpfCnpj.setValue(v);
  });

  $.tfIpr.addEventListener('change', function (e) {
    var v = mask.removeCaracter(e.value);
    if ($.tfIpr.getValue() != v) $.tfIpr.setValue(v);
  });

  $.tfRG.addEventListener('change', function (e) {
    var v = mask.removeCaracter(e.value);
    if ($.tfRG.getValue() != v) $.tfRG.setValue(v);
  });

  $.viewFisica.addEventListener('click', function (e) {
    if (dados) return true;
    tipo_pessoa = 'F';
    checkPessoa(tipo_pessoa);
  });

  $.viewJuridica.addEventListener('click', function (e) {
    if (dados) return true;
    tipo_pessoa = 'J';
    checkPessoa(tipo_pessoa);
  });

  $.simME.addEventListener('click', function (e) {
    if (dados) return true;
    me = '1';
    checkME(me);
  });

  $.naoME.addEventListener('click', function (e) {
    if (dados) return true;
    me = '2';
    checkME(me);
  });























  _.extend($, exports);
}

module.exports = Controller;