var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'Menu';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainView = Ti.UI.createView(
  { id: "mainView", backgroundColor: "white" });

  $.__views.mainView && $.addTopLevelView($.__views.mainView);
  var __alloyId419 = {};var __alloyId422 = [];var __alloyId424 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId425 = [];var __alloyId427 = { type: 'Ti.UI.ImageView', bindId: 'viewProfile', properties: { bindId: "viewProfile", width: "68dp", height: "68dp", left: "15dp", top: "50dp", defaultImage: "/images/perfil_green.png" } };__alloyId425.push(__alloyId427);var __alloyId429 = { type: 'Ti.UI.Label', bindId: 'lbNome', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbNome", left: "15dp", top: "7dp" } };__alloyId425.push(__alloyId429);var __alloyId431 = { type: 'Ti.UI.Label', bindId: 'lbEmail', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "16dp", fontWeight: "semibold" }, bindId: "lbEmail", left: "15dp" } };__alloyId425.push(__alloyId431);return __alloyId425;}(), properties: { layout: "vertical" } };__alloyId422.push(__alloyId424);var __alloyId433 = { type: 'Ti.UI.View', properties: { bottom: 0, width: Ti.UI.FILL, height: 1, backgroundColor: "#ddd" } };__alloyId422.push(__alloyId433);var __alloyId421 = { properties: { name: "templateUsuario", layout: "vertical", backgroundImage: "/images/wallpaper.png", height: "180dp", width: Ti.UI.FILL, separatorColor: "none" }, childTemplates: __alloyId422 };__alloyId419["templateUsuario"] = __alloyId421;var __alloyId435 = { properties: { name: "templateSeparator", backgroundColor: "#d0d3d4", height: "1dp", width: Titanium.UI.FILL, touchEnabled: false } };__alloyId419["templateSeparator"] = __alloyId435;var __alloyId438 = [];var __alloyId440 = { type: 'Ti.UI.View', bindId: 'menu', childTemplates: function () {var __alloyId441 = [];var __alloyId443 = { type: 'Ti.UI.ImageView', bindId: 'imgOpcao', properties: { bindId: "imgOpcao", height: "25dp", width: "25dp", left: "0dp" } };__alloyId441.push(__alloyId443);var __alloyId445 = { type: 'Ti.UI.Label', bindId: 'lbOpcao', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbOpcao", textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT, left: "12dp" } };__alloyId441.push(__alloyId445);return __alloyId441;}(), properties: { bindId: "menu", layout: "horizontal", height: Ti.UI.SIZE, width: Ti.UI.FILL, left: "15dp", right: "15dp", top: "12dp", bottom: "12dp" } };__alloyId438.push(__alloyId440);var __alloyId437 = { properties: { name: "templateMenu", backgroundColor: "white", height: Ti.UI.SIZE, top: "6dp", bottom: 0, separatorColor: "none" }, childTemplates: __alloyId438 };__alloyId419["templateMenu"] = __alloyId437;$.__views.listView = Ti.UI.createListView(
  { width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: "none", backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId419, id: "listView", top: "-2dp" });

  $.__views.mainView.add($.__views.listView);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var ClientManager = require('ClientManager');
  var LocalData = require('LocalData');
  var RefreshData = require("RefreshData");

  function createMenu() {
    console.log('CREATE MENU: ', Alloy.Globals.getLocalDataUsuario());
    var usuario = Alloy.Globals.getLocalDataUsuario().data;

    var sections = [];

    var serctionUsuario = Ti.UI.createListSection();
    var sectionNovos = Ti.UI.createListSection();
    var sectionSeparator = Ti.UI.createListSection();
    var sectionConsultas = Ti.UI.createListSection();
    var sectionSync = Ti.UI.createListSection();

    serctionUsuario.setItems([createOptionUsuario(usuario)]);
    sectionNovos.setItems([
    createOption({ opc: 'Novo Pedido', img: '/images/novo_pedido_green.png' }),
    createOption({ opc: 'Novo Cliente', img: '/images/novo_cliente_green.png' })]);


    sectionSeparator.setItems([createSeparator()]);
    sectionConsultas.setItems([
    createOption({ opc: 'Ver Pedidos', img: '/images/pedidos_green.png' }),
    createOption({ opc: 'Ver Clientes', img: '/images/clientes_green.png' })]);


    sectionSync.setItems([
    createSeparator(),
    createOption({ opc: 'Baixar dados', img: '/images/download_green.png' }),
    createOption({ opc: 'Enviar dados', img: '/images/upload_green.png' })]);


    sections.push(serctionUsuario);
    sections.push(sectionNovos);
    sections.push(sectionSeparator);
    sections.push(sectionConsultas);
    sections.push(sectionSync);

    $.listView.setSections(sections);
  }

  function createOptionUsuario(usr) {
    return {
      template: 'templateUsuario',
      lbNome: {
        text: usr.USR_NOME },

      lbEmail: {
        text: usr.USR_EMAIL },

      viewProfile: {
        image: usr.USR_FOTO.trim() ? usr.USR_FOTO : '/images/perfil_green.png' },

      properties: {
        data: usr,
        type: 'perfil' },

      events: {
        "itemClick": function () {
          console.log('CLICOU!');
        },
        itemClick: function () {
          console.log('CLICOU!');
        } } };


  }

  function createOption(obj) {
    return {
      template: 'templateMenu',
      lbOpcao: {
        text: obj.opc },

      imgOpcao: {
        image: obj.img },

      properties: {
        data: obj,
        type: 'menu' } };


  }

  function createSeparator() {
    return {
      template: 'templateSeparator' };

  }

  function openWindowTabGroup(tab) {
    var WindowTabGroup = Alloy.createController(tab).getView();
    if (false)
    WindowTabGroup.open();else

    WindowTabGroup.open({ modal: false });
  }

  createMenu();

  $.listView.addEventListener("itemclick", function (e) {
    var item = e.section.getItemAt(e.itemIndex);
    var data = item ? item.properties.data : null;
    var type = item ? item.properties.type : null;

    if (data && data.opc == 'Novo Pedido') {
      Alloy.Globals.openWindow('FormPedido');
    } else
    if (data && data.opc == 'Novo Cliente') {
      ClientManager.delete();
      Alloy.Globals.openWindow('FormCliente');
    } else
    if (data && data.opc == 'Nova Reclamação') {
      console.log('CADASTRAR RECLAMACAO');
    } else
    if (data && data.opc == 'Ver Pedidos') {
      Alloy.Globals.openWindow('Pedidos');
    } else
    if (data && data.opc == 'Ver Clientes') {
      Alloy.Globals.openWindow('Clientes');
    } else
    if (data && data.opc == 'Baixar dados') {
      if (Ti.Network.online) {
        Alloy.Globals.openWindow("Sync", { download: true });
      } else
      {
        return Alloy.Globals.showAlert("Erro!", "É necessário estar conectado à internet para sincronizar os dados!");
      }
    } else
    if (data && data.opc == 'Enviar dados') {
      if (!Ti.Network.online) {
        return Alloy.Globals.showAlert("Erro!", "É necessário estar conectado à internet para sincronizar os dados!");
      } else
      {
        LocalData.countCadCli({
          success: function (count) {
            console.log('COUNT: ', count);
            if (!count || count == 0) {
              return Alloy.Globals.showAlert("Ops!", "Não existem dados para serem enviados!");
            } else
            {
              Alloy.Globals.openWindow("Sync", { upload: true });
            }
          },
          error: function (err) {
            console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
          } });

      }
    } else
    if (type == 'perfil') {
      Alloy.Globals.openWindow('Perfil');
    }
  });









  _.extend($, exports);
}

module.exports = Controller;