var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
		var arg = null;
		if (obj) {
				arg = obj[key] || null;
		}
		return arg;
}

function Controller() {

		require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
		this.__controllerPath = 'CadPedido/Frete';
		this.args = arguments[0] || {};

		if (arguments[0]) {
				var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
				var $model = __processArg(arguments[0], '$model');
				var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
		}
		var $ = this;
		var exports = {};
		var __defers = {};







		$.__views.mainWindow = Ti.UI.createWindow(
		{ navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Frete" });

		$.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
		if (true) {
				function __alloyId145() {
						$.__views.mainWindow.removeEventListener('open', __alloyId145);
						if ($.__views.mainWindow.activity) {
								$.__views.mainWindow.activity.actionBar.title = "Frete";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
						} else {
								Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
								Ti.API.warn('UI component which does not have an Android activity. Android Activities');
								Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
						}
				}
				$.__views.mainWindow.addEventListener('open', __alloyId145);
		}
		$.__views.__alloyId146 = Ti.UI.createScrollView(
		{ top: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "__alloyId146" });

		$.__views.mainWindow.add($.__views.__alloyId146);
		$.__views.viewFob = Ti.UI.createView(
		{ id: "viewFob", height: Ti.UI.SIZE, width: Ti.UI.FILL, top: "15dp" });

		$.__views.__alloyId146.add($.__views.viewFob);
		$.__views.checkOnFob = Ti.UI.createImageView(
		{ touchEnabled: false, id: "checkOnFob", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "15dp", visible: true });

		$.__views.viewFob.add($.__views.checkOnFob);
		$.__views.checkOffFob = Ti.UI.createImageView(
		{ touchEnabled: false, id: "checkOffFob", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "15dp", visible: false });

		$.__views.viewFob.add($.__views.checkOffFob);
		$.__views.labelFob = Ti.UI.createLabel(
		{ font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Frete FOB', touchEnabled: false, id: "labelFob", left: "55dp", height: Ti.UI.SIZE, color: "gray" });

		$.__views.viewFob.add($.__views.labelFob);
		$.__views.viewCif = Ti.UI.createView(
		{ id: "viewCif", height: Ti.UI.SIZE, width: Ti.UI.FILL });

		$.__views.__alloyId146.add($.__views.viewCif);
		$.__views.checkOnCif = Ti.UI.createImageView(
		{ touchEnabled: false, id: "checkOnCif", image: "/images/checkOn.png", width: "35dp", height: "35dp", left: "15dp", visible: false });

		$.__views.viewCif.add($.__views.checkOnCif);
		$.__views.checkOffCif = Ti.UI.createImageView(
		{ touchEnabled: false, id: "checkOffCif", image: "/images/checkOff.png", width: "35dp", height: "35dp", left: "15dp", visible: true });

		$.__views.viewCif.add($.__views.checkOffCif);
		$.__views.labelCif = Ti.UI.createLabel(
		{ font: { fontSize: "16dp", fontWeight: "semibold" }, text: 'Frete CIF', touchEnabled: false, id: "labelCif", left: "55dp", height: Ti.UI.SIZE, color: "gray" });

		$.__views.viewCif.add($.__views.labelCif);
		$.__views.__alloyId147 = Ti.UI.createView(
		{ width: Ti.UI.FILL, height: "45dp", top: "0dp", id: "__alloyId147" });

		$.__views.__alloyId146.add($.__views.__alloyId147);
		$.__views.tfFrete = Ti.UI.createTextField(
		{ font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfFrete", hintText: "0,00", autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS, keyboardType: Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD });

		$.__views.__alloyId147.add($.__views.tfFrete);
		$.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
		$.__views.activityIndicator.setParent($.__views.mainWindow);
		exports.destroy = function () {};




		_.extend($, $.__views);



		var args = $.args;
		var tipo_frete = 'F';
		var callback = args.callback;
		var Mask = require("Mask");

		const doClose = () => {
				$.mainWindow.close();
		};

		const createMenuAndroid = () => {
				if (true) {
						var activity = $.mainWindow.activity;
						activity.onCreateOptionsMenu = function (e) {
								var btDone = e.menu.add({
										title: "Salvar",
										showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

								btDone.addEventListener("click", () => {
										validateFrete();
								});
						};
						activity.invalidateOptionsMenu();
				}
		};

		const validateFrete = () => {
				var frete = {
						CJ_TPFRETE: tipo_frete,
						CJ_FRTBAST: $.tfFrete.getValue() };


				callback(frete);
				doClose();
		};

		const checkFrete = value => {
				$.checkOnFob.setVisible(value == 'F' ? true : false);
				$.checkOffFob.setVisible(value == 'F' ? false : true);
				$.checkOnCif.setVisible(value == 'F' ? false : true);
				$.checkOffCif.setVisible(value == 'F' ? true : false);

				$.tfFrete.setHintText(value == 'F' ? '0,00' : '0,00');






				$.tfFrete.setValue('0,00');
		};

		$.viewFob.addEventListener('click', function (e) {

				tipo_frete = 'F';
				checkFrete(tipo_frete);
		});

		$.viewCif.addEventListener('click', function (e) {

				tipo_frete = 'C';
				checkFrete(tipo_frete);
		});

		$.tfFrete.addEventListener("touchend", function (e) {
				$.tfFrete.setValue('');
		});

		$.tfFrete.addEventListener("change", function (e) {
				$.tfFrete.setValue(Mask.real(e.source.value));
		});

		$.mainWindow.addEventListener("open", function (e) {
				createMenuAndroid();
		});









		_.extend($, exports);
}

module.exports = Controller;