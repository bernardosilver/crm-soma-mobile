var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
  var arg = null;
  if (obj) {
    arg = obj[key] || null;
  }
  return arg;
}

function Controller() {

  require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
  this.__controllerPath = 'CadPedido/Parcela';
  this.args = arguments[0] || {};

  if (arguments[0]) {
    var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
    var $model = __processArg(arguments[0], '$model');
    var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
  }
  var $ = this;
  var exports = {};
  var __defers = {};







  $.__views.mainWindow = Ti.UI.createWindow(
  { navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Parcelas" });

  $.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
  if (true) {
    function __alloyId150() {
      $.__views.mainWindow.removeEventListener('open', __alloyId150);
      if ($.__views.mainWindow.activity) {
        $.__views.mainWindow.activity.actionBar.title = "Parcelas";$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
      } else {
        Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
        Ti.API.warn('UI component which does not have an Android activity. Android Activities');
        Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
      }
    }
    $.__views.mainWindow.addEventListener('open', __alloyId150);
  }
  $.__views.scrollView = Ti.UI.createScrollView(
  { id: "scrollView", top: 0, width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical" });

  $.__views.mainWindow.add($.__views.scrollView);
  $.__views.viewParcela = Ti.UI.createView(
  { id: "viewParcela", width: Ti.UI.FILL, top: "10dp", height: Ti.UI.SIZE });

  $.__views.scrollView.add($.__views.viewParcela);
  $.__views.tfParcela = Ti.UI.createTextField(
  { font: { fontSize: "17dp" }, color: "black", height: "45dp", width: "90%", backgroundColor: "white", hintTextColor: "gray", clearButtonMode: Titanium.UI.INPUT_BUTTONMODE_ONFOCUS, bubbleParent: false, returnKeyType: Titanium.UI.RETURNKEY_NEXT, id: "tfParcela", top: 0, hintText: "Clique para selecionar a data da parcela", editable: false, keyboardType: Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD });

  $.__views.viewParcela.add($.__views.tfParcela);
  $.__views.__alloyId151 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", top: "45dp", id: "__alloyId151" });

  $.__views.viewParcela.add($.__views.__alloyId151);
  $.__views.viewAddParcela = Ti.UI.createView(
  { width: Ti.UI.FILL, height: Ti.UI.SIZE, layout: "vertical", id: "viewAddParcela", top: "5dp" });

  $.__views.scrollView.add($.__views.viewAddParcela);
  $.__views.addParcela = Ti.UI.createView(
  { width: Ti.UI.FILL, height: "40dp", backgroundSelectedColor: Alloy.Globals.BACKGROUND_COLOR, id: "addParcela", top: "5dp" });

  $.__views.scrollView.add($.__views.addParcela);
  $.__views.__alloyId152 = Ti.UI.createImageView(
  { font: { fontSize: "15dp" }, width: "40dp", height: "40dp", left: "5dp", image: "/images/addGreen.png", touchEnabled: false, id: "__alloyId152" });

  $.__views.addParcela.add($.__views.__alloyId152);
  $.__views.__alloyId153 = Ti.UI.createLabel(
  { color: Alloy.Globals.BLUE_COLOR, left: "50dp", touchEnabled: false, text: 'Adicionar outra parcela', id: "__alloyId153" });

  $.__views.addParcela.add($.__views.__alloyId153);
  $.__views.__alloyId154 = Ti.UI.createView(
  { width: "95%", height: "1px", right: 0, backgroundColor: "#d0d2d2", id: "__alloyId154" });

  $.__views.scrollView.add($.__views.__alloyId154);
  $.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.mainWindow });
  $.__views.activityIndicator.setParent($.__views.mainWindow);
  exports.destroy = function () {};




  _.extend($, $.__views);



  var args = $.args;
  var dados = args.dados || null;
  var callback = args.callback || null;
  var mask = require('Mask');
  var moment = require("alloy/moment");
  var parcelas = [];

  const doClose = () => {
    $.mainWindow.close();
  };

  const doSave = () => {
    console.log('SALVOU!!!!!');
    var parc = [];
    parc.push($.tfParcela.getValue());
    for (var i in parcelas) {
      parc.push(parcelas[i].getValues().parcela);
      console.log('PARCELA ', parcelas[i].getValues().parcela);
    }
    callback(parc);
    doClose();
  };


  const createMenuAndroid = () => {
    if (true) {
      var activity = $.mainWindow.activity;
      activity.onCreateOptionsMenu = function (e) {
        var btDone = e.menu.add({
          title: "Salvar",
          showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS });

        btDone.addEventListener("click", doSave);
      };
      activity.invalidateOptionsMenu();
    }
  };


  const clickViewParcela = data => {
    if (parcelas.length >= 5) {
      Alloy.Globals.showAlert("Ops", "É possível adicionar apenas 6 parcelas!");
      return false;
    }
    var ult = getUltimaParcela();
    if (!ult) return Alloy.Globals.showAlert('Ops!', 'É necessário inserir uma data para a parcela anterior');
    var parcela = Alloy.createWidget('CadParcela', 'widget', { activityIndicator: $.activityIndicator, ultima: ult, data: data, editable: dados ? false : true });
    $.viewAddParcela.add(parcela.getView());

    parcelas.push(parcela);

    parcela.setFocus();

    parcela.setActionApagar(function () {
      $.viewAddParcela.remove(parcela.getView());
      for (var i in parcelas) {
        if (parcelas[i] == parcela) {
          parcelas.splice(i, 1);
          break;
        }
      }
    });
  };

  const getUltimaParcela = () => {
    var tam = parcelas.length;
    var parc = null;
    console.log('QUANTIDADE DE PARCELAS: ', tam);
    if (parcelas.length)
    parc = parcelas[tam - 1].getValues().parcela;else

    parc = $.tfParcela.getValue();
    console.log('ULT PARC: ', parc);
    return parc;
  };

  const showPicker = () => {
    var anoAtual = moment().year();
    var mesAtual = moment().month();
    var diaAtual = moment().date();


    var maxDate = moment().add(3, 'd');
    if (maxDate.day() == 0) {
      maxDate = moment().add(4, 'd');
    } else
    if (maxDate.day() == 6) {
      maxDate = moment().add(5, 'd');
    }

    maxDate = moment().add(65, 'd');
    var maxAno = moment(maxDate).year();
    var maxMes = moment(maxDate).month();
    var maxDia = moment(maxDate).date();

    var value = moment().add(3, 'days');
    var valueAno = moment(value).year();
    var valueMes = moment(value).month();
    var valueDia = moment(value).date();
    console.log('VALUE: ', value.year(), value.month(), value.date());
    var picker = Ti.UI.createPicker({
      type: Ti.UI.PICKER_TYPE_DATE,
      locale: 'pt_BR',
      minDate: new Date(anoAtual, mesAtual, diaAtual),
      maxDate: new Date(maxAno, maxMes, maxDia),
      value: new Date(valueAno, valueMes, valueDia) });


    picker.showDatePickerDialog({
      value: new Date(valueAno, valueMes, valueDia),
      callback: function (e) {
        if (e.cancel) {
          Ti.API.info('User canceled dialog');
        } else {
          Ti.API.info('User selected date: ' + moment(e.value).weekday());
          $.tfParcela.setValue(moment(e.value).format('DD/MM/YYYY'));
          removeParcelas();
        }
      } });

  };

  const removeParcelas = () => {
    for (var i in parcelas) {
      $.viewAddParcela.remove(parcelas[i].getView());
    }
    parcelas = [];
  };

  $.mainWindow.addEventListener("open", function (e) {
    createMenuAndroid();
  });

  $.addParcela.addEventListener("click", function (e) {
    if (dados) return true;
    clickViewParcela();
  });

  $.tfParcela.addEventListener("focus", function (e) {
    showPicker();
  });

  $.tfParcela.addEventListener("click", function (e) {
    showPicker();
  });









  _.extend($, exports);
}

module.exports = Controller;