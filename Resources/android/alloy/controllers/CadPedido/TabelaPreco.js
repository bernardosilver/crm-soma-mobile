var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'CadPedido/TabelaPreco';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.mainWindow = Ti.UI.createWindow(
	{ navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Tabelas de Preço", tabBarHidden: true, modal: true });

	$.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
	if (true) {
		function __alloyId155() {
			$.__views.mainWindow.removeEventListener('open', __alloyId155);
			if ($.__views.mainWindow.activity) {
				$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
			} else {
				Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
				Ti.API.warn('UI component which does not have an Android activity. Android Activities');
				Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
			}
		}
		$.__views.mainWindow.addEventListener('open', __alloyId155);
	}
	$.__views.__alloyId156 = Ti.UI.createView(
	{ id: "__alloyId156" });

	$.__views.mainWindow.add($.__views.__alloyId156);
	var __alloyId157 = {};var __alloyId160 = [];var __alloyId162 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId163 = [];var __alloyId165 = { type: 'Ti.UI.Label', bindId: 'lbDesc', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbDesc", maxLines: 2, left: 0 } };__alloyId163.push(__alloyId165);return __alloyId163;}(), properties: { layout: "vertical", width: Ti.UI.FILL, right: "15dp" } };__alloyId160.push(__alloyId162);var __alloyId159 = { properties: { name: "templateTabela", backgroundColor: "white", width: Ti.UI.FILL, height: "40dp", top: "10dp", left: "15dp", right: "20dp" }, childTemplates: __alloyId160 };__alloyId157["templateTabela"] = __alloyId159;$.__views.listView = Ti.UI.createListView(
	{ width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId157, id: "listView" });

	$.__views.__alloyId156.add($.__views.listView);
	$.__views.popup = Alloy.createWidget('PopUp', 'widget', { id: "popup", __parentSymbol: $.__views.__alloyId156 });
	$.__views.popup.setParent($.__views.__alloyId156);
	$.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.__alloyId156 });
	$.__views.activityIndicator.setParent($.__views.__alloyId156);
	exports.destroy = function () {};




	_.extend($, $.__views);



	var args = $.args;
	var pedido = args.pedido || false;
	var callback = args.callback || (() => {});
	var usuario = Alloy.Globals.getLocalDataUsuario().data;
	var LocalData = require('LocalData');

	const doClose = () => {
		$.mainWindow.close();
	};

	async function getTabelas() {
		$.activityIndicator.show();
		var tabelas = await LocalData.getTabelas(usuario.USR_LOGIN);
		createListView(tabelas);
		$.activityIndicator.hide();
		console.log('TABELAS: ', tabelas);
	}

	function createListView(array) {
		var section = Ti.UI.createListSection();
		var itens = [];
		for (var i in array) {
			itens.push(createItemList(array[i]));
		}
		section.setItems(itens);
		$.listView.setSections([section]);
	}

	function createItemList(item) {
		var searchableText = item.Filial + item.CodTab + item.Descr;
		return {
			template: 'templateTabela',
			lbDesc: {
				text: item.Descr + ' - Filial: ' + item.Filial },

			properties: {
				data: item,
				accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
				selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
				searchableText: searchableText } };


	}

	$.mainWindow.addEventListener("itemclick", function (e) {
		var click = e.section.getItemAt(e.itemIndex);
		var tabela = click.properties.data;
		if (pedido) {
			callback(tabela);
			doClose();
		}
	});

	$.mainWindow.addEventListener("focus", function () {
		getTabelas();
	});









	_.extend($, exports);
}

module.exports = Controller;