var Alloy = require('/alloy'),
Backbone = Alloy.Backbone,
_ = Alloy._;




function __processArg(obj, key) {
	var arg = null;
	if (obj) {
		arg = obj[key] || null;
	}
	return arg;
}

function Controller() {

	require('/alloy/controllers/' + 'BaseController').apply(this, Array.prototype.slice.call(arguments));
	this.__controllerPath = 'CadPedido/CondPagamento';
	this.args = arguments[0] || {};

	if (arguments[0]) {
		var __parentSymbol = __processArg(arguments[0], '__parentSymbol');
		var $model = __processArg(arguments[0], '$model');
		var __itemTemplate = __processArg(arguments[0], '__itemTemplate');
	}
	var $ = this;
	var exports = {};
	var __defers = {};







	$.__views.mainWindow = Ti.UI.createWindow(
	{ navTintColor: Alloy.Globals.WHITE_COLOR, backgroundColor: Alloy.Globals.WHITE_COLOR, barColor: Alloy.Globals.MAIN_COLOR, theme: "Theme.Profile", id: "mainWindow", title: "Forma de pagamento", tabBarHidden: true, modal: true });

	$.__views.mainWindow && $.addTopLevelView($.__views.mainWindow);
	if (true) {
		function __alloyId134() {
			$.__views.mainWindow.removeEventListener('open', __alloyId134);
			if ($.__views.mainWindow.activity) {
				$.__views.mainWindow.activity.actionBar.displayHomeAsUp = true;$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected = doClose;
			} else {
				Ti.API.warn('You attempted to access an Activity on a lightweight Window or other');
				Ti.API.warn('UI component which does not have an Android activity. Android Activities');
				Ti.API.warn('are valid with only windows in TabGroups or heavyweight Windows.');
			}
		}
		$.__views.mainWindow.addEventListener('open', __alloyId134);
	}
	$.__views.__alloyId135 = Ti.UI.createView(
	{ id: "__alloyId135" });

	$.__views.mainWindow.add($.__views.__alloyId135);
	var __alloyId136 = {};var __alloyId139 = [];var __alloyId141 = { type: 'Ti.UI.View', childTemplates: function () {var __alloyId142 = [];var __alloyId144 = { type: 'Ti.UI.Label', bindId: 'lbDesc', properties: { color: Alloy.Globals.DARK_MAIN_COLOR, font: { fontSize: "17dp", fontWeight: "bold" }, bindId: "lbDesc", maxLines: 2, left: 0 } };__alloyId142.push(__alloyId144);return __alloyId142;}(), properties: { layout: "vertical", width: Ti.UI.FILL, right: "15dp" } };__alloyId139.push(__alloyId141);var __alloyId138 = { properties: { name: "templateForma", backgroundColor: "white", width: Ti.UI.FILL, height: "35dp", top: "10dp", left: "15dp", right: "20dp" }, childTemplates: __alloyId139 };__alloyId136["templateForma"] = __alloyId138;$.__views.listView = Ti.UI.createListView(
	{ width: Ti.UI.FILL, height: Ti.UI.FILL, separatorColor: Alloy.Globals.LIGHT_GRAY_COLOR2, backgroundColor: "transparent", listSeparatorInsets: { left: 0, right: 0 }, templates: __alloyId136, id: "listView" });

	$.__views.__alloyId135.add($.__views.listView);
	$.__views.popup = Alloy.createWidget('PopUp', 'widget', { id: "popup", __parentSymbol: $.__views.__alloyId135 });
	$.__views.popup.setParent($.__views.__alloyId135);
	$.__views.activityIndicator = Alloy.createWidget('ActivityIndicator', 'widget', { id: "activityIndicator", __parentSymbol: $.__views.__alloyId135 });
	$.__views.activityIndicator.setParent($.__views.__alloyId135);
	exports.destroy = function () {};




	_.extend($, $.__views);



	var args = $.args;
	var pedido = args.pedido || false;
	var callback = args.callback || (() => {});
	var usuario = Alloy.Globals.getLocalDataUsuario().data;
	var LocalData = require('LocalData');

	var OrderManager = require('OrderManager');
	var cliente = OrderManager.getCliente();

	const doClose = () => {
		$.mainWindow.close();
	};

	async function getCondPag() {
		$.activityIndicator.show();
		var condPag = await LocalData.getCondPag({ A1_COD: cliente.A1_COD });
		console.log('TABELAS: ', condPag);
		createListView(condPag);
		$.activityIndicator.hide();
	}

	function createListView(array) {
		var section = Ti.UI.createListSection();
		var itens = [];
		for (var i in array) {
			itens.push(createItemList(array[i]));
		}
		section.setItems(itens);
		$.listView.setSections([section]);
	}

	function createItemList(item) {
		var searchableText = item.Filial + item.CodTab + item.Descr;
		return {
			template: 'templateForma',
			lbDesc: {
				text: item.DESCRI },

			properties: {
				data: item,
				accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
				selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
				searchableText: searchableText } };


	}

	$.mainWindow.addEventListener("itemclick", function (e) {
		var click = e.section.getItemAt(e.itemIndex);
		var condPag = click.properties.data;
		if (pedido) {
			callback(condPag);
			doClose();
		}
	});

	$.mainWindow.addEventListener("focus", function () {
		getCondPag();
	});









	_.extend($, exports);
}

module.exports = Controller;