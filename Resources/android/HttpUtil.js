
function HttpUtil() {

    var timeout = 30000;
    var token = Alloy.Globals.getLocalDataUsuario() ? Alloy.Globals.getLocalDataUsuario().token : null;
    var headers = [{ key: "authorization", value: "123456" }, { key: "token", value: token }];

    var httpClient = Titanium.Network.createHTTPClient({
        timeout: timeout
    });

    var errorCallBack = function () {};
    var successCallBack = function () {};

    function addQueryURL(url, params) {
        var params_url = "";
        for (prop in params) {
            if (params_url == "") params_url += "?";else params_url += "&";

            if (params[prop] != null) params_url += prop + "=" + encodeURIComponent(params[prop]);
        }
        Ti.API.info(url + "" + params_url);
        return url + "" + params_url;
    }

    function setHeaders(headers) {
        for (var i in headers) {
            httpClient.setRequestHeader(headers[i].key, headers[i].value);
        }
    }

    this.post = function (params) {
        var url = params.url;
        var data = params.data || {};

        if (params.headers) headers = headers.concat(params.headers);

        if (Ti.Network.online == true) {
            httpClient.open("POST", url);
            setHeaders(headers);
            httpClient.send(JSON.stringify(data));
        } else {
            errorCallBack();
        }
    };

    this.get = function (params) {
        var data = params.data || {};
        var url = params.url;

        if (params.headers) headers = headers.concat(params.headers);

        if (Ti.Network.online == true) {
            httpClient.open("GET", addQueryURL(url, data));
            setHeaders(headers);
            httpClient.send();
        } else {
            errorCallBack();
        }
    };

    this.delete = function (params) {
        var data = params.data || {};
        var url = params.url;

        if (params.headers) headers = headers.concat(params.headers);

        if (Ti.Network.online == true) {
            httpClient.open("DELETE", addQueryURL(url, data));
            setHeaders(headers);
            httpClient.send();
        } else {
            errorCallBack();
        }
    };

    this.upload = function (params) {
        var data = params.data || {};
        var url = params.url;

        if (params.headers) headers = headers.concat(params.headers);

        if (Ti.Network.online == true) {
            httpClient.open("POST", url);
            setHeaders(headers);
            httpClient.send(data);
        } else {
            errorCallBack();
        }
    };

    this.put = function (params) {
        var url = params.url;
        var data = params.data || {};

        if (params.headers) headers = headers.concat(params.headers);

        if (Ti.Network.online == true) {
            httpClient.open("PUT", url);
            setHeaders(headers);
            httpClient.send(JSON.stringify(data));
        } else {
            errorCallBack();
        }
    };

    this.setSuccessCallBack = function (action) {
        successCallBack = action;
        httpClient.onload = function (e) {
            try {
                var json = this.responseText ? JSON.parse(this.responseText) : {};
                successCallBack(json, this);
            } catch (e) {
                Ti.API.error("Erro: " + e.message);
                errorCallBack({ error: e.message });
            }
        };
    };

    this.setErrorCallBack = function (action) {
        errorCallBack = action;
        httpClient.onerror = function (e) {
            Ti.API.error('onerror called, readyState = ' + this.readyState + ' error = ' + e.error + ' text= ' + this.responseText);
            try {
                var json = this.responseText ? JSON.parse(this.responseText) : {};
                json.code = e.code ? parseInt(e.code) : 0;
                errorCallBack(json);
            } catch (e) {
                errorCallBack({
                    error: this.responseText,
                    code: 500
                });
            }
        };
    };
}

module.exports = HttpUtil;