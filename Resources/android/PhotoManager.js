
var OptionDialog = require("OptionDialog");
var chooseCallBack = function () {};

function callbackOption(e) {
    if (e.index == 0) {
        requestCameraPermission(function () {
            showCamera();
        });
    } else if (e.index == 1) {
        requestCameraPermission(function () {
            showGallery();
        });
    }
    return true;
}

function showCamera() {
    Ti.API.error("SHOW CAMERA");
    Ti.Media.showCamera({
        success: function (event) {
            var preview = event.media;
            editPhoto(preview);
        },
        cancel: function (event) {
            Ti.API.info("CANCEL");
        },
        error: function (error) {
            Ti.API.error("ERRO");
            Ti.API.error(JSON.stringify(error));
        },
        saveToPhotoGallery: true,
        whichCamera: Titanium.Media.CAMERA_REAR,
        showControls: true,
        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
        autohide: true
    });
}

function showGallery() {

    Ti.Media.openPhotoGallery({
        success: function (event) {
            var preview = event.media;
            editPhoto(preview);
        },
        cancel: function () {},
        error: function (error) {
            Ti.API.error("ERRO");
            Ti.API.error(JSON.stringify(error));
        },
        showControls: true,
        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
        autohide: true
    });
}

function requestCameraPermission(action) {
    if (true) {
        if (Titanium.Android.hasPermission("android.permission.CAMERA") == false || Titanium.Android.hasPermission("android.permission.WRITE_EXTERNAL_STORAGE") == false) {
            Titanium.Android.requestPermissions(["android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"], function (e) {
                if (e.success == true) {
                    action();
                }
            });
        } else {
            action();
        }
    } else {
        if (Titanium.Media.hasCameraPermissions()) {
            Titanium.Media.requestCameraPermissions(function (e) {
                if (e.success == true) {
                    action();
                }
            });
        } else {
            action();
        }
    }
}

function editPhoto(preview) {
    var previewImage = preview;

    if (false) {
        previewImage = Ti.UI.createImageView({
            height: 'auto',
            width: 'auto',
            image: preview
        }).toBlob();
    }

    chooseCallBack(previewImage);
}

exports.showOptions = function (_chooseCallBack) {
    chooseCallBack = _chooseCallBack || function () {};
    OptionDialog.show({ callback: callbackOption, options: ['Tirar Foto', 'Escolher Foto'] });
};