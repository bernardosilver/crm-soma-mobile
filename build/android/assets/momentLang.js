var moment=require("alloy/moment"),

languages=["pt-br"];

require("alloy/moment/lang/pt-br");

function selectLanguage(lang){
[lang.split("-")[0],lang.toLowerCase()].forEach(function(lang){
-1!==languages.indexOf(lang)&&
moment.lang(lang);

});
}

exports.selectLanguage=selectLanguage;