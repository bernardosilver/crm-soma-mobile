
exports.removeCaracter=function(txt){
var tmp=txt.replace(/[ÁÀÂÃÄáàãâä]/g,"A");







return tmp=tmp.replace(/[ÉÈÊËéèêë]/g,"E"),tmp=tmp.replace(/[ÍÌÎÏíìîï]/g,"I"),tmp=tmp.replace(/[ÓÓÕÔÖóòõôö]/g,"O"),tmp=tmp.replace(/[ÚÚÛÜúùûü]/g,"U"),tmp=tmp.replace(/[çÇ]/g,"C"),tmp=tmp.replace(/[^A-Za-z0-9-, ]{1,}/g,""),tmp;
},

exports.removeAcentos=function(txt){
var tmp=txt.replace(/[ÁÀÂÃÄáàãâä]/g,"A");






return tmp=tmp.replace(/[ÉÈÊËéèêë]/g,"E"),tmp=tmp.replace(/[ÍÌÎÏíìîï]/g,"I"),tmp=tmp.replace(/[ÓÓÕÔÖóòõôö]/g,"O"),tmp=tmp.replace(/[ÚÚÛÜúùûü]/g,"U"),tmp=tmp.replace(/[çÇ]/g,"C"),tmp;
},

exports.removeTelMask=function(tel){
return tel.replace("(","").replace(")","").replace("-","");
},

exports.removeCPFCNPJMask=function(value){
return value.replace(".","").replace(".","").replace("/","").replace("-","");
},

exports.real=function(int){
var tmp=int.replace(",","").replace(".","")+"";






return tmp=tmp.replace(/(\d)(\d{1,2})$/g,"$1,$2"),tmp=tmp.replace(/(\D{1,})$/g,""),6<tmp.length&&(tmp=tmp.replace(/([0-9]{3}),([0-9]{2}$)/g,".$1,$2")),tmp;
},

exports.float=function(int){
var tmp=int.replace(",","").replace(".","")+"";





return tmp=tmp.replace(/(\d)(\d{1,2})$/g,"$1.$2"),tmp||(tmp=""),6<tmp.length&&(tmp=tmp.replace(/([0-9]{3}),([0-9]{2}$)/g,",$1.$2")),tmp;
},

exports.int=function(int){
var tmp=int.replace(",","").replace(".","")+"";

return tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.cel=function(int){
var tmp=int.replace("(","").replace(")","").replace("-","")+"";






return 11<tmp.length&&(tmp=tmp.slice(0,11)),tmp=tmp.replace(/(\d{0,2})(\d{0,5})(\d{0,4})$/g,"($1)$2-$3"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.tel=function(int){
var tmp=int.replace("(","").replace(")","").replace("-","")+"";






return 10<tmp.length&&(tmp=tmp.slice(0,10)),tmp=tmp.replace(/(\d{0,2})(\d{0,4})(\d{0,4})$/g,"($1)$2-$3"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.telefone=function(int){
var tmp=int.replace("(","").replace(")","").replace("-","")+"";






return 11<tmp.length&&(tmp=tmp.slice(0,11)),tmp=tmp.replace(/(\d{0,2})(\d{0,4}\d{0,1}?)(\d{0,4})$/g,"($1)$2-$3"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.cep=function(int){
var tmp=int.replace("-","")+"";





return 8<tmp.length&&(tmp=tmp.slice(0,8)),tmp=tmp.replace(/(\d{0,5})(\d{0,3})$/g,"$1-$2"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.date=function(int){
var tmp=int.replace("/","").replace("/","")+"";






return 8<tmp.length&&(tmp=tmp.slice(0,8)),tmp=tmp.replace(/(\d{0,1}\d{0,1})(\d{0,1}\d{0,1})(\d{0,3}\d{0,3})$/g,"$1/$2/$3"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.cpf=function(int){
var tmp=int.replace(".","").replace(".","").replace(".","").replace("-","")+"";





return 11<tmp.length&&(tmp=tmp.slice(0,11)),tmp=tmp.replace(/(\d{0,3})(\d{0,3})(\d{0,3})(\d{0,2})$/g,"$1.$2.$3-$4"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
},

exports.cnpj=function(int){
var tmp=int.replace(".","").replace("/","").replace("-","")+"";





return 14<tmp.length&&(tmp=tmp.slice(0,14)),tmp=tmp.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/g,"$1.$2.$3/$4-$5"),tmp=tmp.replace(/(\D{1,})$/g,""),tmp;
};