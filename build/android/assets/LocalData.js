var moment=require("alloy/moment");

exports.createTableCadCliente=function(){
try{
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS cadCliente`),
db.execute(`CREATE TABLE IF NOT EXISTS cadCliente (ID INTEGER PRIMARY KEY, A1_FILIAL TEXT, A1_COD TEXT, A1_LOJA TEXT, A1_PESSOA TEXT, A1_CGC TEXT, A1_NOME TEXT, A1_NREDUZ TEXT, A1_ME TEXT, A1_END TEXT, A1_COMPLEM TEXT, A1_BAIRRO TEXT, A1_EST TEXT, A1_MUN TEXT, A1_CODMUN TEXT, A1_CEP TEXT, A1_LAT TEXT, A1_LONG TEXT, A1_DDD TEXT, A1_PAIS TEXT, A1_TEL TEXT, A1_TEL2 TEXT, A1_TEL3 TEXT, A1_TEL4 TEXT, A1_FAX TEXT, A1_CEL1 TEXT, A1_CEL2 TEXT, A1_ENDCOB TEXT, A1_BAIRROC TEXT, A1_MUNC TEXT, A1_ESTC TEXT, A1_CEPC TEXT, A1_INSCR TEXT, A1_PFISICA TEXT, A1_VEND TEXT, A1_VEND2 TEXT, A1_TPFRET TEXT, A1_RISCO TEXT, A1_LC REAL, A1_CODPAIS TEXT, A1_MSBLQL TEXT, A1_EXPORTA TEXT, A1_MOEDALC TEXT, A1_CONTA TEXT, A1_SUPER TEXT, A1_OBSERV TEXT, A1_EMAIL TEXT, A1_DTCADAS TEXT, A1_DTNASC TEXT, A1_DTALT TEXT, A1_LIBEDIT TEXT, SALDO REAL, A1_INFOROT INTEGER, A1_OBSRC INTEGER, A1_DTULTIT TEXT, A1_COND TEXT, A1_COND2 TEXT, DTSYNC TEXT)`),
db.close();
}catch(err){
return err;
}
},

exports.insertCadCliente=function(params){var
cliente=params.cliente||null,
success=params.success||function(){},
error=params.error||function(){};
if(!cliente)return error("Cliente n\xE3o informado para cadastrar");
var db=Ti.Database.open("somaCRMDB");
try{
db.execute("INSERT INTO cliente(A1_FILIAL  , A1_LOJA   , A1_PESSOA , A1_CGC    , A1_NOME   , A1_DTNASC , A1_NREDUZ   , A1_ME     , A1_END    , A1_COMPLEM, A1_BAIRRO , A1_EST, A1_MUN      , A1_CODMUN , A1_CEP    , A1_LAT    , A1_LONG   , A1_DDD, A1_TEL      , A1_TEL2   , A1_TEL3   , A1_TEL4   , A1_FAX    , A1_CEL1, A1_CEL2     , A1_ENDCOB , A1_BAIRROC, A1_MUNC   , A1_ESTC   , A1_CEPC, A1_INSCR    , A1_PFISICA, A1_VEND   , A1_VEND2  , A1_TPFRET , A1_RISCO, A1_LC       , A1_CODPAIS, A1_MSBLQL , A1_EXPORTA, A1_MOEDALC, A1_CONTA, A1_SUPER    , A1_OBSERV , A1_EMAIL  , A1_DTCADAS, A1_DTALT  , A1_LIBEDIT, SALDO       , A1_INFOROT, A1_OBSRC  , A1_DTULTIT, A1_COND   , A1_COND2, DTSYNC      , A1_PAIS   , A1_MOBILE)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",cliente.A1_FILIAL,cliente.A1_LOJA,cliente.A1_PESSOA,cliente.A1_CGC,cliente.A1_NOME,cliente.A1_DTNASC,cliente.A1_NREDUZ,cliente.A1_ME,cliente.A1_END,cliente.A1_COMPLEM,cliente.A1_BAIRRO,cliente.A1_EST,cliente.A1_MUN,cliente.A1_CODMUN,cliente.A1_CEP,cliente.A1_LAT,cliente.A1_LONG,cliente.A1_DDD,cliente.A1_TEL,cliente.A1_TEL2,cliente.A1_TEL3,cliente.A1_TEL4,cliente.A1_FAX,cliente.A1_CEL1,cliente.A1_CEL2,cliente.A1_ENDCOB,cliente.A1_BAIRROC,cliente.A1_MUNC,cliente.A1_ESTC,cliente.A1_CEPC,cliente.A1_INSCR,cliente.A1_PFISICA,cliente.A1_VEND,cliente.A1_VEND2,cliente.A1_TPFRET,cliente.A1_RISCO,cliente.A1_LC,cliente.A1_CODPAIS,cliente.A1_MSBLQL,cliente.A1_EXPORTA,cliente.A1_MOEDALC,cliente.A1_CONTA,cliente.A1_SUPER,cliente.A1_OBSERV,cliente.A1_EMAIL,cliente.A1_DTCADAS,cliente.A1_DTALT,cliente.A1_LIBEDIT,cliente.SALDO,cliente.A1_INFOROT,cliente.A1_OBSRC,cliente.A1_DTULTIT,cliente.A1_COND,cliente.A1_COND2,cliente.DTSYNC,cliente.A1_PAIS,cliente.A1_MOBILE);

const id=db.lastInsertRowId;
success(id),
db.close();
}catch(err){
console.log("DEU ERRO CAD CLIENTE: ",err),
error(err);
}
},

exports.updateCadCliente=function(params){var
data=params.data||null,
success=params.success||function(){},
error=params.error||function(){},
id=data.id,
cliente=data.cliente;
if(!cliente)return error("Cliente n\xE3o informado para cadastrar");
var db=Ti.Database.open("somaCRMDB");
try{
db.execute("UPDATE cliente SET A1_FILIAL = ? , A1_LOJA = ?     , A1_PESSOA = ?   , A1_CGC = ?      , A1_NOME = ?     , A1_DTNASC = ? , A1_NREDUZ = ?  , A1_ME = ?       , A1_END = ?      , A1_COMPLEM = ?  , A1_BAIRRO = ?   , A1_EST = ?    , A1_MUN = ?     , A1_CODMUN = ?   , A1_CEP = ?      , A1_LAT = ?      , A1_LONG = ?     , A1_DDD = ?    , A1_TEL = ?     , A1_TEL2 = ?     , A1_TEL3 = ?     , A1_TEL4 = ?     , A1_FAX = ?      , A1_CEL1 = ?   , A1_CEL2 = ?    , A1_ENDCOB = ?   , A1_BAIRROC = ?  , A1_MUNC = ?     , A1_ESTC = ?     , A1_CEPC = ?   , A1_INSCR = ?   , A1_PFISICA = ?  , A1_VEND = ?     , A1_VEND2 = ?    , A1_TPFRET = ?   , A1_RISCO = ?  , A1_LC = ?      , A1_CODPAIS = ?  , A1_MSBLQL = ?   , A1_EXPORTA = ?  , A1_MOEDALC = ?  , A1_CONTA = ?  , A1_SUPER = ?   , A1_OBSERV = ?   , A1_EMAIL = ?    , A1_DTCADAS = ?  , A1_DTALT = ?    , A1_LIBEDIT = ?, SALDO = ?      , A1_INFOROT = ?  , A1_OBSRC = ?    , A1_DTULTIT = ?  , A1_COND = ?     , A1_COND2 = ?  , DTSYNC = ?     , A1_PAIS = ?     , A1_MOBILE = ? "+`WHERE ID = '${id}' `,cliente.A1_FILIAL,cliente.A1_LOJA,cliente.A1_PESSOA,cliente.A1_CGC,cliente.A1_NOME,cliente.A1_DTNASC,cliente.A1_NREDUZ,cliente.A1_ME,cliente.A1_END,cliente.A1_COMPLEM,cliente.A1_BAIRRO,cliente.A1_EST,cliente.A1_MUN,cliente.A1_CODMUN,cliente.A1_CEP,cliente.A1_LAT,cliente.A1_LONG,cliente.A1_DDD,cliente.A1_TEL,cliente.A1_TEL2,cliente.A1_TEL3,cliente.A1_TEL4,cliente.A1_FAX,cliente.A1_CEL1,cliente.A1_CEL2,cliente.A1_ENDCOB,cliente.A1_BAIRROC,cliente.A1_MUNC,cliente.A1_ESTC,cliente.A1_CEPC,cliente.A1_INSCR,cliente.A1_PFISICA,cliente.A1_VEND,cliente.A1_VEND2,cliente.A1_TPFRET,cliente.A1_RISCO,cliente.A1_LC,cliente.A1_CODPAIS,cliente.A1_MSBLQL,cliente.A1_EXPORTA,cliente.A1_MOEDALC,cliente.A1_CONTA,cliente.A1_SUPER,cliente.A1_OBSERV,cliente.A1_EMAIL,cliente.A1_DTCADAS,cliente.A1_DTALT,cliente.A1_LIBEDIT,cliente.SALDO,cliente.A1_INFOROT,cliente.A1_OBSRC,cliente.A1_DTULTIT,cliente.A1_COND,cliente.A1_COND2,cliente.DTSYNC,cliente.A1_PAIS,cliente.A1_MOBILE),
success(db.lastInsertRowId),
db.close();
}catch(err){
console.log("DEU ERRO CAD CLIENTE: ",err),
error(err);
}
},

exports.createTableCadRef=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS cadRef`),
db.execute(`CREATE TABLE IF NOT EXISTS cadRef (ID INTEGER PRIMARY KEY, AO_FILIAL TEXT, AO_CLIENTE TEXT, AO_LOJA TEXT, AO_TIPO TEXT, AO_NOMINS TEXT, AO_DATA TEXT, AO_SOCIO TEXT, AO_CGC TEXT, AO_TELEFON TEXT, AO_CONTATO TEXT, AO_OBSERV TEXT, AO_DTALT TEXT, DTSYNC TEXT, ID_CLIENTE INTEGER, RECNO INTEGER, DEL TEXT)`);
},

exports.insertCadRef=function(params){var
ref=params.ref||null,
success=params.success||function(){},
error=params.error||function(){};
if(!ref)return error("Refer\xEAncia n\xE3o informada para cadastrar");
try{
var db=Ti.Database.open("somaCRMDB");
db.execute("INSERT INTO cadRef(AO_FILIAL  , AO_CLIENTE, AO_LOJA  , AO_TIPO, AO_NOMINS   , AO_DATA   , AO_SOCIO , AO_CGC, AO_TELEFON  , AO_CONTATO, AO_OBSERV, AO_DTALT, DTSYNC      , ID_CLIENTE, RECNO    , DEL) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ",ref.AO_FILIAL,ref.AO_CLIENTE,ref.AO_LOJA,ref.AO_TIPO,ref.AO_NOMINS,ref.AO_DATA,ref.AO_SOCIO,ref.AO_CGC,ref.AO_TELEFON,ref.AO_CONTATO,ref.AO_OBSERV,ref.AO_DTALT,ref.DTSYNC,ref.ID_CLIENTE,ref.RECNO,ref.DEL),
success({ref:ref.AO_NOMINS,id:db.lastInsertRowId}),
db.close();
}catch(err){
error(err);
}
},

exports.updateCadRef=function(params){var
data=params.data||null,
id=data.id,
ref=data.ref||null,
success=params.success||function(){},
error=params.error||function(){};
if(!id)return error("REFERENCIA SEM ID: ",ref);
if(!ref)return error("Refer\xEAncia n\xE3o informada para cadastrar");
try{
var db=Ti.Database.open("somaCRMDB");
db.execute("UPDATE cadRef SET AO_FILIAL = ?   , AO_CLIENTE = ?, AO_LOJA = ?  , AO_TIPO = ? , AO_NOMINS = ?   , AO_DATA = ?   , AO_SOCIO = ? , AO_CGC = ?  , AO_TELEFON = ?  , AO_CONTATO = ?, AO_OBSERV = ?, AO_DTALT = ?, DTSYNC = ?      , ID_CLIENTE = ?, RECNO = ?    , DEL = ? "+`WHERE ID = '${id}' `,ref.AO_FILIAL,ref.AO_CLIENTE,ref.AO_LOJA,ref.AO_TIPO,ref.AO_NOMINS,ref.AO_DATA,ref.AO_SOCIO,ref.AO_CGC,ref.AO_TELEFON,ref.AO_CONTATO,ref.AO_OBSERV,ref.AO_DTALT,ref.DTSYNC,ref.ID_CLIENTE,ref.RECNO,ref.DEL),
success({ref:ref.AO_NOMINS,id:id}),
db.close();
}catch(err){
error(err);
}
},

exports.updateRef=function(params){var
data=params.data||null,
id_antigo=data.id_cliLocal,
id_novo=data.ID||null,
A1_COD=data.A1_COD||null,
success=params.success||function(){},
error=params.error||function(){};
try{
var db=Ti.Database.open("somaCRMDB");
db.execute(`UPDATE cadRef SET AO_CLIENTE = ?, ID_CLIENTE = ? WHERE ID_CLIENTE = '${id_antigo}'`,A1_COD,id_novo),
success(),
db.close();
}catch(err){
error(err);
}
},

exports.getCadRef=function(params){var
id=params.cliente_id||null,
success=params.success||function(){},
error=params.error||function(){},
where=id?`WHERE ID_CLIENTE = '${id}'`:"",
refs=[];
try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute(`SELECT ID, AO_FILIAL, AO_CLIENTE, AO_LOJA, AO_TIPO, AO_NOMINS, AO_DATA, AO_SOCIO, AO_CGC, AO_TELEFON, AO_CONTATO, AO_OBSERV, AO_DTALT, ID_CLIENTE, RECNO, DEL FROM cadRef ${where}`);

result.isValidRow();)

refs.push({
ID:result.fieldByName("ID"),
AO_FILIAL:result.fieldByName("AO_FILIAL"),
AO_CLIENTE:result.fieldByName("AO_CLIENTE"),
AO_LOJA:result.fieldByName("AO_LOJA"),
AO_TIPO:result.fieldByName("AO_TIPO"),
AO_NOMINS:result.fieldByName("AO_NOMINS"),
AO_DATA:result.fieldByName("AO_DATA"),
AO_SOCIO:result.fieldByName("AO_SOCIO"),
AO_CGC:result.fieldByName("AO_CGC"),
AO_TELEFON:result.fieldByName("AO_TELEFON"),
AO_CONTATO:result.fieldByName("AO_CONTATO"),
AO_OBSERV:result.fieldByName("AO_OBSERV"),
AO_DTALT:result.fieldByName("AO_DTALT"),
ID_CLIENTE:result.fieldByName("ID_CLIENTE"),
RECNO:result.fieldByName("RECNO"),
DEL:result.fieldByName("DEL")}),

result.next();

result.close(),
db.close(),
success(refs);
}catch(err){
error(err);
}
},

exports.deleteCadRef=function(params){var
id=params.ref_id||null,
success=params.success||function(){},
error=params.error||function(){};
if(!id)return error("ID da refer\xEAncia inv\xE1lida");var
where=`WHERE ID = '${id}'`,
db=Ti.Database.open("somaCRMDB");
db.execute(`DELETE FROM cadRef ${where}`),
db.close();
},

exports.createTableCliente=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS cliente`),
db.execute("CREATE TABLE IF NOT EXISTS cliente ( ID INTEGER PRIMARY KEY, A1_FILIAL TEXT, A1_COD TEXT, A1_MOBILE INTEGER, A1_LOJA TEXT, A1_PESSOA TEXT, A1_CGC TEXT, A1_NOME TEXT, A1_NREDUZ TEXT, A1_TIPO TEXT, A1_ME TEXT, A1_END TEXT, A1_COMPLEM TEXT, A1_BAIRRO TEXT, A1_EST TEXT, A1_MUN TEXT, A1_CODMUN TEXT, A1_CEP TEXT, A1_LAT TEXT, A1_LONG TEXT, A1_DDD TEXT, A1_TEL TEXT, A1_TEL2 TEXT, A1_TEL3 TEXT, A1_TEL4 TEXT, A1_FAX TEXT, A1_CEL1 TEXT, A1_CEL2 TEXT, A1_ENDCOB TEXT, A1_BAIRROC TEXT, A1_MUNC TEXT, A1_ESTC TEXT, A1_CEPC TEXT, A1_INSCR TEXT, A1_PFISICA TEXT, A1_PAIS TEXT, A1_CODPAIS TEXT, A1_EXPORTA TEXT, A1_MOEDALC TEXT, A1_CONTA TEXT, A1_VEND TEXT, A1_VEND2 TEXT, A1_TPFRET TEXT, A1_RISCO TEXT, A1_LC REAL, A1_GRPTRIB TEXT, A1_MSBLQL TEXT, A1_SUPER TEXT, A1_OBSERV TEXT, A1_EMAIL TEXT, A1_DTALT TEXT, A1_DTCADAS TEXT, A1_DTNASC TEXT, A1_LIBEDIT TEXT, A1_GRPVEN TEXT, A1_TITVENC REAL, A1_TLC REAL, SLD REAL, SALDO REAL, A1_INFOROT TEXT, A1_OBSRC TEXT, A1_ULTCOM TEXT, A1_MSALDO REAL, A1_MCOMPRA REAL, A1_METR REAL, A1_MATR REAL, A1_TITPROT REAL, A1_DTULTIT TEXT, A1_CHQDEVO REAL, A1_DTULCHQ TEXT, A1_PRICOM TEXT, DTSYNC TEXT, A1_COND TEXT,  A1_COND2 TEXT,  E4_DESCRI TEXT, E4_DESCRI2 TEXT )");
},

exports.insertClientes=function(array){
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIM"),array)
db.execute("INSERT INTO cliente (A1_FILIAL, A1_COD, A1_MOBILE, A1_LOJA, A1_PESSOA, A1_CGC, A1_NOME, A1_NREDUZ, A1_TIPO, A1_ME, A1_END, A1_COMPLEM, A1_BAIRRO, A1_EST, A1_MUN, A1_CODMUN, A1_CEP, A1_LAT, A1_LONG, A1_DDD, A1_TEL, A1_TEL2, A1_TEL3, A1_TEL4, A1_FAX, A1_CEL1, A1_CEL2, A1_ENDCOB, A1_BAIRROC, A1_MUNC, A1_ESTC, A1_CEPC, A1_INSCR, A1_PFISICA, A1_PAIS, A1_CODPAIS, A1_VEND, A1_VEND2, A1_TPFRET, A1_RISCO, A1_LC, A1_GRPTRIB, A1_MSBLQL, A1_SUPER, A1_OBSERV, A1_EMAIL, A1_DTALT, A1_DTCADAS, A1_DTNASC, A1_LIBEDIT, A1_GRPVEN, A1_TITVENC, A1_TLC, SLD, A1_INFOROT, A1_OBSRC, A1_ULTCOM, A1_MSALDO, A1_MCOMPRA, A1_METR, A1_MATR, A1_TITPROT, A1_DTULTIT, A1_CHQDEVO, A1_DTULCHQ, A1_PRICOM, DTSYNC, A1_COND,  A1_COND2,  E4_DESCRI, E4_DESCRI2) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )",array[i].A1_FILIAL,array[i].A1_COD,array[i].A1_MOBILE,array[i].A1_LOJA,array[i].A1_PESSOA,array[i].A1_CGC,array[i].A1_NOME,array[i].A1_NREDUZ,array[i].A1_TIPO,array[i].A1_ME,array[i].A1_END,array[i].A1_COMPLEM,array[i].A1_BAIRRO,array[i].A1_EST,array[i].A1_MUN,array[i].A1_CODMUN,array[i].A1_CEP,array[i].A1_LAT,array[i].A1_LONG,array[i].A1_DDD,array[i].A1_TEL,array[i].A1_TEL2,array[i].A1_TEL3,array[i].A1_TEL4,array[i].A1_FAX,array[i].A1_CEL1,array[i].A1_CEL2,array[i].A1_ENDCOB,array[i].A1_BAIRROC,array[i].A1_MUNC,array[i].A1_ESTC,array[i].A1_CEPC,array[i].A1_INSCR,array[i].A1_PFISICA,array[i].A1_PAIS,array[i].A1_CODPAIS,array[i].A1_VEND,array[i].A1_VEND2,array[i].A1_TPFRET,array[i].A1_RISCO,array[i].A1_LC,array[i].A1_GRPTRIB,array[i].A1_MSBLQL,array[i].A1_SUPER,array[i].A1_OBSERV,array[i].A1_EMAIL,array[i].A1_DTALT,array[i].A1_DTCADAS,array[i].A1_DTNASC,array[i].A1_LIBEDIT,array[i].A1_GRPVEN,array[i].A1_TITVENC,array[i].A1_TLC,array[i].SLD,array[i].A1_INFOROT,array[i].A1_OBSRC,array[i].A1_ULTCOM,array[i].A1_MSALDO,array[i].A1_MCOMPRA,array[i].A1_METR,array[i].A1_MATR,array[i].A1_TITPROT,array[i].A1_DTULTIT,array[i].A1_CHQDEVO,array[i].A1_DTULCHQ,array[i].A1_PRICOM,array[i].DTSYNC,array[i].A1_COND,array[i].A1_COND2,array[i].E4_DESCRI,array[i].E4_DESCRI2);

db.execute("COMMIT");
},

exports.countCadCli=function(params){var
success=params.success||function(){},
error=params.error||function(){},
count=0,
db=Ti.Database.open("somaCRMDB");
try{
var result=db.execute(`SELECT COUNT(*) AS QNT FROM cliente WHERE A1_MOBILE = '1'`);
count=result.fieldByName("QNT"),
result.close(),
db.close(),
success(count);
}catch(err){
error(err),
console.log("DEU ERRO: ",err);
}
},

exports.getClientes=function(params){var
success=params.success||function(){},
error=params.error||function(){},
clientes=[],
db=Ti.Database.open("somaCRMDB");
try{for(
var result=db.execute("SELECT ID, A1_COD, A1_MOBILE, A1_CGC, A1_NOME, A1_MUN, A1_DDD, A1_TEL, A1_RISCO, A1_MSBLQL, A1_OBSERV, A1_GRPVEN, A1_TITVENC, A1_TLC, SLD, A1_ULTCOM FROM cliente");
result.isValidRow();)
clientes.push({
ID:result.fieldByName("ID"),
A1_COD:result.fieldByName("A1_COD"),
A1_MOBILE:result.fieldByName("A1_MOBILE"),
A1_CGC:result.fieldByName("A1_CGC"),
A1_NOME:result.fieldByName("A1_NOME"),
A1_MUN:result.fieldByName("A1_MUN"),
A1_DDD:result.fieldByName("A1_DDD"),
A1_TEL:result.fieldByName("A1_TEL"),
A1_RISCO:result.fieldByName("A1_RISCO"),
A1_MSBLQL:result.fieldByName("A1_MSBLQL"),
A1_OBSERV:result.fieldByName("A1_OBSERV"),
A1_GRPVEN:result.fieldByName("A1_GRPVEN"),
A1_TITVENC:result.fieldByName("A1_TITVENC"),
A1_TLC:result.fieldByName("A1_TLC"),
SLD:result.fieldByName("SLD"),
A1_ULTCOM:result.fieldByName("A1_ULTCOM")}),

result.next();

result.close(),
db.close(),
success(clientes);
}catch(err){
error(err),
console.log("DEU ERRO: ",err);
}
},

exports.getClienteDetail=function(params){var
id=params.id||null,
success=params.success||function(){},
error=params.error||function(){},
where="",
clientes=[],
db=Ti.Database.open("somaCRMDB");
id.ID?
where=`WHERE ID = '${id.ID}'`:
id.A1_COD?
where=`WHERE A1_COD = '${id.A1_COD}'`:
id.A1_MOBILE&&(
where=`WHERE A1_MOBILE = '${id.A1_MOBILE}'`);


try{for(
var result=db.execute("SELECT ID, A1_FILIAL, A1_COD, A1_MOBILE, A1_LOJA, A1_PESSOA, A1_CGC, A1_NOME, A1_NREDUZ, A1_TIPO, A1_ME, A1_END, A1_COMPLEM, A1_BAIRRO, A1_EST, A1_MUN, A1_CODMUN, A1_CEP, A1_DDD, A1_LAT, A1_LONG, A1_TEL, A1_TEL2, A1_TEL3, A1_TEL4, A1_FAX, A1_CEL1, A1_CEL2, A1_ENDCOB, A1_BAIRROC, A1_MUNC, A1_ESTC, A1_CEPC, A1_INSCR, A1_PFISICA, A1_PAIS, A1_CODPAIS, A1_VEND, A1_VEND2, A1_TPFRET, A1_RISCO, A1_LC, A1_GRPTRIB, A1_MSBLQL, A1_SUPER, A1_OBSERV, A1_EMAIL, A1_DTALT, A1_DTCADAS, A1_DTNASC, A1_LIBEDIT, A1_GRPVEN, A1_TITVENC, A1_TLC, SLD, A1_INFOROT, A1_OBSRC, A1_ULTCOM, A1_MSALDO, A1_MCOMPRA, A1_METR, A1_MATR, A1_TITPROT, A1_DTULTIT, A1_CHQDEVO, A1_DTULCHQ, A1_PRICOM, DTSYNC, A1_COND,  A1_COND2,  E4_DESCRI, E4_DESCRI2 FROM cliente "+`${where}`);
result.isValidRow();)
clientes.push({
ID:result.fieldByName("ID"),
A1_FILIAL:result.fieldByName("A1_FILIAL"),
A1_COD:result.fieldByName("A1_COD"),
A1_MOBILE:result.fieldByName("A1_MOBILE"),
A1_LOJA:result.fieldByName("A1_LOJA"),
A1_PESSOA:result.fieldByName("A1_PESSOA"),
A1_CGC:result.fieldByName("A1_CGC"),
A1_NOME:result.fieldByName("A1_NOME"),
A1_NREDUZ:result.fieldByName("A1_NREDUZ"),
A1_TIPO:result.fieldByName("A1_TIPO"),
A1_ME:result.fieldByName("A1_ME"),
A1_END:result.fieldByName("A1_END"),
A1_COMPLEM:result.fieldByName("A1_COMPLEM"),
A1_BAIRRO:result.fieldByName("A1_BAIRRO"),
A1_EST:result.fieldByName("A1_EST"),
A1_MUN:result.fieldByName("A1_MUN"),
A1_CODMUN:result.fieldByName("A1_CODMUN"),
A1_CEP:result.fieldByName("A1_CEP"),
A1_DDD:result.fieldByName("A1_DDD"),
A1_LAT:result.fieldByName("A1_LAT"),
A1_LONG:result.fieldByName("A1_LONG"),
A1_TEL:result.fieldByName("A1_TEL"),
A1_TEL2:result.fieldByName("A1_TEL2"),
A1_TEL3:result.fieldByName("A1_TEL3"),
A1_TEL4:result.fieldByName("A1_TEL4"),
A1_FAX:result.fieldByName("A1_FAX"),
A1_CEL1:result.fieldByName("A1_CEL1"),
A1_CEL2:result.fieldByName("A1_CEL2"),
A1_ENDCOB:result.fieldByName("A1_ENDCOB"),
A1_BAIRROC:result.fieldByName("A1_BAIRROC"),
A1_MUNC:result.fieldByName("A1_MUNC"),
A1_ESTC:result.fieldByName("A1_ESTC"),
A1_CEPC:result.fieldByName("A1_CEPC"),
A1_INSCR:result.fieldByName("A1_INSCR"),
A1_PFISICA:result.fieldByName("A1_PFISICA"),
A1_PAIS:result.fieldByName("A1_PAIS"),
A1_CODPAIS:result.fieldByName("A1_CODPAIS"),
A1_VEND:result.fieldByName("A1_VEND"),
A1_VEND2:result.fieldByName("A1_VEND2"),
A1_TPFRET:result.fieldByName("A1_TPFRET"),
A1_RISCO:result.fieldByName("A1_RISCO"),
A1_LC:result.fieldByName("A1_LC"),
A1_GRPTRIB:result.fieldByName("A1_GRPTRIB"),
A1_MSBLQL:result.fieldByName("A1_MSBLQL"),
A1_SUPER:result.fieldByName("A1_SUPER"),
A1_OBSERV:result.fieldByName("A1_OBSERV"),
A1_EMAIL:result.fieldByName("A1_EMAIL"),
A1_DTALT:result.fieldByName("A1_DTALT"),
A1_DTCADAS:result.fieldByName("A1_DTCADAS"),
A1_DTNASC:result.fieldByName("A1_DTNASC"),
A1_LIBEDIT:result.fieldByName("A1_LIBEDIT"),
A1_GRPVEN:result.fieldByName("A1_GRPVEN"),
A1_TITVENC:result.fieldByName("A1_TITVENC"),
A1_TLC:result.fieldByName("A1_TLC"),
SLD:result.fieldByName("SLD"),
A1_INFOROT:result.fieldByName("A1_INFOROT"),
A1_OBSRC:result.fieldByName("A1_OBSRC"),
A1_ULTCOM:result.fieldByName("A1_ULTCOM"),
A1_MSALDO:result.fieldByName("A1_MSALDO"),
A1_MCOMPRA:result.fieldByName("A1_MCOMPRA"),
A1_METR:result.fieldByName("A1_METR"),
A1_MATR:result.fieldByName("A1_MATR"),
A1_TITPROT:result.fieldByName("A1_TITPROT"),
A1_DTULTIT:result.fieldByName("A1_DTULTIT"),
A1_CHQDEVO:result.fieldByName("A1_CHQDEVO"),
A1_DTULCHQ:result.fieldByName("A1_DTULCHQ"),
A1_PRICOM:result.fieldByName("A1_PRICOM"),
DTSYNC:result.fieldByName("DTSYNC"),
A1_COND:result.fieldByName("A1_COND"),
A1_COND2:result.fieldByName("A1_COND2"),
E4_DESCRI:result.fieldByName("E4_DESCRI"),
E4_DESCRI2:result.fieldByName("E4_DESCRI2")}),

result.next();

result.close(),
db.close(),
success(clientes);
}catch(err){
error(err),
console.log("DEU ERRO: ",err);
}
},

exports.createTableReferencia=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS referencia`),
db.execute("CREATE TABLE IF NOT EXISTS referencia (ID INTEGER PRIMARY KEY, AO_FILIAL TEXT, AO_CLIENTE TEXT, AO_LOJA TEXT, AO_TIPO TEXT, AO_NOMINS TEXT, AO_DATA TEXT, AO_SOCIO TEXT, AO_CGC TEXT, AO_TELEFON TEXT, AO_CONTATO TEXT, AO_OBSERV TEXT, AO_DTALT TEXT, DTSYNC TEXT, ID_CLIENTE INTEGER, RECNO INTEGER, DEL TEXT)");
},

exports.insertReferencia=function(array){
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIM"),array)
db.execute("INSERT INTO referencia (AO_FILIAL, AO_CLIENTE, AO_LOJA, AO_TIPO, AO_NOMINS, AO_DATA, AO_SOCIO, AO_CGC, AO_TELEFON, AO_CONTATO, AO_OBSERV, AO_DTALT, DTSYNC, ID_CLIENTE, RECNO, DEL ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",array[i].AO_FILIAL,array[i].AO_CLIENTE,array[i].AO_LOJA,array[i].AO_TIPO,array[i].AO_NOMINS,array[i].AO_DATA,array[i].AO_SOCIO,array[i].AO_CGC,array[i].AO_TELEFON,array[i].AO_CONTATO,array[i].AO_OBSERV,array[i].AO_DTALT,array[i].DTSYNC,array[i].ID_CLIENTE,array[i].RECNO,array[i].DEL);

db.execute("COMMIT");
},

exports.getReferencia=function(params){var
success=params.success||function(){},
error=params.error||function(){},
cod_cli=params.cod_cli||null,
refs=[],
db=Ti.Database.open("somaCRMDB"),
where=``;
cod_cli&&(where=`WHERE AO_CLIENTE = '${cod_cli}'`);
try{for(
var result=db.execute("SELECT ID, AO_FILIAL, AO_CLIENTE, AO_LOJA, AO_TIPO, AO_NOMINS, AO_DATA, AO_SOCIO, AO_CGC, AO_TELEFON, AO_CONTATO, AO_OBSERV, AO_DTALT, DTSYNC, ID_CLIENTE, RECNO, DEL  FROM referencia "+`${where}`);
result.isValidRow();)
refs.push({
ID:result.fieldByName("ID"),
AO_FILIAL:result.fieldByName("AO_FILIAL"),
AO_CLIENTE:result.fieldByName("AO_CLIENTE"),
AO_LOJA:result.fieldByName("AO_LOJA"),
AO_TIPO:result.fieldByName("AO_TIPO"),
AO_NOMINS:result.fieldByName("AO_NOMINS"),
AO_DATA:result.fieldByName("AO_DATA"),
AO_SOCIO:result.fieldByName("AO_SOCIO"),
AO_CGC:result.fieldByName("AO_CGC"),
AO_TELEFON:result.fieldByName("AO_TELEFON"),
AO_CONTATO:result.fieldByName("AO_CONTATO"),
AO_OBSERV:result.fieldByName("AO_OBSERV"),
AO_DTALT:result.fieldByName("AO_DTALT"),
DTSYNC:result.fieldByName("DTSYNC"),
ID_CLIENTE:result.fieldByName("ID_CLIENTE"),
RECNO:result.fieldByName("RECNO"),
DEL:result.fieldByName("DEL")}),

result.next();

result.close(),
db.close(),
success(refs);
}catch(err){
error(err),
console.log("DEU ERRO: ",err);
}
},

exports.createTableUF=function(){
try{
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS uf`),
db.execute(`CREATE TABLE IF NOT EXISTS uf (ID INTEGER PRIMARY KEY, UF_COD TEXT, UF_NOME TEXT, UF_SIGLA TEXT)`),
db.close();
}catch(err){

return console.log("ERRO CREATE TABLE UF: ",err),err;
}
},

exports.createTableMnc=function(){
try{
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS mnc`),
db.execute(`CREATE TABLE IF NOT EXISTS mnc (MNC_CODMC INTEGER PRIMARY KEY, MNC_CODUF TEXT, MNC_CODMUN TEXT, MNC_MUN TEXT)`),
db.close();
}catch(err){

return console.log("ERRO CREATE TABLE MNC: ",err),err;
}
},

exports.insertUF=function(array){
try{
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIN"),array)
db.execute("INSERT INTO uf(UF_COD, UF_NOME, UF_SIGLA) VALUES (?, ?, ?) ",array[i].UF_COD,array[i].UF_NOME,array[i].UF_SIGLA);

db.execute("COMMIT"),
db.close();
}catch(err){
return err;
}
},

exports.insertMnc=function(array){
try{
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIN"),array)
db.execute("INSERT INTO mnc(MNC_CODMC  , MNC_CODUF, MNC_CODMUN  , MNC_MUN) VALUES (?, ?, ?, ?) ",array[i].MNC_CODMC,array[i].MNC_CODUF,array[i].MNC_CODMUN,array[i].MNC_MUN);

db.execute("COMMIT"),
db.close();
}catch(err){
return err;
}
},

exports.getUF=function(id){var
where="",
ufs=[];
id&&(where=`WHERE ID = ${id}`);
try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute(`SELECT `+`ID, UF_COD, UF_NOME, UF_SIGLA FROM uf ${where}`);
result.isValidRow();)
ufs.push({
ID:result.fieldByName("ID"),
UF_COD:result.fieldByName("UF_COD"),
UF_NOME:result.fieldByName("UF_NOME"),
UF_SIGLA:result.fieldByName("UF_SIGLA")}),

result.next();



return result.close(),db.close(),ufs;
}catch(err){

return console.log("DEU ERRO: ",err),err;
}
},

exports.getMnc=function(params){var
where="",
mncs=[];
params&&params.codmc?where=`WHERE MNC_CODMC = ${params.codmc}`:params&&params.coduf?where=`WHERE MNC_CODUF = ${params.coduf}`:params&&params.codmun&&(where=`WHERE MNC_CODMUN = ${params.codmun}`);

try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute(`SELECT `+`MNC_CODMC, MNC_CODUF, MNC_CODMUN, MNC_MUN FROM mnc ${where}`);
result.isValidRow();)
mncs.push({
MNC_CODMC:result.fieldByName("MNC_CODMC"),
MNC_CODUF:result.fieldByName("MNC_CODUF"),
MNC_CODMUN:result.fieldByName("MNC_CODMUN"),
MNC_MUN:result.fieldByName("MNC_MUN")}),

result.next();



return result.close(),db.close(),mncs;
}catch(err){

return console.log("DEU ERRO: ",err),err;
}
},

exports.createTableTabelas=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS tabelaPreco`),
db.execute("CREATE TABLE IF NOT EXISTS tabelaPreco (ID INTEGER PRIMARY KEY, Filial TEXT, CodTab TEXT, Descr TEXT, DtTab TEXT, TpVnd TEXT, Vend TEXT)"),
db.close();
},

exports.insertTabelas=function(array,vend){
try{
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIN"),array)
array[i].Vend=vend,
db.execute("INSERT INTO tabelaPreco(Filial, CodTab, Descr, DtTab, TpVnd, Vend) VALUES (?, ?, ?, ?, ?, ?) ",array[i].Filial,array[i].CodTab,array[i].Descr,array[i].DtTab,array[i].TpVnd,array[i].Vend);

db.execute("COMMIT"),
db.close();
}catch(err){
return err;
}
},

exports.getTabelas=function(params){var
where="",
tabelas=[];
params&&params.vend&&(where=`WHERE Vend = ${params.vend}`);

try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute(`SELECT `+`Filial, CodTab, Descr, DtTab, TpVnd, Vend FROM tabelaPreco ${where}`);
result.isValidRow();)
tabelas.push({
Filial:result.fieldByName("Filial"),
CodTab:result.fieldByName("CodTab"),
Descr:result.fieldByName("Descr"),
DtTab:result.fieldByName("DtTab"),
TpVnd:result.fieldByName("TpVnd"),
Vend:result.fieldByName("Vend")}),

result.next();



return result.close(),db.close(),tabelas;
}catch(err){

return console.log("DEU ERRO: ",err),err;
}
},

exports.createTableCondPag=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS condPag`),
db.execute("CREATE TABLE IF NOT EXISTS condPag (ID INTEGER PRIMARY KEY, CODCLI INTEGER, GRPVEN INTEGER, CONDPG TEXT, TIPO TEXT, COND TEXT, DESCRI TEXT, ACRSVEN REAL, DESCVEN REAL)"),
db.close();
},

exports.insertCondPag=function(array){
try{
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIN"),array)
db.execute("INSERT INTO condPag(CODCLI, GRPVEN, CONDPG, TIPO, COND, DESCRI, ACRSVEN, DESCVEN) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ",array[i].CODCLI,array[i].GRPVEN,array[i].CONDPG,array[i].TIPO,array[i].COND,array[i].DESCRI,array[i].ACRSVEN,array[i].DESCVEN);

db.execute("COMMIT"),
db.close();
}catch(err){
return err;
}
},

exports.getCondPag=function(params){var
where="",
condPag=[];
params&&params.A1_COD&&(where=`WHERE CODCLI = '${params.A1_COD}'`);
try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute(`SELECT `+`CODCLI, GRPVEN, CONDPG, TIPO, COND, DESCRI, ACRSVEN, DESCVEN FROM condPag ${where}`);
result.isValidRow();)
condPag.push({
CODCLI:result.fieldByName("CODCLI"),
GRPVEN:result.fieldByName("GRPVEN"),
CONDPG:result.fieldByName("CONDPG"),
TIPO:result.fieldByName("TIPO"),
COND:result.fieldByName("COND"),
DESCRI:result.fieldByName("DESCRI"),
ACRSVEN:result.fieldByName("ACRSVEN"),
DESCVEN:result.fieldByName("DESCVEN")}),

result.next();



return result.close(),db.close(),condPag;
}catch(err){

return console.log("DEU ERRO: ",err),err;
}
},

exports.createTableProd=function(){
var db=Ti.Database.open("somaCRMDB");
db.execute(`DROP TABLE IF EXISTS produto`),
db.execute("CREATE TABLE IF NOT EXISTS produto (ID INTEGER PRIMARY KEY, CODTAB TEXT, CATEGO TEXT, DESCCAT TEXT, CODPRO TEXT, DESCR TEXT, UN TEXT, PESO REAL, COMIS REAL, ARMZ TEXT, PRVEN REAL, GRUPO TEXT, DA1_ESTADO TEXT, B1_IPI REAL, B1_VLR_ICM REAL, ALIQIN REAL, ALIQEX REAL, MVA REAL)"),
db.close();
},

exports.insertProds=function(array){
try{
var db=Ti.Database.open("somaCRMDB");

for(var i in db.execute("BEGIN"),array)
db.execute("INSERT INTO produto(CODTAB, CATEGO, DESCCAT, CODPRO, DESCR, UN, PESO, COMIS, ARMZ, PRVEN, GRUPO, DA1_ESTADO, B1_IPI, B1_VLR_ICM, ALIQIN, ALIQEX, MVA) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ",array[i].CODTAB,array[i].CATEGO,array[i].DESCCAT,array[i].CODPRO,array[i].DESCR,array[i].UN,array[i].PESO,array[i].COMIS,array[i].ARMZ,array[i].PRVEN,array[i].GRUPO,array[i].DA1_ESTADO,array[i].B1_IPI,array[i].B1_VLR_ICM,array[i].ALIQIN,array[i].ALIQEX,array[i].MVA);

db.execute("COMMIT"),
db.close();
}catch(err){
return err;
}
},

exports.getProds=function(params){var
where="",
prods=[];
params&&params.CODTAB&&(where=`WHERE CODTAB = '${params.CODTAB}'`);
try{for(var
db=Ti.Database.open("somaCRMDB"),
result=db.execute("SELECT CODTAB, CATEGO, DESCCAT, CODPRO, DESCR, UN, PESO, COMIS, ARMZ, PRVEN, GRUPO, DA1_ESTADO, B1_IPI, B1_VLR_ICM, ALIQIN, ALIQEX, "+`MVA FROM produto ${where}`);
result.isValidRow();)
prods.push({
CODTAB:result.fieldByName("CODTAB"),
CATEGO:result.fieldByName("CATEGO"),
DESCCAT:result.fieldByName("DESCCAT"),
CODPRO:result.fieldByName("CODPRO"),
DESCR:result.fieldByName("DESCR"),
UN:result.fieldByName("UN"),
PESO:result.fieldByName("PESO"),
COMIS:result.fieldByName("COMIS"),
ARMZ:result.fieldByName("ARMZ"),
PRVEN:result.fieldByName("PRVEN"),
GRUPO:result.fieldByName("GRUPO"),
DA1_ESTADO:result.fieldByName("DA1_ESTADO"),
B1_IPI:result.fieldByName("B1_IPI"),
B1_VLR_ICM:result.fieldByName("B1_VLR_ICM"),
ALIQIN:result.fieldByName("ALIQIN"),
ALIQEX:result.fieldByName("ALIQEX"),
MVA:result.fieldByName("MVA")}),

result.next();



return result.close(),db.close(),prods;
}catch(err){

return console.log("DEU ERRO: ",err),err;
}
},

exports.clearTable=function(params){var
table=params.table,
where=params.where||"",
db=Ti.Database.open("somaCRMDB");
try{
db.execute(`DELETE FROM '${table}' ${where}`),
db.close();
}catch(err){
console.log("DEU ERRO: ",err);
}
};