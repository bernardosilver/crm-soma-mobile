var
OptionDialog=require("OptionDialog"),
chooseCallBack=function(){};

function callbackOption(e){









return 0==e.index?requestCameraPermission(function(){showCamera()}):1==e.index&&requestCameraPermission(function(){showGallery()}),!0;
}

function showCamera(){
Ti.API.error("SHOW CAMERA"),
Ti.Media.showCamera({
success:function(event){
var preview=event.media;
editPhoto(preview);
},
cancel:function(event){
Ti.API.info("CANCEL");
},
error:function(error){
Ti.API.error("ERRO"),
Ti.API.error(JSON.stringify(error));
},
saveToPhotoGallery:!0,
whichCamera:Titanium.Media.CAMERA_REAR,
showControls:!0,
mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
autohide:!0});

}

function showGallery(){

Ti.Media.openPhotoGallery({
success:function(event){
var preview=event.media;
editPhoto(preview);
},
cancel:function(){},
error:function(error){
Ti.API.error("ERRO"),
Ti.API.error(JSON.stringify(error));
},
showControls:!0,
mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
autohide:!0});

}

function requestCameraPermission(action){!0?

!1==Titanium.Android.hasPermission("android.permission.CAMERA")||!1==Titanium.Android.hasPermission("android.permission.WRITE_EXTERNAL_STORAGE")?
Titanium.Android.requestPermissions(["android.permission.CAMERA","android.permission.WRITE_EXTERNAL_STORAGE"],function(e){
!0==e.success&&
action();

}):

action():


Titanium.Media.hasCameraPermissions()?
Titanium.Media.requestCameraPermissions(function(e){
!0==e.success&&
action();

}):

action();


}

function editPhoto(preview){
var previewImage=preview;!1,









chooseCallBack(previewImage);
}

exports.showOptions=function(_chooseCallBack){
chooseCallBack=_chooseCallBack||function(){},
OptionDialog.show({callback:callbackOption,options:["Tirar Foto","Escolher Foto"]});
};