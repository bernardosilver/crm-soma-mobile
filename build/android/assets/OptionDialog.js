var
callback=function(){},
options=[],
dialog=null,
contatos=[];

function createAndroidOption(option,index){var

array=option.split(": "),

view=Ti.UI.createView({
height:"70dp",
width:Ti.UI.SIZE,
left:0}),


viewLine=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
bottom:0,
left:"15dp",
right:"15dp",
backgroundColor:Alloy.Globals.BACKGROUND_COLOR}),


view2=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.SIZE,
layout:"vertical",
left:"20dp"}),


labelEtiqueta=Ti.UI.createLabel({
color:Alloy.Globals.BLUE_COLOR,
left:0,
font:{
fontSize:12},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
text:2==array.length?array[0]:""}),


labelOption=Ti.UI.createLabel({
color:"#000",
left:0,
font:{
fontSize:16,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
text:2==array.length?array[1]:option});












return view.add(viewLine),view.add(view2),2==array.length&&view2.add(labelEtiqueta),view2.add(labelOption),view.addEventListener("click",function(e){callback({index:index}),dialog.hide()}),view;
}

function createAndroidView(){
var androidView=Ti.UI.createView({
layout:"vertical",
backgroundColor:"white"});

for(var i in options)
androidView.add(createAndroidOption(options[i],i));

return androidView;
}

function createDialog(){!0?(

dialog=Ti.UI.createOptionDialog(),
dialog.setAndroidView(createAndroidView())):(

options.push("Cancelar"),
dialog=Ti.UI.createOptionDialog({
options:options,
cancel:options.length-1,
selectedIndex:options.length-1,
destructive:options.length-1}),

dialog.addEventListener("click",callback)),

dialog.show();
}

exports.show=function(params){
callback=params.callback||function(){},
options=params.options||[],
contatos=params.contatos||[],
createDialog();
};