
function HttpUtil(){












function addQueryURL(url,params){
var params_url="";
for(prop in params)
params_url+=""==params_url?"?":"&",

null!=params[prop]&&(params_url+=prop+"="+encodeURIComponent(params[prop]));


return Ti.API.info(url+""+params_url),url+""+params_url;
}

function setHeaders(headers){
for(var i in headers)
httpClient.setRequestHeader(headers[i].key,headers[i].value);

}var timeout=3e4,token=Alloy.Globals.getLocalDataUsuario()?Alloy.Globals.getLocalDataUsuario().token:null,headers=[{key:"authorization",value:"123456"},{key:"token",value:token}],httpClient=Titanium.Network.createHTTPClient({timeout:30000}),errorCallBack=function(){},successCallBack=function(){};

this.post=function(params){var
url=params.url,
data=params.data||{};

params.headers&&(headers=headers.concat(params.headers)),

!0==Ti.Network.online?(
httpClient.open("POST",url),
setHeaders(headers),
httpClient.send(JSON.stringify(data))):

errorCallBack();

},

this.get=function(params){var
data=params.data||{},
url=params.url;

params.headers&&(headers=headers.concat(params.headers)),

!0==Ti.Network.online?(
httpClient.open("GET",addQueryURL(url,data)),
setHeaders(headers),
httpClient.send()):

errorCallBack();

},

this.delete=function(params){var
data=params.data||{},
url=params.url;

params.headers&&(headers=headers.concat(params.headers)),

!0==Ti.Network.online?(
httpClient.open("DELETE",addQueryURL(url,data)),
setHeaders(headers),
httpClient.send()):

errorCallBack();

},

this.upload=function(params){var
data=params.data||{},
url=params.url;

params.headers&&(headers=headers.concat(params.headers)),

!0==Ti.Network.online?(
httpClient.open("POST",url),
setHeaders(headers),
httpClient.send(data)):

errorCallBack();

},

this.put=function(params){var
url=params.url,
data=params.data||{};

params.headers&&(headers=headers.concat(params.headers)),

!0==Ti.Network.online?(
httpClient.open("PUT",url),
setHeaders(headers),
httpClient.send(JSON.stringify(data))):

errorCallBack();

},

this.setSuccessCallBack=function(action){
successCallBack=action,
httpClient.onload=function(e){
try{
var json=this.responseText?JSON.parse(this.responseText):{};
successCallBack(json,this);
}catch(e){
Ti.API.error("Erro: "+e.message),
errorCallBack({error:e.message});
}
};
},

this.setErrorCallBack=function(action){
errorCallBack=action,
httpClient.onerror=function(e){
Ti.API.error("onerror called, readyState = "+this.readyState+" error = "+e.error+" text= "+this.responseText);
try{
var json=this.responseText?JSON.parse(this.responseText):{};
json.code=e.code?parseInt(e.code):0,
errorCallBack(json);
}catch(e){
errorCallBack({
error:this.responseText,
code:500});

}
};
};
}

module.exports=HttpUtil;