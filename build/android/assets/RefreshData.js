var moment=require("alloy/moment");

exports.sendLogin=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"login",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.post(params);
},

exports.sendToken=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"token",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.post(params);
},

exports.updateUsuario=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"usuario",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.put(params);
},

exports.getEstado=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"estado",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.getMnc=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"municipio",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.getCliente=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"cliente",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.cadCliente=function(_params){var
HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"cliente",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.post(params);
},

exports.cadReferencia=function(_params){var
HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"referencia",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.post(params);
},

exports.getReferencia=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"referencia",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.getTabelas=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"tabela",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.getProduto=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"produto",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
},

exports.getCondPag=function(_params){var

HttpUtil=require("HttpUtil"),
httpUtil=new HttpUtil,

params=_params?_params:{};

httpUtil.setSuccessCallBack(params.successCallBack),
httpUtil.setErrorCallBack(params.errorCallBack),

params.url=Alloy.Globals.URL_API+"condPag",
params.headers=[{key:"Content-Type",value:"application/json"}],

Ti.API.info(JSON.stringify(params)),

httpUtil.get(params);
};