




var Alloy=require("/alloy"),
_=Alloy._,
Backbone=Alloy.Backbone;global.Backbone =Backbone,global._ =_,global.Alloy =Alloy,













Alloy.Globals.DEBUG=!1,


Alloy.Globals.URL_API="http://192.168.0.241:9010/app/",



Alloy.Globals.BLUE_COLOR="#3060CA",
Alloy.Globals.MAIN_COLOR="#28a745",
Alloy.Globals.DARK_MAIN_COLOR="#205D33",
Alloy.Globals.LIGHT_GRAY_COLOR2="#ECF0F1",
Alloy.Globals.GRAY_COLOR="#B2B2B2",
Alloy.Globals.DARK_GRAY_COLOR="#4C5454",
Alloy.Globals.WHITE_COLOR="#FFF",
Alloy.Globals.RED_COLOR="#FF4B4B",


Alloy.Globals.defaultBackgroundColor="#f1f1f1";

var mask=require("Mask");global.mask =mask,


Alloy.Globals.isLogged=function(){return!!
Ti.App.Properties.getObject("usuario");



},

Alloy.Globals.setLocalDataUsuario=function(data){
Ti.App.Properties.setObject("usuario",data);
},

Alloy.Globals.removeLocalDataUsuario=function(){
Ti.App.Properties.removeProperty("usuario");
},

Alloy.Globals.getLocalDataUsuario=function(){
var usuario=Ti.App.Properties.getObject("usuario",null);
return usuario?usuario:null;
},

Alloy.Globals.setSync=function(date){
Ti.App.Properties.setString("sync",date);
},

Alloy.Globals.removeSync=function(){
Ti.App.Properties.removeProperty("sync");
},

Alloy.Globals.getSync=function(){
var sincronizacao=Ti.App.Properties.getString("sync",null);
return sincronizacao?sincronizacao:null;
},


Alloy.Globals.setCountCadCli=function(value){
Ti.App.Properties.setInt("cadCli",value);
},

Alloy.Globals.removeCountCadCli=function(){
Ti.App.Properties.removeProperty("cadCli");
},

Alloy.Globals.getCountCadCli=function(){
var sincronizacao=Ti.App.Properties.getString("cadCli",null);
return sincronizacao?sincronizacao:null;
},

Alloy.Globals.setTitleWithDescription=function(title,text){var
texto=title+" "+text,
attr=Ti.UI.createAttributedString({
text:texto,
attributes:[
{
type:Ti.UI.ATTRIBUTE_FONT,
value:{fontSize:14},
range:[texto.indexOf(text),text.length]}]});



return attr;
},


Alloy.Globals.createSectionHeader=function(label,obs){
return Alloy.createWidget("HeaderList",{title:label,subTitle:obs}).getView();
},

Alloy.Globals.mnum=function(v){


return v=v.replace(/\D/g,""),v;
},

Alloy.Globals.showAlert=function(argsOrTitle,message,buttons,listener){var

args="object"==typeof argsOrTitle?argsOrTitle?argsOrTitle:{}:{},

title=args.title||argsOrTitle||"Alerta",
message=args.message||message||"",
buttons=args.buttons||buttons||["Ok"],
listener=args.listener||listener||function(){},
hasTextField=args.hasTextField||!1,
defaultText=args.defaultText||"",
keyboardType=args.keyboardType||Titanium.UI.KEYBOARD_TYPE_DEFAULT,

textField=!0!=hasTextField||0?null:Ti.UI.createTextField({keyboardType:keyboardType});

textField&&
textField.setValue(defaultText);

var dialog;
































return dialog=0?Ti.UI.createAlertDialog({title:title,style:Titanium.UI.iOS.AlertDialogStyle.DEFAULT,message:message,keyboardType:keyboardType,buttonNames:buttons}):Ti.UI.createAlertDialog({title:title,androidView:!0==hasTextField?textField:null,message:message,buttonNames:buttons?buttons:["Ok"]}),dialog.show(),listener&&dialog.addEventListener("click",function(e){var textValue=null;hasTextField&&(textValue=0?e.text:textField.getValue()),listener({index:e.index,text:textValue})}),dialog;
},

Alloy.Globals.removeAcento=function(str){
var rep=" ";

str=str.toLowerCase().replace(/\s+/g," ");




for(var from="\xE1\xE0\xE3\xE2\xE4\xE9\xE8\xEA\xEB\xED\xEC\xEE\xEF\xF3\xF2\xF5\xF4\xF6\xFA\xF9\xFB\xFC\xE7\xC1\xC0\xC3\xC2\xC4\xC9\xC8\xCA\xCB\xCD\xCC\xCE\xCF\xD3\xD2\xD5\xD6\xD4\xDA\xD9\xDB\xDC\xC7",to="aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC",i=0,l=from.length;i<l;i++)
str=str.replace(
new RegExp(from.charAt(i),"g"),
to.charAt(i));





return str=str.replace(/[^a-z0-9 ]/g,"").replace(/-+/g," "),str;
},

Alloy.Globals.PixelsToDPUnits=function(ThePixels){
return ThePixels/(Titanium.Platform.displayCaps.dpi/160);
},

Alloy.Globals.DPUnitsToPixels=function(TheDPUnits){
return TheDPUnits*(Titanium.Platform.displayCaps.dpi/160);
},

Alloy.Globals.createTextEndereco=function(endereco){
var text="";





return text=endereco.A1_END,text=endereco.A1_COMPLEM&&""!=endereco.A1_COMPLEM.trim()?text+" - "+endereco.A1_COMPLEM:text,text=text+" - "+endereco.A1_BAIRRO,text=text+"\n"+endereco.A1_MUN+" - "+endereco.A1_EST,text=endereco.A1_INFOROT&&""!=endereco.A1_INFOROT.trim()?text+"\nRefer\xEAncia: "+endereco.A1_INFOROT:text,text;
},

Alloy.Globals.createTextEnderecoCob=function(endereco){
var text="";



return text=endereco.A1_ENDCOB,text=text+" - "+endereco.A1_BAIRROC,text=text+"\n"+endereco.A1_MUNC+" - "+endereco.A1_ESTC,text;
},

Alloy.Globals.createTextCliente=function(cliente){var
cpfcnpj="F"==cliente.A1_PESSOA?"CPF: "+mask.cpf(cliente.A1_CGC):"CNPJ: "+mask.cnpj(cliente.A1_CGC),
text="";



return text=cliente.A1_NOME,text=cliente.A1_NREDUZ&&""!=cliente.A1_NREDUZ.trim()?text+" - "+cliente.A1_NREDUZ:text,text=text+"\n"+cpfcnpj,text;
},

Alloy.Globals.createTextSocios=function(socios){
var text=`${socios[0].AO_NOMINS} - ${socios[0].AO_CGC}.`;

return 1<socios.length&&(text+="\nMais 1..."),text;
},

Alloy.Globals.createTextRefCom=function(refs){var
count=refs.length,
text=`${count} de 3 referências cadastradas.`;
return text;
},

Alloy.Globals.createTextRefBanc=function(bancos){
var text=`${bancos[0].AO_NOMINS}.`;

return 1<bancos.length&&(text+="\nMais 1..."),text;
},

Alloy.Globals.setNameWithCode=function(nome,cod){var
aux=cod?cod:"",
str=nome?nome:"N\xE3o informado.",
texto=aux+": "+str,
attr=Ti.UI.createAttributedString({
text:texto,
attributes:[
{
type:Ti.UI.ATTRIBUTE_FONT,
value:{fontSize:14,fontWeight:"bold"},
range:[texto.indexOf(cod),aux.length]}]});



return attr;
},

Alloy.createController("index");