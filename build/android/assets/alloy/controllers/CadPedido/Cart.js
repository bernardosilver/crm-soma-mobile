var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





































































































































function doClose(){
$.mainWindow.close();
}

function createResumo(){var
frete=OrderManager.getFrete(),
produtos=OrderManager.getItems(),
total=OrderManager.getTotalPedido();

console.log("FRETE DO PEDIDO: ",frete),
console.log("PRODUTOS DO PEDIDO: ",produtos),
console.log("TOTAL DO PEDIDO: ",total),

$.lbQnt.setText(`${total.qntItens} item(s)`),
$.lbPreco.setText(`R$${Mask.real(parseFloat(total.totalProd).toFixed(2))}`),
$.lbPeso.setText(`${parseFloat(total.pesoTotal).toFixed(2)}`),
$.lbFrete.setText(`R$${Mask.real(parseFloat(total.valorFret).toFixed(2))}`),
$.lbTotal.setText(`Total pedido + frete: R$${Mask.real(parseFloat(total.valor).toFixed(2))}`),

produtos&&produtos.length&&createListView(produtos);
}


function createListView(produtos){var
sections=[],
headerSection=Ti.UI.createListSection();
headerSection.setItems(createList(produtos)),
sections.push(headerSection),
$.listView.setSections(sections);
}

function createList(prods){
var array=[];
for(var i in prods)
array.push(createItem(prods[i]));

return array;
}

function createItem(produto){
return{
template:"templateProduto",
lbNome:{
attributedString:Alloy.Globals.setNameWithCode(produto.DESCR,produto.CODPRO)},

lbQtd:{
text:produto.CK_QTDVEN+" x "},

lbPreco:{
text:`R$${Mask.real(parseFloat(produto.CK_VALOR).toFixed(2))}`},

properties:{
data:produto,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_NONE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor}};


}

function generateProdInfo(prod){let

aux=0,
dif=0,
fc=0;const
condPag=OrderManager.getPagamento(),
parcelas=OrderManager.getParcela(),
dtprv=moment().hours(0).minutes(0).seconds(0).milliseconds(0);
dtprv.add(5,"days");let
dtvenc="",
obj={
PRVEN:prod.PRVEN,
CONDPG:condPag.CJ_CONDPAG,
ACRSVEN:condPag.ACRSVEN,
DESCVEN:condPag.DESCVEN,
prcVnd:0,
prcUni:0,
desconto:OrderManager.getDesconto()};



if(console.log("OBJETO: ",condPag,obj),"003"==obj.CONDPG){
for(var i in parcelas)
-1<i.indexOf("CJ_DATA")&&parcelas[i]&&(
console.log("DIA PARCELA: ",parcelas[i]),
aux+=1,
dtvenc=moment(parcelas[i]),
dif=dtvenc.diff(dtprv,"days"),
console.log("DIFERENCA: ",dif,dtvenc,dtprv),
fc+=7e-4*(dif-7),
console.log("FATOR: ",fc));


var media=fc/aux;
obj.prcVnd=obj.PRVEN*parseFloat(media)+obj.PRVEN,
obj.prcVnd.toFixed(2),
console.log("MEDIA: ",media),
console.log("VENDA: ",obj.prcVnd);
}else


obj.prcVnd=obj.ACRSVEN?obj.PRVEN+obj.PRVEN*obj.ACRSVEN/100:

obj.DESCVEN?
obj.PRVEN-obj.PRVEN*obj.DESCVEN/100:


obj.PRVEN;



return obj.prcVnd.toFixed(2),obj;
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/Cart",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Carrinho",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId112(){$.__views.mainWindow.removeEventListener("open",__alloyId112),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId112)}$.__views.__alloyId113=Ti.UI.createView({id:"__alloyId113"}),$.__views.mainWindow.add($.__views.__alloyId113),$.__views.viewResumo=Ti.UI.createView({id:"viewResumo",top:"0dp",height:Ti.UI.SIZE,borderColor:"#205D33",borderRadius:.5,borderWidth:2,backgroundColor:"white"}),$.__views.__alloyId113.add($.__views.viewResumo),$.__views.lbQnt=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"lbQnt",top:"5dp",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,text:"Resumo do seu pedido"}),$.__views.viewResumo.add($.__views.lbQnt),$.__views.__alloyId114=Ti.UI.createView({top:"35dp",bottom:"15dp",left:"0dp",height:Ti.UI.SIZE,width:"50%",layout:"vertical",id:"__alloyId114"}),$.__views.viewResumo.add($.__views.__alloyId114),$.__views.__alloyId115=Ti.UI.createView({height:Ti.UI.SIZE,left:"10dp",layout:"horizontal",id:"__alloyId115"}),$.__views.__alloyId114.add($.__views.__alloyId115),$.__views.imgQnt=Ti.UI.createImageView({id:"imgQnt",width:"33dp",height:"33dp",image:"/images/itens-cart-green.png"}),$.__views.__alloyId115.add($.__views.imgQnt),$.__views.lbQnt=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},id:"lbQnt",left:"5dp",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,text:"3 item(s)"}),$.__views.__alloyId115.add($.__views.lbQnt),$.__views.__alloyId116=Ti.UI.createView({height:Ti.UI.SIZE,left:"10dp",top:"5dp",layout:"horizontal",id:"__alloyId116"}),$.__views.__alloyId114.add($.__views.__alloyId116),$.__views.imgPreco=Ti.UI.createImageView({id:"imgPreco",width:"33dp",height:"33dp",image:"/images/price.png"}),$.__views.__alloyId116.add($.__views.imgPreco),$.__views.lbPreco=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},id:"lbPreco",left:"5dp",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,text:"R$999.999,99"}),$.__views.__alloyId116.add($.__views.lbPreco),$.__views.__alloyId117=Ti.UI.createView({top:"35dp",bottom:"15dp",height:Ti.UI.SIZE,right:"0dp",width:"50%",layout:"vertical",id:"__alloyId117"}),$.__views.viewResumo.add($.__views.__alloyId117),$.__views.__alloyId118=Ti.UI.createView({height:Ti.UI.SIZE,id:"__alloyId118"}),$.__views.__alloyId117.add($.__views.__alloyId118),$.__views.lbPeso=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},id:"lbPeso",right:"47dp",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,text:"999.999,99 Kg"}),$.__views.__alloyId118.add($.__views.lbPeso),$.__views.imgPeso=Ti.UI.createImageView({id:"imgPeso",width:"33dp",right:"10dp",height:"33dp",image:"/images/peso.png"}),$.__views.__alloyId118.add($.__views.imgPeso),$.__views.__alloyId119=Ti.UI.createView({height:Ti.UI.SIZE,top:"5dp",id:"__alloyId119"}),$.__views.__alloyId117.add($.__views.__alloyId119),$.__views.lbFrete=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},id:"lbFrete",right:"47dp",textAlign:Ti.UI.TEXT_ALIGNMENT_RIGHT,text:"R$999.999,99"}),$.__views.__alloyId119.add($.__views.lbFrete),$.__views.imgFrete=Ti.UI.createImageView({id:"imgFrete",width:"33dp",right:"10dp",height:"33dp",image:"/images/frete.png"}),$.__views.__alloyId119.add($.__views.imgFrete),$.__views.lbTotal=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"lbTotal",top:"115dp",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,text:"Total pedido + frete: R$999.999,99"}),$.__views.viewResumo.add($.__views.lbTotal);var __alloyId120={},__alloyId123=[],__alloyId125={type:"Ti.UI.ImageView",properties:{touchEnabled:!1,image:"/images/removeRed.png",top:"17dp",left:"10dp",width:"18dp",height:"18dp"}};__alloyId123.push(__alloyId125);var __alloyId127={type:"Ti.UI.Label",bindId:"lbQtd",properties:{color:Alloy.Globals.BLUE_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbQtd",height:"40dp",top:"5dp",left:"40dp"}};__alloyId123.push(__alloyId127);var __alloyId129={type:"Ti.UI.Label",bindId:"lbNome",properties:{font:{fontSize:"13dp"},touchEnabled:!1,bindId:"lbNome",height:"40dp",width:Ti.UI.FILL,top:"5dp",left:"80dp",right:"85dp",maxLines:2,color:"gray"}};__alloyId123.push(__alloyId129);var __alloyId131={type:"Ti.UI.Label",bindId:"lbPreco",properties:{font:{fontSize:"15dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbPreco",top:"15dp",right:"15dp",height:Ti.UI.SIZE,color:"gray"}};__alloyId123.push(__alloyId131);var __alloyId122={properties:{name:"templateProduto",height:Ti.UI.SIZE,backgroundColor:"transparent"},childTemplates:__alloyId123};__alloyId120.templateProduto=__alloyId122;var __alloyId133={properties:{name:"templateEmpty",backgroundColor:"white",height:"150dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId120.templateEmpty=__alloyId133,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId120,id:"listView",top:"150dp"}),$.__views.__alloyId113.add($.__views.listView),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId113}),$.__views.popup.setParent($.__views.__alloyId113),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId113}),$.__views.activityIndicator.setParent($.__views.__alloyId113),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,moment=require("alloy/moment"),OrderManager=require("OrderManager"),LocalData=require("LocalData"),Mask=require("Mask");

$.mainWindow.addEventListener("focus",function(e){
createResumo();
}),

$.listView.addEventListener("itemclick",function(e){const
click=e.section.getItemAt(e.itemIndex),
produto=click.properties.data,
info=generateProdInfo(produto);
console.log("INFO PROD: ",info),
$.popup.showPopUpAddItem(produto,info,function(values,prod){
$.popup.hide(),
OrderManager.updateItem(values,prod),
createResumo();


});
}),









_.extend($,exports);
}

module.exports=Controller;