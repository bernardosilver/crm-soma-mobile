var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/Parcela",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};











if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Parcelas"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){
function __alloyId150(){
$.__views.mainWindow.removeEventListener("open",__alloyId150),
$.__views.mainWindow.activity?(
$.__views.mainWindow.activity.actionBar.title="Parcelas",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(

Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),
Ti.API.warn("UI component which does not have an Android activity. Android Activities"),
Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."));

}
$.__views.mainWindow.addEventListener("open",__alloyId150);
}
$.__views.scrollView=Ti.UI.createScrollView(
{id:"scrollView",top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical"}),

$.__views.mainWindow.add($.__views.scrollView),
$.__views.viewParcela=Ti.UI.createView(
{id:"viewParcela",width:Ti.UI.FILL,top:"10dp",height:Ti.UI.SIZE}),

$.__views.scrollView.add($.__views.viewParcela),
$.__views.tfParcela=Ti.UI.createTextField(
{font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfParcela",top:0,hintText:"Clique para selecionar a data da parcela",editable:!1,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),

$.__views.viewParcela.add($.__views.tfParcela),
$.__views.__alloyId151=Ti.UI.createView(
{width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",top:"45dp",id:"__alloyId151"}),

$.__views.viewParcela.add($.__views.__alloyId151),
$.__views.viewAddParcela=Ti.UI.createView(
{width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"viewAddParcela",top:"5dp"}),

$.__views.scrollView.add($.__views.viewAddParcela),
$.__views.addParcela=Ti.UI.createView(
{width:Ti.UI.FILL,height:"40dp",backgroundSelectedColor:Alloy.Globals.BACKGROUND_COLOR,id:"addParcela",top:"5dp"}),

$.__views.scrollView.add($.__views.addParcela),
$.__views.__alloyId152=Ti.UI.createImageView(
{font:{fontSize:"15dp"},width:"40dp",height:"40dp",left:"5dp",image:"/images/addGreen.png",touchEnabled:!1,id:"__alloyId152"}),

$.__views.addParcela.add($.__views.__alloyId152),
$.__views.__alloyId153=Ti.UI.createLabel(
{color:Alloy.Globals.BLUE_COLOR,left:"50dp",touchEnabled:!1,text:"Adicionar outra parcela",id:"__alloyId153"}),

$.__views.addParcela.add($.__views.__alloyId153),
$.__views.__alloyId154=Ti.UI.createView(
{width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",id:"__alloyId154"}),

$.__views.scrollView.add($.__views.__alloyId154),
$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),
$.__views.activityIndicator.setParent($.__views.mainWindow),
exports.destroy=function(){},




_.extend($,$.__views);var



args=$.args,
dados=args.dados||null,
callback=args.callback||null,
mask=require("Mask"),
moment=require("alloy/moment"),
parcelas=[];const

doClose=()=>{
$.mainWindow.close();
},

doSave=()=>{
console.log("SALVOU!!!!!");
var parc=[
$.tfParcela.getValue()];
for(var i in parcelas)
parc.push(parcelas[i].getValues().parcela),
console.log("PARCELA ",parcelas[i].getValues().parcela);

callback(parc),
doClose();
},


createMenuAndroid=()=>{
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",doSave);
},
activity.invalidateOptionsMenu();
}
},


clickViewParcela=data=>{
if(5<=parcelas.length)

return Alloy.Globals.showAlert("Ops","\xC9 poss\xEDvel adicionar apenas 6 parcelas!"),!1;

var ult=getUltimaParcela();
if(!ult)return Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio inserir uma data para a parcela anterior");
var parcela=Alloy.createWidget("CadParcela","widget",{activityIndicator:$.activityIndicator,ultima:ult,data:data,editable:!dados});
$.viewAddParcela.add(parcela.getView()),

parcelas.push(parcela),

parcela.setFocus(),

parcela.setActionApagar(function(){

for(var i in $.viewAddParcela.remove(parcela.getView()),parcelas)
if(parcelas[i]==parcela){
parcelas.splice(i,1);
break;
}

});
},

getUltimaParcela=()=>{var
tam=parcelas.length,
parc=null;






return console.log("QUANTIDADE DE PARCELAS: ",tam),parc=parcelas.length?parcelas[tam-1].getValues().parcela:$.tfParcela.getValue(),console.log("ULT PARC: ",parc),parc;
},

showPicker=()=>{var
anoAtual=moment().year(),
mesAtual=moment().month(),
diaAtual=moment().date(),


maxDate=moment().add(3,"d");
0==maxDate.day()?
maxDate=moment().add(4,"d"):

6==maxDate.day()&&(
maxDate=moment().add(5,"d")),


maxDate=moment().add(65,"d");var
maxAno=moment(maxDate).year(),
maxMes=moment(maxDate).month(),
maxDia=moment(maxDate).date(),

value=moment().add(3,"days"),
valueAno=moment(value).year(),
valueMes=moment(value).month(),
valueDia=moment(value).date();
console.log("VALUE: ",value.year(),value.month(),value.date());
var picker=Ti.UI.createPicker({
type:Ti.UI.PICKER_TYPE_DATE,
locale:"pt_BR",
minDate:new Date(anoAtual,mesAtual,diaAtual),
maxDate:new Date(maxAno,maxMes,maxDia),
value:new Date(valueAno,valueMes,valueDia)});


picker.showDatePickerDialog({
value:new Date(valueAno,valueMes,valueDia),
callback:function(e){
e.cancel?
Ti.API.info("User canceled dialog"):(

Ti.API.info("User selected date: "+moment(e.value).weekday()),
$.tfParcela.setValue(moment(e.value).format("DD/MM/YYYY")),
removeParcelas());

}});

},

removeParcelas=()=>{
for(var i in parcelas)
$.viewAddParcela.remove(parcelas[i].getView());

parcelas=[];
};

$.mainWindow.addEventListener("open",function(e){
createMenuAndroid();
}),

$.addParcela.addEventListener("click",function(e){return!!
dados||void
clickViewParcela();
}),

$.tfParcela.addEventListener("focus",function(e){
showPicker();
}),

$.tfParcela.addEventListener("click",function(e){
showPicker();
}),









_.extend($,exports);
}

module.exports=Controller;