var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/Frete",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};











if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Frete"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){
function __alloyId145(){
$.__views.mainWindow.removeEventListener("open",__alloyId145),
$.__views.mainWindow.activity?(
$.__views.mainWindow.activity.actionBar.title="Frete",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(

Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),
Ti.API.warn("UI component which does not have an Android activity. Android Activities"),
Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."));

}
$.__views.mainWindow.addEventListener("open",__alloyId145);
}
$.__views.__alloyId146=Ti.UI.createScrollView(
{top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"__alloyId146"}),

$.__views.mainWindow.add($.__views.__alloyId146),
$.__views.viewFob=Ti.UI.createView(
{id:"viewFob",height:Ti.UI.SIZE,width:Ti.UI.FILL,top:"15dp"}),

$.__views.__alloyId146.add($.__views.viewFob),
$.__views.checkOnFob=Ti.UI.createImageView(
{touchEnabled:!1,id:"checkOnFob",image:"/images/checkOn.png",width:"35dp",height:"35dp",left:"15dp",visible:!0}),

$.__views.viewFob.add($.__views.checkOnFob),
$.__views.checkOffFob=Ti.UI.createImageView(
{touchEnabled:!1,id:"checkOffFob",image:"/images/checkOff.png",width:"35dp",height:"35dp",left:"15dp",visible:!1}),

$.__views.viewFob.add($.__views.checkOffFob),
$.__views.labelFob=Ti.UI.createLabel(
{font:{fontSize:"16dp",fontWeight:"semibold"},text:"Frete FOB",touchEnabled:!1,id:"labelFob",left:"55dp",height:Ti.UI.SIZE,color:"gray"}),

$.__views.viewFob.add($.__views.labelFob),
$.__views.viewCif=Ti.UI.createView(
{id:"viewCif",height:Ti.UI.SIZE,width:Ti.UI.FILL}),

$.__views.__alloyId146.add($.__views.viewCif),
$.__views.checkOnCif=Ti.UI.createImageView(
{touchEnabled:!1,id:"checkOnCif",image:"/images/checkOn.png",width:"35dp",height:"35dp",left:"15dp",visible:!1}),

$.__views.viewCif.add($.__views.checkOnCif),
$.__views.checkOffCif=Ti.UI.createImageView(
{touchEnabled:!1,id:"checkOffCif",image:"/images/checkOff.png",width:"35dp",height:"35dp",left:"15dp",visible:!0}),

$.__views.viewCif.add($.__views.checkOffCif),
$.__views.labelCif=Ti.UI.createLabel(
{font:{fontSize:"16dp",fontWeight:"semibold"},text:"Frete CIF",touchEnabled:!1,id:"labelCif",left:"55dp",height:Ti.UI.SIZE,color:"gray"}),

$.__views.viewCif.add($.__views.labelCif),
$.__views.__alloyId147=Ti.UI.createView(
{width:Ti.UI.FILL,height:"45dp",top:"0dp",id:"__alloyId147"}),

$.__views.__alloyId146.add($.__views.__alloyId147),
$.__views.tfFrete=Ti.UI.createTextField(
{font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfFrete",hintText:"0,00",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD}),

$.__views.__alloyId147.add($.__views.tfFrete),
$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),
$.__views.activityIndicator.setParent($.__views.mainWindow),
exports.destroy=function(){},




_.extend($,$.__views);var



args=$.args,
tipo_frete="F",
callback=args.callback,
Mask=require("Mask");const

doClose=()=>{
$.mainWindow.close();
},

createMenuAndroid=()=>{
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",()=>{
validateFrete();
});
},
activity.invalidateOptionsMenu();
}
},

validateFrete=()=>{
var frete={
CJ_TPFRETE:tipo_frete,
CJ_FRTBAST:$.tfFrete.getValue()};


callback(frete),
doClose();
},

checkFrete=value=>{
$.checkOnFob.setVisible("F"==value),
$.checkOffFob.setVisible("F"!=value),
$.checkOnCif.setVisible("F"!=value),
$.checkOffCif.setVisible("F"==value),

$.tfFrete.setHintText("F"==value?"0,00":"0,00"),






$.tfFrete.setValue("0,00");
};

$.viewFob.addEventListener("click",function(e){

tipo_frete="F",
checkFrete(tipo_frete);
}),

$.viewCif.addEventListener("click",function(e){

tipo_frete="C",
checkFrete(tipo_frete);
}),

$.tfFrete.addEventListener("touchend",function(e){
$.tfFrete.setValue("");
}),

$.tfFrete.addEventListener("change",function(e){
$.tfFrete.setValue(Mask.real(e.source.value));
}),

$.mainWindow.addEventListener("open",function(e){
createMenuAndroid();
}),









_.extend($,exports);
}

module.exports=Controller;