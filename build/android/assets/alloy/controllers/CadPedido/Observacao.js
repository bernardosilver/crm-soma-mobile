var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/Observacao",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};











if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Observa\xE7\xE3o"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){
function __alloyId148(){
$.__views.mainWindow.removeEventListener("open",__alloyId148),
$.__views.mainWindow.activity?(
$.__views.mainWindow.activity.actionBar.title="Observa\xE7\xE3o",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(

Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),
Ti.API.warn("UI component which does not have an Android activity. Android Activities"),
Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."));

}
$.__views.mainWindow.addEventListener("open",__alloyId148);
}
$.__views.__alloyId149=Ti.UI.createScrollView(
{top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"__alloyId149"}),

$.__views.mainWindow.add($.__views.__alloyId149),
$.__views.viewReferencia=Ti.UI.createView(
{id:"viewReferencia",width:Ti.UI.FILL,height:"200dp"}),

$.__views.__alloyId149.add($.__views.viewReferencia),
$.__views.tfReferencia=Ti.UI.createTextArea(
{id:"tfReferencia",height:"180dp",width:"90%",color:"#205D33",borderWidth:1,borderRadius:4,borderColor:"#205D33",hintTextColor:"#B2B2B2",keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII,hintText:"Informacoes complementares"}),

$.__views.viewReferencia.add($.__views.tfReferencia),
$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),
$.__views.activityIndicator.setParent($.__views.mainWindow),
exports.destroy=function(){},




_.extend($,$.__views);var



args=$.args,
callback=args.callback,
Mask=require("Mask");const

doClose=()=>{
$.mainWindow.close();
},

createMenuAndroid=()=>{
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",()=>{
var obs=$.tfReferencia.getValue().trim();
callback(obs),
doClose();
});
},
activity.invalidateOptionsMenu();
}
};

$.mainWindow.addEventListener("open",function(e){
createMenuAndroid();
}),









_.extend($,exports);
}

module.exports=Controller;