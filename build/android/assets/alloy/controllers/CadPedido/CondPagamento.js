function asyncGeneratorStep(gen,resolve,reject,_next,_throw,key,arg){try{var info=gen[key](arg),value=info.value}catch(error){return void reject(error)}info.done?resolve(value):Promise.resolve(value).then(_next,_throw)}function _asyncToGenerator(fn){return function(){var self=this,args=arguments;return new Promise(function(resolve,reject){function _next(value){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"next",value)}function _throw(err){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"throw",err)}var gen=fn.apply(self,args);_next(void 0)})}}var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){function







































































getCondPag(){return _getCondPag.apply(this,arguments)}function _getCondPag(){return _getCondPag=_asyncToGenerator(function*(){
$.activityIndicator.show();
var condPag=yield LocalData.getCondPag({A1_COD:cliente.A1_COD});
console.log("TABELAS: ",condPag),
createListView(condPag),
$.activityIndicator.hide();
}),_getCondPag.apply(this,arguments)}

function createListView(array){var
section=Ti.UI.createListSection(),
itens=[];
for(var i in array)
itens.push(createItemList(array[i]));

section.setItems(itens),
$.listView.setSections([section]);
}

function createItemList(item){
var searchableText=item.Filial+item.CodTab+item.Descr;
return{
template:"templateForma",
lbDesc:{
text:item.DESCRI},

properties:{
data:item,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/CondPagamento",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Forma de pagamento",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId134(){$.__views.mainWindow.removeEventListener("open",__alloyId134),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId134)}$.__views.__alloyId135=Ti.UI.createView({id:"__alloyId135"}),$.__views.mainWindow.add($.__views.__alloyId135);var __alloyId136={},__alloyId139=[],__alloyId141={type:"Ti.UI.View",childTemplates:function(){var __alloyId142=[],__alloyId144={type:"Ti.UI.Label",bindId:"lbDesc",properties:{color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},bindId:"lbDesc",maxLines:2,left:0}};return __alloyId142.push(__alloyId144),__alloyId142}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"15dp"}};__alloyId139.push(__alloyId141);var __alloyId138={properties:{name:"templateForma",backgroundColor:"white",width:Ti.UI.FILL,height:"35dp",top:"10dp",left:"15dp",right:"20dp"},childTemplates:__alloyId139};__alloyId136.templateForma=__alloyId138,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId136,id:"listView"}),$.__views.__alloyId135.add($.__views.listView),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId135}),$.__views.popup.setParent($.__views.__alloyId135),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId135}),$.__views.activityIndicator.setParent($.__views.__alloyId135),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,pedido=args.pedido||!1,callback=args.callback||(()=>{}),usuario=Alloy.Globals.getLocalDataUsuario().data,LocalData=require("LocalData"),OrderManager=require("OrderManager"),cliente=OrderManager.getCliente();const doClose=()=>{$.mainWindow.close()};

$.mainWindow.addEventListener("itemclick",function(e){var
click=e.section.getItemAt(e.itemIndex),
condPag=click.properties.data;
pedido&&(
callback(condPag),
doClose());

}),

$.mainWindow.addEventListener("focus",function(){
getCondPag();
}),









_.extend($,exports);
}

module.exports=Controller;