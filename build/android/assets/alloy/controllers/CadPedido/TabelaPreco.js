function asyncGeneratorStep(gen,resolve,reject,_next,_throw,key,arg){try{var info=gen[key](arg),value=info.value}catch(error){return void reject(error)}info.done?resolve(value):Promise.resolve(value).then(_next,_throw)}function _asyncToGenerator(fn){return function(){var self=this,args=arguments;return new Promise(function(resolve,reject){function _next(value){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"next",value)}function _throw(err){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"throw",err)}var gen=fn.apply(self,args);_next(void 0)})}}var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){function




































































getTabelas(){return _getTabelas.apply(this,arguments)}function _getTabelas(){return _getTabelas=_asyncToGenerator(function*(){
$.activityIndicator.show();
var tabelas=yield LocalData.getTabelas(usuario.USR_LOGIN);
createListView(tabelas),
$.activityIndicator.hide(),
console.log("TABELAS: ",tabelas);
}),_getTabelas.apply(this,arguments)}

function createListView(array){var
section=Ti.UI.createListSection(),
itens=[];
for(var i in array)
itens.push(createItemList(array[i]));

section.setItems(itens),
$.listView.setSections([section]);
}

function createItemList(item){
var searchableText=item.Filial+item.CodTab+item.Descr;
return{
template:"templateTabela",
lbDesc:{
text:item.Descr+" - Filial: "+item.Filial},

properties:{
data:item,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadPedido/TabelaPreco",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Tabelas de Pre\xE7o",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId155(){$.__views.mainWindow.removeEventListener("open",__alloyId155),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId155)}$.__views.__alloyId156=Ti.UI.createView({id:"__alloyId156"}),$.__views.mainWindow.add($.__views.__alloyId156);var __alloyId157={},__alloyId160=[],__alloyId162={type:"Ti.UI.View",childTemplates:function(){var __alloyId163=[],__alloyId165={type:"Ti.UI.Label",bindId:"lbDesc",properties:{color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},bindId:"lbDesc",maxLines:2,left:0}};return __alloyId163.push(__alloyId165),__alloyId163}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"15dp"}};__alloyId160.push(__alloyId162);var __alloyId159={properties:{name:"templateTabela",backgroundColor:"white",width:Ti.UI.FILL,height:"40dp",top:"10dp",left:"15dp",right:"20dp"},childTemplates:__alloyId160};__alloyId157.templateTabela=__alloyId159,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId157,id:"listView"}),$.__views.__alloyId156.add($.__views.listView),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId156}),$.__views.popup.setParent($.__views.__alloyId156),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId156}),$.__views.activityIndicator.setParent($.__views.__alloyId156),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,pedido=args.pedido||!1,callback=args.callback||(()=>{}),usuario=Alloy.Globals.getLocalDataUsuario().data,LocalData=require("LocalData");const doClose=()=>{$.mainWindow.close()};

$.mainWindow.addEventListener("itemclick",function(e){var
click=e.section.getItemAt(e.itemIndex),
tabela=click.properties.data;
pedido&&(
callback(tabela),
doClose());

}),

$.mainWindow.addEventListener("focus",function(){
getTabelas();
}),









_.extend($,exports);
}

module.exports=Controller;