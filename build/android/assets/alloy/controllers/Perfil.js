var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){
























































































































































function doClose(){
$.mainWindow.close();
}

function createScrollView(){
$.txtName.setValue(usuario.USR_NOME),
$.txtEmail.setValue(usuario.USR_EMAIL),
$.txtApelido.setValue(usuario.USR_LOGIN),
$.txtPhone.setValue(mask.telefone(usuario.USR_MOBILE));

}

function allertLogOut(){
Alloy.Globals.showAlert({
title:"Deseja encerrar sua sess\xE3o?",
buttons:["Sim","N\xE3o, n\xE3o fazer nada"],
listener:function(e){
0==e.index&&
logout();

}});

}

function logout(){
doClose(),
Alloy.Globals.removeLocalDataUsuario(),
Alloy.Globals.setSync(null),
Alloy.Globals.openWindow("Login",{navigation:!0});
}

function showPhoto(user){
user.USR_FOTO.trim()?(
$.iconProfile.setImage(user.USR_FOTO),
$.editProfile.setText("Alterar"),
$.viewLabel.setLeft("13dp")):(


$.editProfile.setText("Cadastrar"),
$.viewLabel.setLeft("5dp")),1?










$.profile.setElevation(8):($.profile.setViewShadowRadius(4),$.profile.setViewShadowOffset({y:3}),$.profile.setViewShadowColor("#434343"));


}

function doTakePhoto(){
PhotoManager.showOptions(function(previewImage){
Alloy.Globals.openWindowModal("EditPhoto",{
previewImage:previewImage,
usuario:Alloy.Globals.getLocalDataUsuario(),
errorCallBack:function(){
Alloy.Globals.showAlert("Ops!","Ocorreu um erro ao enviar a foto. Verifique sua conex\xE3o e tente novamente.");
},
successCallBack:function(){
setInfo();
}});

});
}

function salvar(){return(
Ti.Network.online?
validateForm()?
validateSenha()?void

updateUsuario():Alloy.Globals.showAlert("Erro!","Favor preencher corretamente os campos de altera\xE7\xE3o de senha!"):Alloy.Globals.showAlert("Erro!","Favor preencher corretamente os campos Nome, Email e Telefone!"):Alloy.Globals.showAlert("Erro!","\xC9 necess\xE1rio estar conectado \xE0 internet para alterar a senha!"));

}































function updateUsuario(){
$.activityIndicator.show();var
nome=mask.removeCaracter($.txtName.getValue()).toUpperCase(),
email=$.txtEmail.getValue().toLowerCase(),
phone=mask.removeTelMask($.txtPhone.getValue()),
senha=$.txtNovaSenha.getValue().trim();
console.log("USUARIO: ",usuario);
var user={
USR_ID:usuario.USR_ID,
USR_NOME:nome,
USR_EMAIL:email,
USR_MOBILE:phone,
USR_PASWD:senha};


refreshData.updateUsuario({
data:user,
successCallBack:function(response){
Alloy.Globals.showAlert("Sucesso!","Dados atualizados!"),
refreshLocalData(user);
},
errorCallBack:function(response){
Alloy.Globals.showAlert("Erro!","Ocorreu um erro ao atualizar os dados. Tente novamente!"),
$.activityIndicator.hide();
}});


}

function refreshLocalData(novo){var
usr=Alloy.Globals.getLocalDataUsuario().data,
token=Alloy.Globals.getLocalDataUsuario().token;
usr.USR_NOME=novo.USR_NOME,
usr.USR_EMAIL=novo.USR_EMAIL,
usr.USR_MOBILE=novo.USR_MOBILE,
Alloy.Globals.setLocalDataUsuario({data:usr,token:token}),
$.activityIndicator.hide(),
doClose();
}

function refreshMenuActionBar(){
var activity=$.mainWindow.getActivity();
activity.onCreateOptionsMenu=function(e){
var menuItemPin=e.menu.add({
icon:"/images/exit.png",
width:"40dp",
height:"40dp",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

menuItemPin.addEventListener("click",function(e){
allertLogOut();
});
},
activity.invalidateOptionsMenu();
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Perfil",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Perfil",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId473(){$.__views.mainWindow.removeEventListener("open",__alloyId473),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId473)}$.__views.__alloyId474=Ti.UI.createScrollView({top:0,contentWidth:"100%",width:Ti.UI.FILL,height:Ti.UI.FILL,layout:"vertical",id:"__alloyId474"}),$.__views.mainWindow.add($.__views.__alloyId474),$.__views.__alloyId475=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,top:"15dp",id:"__alloyId475"}),$.__views.__alloyId474.add($.__views.__alloyId475),$.__views.iconProfile=Ti.UI.createImageView({touchEnabled:!1,id:"iconProfile",width:"30dp",height:"30dp",left:"20dp",image:"/images/perfil_gray.png"}),$.__views.__alloyId475.add($.__views.iconProfile),$.__views.txtName=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"txtName",left:"60dp",right:"40dp"}),$.__views.__alloyId475.add($.__views.txtName),$.__views.__alloyId476=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,id:"__alloyId476"}),$.__views.__alloyId474.add($.__views.__alloyId476),$.__views.txtApelido=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"txtApelido",left:"60dp",right:"40dp"}),$.__views.__alloyId476.add($.__views.txtApelido),$.__views.__alloyId477=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,id:"__alloyId477"}),$.__views.__alloyId474.add($.__views.__alloyId477),$.__views.iconEmail=Ti.UI.createImageView({touchEnabled:!1,id:"iconEmail",width:"30dp",height:"30dp",left:"20dp",image:"/images/email.png"}),$.__views.__alloyId477.add($.__views.iconEmail),$.__views.txtEmail=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"txtEmail",left:"60dp",right:"40dp"}),$.__views.__alloyId477.add($.__views.txtEmail),$.__views.__alloyId478=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,id:"__alloyId478"}),$.__views.__alloyId474.add($.__views.__alloyId478),$.__views.iconPhone=Ti.UI.createImageView({touchEnabled:!1,id:"iconPhone",width:"30dp",height:"30dp",left:"20dp",image:"/images/phone.png"}),$.__views.__alloyId478.add($.__views.iconPhone),$.__views.txtPhone=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},id:"txtPhone",left:"60dp",right:"40dp",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),$.__views.__alloyId478.add($.__views.txtPhone),$.__views.separatedLine=Ti.UI.createView({id:"separatedLine",height:"1dp",width:Ti.UI.FILL,top:"10dp",bottom:"10dp",left:"10dp",right:"10dp",backgroundColor:"#B2B2B2",opacity:.4}),$.__views.__alloyId474.add($.__views.separatedLine),$.__views.labelAlterarSenha=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"18dp",fontWeight:"bold"},text:"Alterar senha",touchEnabled:!1,id:"labelAlterarSenha",top:"15dp",left:"20dp",right:"20dp",width:Ti.UI.FILL,textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER}),$.__views.__alloyId474.add($.__views.labelAlterarSenha),$.__views.labelNovaSenha=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},text:"Nova senha",touchEnabled:!1,id:"labelNovaSenha",left:"20dp",right:"20dp",width:Ti.UI.FILL,textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT}),$.__views.__alloyId474.add($.__views.labelNovaSenha),$.__views.__alloyId479=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,id:"__alloyId479"}),$.__views.__alloyId474.add($.__views.__alloyId479),$.__views.iconLock=Ti.UI.createImageView({touchEnabled:!1,id:"iconLock",width:"30dp",height:"30dp",left:"20dp",image:"/images/lock.png"}),$.__views.__alloyId479.add($.__views.iconLock),$.__views.txtNovaSenha=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"semibold"},id:"txtNovaSenha",left:"60dp",right:"40dp",passwordMask:!0}),$.__views.__alloyId479.add($.__views.txtNovaSenha),$.__views.labelRepetirSenha=Ti.UI.createLabel({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},text:"Repetir senha",touchEnabled:!1,id:"labelRepetirSenha",top:"10dp",left:"20dp",right:"20dp",width:Ti.UI.FILL,textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT}),$.__views.__alloyId474.add($.__views.labelRepetirSenha),$.__views.__alloyId480=Ti.UI.createView({backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,id:"__alloyId480"}),$.__views.__alloyId474.add($.__views.__alloyId480),$.__views.iconLock=Ti.UI.createImageView({touchEnabled:!1,id:"iconLock",width:"30dp",height:"30dp",left:"20dp",image:"/images/lock.png"}),$.__views.__alloyId480.add($.__views.iconLock),$.__views.txtRepetirSenha=Ti.UI.createTextField({color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"semibold"},id:"txtRepetirSenha",left:"60dp",right:"40dp",passwordMask:!0}),$.__views.__alloyId480.add($.__views.txtRepetirSenha),$.__views.separatedLine=Ti.UI.createView({id:"separatedLine",height:"1dp",width:Ti.UI.FILL,top:"15dp",bottom:"15dp",left:"10dp",right:"10dp",backgroundColor:"#B2B2B2",opacity:.4}),$.__views.__alloyId474.add($.__views.separatedLine),$.__views.btnSalvar=Ti.UI.createButton({backgroundColor:Alloy.Globals.BLUE_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},id:"btnSalvar",color:"white",title:"Salvar",height:"50dp",width:"175dp",bottom:"10dp"}),$.__views.__alloyId474.add($.__views.btnSalvar),salvar?$.addListener($.__views.btnSalvar,"click",salvar):__defers["$.__views.btnSalvar!click!salvar"]=!0,$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,mask=require("Mask"),sha512=require("sha512"),refreshData=require("RefreshData"),PhotoManager=require("PhotoManager"),usuario=Alloy.Globals.getLocalDataUsuario().data,refTextField=null;const validateForm=function(){var nome=mask.removeCaracter($.txtName.getValue());return!!nome.match(/[A-Z][a-z]* [A-Z][a-z]*/)&&!(4>nome.length)&&!!validaEmail($.txtEmail.getValue())&&!(10>mask.removeTelMask($.txtPhone.getValue()).length)},validaEmail=function(email){var re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;return re.test((email+"").toLowerCase())},validateSenha=function(){var nova=$.txtNovaSenha.getValue(),confirm=$.txtRepetirSenha.getValue();return!(nova||confirm)||(nova||!confirm)&&(!nova||confirm)&&nova==confirm&&nova==confirm};

$.txtApelido.setEditable(!1),

$.txtPhone.addEventListener("change",function(e){
var v=mask.telefone(e.value);
$.txtPhone.getValue()!=v&&$.txtPhone.setValue(v),
$.txtPhone.setSelection($.txtPhone.getValue().length,$.txtPhone.getValue().length);
}),





$.mainWindow.addEventListener("open",function(e){
createScrollView();
}),

$.mainWindow.addEventListener("open",function(){
refreshMenuActionBar();
}),





__defers["$.__views.btnSalvar!click!salvar"]&&$.addListener($.__views.btnSalvar,"click",salvar),



_.extend($,exports);
}

module.exports=Controller;