var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){




























































































function doClose(){
$.mainWindow.close();
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Login",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Login",backgroundGradient:{type:"linear",startPoint:{x:"0%",y:"0%"},endPoint:{x:"100%",y:"100%"},colors:[{color:Alloy.Globals.MAIN_COLOR,offset:0},{color:Alloy.Globals.DARK_MAIN_COLOR,offset:1}]},id:"mainWindow",title:"Soma Nutri\xE7\xE3o Animal"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),$.__views.__alloyId416=Ti.UI.createView({height:Ti.UI.SIZE,width:"90%",layout:"vertical",top:"100dp",id:"__alloyId416"}),$.__views.mainWindow.add($.__views.__alloyId416),$.__views.__alloyId417=Ti.UI.createLabel({font:{fontSize:"23dp",fontWeight:"bold"},text:"Bem vindo ao Soma CRM!",color:"white",textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,top:0,id:"__alloyId417"}),$.__views.__alloyId416.add($.__views.__alloyId417),$.__views.__alloyId418=Ti.UI.createImageView({image:"/images/logosomaBRANCO.png",height:"120dp",width:"339dp",top:"90dp",bottom:"50dp",id:"__alloyId418"}),$.__views.__alloyId416.add($.__views.__alloyId418),$.__views.widgetInputUser=Alloy.createWidget("Input","widget",{id:"widgetInputUser",__parentSymbol:$.__views.__alloyId416}),$.__views.widgetInputUser.setParent($.__views.__alloyId416),$.__views.widgetInputSenha=Alloy.createWidget("Input","widget",{id:"widgetInputSenha",__parentSymbol:$.__views.__alloyId416}),$.__views.widgetInputSenha.setParent($.__views.__alloyId416),$.__views.btEntrar=Ti.UI.createButton({backgroundColor:Alloy.Globals.DARK_MAIN_COLOR,title:"ENTRAR",id:"btEntrar",top:"10dp",height:"50dp",width:Ti.UI.FILL,color:"white"}),$.__views.__alloyId416.add($.__views.btEntrar),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=arguments[0]||{},RefreshData=require("RefreshData");$.widgetInputUser.setHintText("Nome de usu\xE1rio"),$.widgetInputUser.setKeyboardType("user"),$.widgetInputUser.setReturnKeyType("next"),$.widgetInputSenha.setHintText("Senha"),$.widgetInputSenha.setPasswordMask(!0),$.widgetInputSenha.setReturnKeyType("done"),$.btEntrar.addEventListener("click",function(e){var data={};data.login=$.widgetInputUser.getValue(),data.senha=$.widgetInputSenha.getValue(),$.activityIndicator.show(),$.activityIndicator.setTouch(!0),RefreshData.sendLogin({successCallBack:function(response){console.log("LOGOU: ",response),$.activityIndicator.hide();var usuario=response;Alloy.Globals.setLocalDataUsuario(usuario),$.widgetInputUser.setValue(""),$.widgetInputSenha.setValue(""),Alloy.Globals.openWindow("MainWindow"),doClose()},errorCallBack:function(response){console.log("ERRO!!: ",response),Alloy.Globals.showAlert("Ops!","Ocorreu um erro ao fazer login. "+response.error),$.activityIndicator.hide()},data:data})}),









_.extend($,exports);
}

module.exports=Controller;