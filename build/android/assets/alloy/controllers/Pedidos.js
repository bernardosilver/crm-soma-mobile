var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





















































































function doClose(){
$.mainWindow.close();
}

function onFilter(){
$.popup.showLabelWithTag(optionsFilter,function(categoria,index){var
itemIndex=0?0:firstVisibleSectionIndex<index?0:0,
sectionIndex=parseInt(index);
scrollTo(sectionIndex,itemIndex),
$.popup.hide();
},"Toque para filtrar");
}

function refreshMenuActionBar(){
var activity=$.mainWindow.getActivity();
activity.onCreateOptionsMenu=function(e){
var menuItemPin=e.menu.add({
icon:"/images/add.png",
width:"40dp",
height:"40dp",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

menuItemPin.addEventListener("click",function(e){
var WindowTabGroup=Alloy.createController("FormPedido").getView();1?



WindowTabGroup.open({modal:!1}):WindowTabGroup.open();
});
},
activity.invalidateOptionsMenu();
}

function createListView(){var
section=Ti.UI.createListSection(),
listPedidos=[],
list=[{
cod_ped:"227032",
cliente:"BERNARDO SILVEIRA MARTINS SILVEIRA MARTINS",
cod_cli:"999999",
cpf_cli:"01254438629",
municipio:"RIO POMBA",
telefone_cli:"32988353599",
status:"1",
dt_emissao:"20190716",
dt_carga:"20190719"},
{
cod_ped:"227031",
cliente:"T L VIEIRA AGROPECUARIA",
cod_cli:"999999",
cpf_cli:"31699870000124",
municipio:"MANHUACU",
telefone_cli:"(38)8436-9706",
status:"2",
dt_emissao:"20190715",
dt_carga:"20190718"},
{
cod_ped:"227030",
cliente:"BRUNA LACERDA MACHADO",
cod_cli:"027922",
cpf_cli:"01802679650",
municipio:"BELO HORIZONTE",
telefone_cli:"(38)9114-2311",
status:"3",
dt_emissao:"20190719",
dt_carga:"20190723"},
{
cod_ped:"227029",
cliente:"DISCOM DISTRIBUICAO E COMERCIO LTDA",
cod_cli:"027916",
cpf_cli:"21712570000184",
municipio:"RIO DE JANEIRO",
telefone_cli:"(32) 99836-7510",
status:"2",
dt_emissao:"20190719",
dt_carga:"20190722"},
{
cod_ped:"227028",
cliente:"EDSON MACIEL ALVES FILHO",
cod_cli:"027908",
cpf_cli:"09083351645",
municipio:"SAO PAULO",
telefone_cli:"(31) 8945-4137",
status:"1",
dt_emissao:"20190621",
dt_carga:"20190723"},
{
cod_ped:"227024",
cliente:"CV GONCALVES MAGALHAES",
cod_cli:"027907",
cpf_cli:"33860816000144",
municipio:"BELO HORIZONTE",
telefone_cli:"(31)99539-4674",
status:"1",
dt_emissao:"20190710",
dt_carga:"20190714"}],

pedidos=_.sortBy(list,function(ped){return ped.dt_emissao}).reverse();
for(var i in pedidos)
listPedidos.push(createItem(pedidos[i]));

listPedidos.push(setEmptyBottom()),
section.setItems(listPedidos),
$.listView.setSections([section]);
}

function createItem(pedido){
var searchableText=pedido.cliente+pedido.cod_cli+pedido.cod_ped+pedido.municipio+pedido.cli_cpf+pedido.telefone_cli+pedido.cpf_cli+moment(pedido.dt_emissao).format("DDMMYYYY")+moment(pedido.dt_carga).format("DDMMYYYY");
return{
template:"templatePedido",
lbCod:{
text:"Pedido n\xBA: "+pedido.cod_ped},

lbNome:{
attributedString:Alloy.Globals.setNameWithCode(pedido.cliente,pedido.cod_cli)},

lbMunicipio:{
text:pedido.municipio},

lbDtEmissao:{
attributedString:Alloy.Globals.setNameWithCode(moment(pedido.dt_emissao).format("DD/MM/YYYY"),"Data Emissao")},

lbDtCarga:{
attributedString:Alloy.Globals.setNameWithCode(moment(pedido.dt_carga).format("DD/MM/YYYY"),"Prev. Carga")},

status:{
backgroundColor:"1"==pedido.status?Alloy.Globals.MAIN_COLOR:"2"==pedido.status?Alloy.Globals.BLUE_COLOR:"black"},

properties:{
data:pedido,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}

function setEmptyBottom(){
return{
template:"templateEmpty",
properties:{
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
selectedBackgroundColor:Alloy.Globals.BACKGROUND_COLOR}};


}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Pedidos",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Pedidos",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId446(){$.__views.mainWindow.removeEventListener("open",__alloyId446),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId446)}$.__views.__alloyId447=Ti.UI.createView({id:"__alloyId447"}),$.__views.mainWindow.add($.__views.__alloyId447);var __alloyId448={},__alloyId451=[],__alloyId453={type:"Ti.UI.View",childTemplates:function(){var __alloyId454=[],__alloyId456={type:"Ti.UI.Label",bindId:"lbCod",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},bindId:"lbCod",maxLines:1,left:"0dp"}};__alloyId454.push(__alloyId456);var __alloyId458={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbNome",maxLines:2,left:"0dp"}};__alloyId454.push(__alloyId458);var __alloyId460={type:"Ti.UI.Label",bindId:"lbMunicipio",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbMunicipio",maxLines:1,left:"0dp"}};__alloyId454.push(__alloyId460);var __alloyId462={type:"Ti.UI.Label",bindId:"lbDtEmissao",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbDtEmissao",maxLines:1,left:"0dp"}};__alloyId454.push(__alloyId462);var __alloyId464={type:"Ti.UI.Label",bindId:"lbDtCarga",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbDtCarga",maxLines:1,left:"0dp"}};return __alloyId454.push(__alloyId464),__alloyId454}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"55dp",left:0,top:"5dp"}};__alloyId451.push(__alloyId453);var __alloyId466={type:"Ti.UI.View",childTemplates:function(){var __alloyId467=[],__alloyId469={type:"Ti.UI.View",bindId:"status",properties:{height:"25dp",bindId:"status",width:"25dp",borderRadius:15,backgroundColor:"red",right:0}};return __alloyId467.push(__alloyId469),__alloyId467}(),properties:{right:"5dp"}};__alloyId451.push(__alloyId466);var __alloyId450={properties:{name:"templatePedido",backgroundColor:"white",width:Ti.UI.FILL,height:"118dp",left:"15dp",right:"20dp"},childTemplates:__alloyId451};__alloyId448.templatePedido=__alloyId450;var __alloyId471={properties:{name:"templateEmpty",backgroundColor:"white",height:"70dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId448.templateEmpty=__alloyId471,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId448,id:"listView"}),$.__views.__alloyId447.add($.__views.listView),$.__views.btFilter=Ti.UI.createView({backgroundColor:Alloy.Globals.GRAY_COLOR,id:"btFilter",width:Ti.UI.SIZE,height:"55dp",bottom:"10dp",borderRadius:18,elevation:11}),$.__views.__alloyId447.add($.__views.btFilter),onFilter?$.addListener($.__views.btFilter,"click",onFilter):__defers["$.__views.btFilter!click!onFilter"]=!0,$.__views.__alloyId472=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Todas op\xE7\xF5es",color:"white",width:Ti.UI.SIZE,left:"18dp",right:"18dp",id:"__alloyId472"}),$.__views.btFilter.add($.__views.__alloyId472),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId447}),$.__views.popup.setParent($.__views.__alloyId447),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId447}),$.__views.activityIndicator.setParent($.__views.__alloyId447),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,moment=require("alloy/moment"),optionsFilter=[];1?$.listView.setSearchView(Ti.UI.Android.createSearchView({hintText:"Procurar pedido",backgroundColor:"#a5a5a5",color:"#205D33"})):$.listView.setSearchView(Titanium.UI.createSearchBar({hintText:"Procurar pedido",barColor:"#e4e8e9",borderColor:"#e4e8e9",height:"45dp"})),

$.mainWindow.addEventListener("open",function(){
refreshMenuActionBar();
}),
$.mainWindow.addEventListener("focus",function(){
createListView();
}),





__defers["$.__views.btFilter!click!onFilter"]&&$.addListener($.__views.btFilter,"click",onFilter),



_.extend($,exports);
}

module.exports=Controller;