var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){















































































































function doClose(e){
$.mainWindow.close();
}

function doSave(e){

if($.activityIndicator.show(),validadeCPFs())


return Alloy.Globals.showAlert("Ops!","Voc\xEA n\xE3o pode cadastrar o mesmo CPF para o cliente e o s\xF3cio!"),$.activityIndicator.hide(),!1;

var message=validate();return(
message?(
Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio informar o "+message.name+"!"),
message.field&&message.field.focus(),
$.activityIndicator.hide(),!1):void(


getValues(),
ClientManager.setSocios(new_socios),
callback(new_socios),
doClose()));
}

function validadeCPFs(){return!!
ClientManager.validateCGC(mask.removeCPFCNPJMask($.tfCpf.getValue()),"soc")||!!(


1<=formularios.length&&ClientManager.validateCGC(mask.removeCPFCNPJMask(formularios[0].getValues().AO_CGC),"soc"))||void 0;


}

function validate(){
var nome=mask.removeCaracter($.tfNome.getValue());return(
!!!nome.match(/[aA-zZ]* [aA-zZ]*/)||4>nome.length?
{field:$.tfNome,name:"Nome"}:

validaCPF($.tfCpf.getValue())?


1<=formularios.length&&(
!validaCPF(formularios[0].getValues().AO_CGC)||!!!formularios[0].getValues().AO_NOMINS.match(/[aA-zZ]* [aA-zZ]*/)||4>formularios[0].getValues().AO_NOMINS.length)?
{name:"segundo s\xF3cio"}:void 0:{field:$.tfCpf,name:"CPF"});


}

function getValues(){
new_socios=[],
new_socios.push({
AO_NOMINS:mask.removeCaracter($.tfNome.getValue()).toUpperCase(),
AO_CGC:mask.removeCPFCNPJMask($.tfCpf.getValue()),
ID:$.lbId.getText()});

for(var i=0;i<formularios.length;i++){var
count=parseInt(i),
obj=formularios[count].getValues();
new_socios.push({
AO_NOMINS:mask.removeCaracter(obj.AO_NOMINS).toUpperCase(),
AO_CGC:mask.removeCPFCNPJMask(obj.AO_CGC),
ID:obj.ID});

}
}

function setValues(obj){

if(new_socios=obj?obj:ClientManager.getSocios(),new_socios.length){
$.tfNome.setValue(new_socios[0].AO_NOMINS),
$.tfCpf.setValue(mask.cnpj(new_socios[0].AO_CGC)),
$.lbId.setText(new_socios[0].ID);

for(var
count,i=1;i<new_socios.length;i++)count=parseInt(i),
clickViewSocio(new_socios[count]);

}
}

function createMenuAndroid(){
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",doSave);
},
activity.invalidateOptionsMenu();
}
}

function validaCPF(value){var
copy=value.replace(".","").replace(".","").replace("-",""),
num1=parseInt(copy[0]),
num2=parseInt(copy[1]),
num3=parseInt(copy[2]),
num4=parseInt(copy[3]),
num5=parseInt(copy[4]),
num6=parseInt(copy[5]),
num7=parseInt(copy[6]),
num8=parseInt(copy[7]),
num9=parseInt(copy[8]),
num10=parseInt(copy[9]),
num11=parseInt(copy[10]);

if(num1==num2&&num2==num3&&num3==num4&&num4==num5&&num5==num6&&num6==num7&&num7==num8&&num8==num9&&num9==num10&&num10==num11)return!1;var
soma1=10*num1+9*num2+8*num3+7*num4+6*num5+5*num6+4*num7+3*num8+2*num9,
resto1=10*soma1%11;
if(10==resto1||11==resto1)resto1=0;else
if(resto1!=num10)return!1;var

soma2=11*num1+10*num2+9*num3+8*num4+7*num5+6*num6+5*num7+4*num8+3*num9+2*num10,
resto2=10*soma2%11;
if(10==resto2||11==resto2)resto2=0;else
if(resto2!=num11)return!1;

return!0;
}

function clickViewSocio(obj){
if(1<=formularios.length)

return Alloy.Globals.showAlert("Ops","\xC9 poss\xEDvel adicionar apenas 2 s\xF3cios!"),!1;

var socio=Alloy.createWidget("CadSocio","widget",{activityIndicator:$.activityIndicator,socio:obj,editable:!socios});
$.viewAddSocio.add(socio.getView()),

formularios.push(socio),

socio.setFocus(),

socio.setActionApagar(function(){

for(var i in $.viewAddSocio.remove(socio.getView()),formularios)
if(formularios[i]==socio){
formularios.splice(i,1);
break;
}

});
}

function setEditable(bool){
$.tfNome.setEditable(bool),
$.tfCpf.setEditable(bool);
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadCliente/Socios",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"S\xF3cios"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId102(){$.__views.mainWindow.removeEventListener("open",__alloyId102),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.title="S\xF3cios",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId102)}$.__views.__alloyId103=Ti.UI.createScrollView({top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"__alloyId103"}),$.__views.mainWindow.add($.__views.__alloyId103),$.__views.__alloyId104=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"10dp",id:"__alloyId104"}),$.__views.__alloyId103.add($.__views.__alloyId104),$.__views.tfNome=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfNome",hintText:"Nome completo do s\xF3cio",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId104.add($.__views.tfNome),$.__views.__alloyId105=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId105"}),$.__views.__alloyId104.add($.__views.__alloyId105),$.__views.__alloyId106=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId106"}),$.__views.__alloyId104.add($.__views.__alloyId106),$.__views.__alloyId107=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"0dp",id:"__alloyId107"}),$.__views.__alloyId103.add($.__views.__alloyId107),$.__views.tfCpf=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfCpf",hintText:"CPF - somente numeros",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),$.__views.__alloyId107.add($.__views.tfCpf),$.__views.__alloyId108=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId108"}),$.__views.__alloyId107.add($.__views.__alloyId108),$.__views.__alloyId109=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId109"}),$.__views.__alloyId107.add($.__views.__alloyId109),$.__views.lbId=Ti.UI.createLabel({id:"lbId",visible:!1}),$.__views.__alloyId103.add($.__views.lbId),$.__views.viewAddSocio=Ti.UI.createView({width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"viewAddSocio"}),$.__views.__alloyId103.add($.__views.viewAddSocio),$.__views.viewSocio=Ti.UI.createView({width:Ti.UI.FILL,height:"40dp",backgroundSelectedColor:Alloy.Globals.BACKGROUND_COLOR,id:"viewSocio",top:"5dp"}),$.__views.__alloyId103.add($.__views.viewSocio),$.__views.__alloyId110=Ti.UI.createImageView({font:{fontSize:"15dp"},width:"40dp",height:"40dp",left:"5dp",image:"/images/addGreen.png",touchEnabled:!1,id:"__alloyId110"}),$.__views.viewSocio.add($.__views.__alloyId110),$.__views.__alloyId111=Ti.UI.createLabel({color:Alloy.Globals.BLUE_COLOR,left:"50dp",touchEnabled:!1,text:"Adicionar outro S\xF3cio",id:"__alloyId111"}),$.__views.viewSocio.add($.__views.__alloyId111),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,mask=require("Mask"),callback=args.callback||function(){},socios=args.socios||null,new_socios=[],formularios=[],ClientManager=require("ClientManager");

$.mainWindow.addEventListener("open",function(e){





socios?(





setValues(socios),
setEditable(!1)):(setValues(),setEditable(!0),createMenuAndroid());

}),

$.tfCpf.addEventListener("change",function(e){
var v="";
v=mask.cpf(e.value),
$.tfCpf.setColor(validaCPF(v)?"blue":"red"),
$.tfCpf.getValue()!=v&&$.tfCpf.setValue(v);
}),

$.viewSocio.addEventListener("click",function(e){return!!
socios||void
clickViewSocio();
}),









_.extend($,exports);
}

module.exports=Controller;