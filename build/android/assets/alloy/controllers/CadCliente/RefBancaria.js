var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){

















































































































































function doClose(e){
$.mainWindow.close();
}

function doSave(e){
$.activityIndicator.show();
var message=validate();return(
message?(
Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio informar o(a) "+message.name+"!"),
message.field&&message.field.focus(),
$.activityIndicator.hide(),!1):void(


getValues(),
ClientManager.setReferenciasBanc(referencias),
callback(referencias),
doClose()));
}

function validate(){var
nome=mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase(),
telefone=mask.removeTelMask($.tfTelefone.getValue()),
cidadeUF=mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase(),
contato=mask.removeCaracter($.tfContato.getValue()).toUpperCase();
if(!!!nome.match(/[aA-zZ]*/)||4>nome.length)
return{field:$.tfEmpresa,name:"nome da empresa"};

if(""==telefone.trim()||10>telefone.length)
return{field:$.tfTelefone,name:"Telefone"};

if(""==cidadeUF.trim()||5>cidadeUF.length)
return{field:$.tfCidadeUF,name:"Cidade e UF"};

if(""==contato.trim()||4>contato.length)
return{field:$.tfContato,name:"Contato"};

if(1<=formularios.length)






for(var count=0,obj={},nome="",telefone="",cidadeUF="",contato="",i=0;i<formularios.length;i++){var
count=parseInt(i),
obj=formularios[count].getValues(),
nome=mask.removeCaracter(obj.nome).toUpperCase(),
telefone=mask.removeTelMask(obj.telefone),
cidadeUF=mask.removeCaracter(obj.cidadeUF).toUpperCase(),
contato=mask.removeCaracter(obj.contato).toUpperCase();
if(!!!nome.match(/[aA-zZ]*/)||4>nome.length)
return{name:`nome da empresa da ${count+2}ª referência`};

if(""==telefone.trim()||10>telefone.length)
return{name:`Telefone da ${count+2}ª referência`};

if(""==cidadeUF.trim()||5>cidadeUF.length)
return{name:`Cidade e UF da ${count+2}ª referência`};

if(""==contato.trim()||4>contato.length)
return{name:`Contato da ${count+2}ª referência`};

}

}

function getValues(){
referencias=[],
referencias.push({
AO_NOMINS:mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase(),
AO_TELEFON:mask.removeTelMask($.tfTelefone.getValue()),
AO_CONTATO:mask.removeCaracter($.tfContato.getValue()).toUpperCase(),
AO_OBSERV:mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase(),
ID:$.lbId.getText()});

for(var i=0;i<formularios.length;i++){var
count=parseInt(i),
obj=formularios[count].getValues();
referencias.push({
AO_NOMINS:mask.removeCaracter(obj.nome).toUpperCase(),
AO_TELEFON:mask.removeTelMask(obj.telefone),
AO_CONTATO:mask.removeCaracter(obj.contato).toUpperCase(),
AO_OBSERV:mask.removeCaracter(obj.cidadeUF).toUpperCase(),
ID:obj.ID});

}
}

function setValues(obj){








if(referencias=obj?obj:ClientManager.getReferenciasBanc(),referencias.length&&($.tfEmpresa.setValue(referencias[0].AO_NOMINS),$.tfTelefone.setValue(mask.tel(referencias[0].AO_TELEFON)),$.tfContato.setValue(referencias[0].AO_CONTATO),$.tfCidadeUF.setValue(referencias[0].AO_OBSERV),$.lbId.setText(referencias[0].ID)),1<referencias.length)
for(var
count,i=1;i<referencias.length;i++)count=parseInt(i),
clickViewRefBancaria(referencias[count]);


}

function createMenuAndroid(){
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",doSave);
},
activity.invalidateOptionsMenu();
}
}

function clickViewRefBancaria(obj){
if(1<=formularios.length)

return Alloy.Globals.showAlert("Ops","\xC9 poss\xEDvel adicionar apenas 2 referencias!"),!1;

var referencia=Alloy.createWidget("CadRefComercial","widget",{activityIndicator:$.activityIndicator,ref:obj,editable:!refs});
$.viewAddReferencia.add(referencia.getView()),

formularios.push(referencia),

referencia.setFocus(),

referencia.setActionApagar(function(){

for(var i in $.viewAddReferencia.remove(referencia.getView()),formularios)
if(formularios[i]==referencia){
formularios.splice(i,1);
break;
}

});
}

function setEditable(bool){
$.tfEmpresa.setEditable(bool),
$.tfTelefone.setEditable(bool),
$.tfContato.setEditable(bool),
$.tfCidadeUF.setEditable(bool);
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="CadCliente/RefBancaria",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Ref. Banc\xE1rias"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId70(){$.__views.mainWindow.removeEventListener("open",__alloyId70),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.title="Ref. Banc\xE1rias",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId70)}$.__views.__alloyId71=Ti.UI.createScrollView({top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"__alloyId71"}),$.__views.mainWindow.add($.__views.__alloyId71),$.__views.__alloyId72=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"10dp",id:"__alloyId72"}),$.__views.__alloyId71.add($.__views.__alloyId72),$.__views.tfEmpresa=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfEmpresa",maxLength:30,hintText:"Nome do banco",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId72.add($.__views.tfEmpresa),$.__views.__alloyId73=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId73"}),$.__views.__alloyId72.add($.__views.__alloyId73),$.__views.__alloyId74=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId74"}),$.__views.__alloyId72.add($.__views.__alloyId74),$.__views.__alloyId75=Ti.UI.createView({width:Ti.UI.FILL,top:"10dp",height:Ti.UI.SIZE,id:"__alloyId75"}),$.__views.__alloyId71.add($.__views.__alloyId75),$.__views.tfTelefone=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfTelefone",hintText:"Telefone - somente n\xFAmeros",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),$.__views.__alloyId75.add($.__views.tfTelefone),$.__views.__alloyId76=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",top:"25dp",id:"__alloyId76"}),$.__views.__alloyId75.add($.__views.__alloyId76),$.__views.__alloyId77=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",top:"45dp",id:"__alloyId77"}),$.__views.__alloyId75.add($.__views.__alloyId77),$.__views.__alloyId78=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"10dp",id:"__alloyId78"}),$.__views.__alloyId71.add($.__views.__alloyId78),$.__views.tfCidadeUF=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfCidadeUF",maxLength:40,hintText:"Cidade/UF",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId78.add($.__views.tfCidadeUF),$.__views.__alloyId79=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId79"}),$.__views.__alloyId78.add($.__views.__alloyId79),$.__views.__alloyId80=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId80"}),$.__views.__alloyId78.add($.__views.__alloyId80),$.__views.__alloyId81=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"10dp",id:"__alloyId81"}),$.__views.__alloyId71.add($.__views.__alloyId81),$.__views.tfContato=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfContato",maxLength:30,hintText:"Contato",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId81.add($.__views.tfContato),$.__views.__alloyId82=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId82"}),$.__views.__alloyId81.add($.__views.__alloyId82),$.__views.__alloyId83=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId83"}),$.__views.__alloyId81.add($.__views.__alloyId83),$.__views.lbId=Ti.UI.createLabel({id:"lbId",visible:!1}),$.__views.__alloyId71.add($.__views.lbId),$.__views.viewAddReferencia=Ti.UI.createView({width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"viewAddReferencia",top:"10dp"}),$.__views.__alloyId71.add($.__views.viewAddReferencia),$.__views.viewReferencia=Ti.UI.createView({width:Ti.UI.FILL,height:"40dp",backgroundSelectedColor:Alloy.Globals.BACKGROUND_COLOR,id:"viewReferencia",top:"15dp",bottom:"40dp"}),$.__views.__alloyId71.add($.__views.viewReferencia),$.__views.__alloyId84=Ti.UI.createImageView({font:{fontSize:"15dp"},width:"40dp",height:"40dp",left:"5dp",image:"/images/addGreen.png",touchEnabled:!1,id:"__alloyId84"}),$.__views.viewReferencia.add($.__views.__alloyId84),$.__views.__alloyId85=Ti.UI.createLabel({color:Alloy.Globals.BLUE_COLOR,left:"50dp",touchEnabled:!1,text:"Adicionar outra refer\xEAncia",id:"__alloyId85"}),$.__views.viewReferencia.add($.__views.__alloyId85),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,refs=args.refs,callback=args.callback||function(){},mask=require("Mask"),referencias=[],formularios=[],ClientManager=require("ClientManager");

$.mainWindow.addEventListener("open",function(e){



refs?(





setValues(refs),
setEditable(!1)):(setValues(),setEditable(!0),createMenuAndroid());

}),

$.viewReferencia.addEventListener("click",function(){return!!
refs||void
clickViewRefBancaria();
}),

$.tfTelefone.addEventListener("change",function(e){
var v=mask.tel(e.value);
$.tfTelefone.getValue()!=v&&$.tfTelefone.setValue(v),
$.tfTelefone.setSelection($.tfTelefone.getValue().length,$.tfTelefone.getValue().length);
}),









_.extend($,exports);
}

module.exports=Controller;