function asyncGeneratorStep(gen,resolve,reject,_next,_throw,key,arg){try{var info=gen[key](arg),value=info.value}catch(error){return void reject(error)}info.done?resolve(value):Promise.resolve(value).then(_next,_throw)}function _asyncToGenerator(fn){return function(){var self=this,args=arguments;return new Promise(function(resolve,reject){function _next(value){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"next",value)}function _throw(err){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"throw",err)}var gen=fn.apply(self,args);_next(void 0)})}}var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){




























































function doClose(){
$.mainWindow.close();
}

function showErrorMessage(){
$.message.setVisible(!0),
$.message.setTextAlign(Ti.UI.TEXT_ALIGNMENT_CENTER),
$.message.setText("Ocorreu um erro ao sincronizar os dados! Verifique a conex\xE3o com a internet e tente novamente!"),
$.message.setBtTitle("Tentar Novamente"),
$.message.setBtAction(function(){
downloadDados(),
$.message.setVisible(!1);
}),
$.activityIndicator.hide();
}function

uploadDados(){return _uploadDados.apply(this,arguments)}function _uploadDados(){return _uploadDados=_asyncToGenerator(function*(){let
insertCli={},
id_cliLocal=0,
refs=[];
const qnt=yield countCadCli();

$.activityIndicator.show(),
$.activityIndicator.setText(`... fazendo upload dos clientess. 1 de ${qnt} ...`);


for(var clientes=yield getCadCli(),i=0;i<clientes.length;i++)
$.activityIndicator.setText(`... fazendo upload dos clientes. '${i+1} de ${qnt}' ...`),
id_cliLocal=clientes[i].ID,
insertCli=yield uploadCli(clientes[i]),
insertCli.id_cliLocal=id_cliLocal,yield(
updateReferencias(insertCli)),
refs=yield getCadRef(insertCli.ID),yield(
uploadReferencias(refs));

$.activityIndicator.hide(),yield(
downloadDados());
}),_uploadDados.apply(this,arguments)}function

downloadDados(){return _downloadDados.apply(this,arguments)}function _downloadDados(){return _downloadDados=_asyncToGenerator(function*(){var
clientes=[],
tabelas=[];
$.activityIndicator.show(),
$.activityIndicator.setText("... fazendo download da lista de estados ..."),yield(
downloadEstados()),
$.activityIndicator.setText("... conclu\xEDdo! ..."),
$.activityIndicator.setText("... fazendo download da lista de cidades ..."),yield(
downloadMunicipios()),
$.activityIndicator.setText("... conclu\xEDdo! ..."),
$.activityIndicator.setText("... fazendo download da lista de clientes ..."),
clientes=yield downloadClientes(),
$.activityIndicator.setText("... conclu\xEDdo! ..."),
$.activityIndicator.setText("... fazendo download das condi\xE7\xF5es de pagamento ..."),yield(
downloadCondPag(clientes)),
$.activityIndicator.setText("... conclu\xEDdo! ..."),
$.activityIndicator.setText("... fazendo download das tabelas de pre\xE7os ..."),
tabelas=yield downloadTabelas(),
$.activityIndicator.setText("... conclu\xEDdo! ..."),
$.activityIndicator.setText("... fazendo download dos produtos ..."),yield(
downloadProdutos(tabelas)),
Alloy.Globals.setSync(moment().hours(0).minutes(0).seconds(0).milliseconds(0)),
$.activityIndicator.hide(),
doClose();
}),_downloadDados.apply(this,arguments)}function

downloadEstados(){return _downloadEstados.apply(this,arguments)}function _downloadEstados(){return _downloadEstados=_asyncToGenerator(function*(){
try{
let estados=[];
console.log("1"),yield(
LocalData.createTableUF()),
console.log("2"),yield(
LocalData.clearTable({table:"uf"})),
console.log("3"),
estados=yield getUf(),
console.log("4"),yield(
LocalData.insertUF(estados)),
console.log("5");
}
catch(err){
console.log("DEU ERRO DOWNLOAD ESTADOS: ",err);
}
}),_downloadEstados.apply(this,arguments)}function


















downloadMunicipios(){return _downloadMunicipios.apply(this,arguments)}function _downloadMunicipios(){return _downloadMunicipios=_asyncToGenerator(function*(){
try{
let mnc=[];yield(
LocalData.createTableMnc()),yield(
LocalData.clearTable({table:"mnc"})),
mnc=yield getMnc(),yield(
LocalData.insertMnc(mnc));
}
catch(err){
console.log("DEU ERRO DOWNLOAD CIDADES: ",err);
}
}),_downloadMunicipios.apply(this,arguments)}function


















downloadClientes(){return _downloadClientes.apply(this,arguments)}function _downloadClientes(){return _downloadClientes=_asyncToGenerator(function*(){
try{
let clientes=[];





return yield LocalData.createTableCliente(),yield LocalData.clearTable({table:"cliente",where:" WHERE A1_MOBILE !=1 "}),clientes=yield getClientes(),yield LocalData.insertClientes(clientes),clientes;
}
catch(err){
console.log("DEU ERRO DOWNLOAD CLIENTES: ",err);
}
}),_downloadClientes.apply(this,arguments)}function





















downloadCondPag(_x){return _downloadCondPag.apply(this,arguments)}function _downloadCondPag(){return _downloadCondPag=_asyncToGenerator(function*(clientes){
try{
let cond=[];


for(var i in yield LocalData.clearTable({table:"condPag"}),yield LocalData.createTableCondPag(),clientes)
cond=yield getCondPag({cod:clientes[i].A1_COD,grupo:clientes[i].A1_GRPVEN}),yield(
LocalData.insertCondPag(cond));

}
catch(err){
console.log("DEU ERRO DOWNLOAD CONDI\xC7\xC3O DE PAGAMENTO: ",err);
}
}),_downloadCondPag.apply(this,arguments)}function




















downloadReferencias(){return _downloadReferencias.apply(this,arguments)}function _downloadReferencias(){return _downloadReferencias=_asyncToGenerator(function*(){
try{
let refs=[];yield(
LocalData.createTableReferencia()),yield(
LocalData.clearTable({table:"referencia"})),
refs=yield getReferencias(),yield(
LocalData.insertReferencia(refs));
}
catch(err){
console.log("DEU ERRO DOWNLOAD REFERENCIAS: ",err);
}
}),_downloadReferencias.apply(this,arguments)}function



















downloadTabelas(){return _downloadTabelas.apply(this,arguments)}function _downloadTabelas(){return _downloadTabelas=_asyncToGenerator(function*(){
try{
let tabelas=[];






return yield LocalData.createTableTabelas(),yield LocalData.clearTable({table:"tabelaPreco"}),yield LocalData.createTableProd(),yield LocalData.clearTable({table:"produto"}),tabelas=yield getTabelas(),yield LocalData.insertTabelas(tabelas,usuario.USR_LOGIN),tabelas;
}
catch(err){
console.log("DEU ERRO DOWNLOAD TABELAS: ",err);
}
}),_downloadTabelas.apply(this,arguments)}function

downloadProdutos(_x2){return _downloadProdutos.apply(this,arguments)}function _downloadProdutos(){return _downloadProdutos=_asyncToGenerator(function*(tabelas){
try{
let prods=[];
for(var i in tabelas)
prods=yield getProduto(tabelas[i]),yield(
LocalData.insertProds(prods));

}
catch(err){
console.log("DEU ERRO DOWNLOAD PRODUTOS: ",err);
}
}),_downloadProdutos.apply(this,arguments)}function













































































































uploadReferencias(_x3){return _uploadReferencias.apply(this,arguments)}function _uploadReferencias(){return _uploadReferencias=_asyncToGenerator(function*(refs){
let id_refLocal=0;
var success=!1;
for(var i in refs)
id_refLocal=refs[i].ID,
delete refs[i].ID,yield(
RefreshData.cadReferencia({
successCallBack:function(response){
LocalData.clearTable({
table:"cadRef",
where:`WHERE ID = '${id_refLocal}'`});

},
errorCallBack:function(response){


return console.log("ERRO!!: ",response),Alloy.Globals.showAlert("Ops!","Ocorreu um erro ao cadastrar cliente. "+response.error),!1;
},
data:refs[i]}));


}),_uploadReferencias.apply(this,arguments)}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Sync",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Login",backgroundGradient:{type:"linear",startPoint:{x:"0%",y:"0%"},endPoint:{x:"100%",y:"100%"},colors:[{color:Alloy.Globals.MAIN_COLOR,offset:0},{color:Alloy.Globals.DARK_MAIN_COLOR,offset:1}]},id:"mainWindow",title:"Soma Nutri\xE7\xE3o Animal"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),$.__views.__alloyId522=Ti.UI.createView({height:Ti.UI.SIZE,width:"90%",layout:"vertical",top:"100dp",id:"__alloyId522"}),$.__views.mainWindow.add($.__views.__alloyId522),$.__views.__alloyId523=Ti.UI.createLabel({font:{fontSize:"23dp",fontWeight:"bold"},text:"Bem vindo ao Soma CRM!",color:"white",textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,top:0,id:"__alloyId523"}),$.__views.__alloyId522.add($.__views.__alloyId523),$.__views.__alloyId524=Ti.UI.createLabel({font:{fontSize:"18dp",fontWeight:"bold"},text:"Aguarde... estamos carregando todas as informa\xE7\xF5es para voc\xEA!",color:"white",textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,top:"20dp",id:"__alloyId524"}),$.__views.__alloyId522.add($.__views.__alloyId524),$.__views.__alloyId525=Ti.UI.createImageView({image:"/images/logosomaBRANCO.png",height:"120dp",width:"339dp",top:"90dp",bottom:"50dp",id:"__alloyId525"}),$.__views.__alloyId522.add($.__views.__alloyId525),$.__views.message=Alloy.createWidget("ErrorMessage","widget",{id:"message",top:0,__parentSymbol:$.__views.mainWindow}),$.__views.message.setParent($.__views.mainWindow),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,download=args.download||!1,upload=args.upload||!1,moment=require("alloy/moment"),RefreshData=require("RefreshData"),LocalData=require("LocalData"),usuario=Alloy.Globals.getLocalDataUsuario().data;const getUf=()=>new Promise((resolve,reject)=>{RefreshData.getEstado({successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO UF: ",err),401==err.code?(Alloy.Globals.openWindow("Login"),doClose()):showErrorMessage()}})}),getMnc=()=>new Promise((resolve,reject)=>{RefreshData.getMnc({successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO MNC: ",err),401==err.code?(Alloy.Globals.openWindow("Login"),doClose()):showErrorMessage()}})}),getClientes=()=>new Promise((resolve,reject)=>{var data={};data.cod=usuario.USR_LOGIN,RefreshData.getCliente({data:data,successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO CLI: ",err),401==err.code?(Alloy.Globals.openWindow("Login"),doClose()):showErrorMessage()}})}),getCondPag=data=>new Promise((resolve,reject)=>{RefreshData.getCondPag({data:data,successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO COND PAG: ",err),401==err.code?(Alloy.Globals.openWindow("Login"),doClose()):showErrorMessage()}})}),getReferencias=()=>new Promise((resolve,reject)=>{RefreshData.getReferencia({successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO REFS: ",err),401==err.code?(Alloy.Globals.openWindow("Login"),doClose()):showErrorMessage()}})}),getTabelas=()=>new Promise((resolve,reject)=>{RefreshData.getTabelas({data:{user:usuario.USR_LOGIN},successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO TABELAS: ",err)}})}),getProduto=tabela=>new Promise((resolve,reject)=>{RefreshData.getProduto({data:tabela,successCallBack:function(response){resolve(response.data)},errorCallBack:function(err){console.log("DEU ERRO TABELAS: ",err)}})}),countCadCli=()=>new Promise((resolve,reject)=>{LocalData.countCadCli({success:function(count){resolve(count)},error:function(err){console.log("DEU ERRADO AO CONTAR CLIENTE: ",err),reject(err)}})}),getCadCli=()=>new Promise((resolve,reject)=>{LocalData.getClienteDetail({id:{A1_MOBILE:"1"},success:function(clientes){resolve(clientes)},error:function(err){reject(err),console.log("DEU ERRADO AO CONTAR CLIENTE: ",err)}})}),uploadCli=cli=>new Promise((resolve,reject)=>{var id=cli.ID;delete cli.ID,delete cli.A1_MOBILE,delete cli.A1_LAT,delete cli.A1_LONG,delete cli.SLD,delete cli.E4_DESCRI,delete cli.E4_DESCRI2,cli.A1_MSALDO=0,cli.A1_MCOMPRA=0,cli.A1_METR=0,cli.A1_MATR=0,cli.A1_TITPROT=0,cli.A1_CHQDEVO=0,cli.A1_DTULCHQ="",cli.A1_PRICOM="",RefreshData.cadCliente({successCallBack:function(response){LocalData.clearTable({table:"cliente",where:`WHERE ID = '${id}'`}),resolve(response.data)},errorCallBack:function(response){console.log("ERRO!!: ",response),Alloy.Globals.showAlert("Ops!","Ocorreu um erro ao cadastrar cliente. "+response.error)},data:cli})}),updateReferencias=obj=>new Promise((resolve,reject)=>{LocalData.updateRef({data:obj,success:function(response){resolve()},error:function(err){console.log("DEU ERRADO AO CONTAR CLIENTE: ",err),reject(err)}})}),getCadRef=id=>new Promise((resolve,reject)=>{LocalData.getCadRef({cod_cli:id,success:function(response){resolve(response)},error:function(err){console.log("ERRO AO BUSCAR REFS: ",err),reject(err)}})});


$.mainWindow.addEventListener("open",function(){
download?downloadDados():
upload&&uploadDados();
}),









_.extend($,exports);
}

module.exports=Controller;