var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){






















































































function doClose(){
$.mainWindow.close();
}

function onFilter(){
$.popup.showLabelWithTag(optionsFilter,function(categoria,index){var
itemIndex=0?0:0<index?0:0,
sectionIndex=parseInt(index);
scrollTo(sectionIndex,itemIndex),
$.popup.hide();
},"Toque para filtrar");
}

function scrollTo(sectionIndex,itemIndex){
var options=1?{animated:!1}:{position:Titanium.UI.iOS.ListViewScrollPosition.TOP,animated:!1};
$.listView.scrollToItem(sectionIndex,itemIndex,options);

}

function refreshMenuActionBar(){
var activity=$.mainWindow.getActivity();
activity.onCreateOptionsMenu=function(e){
var menuItemPin=e.menu.add({
icon:"/images/add.png",
width:"40dp",
height:"40dp",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

menuItemPin.addEventListener("click",function(e){
var WindowTabGroup=Alloy.createController("FormCliente").getView();1?



WindowTabGroup.open({modal:!1}):WindowTabGroup.open();
});
},
activity.invalidateOptionsMenu();
}

function createListView(){var
sections=[],
registradas=[],
encerradas=[],
canceladas=[],
list=[
{
descricao:"BERNARDO SILVEIRA MARTINS SILVEIRA MARTINS",
setor:"LOGISTICA",
roteiro:"999999",
codigo:"01254438629/2019",
dt_registro:"20190715",
dt_ocorrencia:"20190715",
status:"1"},
{
descricao:"T L VIEIRA AGROPECUARIA",
setor:"C.Q.",
roteiro:"",
codigo:"31699870000/2019",
dt_registro:"20190708",
dt_ocorrencia:"20190708",
status:"2"},
{
descricao:"BRUNA LACERDA MACHADO",
setor:"C.Q.",
roteiro:"",
codigo:"01802679650/2019",
dt_registro:"20190705",
dt_ocorrencia:"20190620",
status:"3"},
{
descricao:"DISCOM DISTRIBUICAO E COMERCIO LTDA",
setor:"C.Q.",
roteiro:"",
codigo:"21712570000/2019",
dt_registro:"20190704",
dt_ocorrencia:"20190704",
status:"3"},
{
descricao:"EDSON MACIEL ALVES FILHO",
setor:"LOGISTICA",
roteiro:"027908",
codigo:"09083351645/2019",
dt_registro:"20190625",
dt_ocorrencia:"20190625",
status:"2"},
{
descricao:"CV GONCALVES MAGALHAES",
setor:"LOGISTICA",
roteiro:"027907",
codigo:"33860816000/2019",
dt_registro:"20190415",
dt_ocorrencia:"20190315",
status:"1"}],



reclamacoes=_.sortBy(list,function(recl){return recl.dt_registro}).reverse();

for(var i in reclamacoes)
"1"==reclamacoes[i].status?
registradas.push(createItem(reclamacoes[i])):

"2"==reclamacoes[i].status?
encerradas.push(createItem(reclamacoes[i])):

"3"==reclamacoes[i].status&&
canceladas.push(createItem(reclamacoes[i]));



if(registradas.length){
optionsFilter.push("REGISTRADAS");
var sectionRegistradas=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Registradas")});

sectionRegistradas.setItems(registradas),
sections.push(sectionRegistradas);
}
if(encerradas.length){
optionsFilter.push("ENCERRADAS");
var sectionEncerradas=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Encerradas")});

sectionEncerradas.setItems(encerradas),
sections.push(sectionEncerradas);
}
if(canceladas.length){
optionsFilter.push("CANCELADAS");
var sectionCanceladas=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Canceladas")});

sectionCanceladas.setItems(canceladas),
sections.push(sectionCanceladas);
}
sections.length&&sections[sections.length-1].appendItems([setEmptyBottom()]),
$.listView.setSections(sections);

}

function createItem(reclamacao){
var searchableText=reclamacao.descricao+reclamacao.codigo+reclamacao.roteiro+reclamacao.dt_registro+reclamacao.dt_ocorrencia;
return{
template:"templateReclamacao",
lbReclamacao:{
attributedString:Alloy.Globals.setNameWithCode(reclamacao.descricao,reclamacao.codigo)},

lbSetor:{
attributedString:Alloy.Globals.setNameWithCode(reclamacao.setor,"Setor")},

lbRoteiro:{
attributedString:reclamacao.roteiro?Alloy.Globals.setNameWithCode(reclamacao.roteiro,"Roteiro"):"",
visible:!!reclamacao.roteiro,
height:reclamacao.roteiro?Ti.UI.SIZE:"0dp"},

lbRegistro:{
attributedString:Alloy.Globals.setNameWithCode(moment(reclamacao.dt_registro).format("DD/MM/YYYY"),"Data registro")},

lbOcorrencia:{
attributedString:Alloy.Globals.setNameWithCode(moment(reclamacao.dt_ocorrencia).format("DD/MM/YYYY"),"Data ocorr\xEAncia")},

status:{
backgroundColor:"1"==reclamacao.status?Alloy.Globals.MAIN_COLOR:"2"==reclamacao.status?"black":Alloy.Globals.GRAY_COLOR},

properties:{
data:reclamacao,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}

function setEmptyBottom(){
return{
template:"templateEmpty",
properties:{
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
selectedBackgroundColor:Alloy.Globals.BACKGROUND_COLOR}};


}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Reclamacoes",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Reclama\xE7\xF5es",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId495(){$.__views.mainWindow.removeEventListener("open",__alloyId495),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId495)}$.__views.__alloyId496=Ti.UI.createView({id:"__alloyId496"}),$.__views.mainWindow.add($.__views.__alloyId496);var __alloyId497={},__alloyId500=[],__alloyId502={type:"Ti.UI.View",childTemplates:function(){var __alloyId503=[],__alloyId505={type:"Ti.UI.Label",bindId:"lbReclamacao",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbReclamacao",maxLines:2,left:"0dp"}};__alloyId503.push(__alloyId505);var __alloyId507={type:"Ti.UI.Label",bindId:"lbSetor",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbSetor",maxLines:2,left:"0dp"}};__alloyId503.push(__alloyId507);var __alloyId509={type:"Ti.UI.Label",bindId:"lbRoteiro",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbRoteiro",maxLines:1,left:"0dp"}};__alloyId503.push(__alloyId509);var __alloyId511={type:"Ti.UI.Label",bindId:"lbRegistro",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbRegistro",maxLines:1,left:"0dp"}};__alloyId503.push(__alloyId511);var __alloyId513={type:"Ti.UI.Label",bindId:"lbOcorrencia",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbOcorrencia",maxLines:1,left:"0dp"}};return __alloyId503.push(__alloyId513),__alloyId503}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"55dp",left:0,top:"5dp"}};__alloyId500.push(__alloyId502);var __alloyId515={type:"Ti.UI.View",childTemplates:function(){var __alloyId516=[],__alloyId518={type:"Ti.UI.View",bindId:"status",properties:{height:"25dp",bindId:"status",width:"25dp",borderRadius:15,backgroundColor:"red",right:0}};return __alloyId516.push(__alloyId518),__alloyId516}(),properties:{right:"5dp"}};__alloyId500.push(__alloyId515);var __alloyId499={properties:{name:"templateReclamacao",backgroundColor:"white",width:Ti.UI.FILL,height:"100dp",left:"15dp",right:"20dp"},childTemplates:__alloyId500};__alloyId497.templateReclamacao=__alloyId499;var __alloyId520={properties:{name:"templateEmpty",backgroundColor:"white",height:"60dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId497.templateEmpty=__alloyId520,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId497,id:"listView"}),$.__views.__alloyId496.add($.__views.listView),$.__views.btFilter=Ti.UI.createView({backgroundColor:Alloy.Globals.GRAY_COLOR,id:"btFilter",width:Ti.UI.SIZE,height:"55dp",bottom:"10dp",borderRadius:18,elevation:11}),$.__views.__alloyId496.add($.__views.btFilter),onFilter?$.addListener($.__views.btFilter,"click",onFilter):__defers["$.__views.btFilter!click!onFilter"]=!0,$.__views.__alloyId521=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Todas op\xE7\xF5es",color:"white",width:Ti.UI.SIZE,left:"18dp",right:"18dp",id:"__alloyId521"}),$.__views.btFilter.add($.__views.__alloyId521),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId496}),$.__views.popup.setParent($.__views.__alloyId496),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId496}),$.__views.activityIndicator.setParent($.__views.__alloyId496),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,moment=require("alloy/moment"),optionsFilter=[],firstVisibleSectionIndex=0;1?$.listView.setSearchView(Ti.UI.Android.createSearchView({hintText:"Procurar reclama\xE7\xE3o",backgroundColor:"#a5a5a5",color:"#205D33"})):$.listView.setSearchView(Titanium.UI.createSearchBar({hintText:"Procurar reclama\xE7\xE3o",barColor:"#e4e8e9",borderColor:"#e4e8e9",height:"45dp"})),

$.mainWindow.addEventListener("open",function(){
refreshMenuActionBar(),
createListView();
}),





__defers["$.__views.btFilter!click!onFilter"]&&$.addListener($.__views.btFilter,"click",onFilter),



_.extend($,exports);
}

module.exports=Controller;