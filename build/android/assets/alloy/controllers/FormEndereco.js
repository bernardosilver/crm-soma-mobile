var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){



































































































































































































function doClose(e){
$.mainWindow.close();
}

function doSave(e){
$.activityIndicator.show();
var message=validate();return(
message?(
Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio informar o "+message+"!"),
$.activityIndicator.hide(),!1):void


getValues());
}

function copyValues(end){
console.log("COPIAR END: ",end),
$.tfLogradouro.value=end.A1_END,
$.lbUF.text=end.A1_EST,
$.lbMunicipio.text=end.A1_MUN,
$.tfBairro.value=end.A1_BAIRRO,
$.tfCEP.value=end.A1_CEP,
$.lbUF.setColor("black"),
$.lbMunicipio.setColor("black"),
new_end.CODC_UF=end.COD_UF;
}

function setValues(){
if(cobranca&&!end_cobranca){
var endereco=ClientManager.getEnderecoCobranca();
$.tfLogradouro.value=endereco.A1_ENDCOB,
$.tfBairro.value=endereco.A1_BAIRROC,
$.lbUF.text=endereco.A1_ESTC||"UF",
$.lbMunicipio.text=endereco.A1_MUNC||"Munic\xEDpio",
$.tfCEP.value=endereco.A1_CEPC,
new_end.CODC_UF=endereco.CODC_UF,
$.viewGeo.setHeight("0dp"),
$.viewGeo.setVisible(!1),
$.lbUF.setColor(endereco.A1_ESTC?"black":"gray"),
$.lbMunicipio.setColor(endereco.A1_MUNC?"black":"gray");
}else
if(!cobranca&&!end_cobranca&&!end_entrega){
var endereco=ClientManager.getEndereco();
$.tfLogradouro.value=endereco.A1_END,
$.tfComplemento.value=endereco.A1_COMPLEM,
$.lbUF.text=endereco.A1_EST||"UF",
$.lbMunicipio.text=endereco.A1_MUN||"Munic\xEDpio",
$.tfBairro.value=endereco.A1_BAIRRO,
$.tfCEP.value=endereco.A1_CEP,
$.tfReferencia.value=endereco.A1_INFOROT,
new_end.COD_UF=endereco.COD_UF,
$.viewGeo.setHeight("0dp"),
$.viewGeo.setVisible(!1),
$.lbUF.setColor(endereco.A1_EST?"black":"gray"),
$.lbMunicipio.setColor(endereco.A1_MUN?"black":"gray");
}else
end_cobranca?(
$.tfLogradouro.value=end_cobranca.A1_ENDCOB,
$.tfBairro.value=end_cobranca.A1_BAIRROC,
$.lbUF.text=end_cobranca.A1_ESTC||"UF",
$.lbMunicipio.text=end_cobranca.A1_MUNC||"Munic\xEDpio",
$.tfCEP.value=end_cobranca.A1_CEPC,
$.viewGeo.setHeight("0dp"),
$.viewGeo.setVisible(!1),
$.lbUF.setColor(end_cobranca.A1_ESTC?"black":"gray"),
$.lbMunicipio.setColor(end_cobranca.A1_MUNC?"black":"gray")):

end_entrega&&(
$.tfLogradouro.value=end_entrega.A1_END,
$.tfComplemento.value=end_entrega.A1_COMPLEM,
$.lbUF.text=end_entrega.A1_EST||"UF",
$.lbMunicipio.text=end_entrega.A1_MUN||"Munic\xEDpio",
$.tfBairro.value=end_entrega.A1_BAIRRO,
$.tfCEP.value=end_entrega.A1_CEP,
$.tfReferencia.value=end_entrega.A1_INFOROT,
$.lbUF.setColor(end_entrega.A1_EST?"black":"gray"),
$.lbMunicipio.setColor(end_entrega.A1_MUN?"black":"gray"),
end_entrega.A1_LAT&&""!=end_entrega.A1_LAT&&end_entrega.A1_LONG&&""!=end_entrega.A1_LONG?
$.tfGeo.text="Clique para visualizar no mapa":(


$.viewGeo.setHeight("0dp"),
$.viewGeo.setVisible(!1)));


}

function createViewGeolocalizacao(lat,long){
if(lat&&long){var
coord=lat+","+long,
url_google="http://maps.apple.com/?q="+coord+"&z=14&t=k",
url_waze="https://waze.com/ul?q="+coord+"&navigate=yes&zoom=17",
dialog=Ti.UI.createAlertDialog({
title:"Mapa",
message:"Deseja abrir a localiza\xE7\xE3o por qual aplicativo?",
buttonNames:["Google Maps","Cancelar","Waze"]});

dialog.addEventListener("click",function(e){
if(0==e.index)result=Ti.Platform.openURL(url_google);else
if(2==e.index)result=Ti.Platform.openURL(url_waze);else
return!1;
}),
dialog.show();
}
}

function getValues(){
new_end.logradouro=mask.removeCaracter($.tfLogradouro.value).toUpperCase(),
new_end.complemento=mask.removeCaracter($.tfComplemento.value||"").toUpperCase(),
new_end.uf=mask.removeCaracter($.lbUF.text).toUpperCase(),
new_end.municipio=mask.removeCaracter($.lbMunicipio.text).toUpperCase(),
new_end.bairro=mask.removeCaracter($.tfBairro.value).toUpperCase(),
new_end.cep=mask.removeCaracter($.tfCEP.value).toUpperCase(),
new_end.referencia=mask.removeCaracter($.tfReferencia.value||"").toUpperCase(),
cobranca?(
ClientManager.setEnderecoCobranca(new_end),
callback(ClientManager.getEnderecoCobranca()),
$.activityIndicator.hide(),
doClose()):


customGeolocation.getCoordinates(function(coords){
coords?(
new_end.latitude=coords.latitude,
new_end.longitude=coords.longitude,
ClientManager.setEndereco(new_end),
callback(ClientManager.getEndereco()),
$.activityIndicator.hide(),
doClose()):(


Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio permitir a geolocaliza\xE7\xE3o para cadastrar o cliente!"),
$.activityIndicator.hide());

});

}

function validate(){
for(var i in fields)
if(!fields[i].field.getValue()||""==mask.removeCaracter(fields[i].field.getValue().trim()))return fields[i].name;return(

"UF"==$.lbUF.text?"UF":
"Munic\xEDpio"==$.lbMunicipio.text?"Munic\xEDpio":
null);
}

function createMenuAndroid(){
if(!0){
var activity=$.mainWindow.activity;
activity.onCreateOptionsMenu=function(e){
var btDone=e.menu.add({
title:"Salvar",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

btDone.addEventListener("click",doSave);
},
activity.invalidateOptionsMenu();
}
}

function activeCopy(bool){
bool&&end_ent.A1_END?(
$.copiarEndereco.setVisible(!0),
$.copiarEndereco.setOpacity(1),
$.copiarEndereco.setTop("20dp"),
$.copiarEndereco.setHeight(Ti.UI.SIZE)):

bool&&!end_ent.A1_END?(
$.copiarEndereco.setVisible(!0),
$.copiarEndereco.setOpacity(.5),
$.copiarEndereco.setTop("20dp"),
$.copiarEndereco.setHeight(Ti.UI.SIZE)):(


$.copiarEndereco.setVisible(!1),
$.copiarEndereco.setTop("0dp"),
$.copiarEndereco.setHeight("0dp"));

}

function getMaps(q1){
var url="http://maps.google.com?q="+q1+"&zoom=14";
result=Ti.Platform.openURL(url);
}

function setEditable(bool){
$.tfLogradouro.setEditable(bool),
$.tfComplemento.setEditable(bool),
$.tfBairro.setEditable(bool),
$.tfCEP.setEditable(bool),
$.tfReferencia.setEditable(bool);
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="FormEndereco",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Endere\xE7o"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId255(){$.__views.mainWindow.removeEventListener("open",__alloyId255),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.title="Endere\xE7o",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId255)}$.__views.__alloyId256=Ti.UI.createScrollView({top:0,width:Ti.UI.FILL,height:Ti.UI.SIZE,layout:"vertical",id:"__alloyId256"}),$.__views.mainWindow.add($.__views.__alloyId256),$.__views.copiarEndereco=Ti.UI.createView({backgroundColor:Alloy.Globals.DARK_MAIN_COLOR,id:"copiarEndereco",left:"20dp",width:"245dp",height:Ti.UI.SIZE,top:"20dp",borderRadius:4,visible:!1}),$.__views.__alloyId256.add($.__views.copiarEndereco),$.__views.__alloyId257=Ti.UI.createImageView({image:"/images/copy.png",width:"25dp",height:"25dp",left:"10dp",top:"5dp",bottom:"5dp",id:"__alloyId257"}),$.__views.copiarEndereco.add($.__views.__alloyId257),$.__views.__alloyId258=Ti.UI.createLabel({color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},text:"Copiar endere\xE7o principal",left:"45dp",id:"__alloyId258"}),$.__views.copiarEndereco.add($.__views.__alloyId258),$.__views.__alloyId259=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"10dp",id:"__alloyId259"}),$.__views.__alloyId256.add($.__views.__alloyId259),$.__views.tfLogradouro=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfLogradouro",hintText:"Rua, Avenida, Pra\xE7a, etc...",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId259.add($.__views.tfLogradouro),$.__views.__alloyId260=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId260"}),$.__views.__alloyId259.add($.__views.__alloyId260),$.__views.__alloyId261=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId261"}),$.__views.__alloyId259.add($.__views.__alloyId261),$.__views.viewComplemento=Ti.UI.createView({id:"viewComplemento",width:Ti.UI.FILL,height:"45dp"}),$.__views.__alloyId256.add($.__views.viewComplemento),$.__views.tfComplemento=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfComplemento",hintText:"Complemento"}),$.__views.viewComplemento.add($.__views.tfComplemento),$.__views.__alloyId262=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId262"}),$.__views.viewComplemento.add($.__views.__alloyId262),$.__views.__alloyId263=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",id:"__alloyId263"}),$.__views.__alloyId256.add($.__views.__alloyId263),$.__views.tfBairro=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfBairro",hintText:"Bairro",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId263.add($.__views.tfBairro),$.__views.__alloyId264=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId264"}),$.__views.__alloyId263.add($.__views.__alloyId264),$.__views.__alloyId265=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId265"}),$.__views.__alloyId263.add($.__views.__alloyId265),$.__views.viewUF=Ti.UI.createView({id:"viewUF",width:Ti.UI.FILL,height:"45dp"}),$.__views.__alloyId256.add($.__views.viewUF),$.__views.lbUF=Ti.UI.createLabel({font:{fontSize:"17dp"},color:"gray",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",text:"UF",id:"lbUF"}),$.__views.viewUF.add($.__views.lbUF),$.__views.__alloyId266=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId266"}),$.__views.viewUF.add($.__views.__alloyId266),$.__views.__alloyId267=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId267"}),$.__views.viewUF.add($.__views.__alloyId267),$.__views.btUF=Ti.UI.createView({id:"btUF",width:Ti.UI.FILL,height:"45dp",backgroundColor:"transparent",visible:!1}),$.__views.viewUF.add($.__views.btUF),$.__views.viewCidade=Ti.UI.createView({id:"viewCidade",width:Ti.UI.FILL,height:"45dp"}),$.__views.__alloyId256.add($.__views.viewCidade),$.__views.lbMunicipio=Ti.UI.createLabel({font:{fontSize:"17dp"},color:"gray",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",text:"Munic\xEDpio",id:"lbMunicipio"}),$.__views.viewCidade.add($.__views.lbMunicipio),$.__views.__alloyId268=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId268"}),$.__views.viewCidade.add($.__views.__alloyId268),$.__views.__alloyId269=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId269"}),$.__views.viewCidade.add($.__views.__alloyId269),$.__views.btMunicipio=Ti.UI.createView({id:"btMunicipio",width:Ti.UI.FILL,height:"45dp",backgroundColor:"transparent",visible:!1}),$.__views.viewCidade.add($.__views.btMunicipio),$.__views.__alloyId270=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",id:"__alloyId270"}),$.__views.__alloyId256.add($.__views.__alloyId270),$.__views.tfCEP=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",width:"90%",backgroundColor:"white",hintTextColor:"gray",clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,bubbleParent:!1,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfCEP",hintText:"CEP",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),$.__views.__alloyId270.add($.__views.tfCEP),$.__views.__alloyId271=Ti.UI.createLabel({font:{fontSize:"11dp",fontWeight:"bold"},color:Alloy.Globals.RED_COLOR,right:"10dp",bottom:"5dp",height:Ti.UI.SIZE,width:Ti.UI.SIZE,touchEnabled:!1,text:"Obrigat\xF3rio",id:"__alloyId271"}),$.__views.__alloyId270.add($.__views.__alloyId271),$.__views.__alloyId272=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId272"}),$.__views.__alloyId270.add($.__views.__alloyId272),$.__views.viewGeo=Ti.UI.createView({id:"viewGeo",width:Ti.UI.FILL,height:"45dp",visible:!0}),$.__views.__alloyId256.add($.__views.viewGeo),$.__views.tfGeo=Ti.UI.createLabel({color:Alloy.Globals.BLUE_COLOR,font:{fontSize:"15dp",fontWeight:"bold"},id:"tfGeo",left:"20dp"}),$.__views.viewGeo.add($.__views.tfGeo),$.__views.__alloyId273=Ti.UI.createView({width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId273"}),$.__views.viewGeo.add($.__views.__alloyId273),$.__views.viewReferencia=Ti.UI.createView({id:"viewReferencia",width:Ti.UI.FILL,height:"200dp"}),$.__views.__alloyId256.add($.__views.viewReferencia),$.__views.tfReferencia=Ti.UI.createTextArea({id:"tfReferencia",height:"180dp",width:"90%",color:"#205D33",borderWidth:1,borderRadius:4,borderColor:"#205D33",hintTextColor:"#B2B2B2",keyboardType:Titanium.UI.KEYBOARD_TYPE_ASCII,hintText:"Informacoes complementares"}),$.__views.viewReferencia.add($.__views.tfReferencia),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var ClientManager=require("ClientManager"),mask=require("Mask"),customGeolocation=require("CustomGeolocation"),args=$.args,new_end={},cobranca=args.cobranca||!1,callback=args.callback||function(){},end_entrega=args.end_entrega||null,end_cobranca=args.end_cobranca||null,end_ent=ClientManager.getEndereco(),

fields=[];
fields.push({field:$.tfLogradouro,name:"Rua, Avenida, Pra\xE7a, etc..."}),
fields.push({field:$.tfBairro,name:"Bairro"}),
fields.push({field:$.tfCEP,name:"CEP"}),

$.tfCEP.addEventListener("change",function(e){
var v=mask.cep(e.value);
$.tfCEP.getValue()!=v&&$.tfCEP.setValue(v);
}),

$.copiarEndereco.addEventListener("click",function(e){
end_ent.A1_END&&copyValues(end_ent);
}),

$.viewGeo.addEventListener("click",function(e){
createViewGeolocalizacao(end_entrega.A1_LAT,end_entrega.A1_LONG);
}),

$.viewUF.addEventListener("click",function(e){return!(
end_entrega||end_cobranca)&&void
Alloy.Globals.openWindow("CadCliente/ListUF",{
callback:function(uf){
$.lbUF.text=mask.removeCaracter(uf.UF_SIGLA).toUpperCase(),
$.lbMunicipio.text="Munic\xEDpio",
new_end.MNC_CODMC=null,
$.lbUF.setColor("black"),
$.lbMunicipio.setColor("gray"),
cobranca?new_end.CODC_UF=uf.UF_COD:
new_end.COD_UF=uf.UF_COD;

}});

}),

$.viewCidade.addEventListener("click",function(e){return!(
end_entrega||end_cobranca)&&(
new_end.COD_UF||new_end.CODC_UF?void
Alloy.Globals.openWindow("CadCliente/ListCity",{
uf_id:cobranca?new_end.CODC_UF:new_end.COD_UF,
callback:function(cidade){
$.lbMunicipio.text=mask.removeCaracter(cidade.MNC_MUN).toUpperCase(),
$.lbMunicipio.setColor("black"),
new_end.CODMUN=cidade.MNC_CODMC;
}}):Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio informar o UF para selecionar uma cidade!"));

}),

$.viewReferencia.setVisible(!cobranca),
$.viewComplemento.setVisible(!cobranca),
$.viewComplemento.setHeight(cobranca?"0dp":"45dp"),


$.mainWindow.addEventListener("open",function(e){
end_entrega||end_cobranca?(






activeCopy(!1),
setValues(),
setEditable(!1)):(setValues(),activeCopy(cobranca),setEditable(!0),createMenuAndroid());

}),









_.extend($,exports);
}

module.exports=Controller;