var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){










































function createMenu(){
console.log("CREATE MENU: ",Alloy.Globals.getLocalDataUsuario());var
usuario=Alloy.Globals.getLocalDataUsuario().data,

sections=[],

serctionUsuario=Ti.UI.createListSection(),
sectionNovos=Ti.UI.createListSection(),
sectionSeparator=Ti.UI.createListSection(),
sectionConsultas=Ti.UI.createListSection(),
sectionSync=Ti.UI.createListSection();

serctionUsuario.setItems([createOptionUsuario(usuario)]),
sectionNovos.setItems([
createOption({opc:"Novo Pedido",img:"/images/novo_pedido_green.png"}),
createOption({opc:"Novo Cliente",img:"/images/novo_cliente_green.png"})]),


sectionSeparator.setItems([createSeparator()]),
sectionConsultas.setItems([
createOption({opc:"Ver Pedidos",img:"/images/pedidos_green.png"}),
createOption({opc:"Ver Clientes",img:"/images/clientes_green.png"})]),


sectionSync.setItems([
createSeparator(),
createOption({opc:"Baixar dados",img:"/images/download_green.png"}),
createOption({opc:"Enviar dados",img:"/images/upload_green.png"})]),


sections.push(serctionUsuario),
sections.push(sectionNovos),
sections.push(sectionSeparator),
sections.push(sectionConsultas),
sections.push(sectionSync),

$.listView.setSections(sections);
}

function createOptionUsuario(usr){
return{
template:"templateUsuario",
lbNome:{
text:usr.USR_NOME},

lbEmail:{
text:usr.USR_EMAIL},

viewProfile:{
image:usr.USR_FOTO.trim()?usr.USR_FOTO:"/images/perfil_green.png"},

properties:{
data:usr,
type:"perfil"},

events:{
itemClick:function(){
console.log("CLICOU!");
},
itemClick:function(){
console.log("CLICOU!");
}}};


}

function createOption(obj){
return{
template:"templateMenu",
lbOpcao:{
text:obj.opc},

imgOpcao:{
image:obj.img},

properties:{
data:obj,
type:"menu"}};


}

function createSeparator(){
return{
template:"templateSeparator"};

}

function openWindowTabGroup(tab){
var WindowTabGroup=Alloy.createController(tab).getView();1?



WindowTabGroup.open({modal:!1}):WindowTabGroup.open();
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Menu",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.mainView=Ti.UI.createView({id:"mainView",backgroundColor:"white"}),$.__views.mainView&&$.addTopLevelView($.__views.mainView);var __alloyId419={},__alloyId422=[],__alloyId424={type:"Ti.UI.View",childTemplates:function(){var __alloyId425=[],__alloyId427={type:"Ti.UI.ImageView",bindId:"viewProfile",properties:{bindId:"viewProfile",width:"68dp",height:"68dp",left:"15dp",top:"50dp",defaultImage:"/images/perfil_green.png"}};__alloyId425.push(__alloyId427);var __alloyId429={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},bindId:"lbNome",left:"15dp",top:"7dp"}};__alloyId425.push(__alloyId429);var __alloyId431={type:"Ti.UI.Label",bindId:"lbEmail",properties:{color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"16dp",fontWeight:"semibold"},bindId:"lbEmail",left:"15dp"}};return __alloyId425.push(__alloyId431),__alloyId425}(),properties:{layout:"vertical"}};__alloyId422.push(__alloyId424);var __alloyId433={type:"Ti.UI.View",properties:{bottom:0,width:Ti.UI.FILL,height:1,backgroundColor:"#ddd"}};__alloyId422.push(__alloyId433);var __alloyId421={properties:{name:"templateUsuario",layout:"vertical",backgroundImage:"/images/wallpaper.png",height:"180dp",width:Ti.UI.FILL,separatorColor:"none"},childTemplates:__alloyId422};__alloyId419.templateUsuario=__alloyId421;var __alloyId435={properties:{name:"templateSeparator",backgroundColor:"#d0d3d4",height:"1dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId419.templateSeparator=__alloyId435;var __alloyId438=[],__alloyId440={type:"Ti.UI.View",bindId:"menu",childTemplates:function(){var __alloyId441=[],__alloyId443={type:"Ti.UI.ImageView",bindId:"imgOpcao",properties:{bindId:"imgOpcao",height:"25dp",width:"25dp",left:"0dp"}};__alloyId441.push(__alloyId443);var __alloyId445={type:"Ti.UI.Label",bindId:"lbOpcao",properties:{color:Alloy.Globals.DARK_MAIN_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},bindId:"lbOpcao",textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,left:"12dp"}};return __alloyId441.push(__alloyId445),__alloyId441}(),properties:{bindId:"menu",layout:"horizontal",height:Ti.UI.SIZE,width:Ti.UI.FILL,left:"15dp",right:"15dp",top:"12dp",bottom:"12dp"}};__alloyId438.push(__alloyId440);var __alloyId437={properties:{name:"templateMenu",backgroundColor:"white",height:Ti.UI.SIZE,top:"6dp",bottom:0,separatorColor:"none"},childTemplates:__alloyId438};__alloyId419.templateMenu=__alloyId437,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:"none",backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId419,id:"listView",top:"-2dp"}),$.__views.mainView.add($.__views.listView),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,ClientManager=require("ClientManager"),LocalData=require("LocalData"),RefreshData=require("RefreshData");

createMenu(),

$.listView.addEventListener("itemclick",function(e){var
item=e.section.getItemAt(e.itemIndex),
data=item?item.properties.data:null,
type=item?item.properties.type:null;

if(data&&"Novo Pedido"==data.opc)
Alloy.Globals.openWindow("FormPedido");else

if(data&&"Novo Cliente"==data.opc)
ClientManager.delete(),
Alloy.Globals.openWindow("FormCliente");else

if(data&&"Nova Reclama\xE7\xE3o"==data.opc)
console.log("CADASTRAR RECLAMACAO");else

if(data&&"Ver Pedidos"==data.opc)
Alloy.Globals.openWindow("Pedidos");else

if(data&&"Ver Clientes"==data.opc)
Alloy.Globals.openWindow("Clientes");else

if(data&&"Baixar dados"==data.opc){
if(Ti.Network.online)
Alloy.Globals.openWindow("Sync",{download:!0});else


return Alloy.Globals.showAlert("Erro!","\xC9 necess\xE1rio estar conectado \xE0 internet para sincronizar os dados!");}else


if(data&&"Enviar dados"==data.opc){
if(!Ti.Network.online)
return Alloy.Globals.showAlert("Erro!","\xC9 necess\xE1rio estar conectado \xE0 internet para sincronizar os dados!");


LocalData.countCadCli({
success:function(count){return(
console.log("COUNT: ",count),
count&&0!=count?void



Alloy.Globals.openWindow("Sync",{upload:!0}):Alloy.Globals.showAlert("Ops!","N\xE3o existem dados para serem enviados!"));

},
error:function(err){
console.log("DEU ERRADO AO CONTAR CLIENTE: ",err);
}});


}else
"perfil"==type&&
Alloy.Globals.openWindow("Perfil");

}),









_.extend($,exports);
}

module.exports=Controller;