function asyncGeneratorStep(gen,resolve,reject,_next,_throw,key,arg){try{var info=gen[key](arg),value=info.value}catch(error){return void reject(error)}info.done?resolve(value):Promise.resolve(value).then(_next,_throw)}function _asyncToGenerator(fn){return function(){var self=this,args=arguments;return new Promise(function(resolve,reject){function _next(value){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"next",value)}function _throw(err){asyncGeneratorStep(gen,resolve,reject,_next,_throw,"throw",err)}var gen=fn.apply(self,args);_next(void 0)})}}var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){


























































































function doClose(){
$.mainWindow.close();
}

function onFilter(){
$.popup.showLabelWithTag(optionsFilter,function(categoria,index){var
itemIndex=0?0:0<index?0:0,
sectionIndex=parseInt(index);
scrollTo(sectionIndex,itemIndex),
$.popup.hide();
},"Toque para filtrar");
}

function scrollTo(sectionIndex,itemIndex){
var options=1?{animated:!1}:{position:Titanium.UI.iOS.ListViewScrollPosition.TOP,animated:!1};
$.listView.scrollToItem(sectionIndex,itemIndex,options);

}

function refreshMenuActionBar(){
var activity=$.mainWindow.getActivity();
activity.onCreateOptionsMenu=function(e){
var menuItemPin=e.menu.add({
icon:"/images/add.png",
width:"40dp",
height:"40dp",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

menuItemPin.addEventListener("click",function(e){
ClientManager.delete();
var WindowTabGroup=Alloy.createController("FormCliente").getView();1?



WindowTabGroup.open({modal:!1}):WindowTabGroup.open();
});
},
activity.invalidateOptionsMenu();
}

function createListView(array){var
sections=[],
cadastrados=[],
liberados=[],
bloqueados=[],
aguardando=[],
insuficiente=[],
titulos=[];
optionsFilter=[];

var clientes=_.sortBy(array,function(cli){return cli.A1_NOME});
for(var i in clientes){
var status=ClientManager.getStatusCli(clientes[i].A1_MSBLQL,clientes[i].A1_TITVENC,clientes[i].A1_RISCO,clientes[i].A1_OBSERV,clientes[i].A1_TLC,clientes[i].SLD);
clientes[i].A1_MOBILE?
cadastrados.push(createItem(clientes[i],status)):

1==status.status?
liberados.push(createItem(clientes[i],status)):

2==status.status?
bloqueados.push(createItem(clientes[i],status)):

3==status.status?
aguardando.push(createItem(clientes[i],status)):

4==status.status?
insuficiente.push(createItem(clientes[i],status)):

5==status.status&&
titulos.push(createItem(clientes[i],status));

}

if(liberados.length){
optionsFilter.push("LIBERADOS");
var sectionLiberados=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Liberados")});

sectionLiberados.setItems(liberados),
sections.push(sectionLiberados);
}
if(bloqueados.length){
optionsFilter.push("BLOQUEADOS");
var sectionBloqueados=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Bloqueados")});

sectionBloqueados.setItems(bloqueados),
sections.push(sectionBloqueados);
}
if(aguardando.length){
optionsFilter.push("AGUARD. LIB.");
var sectionAguardando=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Aguardando libera\xE7\xE3o")});

sectionAguardando.setItems(aguardando),
sections.push(sectionAguardando);
}
if(insuficiente.length){
optionsFilter.push("CRED. INSUF.");
var sectionInsuficiente=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Cr\xE9dito insuficiente")});

sectionInsuficiente.setItems(insuficiente),
sections.push(sectionInsuficiente);
}
if(titulos.length){
optionsFilter.push("T\xCDTULOS VENC.");
var sectionVencidos=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("T\xEDtulos Vencidos")});

sectionVencidos.setItems(titulos),
sections.push(sectionVencidos);
}
if(cadastrados.length){
optionsFilter.push("CADASTRADOS");
var sectionCad=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader("Cadastrados")});

sectionCad.setItems(cadastrados),
sections.push(sectionCad);
}
sections.length&&sections[sections.length-1].appendItems([setEmptyBottom()]),
$.listView.setSections(sections),
$.activityIndicator.hide();
}

function createItem(cliente,status){var
searchableText=cliente.A1_NOME+cliente.A1_COD+cliente.A1_MUN+cliente.A1_CGC+cliente.A1_DDD+cliente.A1_TEL,

data=cliente.A1_ULTCOM&&cliente.A1_ULTCOM.trim()?moment(cliente.A1_ULTCOM).format("DD/MM/YYYY"):null;
return{
template:"templateCliente",
lbNome:{
attributedString:Alloy.Globals.setNameWithCode(cliente.A1_NOME,cliente.A1_COD)},

lbMunicipio:{
text:cliente.A1_MUN},

lbFone:{
text:mask.telefone(cliente.A1_DDD.trim()+cliente.A1_TEL.trim())},

lbDtCompra:{
attributedString:Alloy.Globals.setNameWithCode(data,"Ult. compra")},

status:{
backgroundColor:status.color},

lbBlock:{
text:status.block},

properties:{
data:cliente,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}

function setEmptyBottom(){
return{
template:"templateEmpty",
properties:{
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
selectedBackgroundColor:Alloy.Globals.BACKGROUND_COLOR}};


}function

getClientes(){return _getClientes.apply(this,arguments)}function _getClientes(){return _getClientes=_asyncToGenerator(function*(){
var clientes=[];yield(
LocalData.getClientes({
cliente_id:null,
success:function(array){
clientes=array;
},
error:function(err){
console.log("DEU ERRADO AO CADASTRAR CLIENTE: ",err);
}})),yield(

createListView(clientes));
}),_getClientes.apply(this,arguments)}function

excluirCliente(_x){return _excluirCliente.apply(this,arguments)}function _excluirCliente(){return _excluirCliente=_asyncToGenerator(function*(cli){
var codCli=cli.ID;
$.activityIndicator.show(),yield(
LocalData.clearTable({table:"cliente",where:` WHERE ID = '${codCli}'`})),yield(
LocalData.clearTable({table:"cadRef",where:` WHERE ID_CLIENTE = '${codCli}'`})),
$.activityIndicator.hide();
}),_excluirCliente.apply(this,arguments)}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Clientes",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Clientes",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId166(){$.__views.mainWindow.removeEventListener("open",__alloyId166),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId166)}$.__views.__alloyId167=Ti.UI.createView({id:"__alloyId167"}),$.__views.mainWindow.add($.__views.__alloyId167);var __alloyId168={},__alloyId171=[],__alloyId173={type:"Ti.UI.View",childTemplates:function(){var __alloyId174=[],__alloyId176={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbNome",maxLines:2,left:"0dp"}};__alloyId174.push(__alloyId176);var __alloyId178={type:"Ti.UI.Label",bindId:"lbMunicipio",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbMunicipio",maxLines:1,left:"0dp"}};__alloyId174.push(__alloyId178);var __alloyId180={type:"Ti.UI.Label",bindId:"lbFone",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbFone",maxLines:1,left:"0dp"}};__alloyId174.push(__alloyId180);var __alloyId182={type:"Ti.UI.Label",bindId:"lbDtCompra",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbDtCompra",maxLines:1,left:"0dp"}};return __alloyId174.push(__alloyId182),__alloyId174}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"55dp",left:0,top:"5dp"}};__alloyId171.push(__alloyId173);var __alloyId184={type:"Ti.UI.View",childTemplates:function(){var __alloyId185=[],__alloyId187={type:"Ti.UI.View",bindId:"status",childTemplates:function(){var __alloyId188=[],__alloyId190={type:"Ti.UI.Label",bindId:"lbBlock",properties:{color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"20dp",fontWeight:"bold"},bindId:"lbBlock"}};return __alloyId188.push(__alloyId190),__alloyId188}(),properties:{height:"25dp",bindId:"status",width:"25dp",borderRadius:15,backgroundColor:"red",right:0}};return __alloyId185.push(__alloyId187),__alloyId185}(),properties:{right:"5dp"}};__alloyId171.push(__alloyId184);var __alloyId170={properties:{name:"templateCliente",backgroundColor:"white",width:Ti.UI.FILL,height:"100dp",left:"15dp",right:"20dp"},childTemplates:__alloyId171};__alloyId168.templateCliente=__alloyId170;var __alloyId192={properties:{name:"templateEmpty",backgroundColor:"white",height:"60dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId168.templateEmpty=__alloyId192,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId168,id:"listView"}),$.__views.__alloyId167.add($.__views.listView),$.__views.btFilter=Ti.UI.createView({backgroundColor:Alloy.Globals.GRAY_COLOR,id:"btFilter",width:Ti.UI.SIZE,height:"55dp",bottom:"10dp",borderRadius:18,elevation:11}),$.__views.__alloyId167.add($.__views.btFilter),onFilter?$.addListener($.__views.btFilter,"click",onFilter):__defers["$.__views.btFilter!click!onFilter"]=!0,$.__views.__alloyId193=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Todas op\xE7\xF5es",color:"white",width:Ti.UI.SIZE,left:"18dp",right:"18dp",id:"__alloyId193"}),$.__views.btFilter.add($.__views.__alloyId193),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId167}),$.__views.popup.setParent($.__views.__alloyId167),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId167}),$.__views.activityIndicator.setParent($.__views.__alloyId167),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,pedido=args.pedido||!1,callback=args.callback||(()=>{}),optionsFilter=[],firstVisibleSectionIndex=0,ClientManager=require("ClientManager"),LocalData=require("LocalData"),moment=require("alloy/moment");1?$.listView.setSearchView(Ti.UI.Android.createSearchView({hintText:"Procurar cliente",backgroundColor:"#a5a5a5",color:"#205D33"})):$.listView.setSearchView(Titanium.UI.createSearchBar({hintText:"Procurar cliente",barColor:"#e4e8e9",borderColor:"#e4e8e9",height:"45dp"})),

$.listView.addEventListener("itemclick",function(e){var
click=e.section.getItemAt(e.itemIndex),
cliente=click.properties.data;
if(cliente.A1_MOBILE)
Alloy.Globals.showAlert({
title:"ATEN\xC7\xC3O",
message:"Qual op\xE7\xE3o deseja executar?",

buttons:["Excluir","Cancelar","Editar"],

listener:function(e){
0==e.index?

excluirCliente(cliente):

2==e.index&&

Alloy.Globals.openWindow("FormCliente",{cliente:cliente}),

console.log("Index click: ",e.index);
}});else



if(pedido){
var status=ClientManager.getStatusCli(cliente.A1_MSBLQL,cliente.A1_TITVENC,cliente.A1_RISCO,cliente.A1_OBSERV,cliente.A1_TLC,cliente.SLD);
1==status.status?(
callback(cliente),
doClose()):


Alloy.Globals.showAlert({
title:"ATEN\xC7\xC3O",
message:`Este cliente está bloqueado para pedidos!\nMotivo: ${status.desc}.\nDeseja fazer um orçamento?`,

buttons:["Sim","N\xE3o"],

listener:function(e){
console.log("Index click: ",e.index),
0==e.index&&(

callback(cliente),
doClose());




}});



}else

Alloy.Globals.openWindow("FormCliente",{cliente:cliente});

}),

$.mainWindow.addEventListener("open",function(){
refreshMenuActionBar();
}),

$.mainWindow.addEventListener("focus",function(){
$.activityIndicator.show(),
getClientes();
}),





__defers["$.__views.btFilter!click!onFilter"]&&$.addListener($.__views.btFilter,"click",onFilter),



_.extend($,exports);
}

module.exports=Controller;