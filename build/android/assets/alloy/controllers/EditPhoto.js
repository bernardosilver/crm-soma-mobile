var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){
























































































function createCropArea(size,axis,min,max){
Ti.API.error("CROP AREA");

var draggableView=Draggable.createView({
width:size,
height:size,
top:0,
left:0,
borderColor:Alloy.Globals.RED_COLOR,
borderWidth:4,
draggableConfig:{
axis:axis,
minTop:min,
maxTop:max,
minLeft:min,
maxLeft:max}});



$.contentPreview.add(draggableView),

draggableView.addEventListener("end",function(e){
topCrop=1?Alloy.Globals.PixelsToDPUnits(e.top):e.top,
leftCrop=1?Alloy.Globals.PixelsToDPUnits(e.left):e.left;
});
}

function saveFoto(arquivo){
if(usuario)
var fotoAntiga=usuario.imagem||null;



















}

function sendArquivo(arquivo){
$.activityIndicator.show(),
usuario;



















}

function removeProfileImage(imagem){








}


function doClose(){
$.mainWindow.close();
}

function doSave(){var _Mathfloor=



Math.floor;$.activityIndicator.show();var leftCropOriginal=_Mathfloor(leftCrop*previewImage.width/$.preview.size.width),
topCropOriginal=_Mathfloor(topCrop*previewImage.height/$.preview.size.height),
cropSize=null;

previewImage.width<previewImage.height?(
cropSize=_Mathfloor(previewImage.width),
topCropOriginal=cropSize+topCropOriginal>previewImage.height?previewImage.height-cropSize:topCropOriginal):(

cropSize=_Mathfloor(previewImage.height),
leftCropOriginal=cropSize+leftCropOriginal>previewImage.width?previewImage.width-cropSize:leftCropOriginal);


var imageCropped=cropPreviewImage(cropSize,leftCropOriginal,topCropOriginal);

sendArquivo(imageCropped);
}

function cropPreviewImage(cropSize,leftCrop,topCrop){
var imageCropped=previewImage.imageAsResized(previewImage.width,previewImage.height).imageAsCropped({
width:cropSize,
height:cropSize,
x:leftCrop,
y:topCrop});

return imageCropped.imageAsResized(640,640).imageAsCompressed(.9);
}

function showPreviewImagem(width,height){var
cropSize=width<height?width:height,
min=0,
max=width<height?height-cropSize:width-cropSize,
axis=width<height?"y":"x";

createCropArea(cropSize,axis,min,max);
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="EditPhoto",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:"black",barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.NoActionBar",id:"mainWindow"}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),$.__views.__alloyId194=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Recorte a fotografia",color:"white",top:"20dp",width:Ti.UI.FILL,textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,id:"__alloyId194"}),$.__views.mainWindow.add($.__views.__alloyId194),$.__views.contentView=Ti.UI.createView({id:"contentView",top:"50dp",bottom:"50dp",width:Ti.UI.FILL,height:Ti.UI.FILL}),$.__views.mainWindow.add($.__views.contentView),$.__views.contentPreview=Ti.UI.createView({id:"contentPreview",height:Ti.UI.SIZE,width:Ti.UI.SIZE}),$.__views.contentView.add($.__views.contentPreview),$.__views.preview=Ti.UI.createImageView({id:"preview",autorotate:!0,defaultImage:"/images/default-photo.png"}),$.__views.contentPreview.add($.__views.preview),$.__views.navBarView=Ti.UI.createView({bottom:0,width:Ti.UI.FILL,height:"50dp",id:"navBarView"}),$.__views.mainWindow.add($.__views.navBarView),$.__views.btCancel=Ti.UI.createView({id:"btCancel",height:Ti.UI.FILL,right:"10dp",width:"100dp"}),$.__views.navBarView.add($.__views.btCancel),doSave?$.addListener($.__views.btCancel,"click",doSave):__defers["$.__views.btCancel!click!doSave"]=!0,$.__views.__alloyId195=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Salvar",color:"white",height:Ti.UI.FILL,bottom:"5dp",id:"__alloyId195"}),$.__views.btCancel.add($.__views.__alloyId195),$.__views.btCancel=Ti.UI.createView({id:"btCancel",height:Ti.UI.FILL,left:"10dp",width:"100dp"}),$.__views.navBarView.add($.__views.btCancel),doClose?$.addListener($.__views.btCancel,"click",doClose):__defers["$.__views.btCancel!click!doClose"]=!0,$.__views.__alloyId196=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Cancelar",color:"white",height:Ti.UI.FILL,bottom:"5dp",id:"__alloyId196"}),$.__views.btCancel.add($.__views.__alloyId196),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),$.__views.activityIndicator.setParent($.__views.mainWindow),exports.destroy=function(){},_.extend($,$.__views);var args=arguments[0]||{},Draggable=require("ti.draggable"),refreshData=require("RefreshData"),previewImage=args.previewImage,usuario=args.usuario||null,successCallBack=args.successCallBack||function(){},errorCallBack=args.errorCallBack||function(){},leftCrop=0,topCrop=0;$.activityIndicator.setTouch(!0),

$.preview.addEventListener("load",function(e){var
width=$.preview.size.width,
height=$.preview.size.height;
showPreviewImagem(width,height);
}),

$.preview.image=1080<previewImage.width||1080<previewImage.height?previewImage.imageAsResized(.75*previewImage.width,.75*previewImage.height):previewImage,





__defers["$.__views.btCancel!click!doSave"]&&$.addListener($.__views.btCancel,"click",doSave),__defers["$.__views.btCancel!click!doClose"]&&$.addListener($.__views.btCancel,"click",doClose),



_.extend($,exports);
}

module.exports=Controller;