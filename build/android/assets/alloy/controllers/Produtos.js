var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){














































































































function doClose(){
$.mainWindow.close();
}

function refreshMenuActionBar(){
var activity=$.mainWindow.getActivity();
activity.onCreateOptionsMenu=function(e){
var menuItemPin=e.menu.add({
icon:"/images/cart.png",
width:"40dp",
height:"40dp",
showAsAction:Ti.Android.SHOW_AS_ACTION_ALWAYS});

menuItemPin.addEventListener("click",function(e){
Alloy.Globals.openWindow("CadPedido/Cart");
});
},
activity.invalidateOptionsMenu();
}

function onFilter(){
$.popup.showLabelWithTag(optionsFilter,function(categoria,index){var
itemIndex=0?0:0<index?0:0,
sectionIndex=parseInt(index);
scrollTo(sectionIndex,itemIndex),
$.popup.hide();
},"Toque para filtrar");
}

function scrollTo(sectionIndex,itemIndex){
var options=1?{animated:!1}:{position:Titanium.UI.iOS.ListViewScrollPosition.TOP,animated:!1};
$.listView.scrollToItem(sectionIndex,itemIndex,options);
}

function createListView(){var
tab=OrderManager.getTabela(),
list=LocalData.getProds({CODTAB:tab.CJ_TABELA}),
produtos=_.sortBy(list,function(prod){return prod.DESCR}),
group=_.groupBy(produtos,prod=>prod.CATEGO),
sections=[],
headerSection=null;

for(var i in group)
headerSection=Ti.UI.createListSection({
headerView:Alloy.Globals.createSectionHeader(group[i][0].DESCCAT)}),

headerSection.setItems(createList(group[i])),
sections.push(headerSection),
optionsFilter.push(group[i][0].DESCCAT);

sections&&sections.length&&sections[sections.length-1].appendItems([setEmptyBottom()]),
$.listView.setSections(sections);
}

function createList(prods){
var array=[];
for(var i in prods)
array.push(createItem(prods[i]));

return array;
}

function createItem(produto){
var searchableText=produto.DESCR+produto.CODPRO+produto.DESCCAT;
return{
template:"templateProduto",
lbProduto:{
attributedString:Alloy.Globals.setNameWithCode(produto.DESCR,produto.CODPRO)},

properties:{
data:produto,
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
selectedBackgroundColor:Alloy.Globals.defaultBackgroundColor,
searchableText:searchableText}};


}

function setEmptyBottom(){
return{
template:"templateEmpty",
properties:{
accessoryType:Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
selectedBackgroundColor:Alloy.Globals.BACKGROUND_COLOR}};


}

function generateProdInfo(prod){let

aux=0,
dif=0,
fc=0;const
condPag=OrderManager.getPagamento(),
parcelas=OrderManager.getParcela(),
dtprv=moment().hours(0).minutes(0).seconds(0).milliseconds(0);
dtprv.add(5,"days");let
dtvenc="",
obj={
PRVEN:prod.PRVEN,
CONDPG:condPag.CJ_CONDPAG,
ACRSVEN:condPag.ACRSVEN,
DESCVEN:condPag.DESCVEN,
prcVnd:0,
prcUni:0,
desconto:OrderManager.getDesconto()};



if(console.log("OBJETO: ",condPag,obj),"003"==obj.CONDPG){
for(var i in parcelas)
-1<i.indexOf("CJ_DATA")&&parcelas[i]&&(
console.log("DIA PARCELA: ",parcelas[i]),
aux+=1,
dtvenc=moment(parcelas[i]),
dif=dtvenc.diff(dtprv,"days"),
console.log("DIFERENCA: ",dif,dtvenc,dtprv),
fc+=7e-4*(dif-7),
console.log("FATOR: ",fc));


var media=fc/aux;
obj.prcVnd=obj.PRVEN*parseFloat(media)+obj.PRVEN,
obj.prcVnd.toFixed(2),
console.log("MEDIA: ",media),
console.log("VENDA: ",obj.prcVnd);
}else


obj.prcVnd=obj.ACRSVEN?obj.PRVEN+obj.PRVEN*obj.ACRSVEN/100:

obj.DESCVEN?
obj.PRVEN-obj.PRVEN*obj.DESCVEN/100:


obj.PRVEN;



return obj.prcVnd.toFixed(2),obj;
}

function updateViewCart(){var
cart=OrderManager.getTotalPedido(),
saldo=OrderManager.getSaldo(),
calcSaldo=(parseFloat(saldo)-parseFloat(cart.valor)).toFixed(2);

console.log("CALCULO SALDO: ",calcSaldo,saldo,parseFloat(cart.valor));var

lbPrd=`${cart.qntItens} Itens`,
lbCredito=`Créd. restante: R$${calcSaldo}`,
lbTotal=`Total pedido + frete: R$${Mask.real(parseFloat(cart.valor).toFixed(2))}`;
$.produtos.setText(lbPrd),
$.credito.setText(lbCredito),
$.total.setText(lbTotal);
}if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="Produtos",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Profile",id:"mainWindow",title:"Produtos",tabBarHidden:!0,modal:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){function __alloyId481(){$.__views.mainWindow.removeEventListener("open",__alloyId481),$.__views.mainWindow.activity?($.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),Ti.API.warn("UI component which does not have an Android activity. Android Activities"),Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."))}$.__views.mainWindow.addEventListener("open",__alloyId481)}$.__views.__alloyId482=Ti.UI.createView({id:"__alloyId482"}),$.__views.mainWindow.add($.__views.__alloyId482);var __alloyId483={},__alloyId486=[],__alloyId488={type:"Ti.UI.View",childTemplates:function(){var __alloyId489=[],__alloyId491={type:"Ti.UI.Label",bindId:"lbProduto",properties:{color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"14dp"},bindId:"lbProduto",maxLines:2,left:"0dp"}};return __alloyId489.push(__alloyId491),__alloyId489}(),properties:{layout:"vertical",width:Ti.UI.FILL,right:"55dp",left:0,top:"5dp"}};__alloyId486.push(__alloyId488);var __alloyId485={properties:{name:"templateProduto",backgroundColor:"white",width:Ti.UI.FILL,height:"50dp",left:"15dp",right:"20dp"},childTemplates:__alloyId486};__alloyId483.templateProduto=__alloyId485;var __alloyId493={properties:{name:"templateEmpty",backgroundColor:"white",height:"150dp",width:Titanium.UI.FILL,touchEnabled:!1}};__alloyId483.templateEmpty=__alloyId493,$.__views.listView=Ti.UI.createListView({width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId483,id:"listView"}),$.__views.__alloyId482.add($.__views.listView),$.__views.btFilter=Ti.UI.createView({backgroundColor:Alloy.Globals.GRAY_COLOR,id:"btFilter",width:Ti.UI.SIZE,height:"55dp",bottom:"80dp",borderRadius:18,elevation:11}),$.__views.__alloyId482.add($.__views.btFilter),onFilter?$.addListener($.__views.btFilter,"click",onFilter):__defers["$.__views.btFilter!click!onFilter"]=!0,$.__views.__alloyId494=Ti.UI.createLabel({font:{fontSize:"17dp"},text:"Todas op\xE7\xF5es",color:"white",width:Ti.UI.SIZE,left:"18dp",right:"18dp",id:"__alloyId494"}),$.__views.btFilter.add($.__views.__alloyId494),$.__views.viewCart=Ti.UI.createView({backgroundColor:Alloy.Globals.RED_COLOR,id:"viewCart",width:Ti.UI.FILL,height:"70dp",bottom:"0dp"}),$.__views.__alloyId482.add($.__views.viewCart),$.__views.imgCart=Ti.UI.createImageView({id:"imgCart",top:"10dp",left:"15dp",width:"23dp",height:"23dp",image:"/images/cart.png"}),$.__views.viewCart.add($.__views.imgCart),$.__views.produtos=Ti.UI.createLabel({color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},text:"0 Itens",id:"produtos",top:"10dp",left:"44dp",width:"68%"}),$.__views.viewCart.add($.__views.produtos),$.__views.credito=Ti.UI.createLabel({color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},text:"Cr\xE9d. restante: R$00,00",id:"credito",bottom:"10dp",left:"15dp",width:"68%"}),$.__views.viewCart.add($.__views.credito),$.__views.total=Ti.UI.createLabel({color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"14dp",fontWeight:"bold"},text:"Total pedido + frete: R$00,00",id:"total",top:"10dp",right:"15dp",width:"30%"}),$.__views.viewCart.add($.__views.total),$.__views.popup=Alloy.createWidget("PopUp","widget",{id:"popup",__parentSymbol:$.__views.__alloyId482}),$.__views.popup.setParent($.__views.__alloyId482),$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.__alloyId482}),$.__views.activityIndicator.setParent($.__views.__alloyId482),exports.destroy=function(){},_.extend($,$.__views);var args=$.args,optionsFilter=[],firstVisibleSectionIndex=0,moment=require("alloy/moment"),OrderManager=require("OrderManager"),LocalData=require("LocalData"),Mask=require("Mask");1?$.listView.setSearchView(Ti.UI.Android.createSearchView({hintText:"Procurar produto",backgroundColor:"#a5a5a5",color:"#205D33"})):$.listView.setSearchView(Titanium.UI.createSearchBar({hintText:"Procurar produto",barColor:"#e4e8e9",borderColor:"#e4e8e9",height:"45dp"})),

$.mainWindow.addEventListener("open",function(){
refreshMenuActionBar(),
createListView(),
updateViewCart();
}),

$.viewCart.addEventListener("click",function(e){
Alloy.Globals.openWindow("CadPedido/Cart");
}),

$.listView.addEventListener("itemclick",function(e){const
click=e.section.getItemAt(e.itemIndex),
produto=click.properties.data,
info=generateProdInfo(produto);
console.log("INFO PROD: ",info),
$.popup.showPopUpAddItem(produto,info,function(values,prod){
$.popup.hide(),
OrderManager.insertItems(values,prod),
updateViewCart(),
console.log("PRODUTO ADICIONADO: ",OrderManager.getTotalPedido());
});
}),





__defers["$.__views.btFilter!click!onFilter"]&&$.addListener($.__views.btFilter,"click",onFilter),



_.extend($,exports);
}

module.exports=Controller;