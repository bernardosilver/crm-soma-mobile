var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;




function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





if(require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="InfoCliente",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};











if($.__views.mainWindow=Ti.UI.createWindow({navTintColor:Alloy.Globals.WHITE_COLOR,backgroundColor:Alloy.Globals.WHITE_COLOR,barColor:Alloy.Globals.MAIN_COLOR,theme:"Theme.Custom",id:"mainWindow",title:"Novo Cliente",tabBarHidden:!0}),$.__views.mainWindow&&$.addTopLevelView($.__views.mainWindow),!0){
function __alloyId379(){
$.__views.mainWindow.removeEventListener("open",__alloyId379),
$.__views.mainWindow.activity?(
$.__views.mainWindow.activity.actionBar.title="Novo Cliente",$.__views.mainWindow.activity.actionBar.displayHomeAsUp=!0,$.__views.mainWindow.activity.actionBar.onHomeIconItemSelected=doClose):(

Ti.API.warn("You attempted to access an Activity on a lightweight Window or other"),
Ti.API.warn("UI component which does not have an Android activity. Android Activities"),
Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows."));

}
$.__views.mainWindow.addEventListener("open",__alloyId379);
}var
__alloyId380={},__alloyId383=[],__alloyId385={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"14dp"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId383.push(__alloyId385);var __alloyId382={properties:{name:"templateDados",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId383};__alloyId380.templateDados=__alloyId382;var __alloyId388=[],__alloyId390={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"14dp"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId388.push(__alloyId390);var __alloyId387={properties:{name:"templateEndereco",backgroundColor:"white",height:Ti.UI.SIZE,width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId388};__alloyId380.templateEndereco=__alloyId387;var __alloyId393=[],__alloyId395={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId393.push(__alloyId395);var __alloyId392={properties:{name:"templateCobranca",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId393};__alloyId380.templateCobranca=__alloyId392;var __alloyId398=[],__alloyId400={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId398.push(__alloyId400);var __alloyId397={properties:{name:"templateSocios",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId398};__alloyId380.templateSocios=__alloyId397;var __alloyId403=[],__alloyId405={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId403.push(__alloyId405);var __alloyId402={properties:{name:"templateComerciais",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId403};__alloyId380.templateComerciais=__alloyId402;var __alloyId408=[],__alloyId410={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId408.push(__alloyId410);var __alloyId407={properties:{name:"templateBancarias",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId408};__alloyId380.templateBancarias=__alloyId407;var __alloyId413=[],__alloyId415={type:"Ti.UI.Label",bindId:"lbNome",properties:{color:Alloy.Globals.GRAY_COLOR,font:{fontSize:"16dp",fontWeight:"bold"},touchEnabled:!1,bindId:"lbNome",left:"15dp",right:"65dp",height:Ti.UI.SIZE,width:Ti.UI.FILL}};__alloyId413.push(__alloyId415);var __alloyId412={properties:{name:"templateCredito",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,accessoryType:Ti.UI.LIST_ACCESSORY_TYPE_NONE},childTemplates:__alloyId413};__alloyId380.templateCredito=__alloyId412,$.__views.listView=Ti.UI.createListView(
{width:Ti.UI.FILL,height:Ti.UI.FILL,separatorColor:Alloy.Globals.LIGHT_GRAY_COLOR2,backgroundColor:"transparent",listSeparatorInsets:{left:0,right:0},templates:__alloyId380,id:"listView",top:"-2dp",bottom:"60dp"}),

$.__views.mainWindow.add($.__views.listView),
$.__views.activityIndicator=Alloy.createWidget("ActivityIndicator","widget",{id:"activityIndicator",__parentSymbol:$.__views.mainWindow}),
$.__views.activityIndicator.setParent($.__views.mainWindow),
$.__views.viewPanel=Ti.UI.createView(
{backgroundColor:Alloy.Globals.RED_COLOR,id:"viewPanel",height:"40dp",bottom:"10dp",left:"15dp",right:"15dp",width:Ti.UI.FILL,borderRadius:4,elevation:11}),

$.__views.mainWindow.add($.__views.viewPanel),
$.__views.btCheckOut=Ti.UI.createLabel(
{color:Alloy.Globals.WHITE_COLOR,font:{fontSize:"17dp",fontWeight:"bold"},id:"btCheckOut",textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER}),

$.__views.viewPanel.add($.__views.btCheckOut),
exports.destroy=function(){},




_.extend($,$.__views);



var args=$.args;









_.extend($,exports);
}

module.exports=Controller;