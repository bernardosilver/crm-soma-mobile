

(function(global,factory){
"object"==typeof exports&&"undefined"!=typeof module?factory(require("/alloy/moment")):"function"==typeof define&&define.amd?define(["moment"],factory):factory(global.moment);
})(global,function(moment){
"use strict";

var pt_br=moment.defineLocale("pt-br",{
months:["Janeiro","Fevereiro","Mar\xE7o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
monthsShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],
weekdays:["Domingo","Segunda-Feira","Ter\xE7a-Feira","Quarta-Feira","Quinta-Feira","Sexta-Feira","S\xE1bado"],
weekdaysShort:["Dom","Seg","Ter","Qua","Qui","Sex","S\xE1b"],
weekdaysMin:["Dom","2\xAA","3\xAA","4\xAA","5\xAA","6\xAA","S\xE1b"],
longDateFormat:{
LT:"HH:mm",
LTS:"HH:mm:ss",
L:"DD/MM/YYYY",
LL:"D [de] MMMM [de] YYYY",
LLL:"D [de] MMMM [de] YYYY [\xE0s] HH:mm",
LLLL:"dddd, D [de] MMMM [de] YYYY [\xE0s] HH:mm"},

calendar:{
sameDay:"[Hoje \xE0s] LT",
nextDay:"[Amanh\xE3 \xE0s] LT",
nextWeek:"dddd [\xE0s] LT",
lastDay:"[Ontem \xE0s] LT",
lastWeek:function(){
return 0===this.day()||6===this.day()?"[\xDAltimo] dddd [\xE0s] LT":"[\xDAltima] dddd [\xE0s] LT";
},
sameElse:"L"},

relativeTime:{
future:"em %s",
past:"%s atr\xE1s",
s:"poucos segundos",
m:"um minuto",
mm:"%d minutos",
h:"uma hora",
hh:"%d horas",
d:"um dia",
dd:"%d dias",
M:"um m\xEAs",
MM:"%d meses",
y:"um ano",
yy:"%d anos"},

ordinalParse:/\d{1,2}º/,
ordinal:"%d\xBA"});


return pt_br;
});