var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"ErrorMessage/"+s:
s.substring(0,index)+"/ErrorMessage/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){





























































































function click(e){
actionButton();
}var Widget=new(require("/alloy/widget"))("ErrorMessage");if(this.__widgetId="ErrorMessage",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.view=Ti.UI.createScrollView({id:"view",backgroundColor:"white",height:Ti.UI.FILL,width:Ti.UI.FILL,visible:!1}),$.__views.view&&$.addTopLevelView($.__views.view),$.__views.__alloyId19=Ti.UI.createView({height:Ti.UI.SIZE,width:Ti.UI.SIZE,layout:"vertical",top:"100dp",id:"__alloyId19"}),$.__views.view.add($.__views.__alloyId19),$.__views.label=Ti.UI.createLabel({color:Alloy.Globals.DARK_GRAY_COLOR,font:{fontSize:"17dp"},id:"label",width:"90%",textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT}),$.__views.__alloyId19.add($.__views.label),$.__views.button=Ti.UI.createButton({backgroundColor:Alloy.Globals.MAIN_COLOR,id:"button",height:"50dp",top:"30dp",color:"white",width:"60%",borderRadius:3}),$.__views.__alloyId19.add($.__views.button),click?$.addListener($.__views.button,"click",click):__defers["$.__views.button!click!click"]=!0,exports.destroy=function(){},_.extend($,$.__views);var args=arguments[0]||{},_class=args.class;for(prop in args)if("class"!=prop)$.view[prop]=args[prop];else{var style=$.createStyle({classes:args[prop]});$.view.applyProperties(style)}var actionButton=function(){};$.button.setVisible(!0),exports.setVisible=function(bool){$.view.setVisible(bool),$.view.setTouchEnabled(bool)},exports.setText=function(text){text=text||defaultMessage,$.label.setText(text)},exports.setTop=function(size){$.view.setTop(size)},exports.setTextAlign=function(align){$.label.setTextAlign(align)},exports.setBtTitle=function(title){title=title||defaultTitle,$.button.setTitle(title)},exports.setBtAction=function(action){actionButton=action||function(){},action?$.button.setVisible(!0):$.button.setVisible(!1)},





__defers["$.__views.button!click!click"]&&$.addListener($.__views.button,"click",click),



_.extend($,exports);
}

module.exports=Controller;