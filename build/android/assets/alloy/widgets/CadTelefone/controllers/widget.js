var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"CadTelefone/"+s:
s.substring(0,index)+"/CadTelefone/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){
var Widget=new(require("/alloy/widget"))("CadTelefone");




if(this.__widgetId="CadTelefone",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};







$.__views.viewMain=Ti.UI.createView(
{id:"viewMain",width:Ti.UI.FILL,height:"45dp"}),

$.__views.viewMain&&$.addTopLevelView($.__views.viewMain),
$.__views.viewBtApagar=Ti.UI.createView(
{id:"viewBtApagar",height:Ti.UI.FILL,width:"80dp",right:0}),

$.__views.viewMain.add($.__views.viewBtApagar),
$.__views.btApagar=Ti.UI.createButton(
{backgroundColor:Alloy.Globals.RED_COLOR,id:"btApagar",height:Ti.UI.FILL,width:Ti.UI.FILL,title:"Apagar",color:"white"}),

$.__views.viewBtApagar.add($.__views.btApagar),
$.__views.viewContents=Ti.UI.createView(
{id:"viewContents",height:Ti.UI.FILL,width:"100%",backgroundColor:"white"}),

$.__views.viewMain.add($.__views.viewContents),
$.__views.__alloyId1=Ti.UI.createImageView(
{touchEnabled:!1,image:"/images/exit.png",height:"25dp",width:"25dp",left:"130dp",id:"__alloyId1"}),

$.__views.viewContents.add($.__views.__alloyId1),
$.__views.tfTelefone1=Ti.UI.createTextField(
{font:{fontSize:"17dp"},color:"black",height:"45dp",backgroundColor:"white",right:0,borderRadius:5,paddingLeft:10,paddingRight:0,hintTextColor:"gray",bubbleParent:!0,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfTelefone1",left:"50dp",bottom:0,hintText:"Telefone alternativo",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),

$.__views.viewContents.add($.__views.tfTelefone1),
$.__views.picMenos=Ti.UI.createView(
{id:"picMenos",width:"50dp",left:0,bubbleParent:!1,height:Ti.UI.FILL}),

$.__views.viewContents.add($.__views.picMenos),
$.__views.__alloyId2=Ti.UI.createImageView(
{width:"30dp",height:"30dp",left:"10dp",image:"/images/removeRed.png",id:"__alloyId2"}),

$.__views.picMenos.add($.__views.__alloyId2),
$.__views.__alloyId3=Ti.UI.createView(
{width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId3"}),

$.__views.viewMain.add($.__views.__alloyId3),
exports.destroy=function(){},




_.extend($,$.__views);var







actionApagar,args=arguments[0]||{},tipo=args.tipo||"",tel=args.tel||"",editable=args.editable||!1,mask=require("Mask");

"fixo"==tipo?
$.tfTelefone1.setHintText("Telefone alternativo"):
"cel"==tipo&&
$.tfTelefone1.setHintText("Telefone alternativo"),
tel&&$.tfTelefone1.setValue(tel),

$.tfTelefone1.setEditable(editable),

exports.setFocus=function(){!1,




$.viewMain.setHeight("1dp"),
$.viewMain.animate({height:"45dp",duration:200},function(){});
},

exports.getValues=function(){
return{
telefone:$.tfTelefone1.getValue()};

},

exports.setActionApagar=function(action){
actionApagar=action;
},

$.picMenos.addEventListener("click",function(e){return!
editable||void
$.viewContents.animate({left:"-80dp",duration:200},function(){});
}),

$.btApagar.addEventListener("click",function(e){return!
editable||void(
setTimeout(function(){actionApagar()},200),
$.viewMain.animate({height:"1dp",duration:200},function(){}));




}),

$.tfTelefone1.addEventListener("change",function(e){
var v="fixo"==tipo?mask.tel(e.value):mask.telefone(e.value);
$.tfTelefone1.getValue()!=v&&$.tfTelefone1.setValue(v),
$.tfTelefone1.setSelection($.tfTelefone1.getValue().length,$.tfTelefone1.getValue().length);
}),









_.extend($,exports);
}

module.exports=Controller;