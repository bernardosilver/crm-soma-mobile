var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"Input/"+s:
s.substring(0,index)+"/Input/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){























































































































function click(e){
$.textField.focus();
}var Widget=new(require("/alloy/widget"))("Input");if(this.__widgetId="Input",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.viewMain=Ti.UI.createView({id:"viewMain",top:"5dp",bottom:"5dp",height:"50dp",width:Ti.UI.FILL}),$.__views.viewMain&&$.addTopLevelView($.__views.viewMain),click?$.addListener($.__views.viewMain,"click",click):__defers["$.__views.viewMain!click!click"]=!0,$.__views.viewTf=Ti.UI.createView({id:"viewTf",backgroundColor:"white",height:"50dp",width:Ti.UI.FILL,borderWidth:1,borderColor:"transparent"}),$.__views.viewMain.add($.__views.viewTf),click?$.addListener($.__views.viewTf,"click",click):__defers["$.__views.viewTf!click!click"]=!0,$.__views.textField=Ti.UI.createTextField({hintTextColor:Alloy.Globals.GRAY_COLOR,id:"textField",height:Ti.UI.FILL,width:Ti.UI.FILL,left:"5dp",right:"5dp",backgroundColor:"transparent"}),$.__views.viewTf.add($.__views.textField),exports.destroy=function(){},_.extend($,$.__views);var args=arguments[0]||{};args.hintText&&$.textField.setHintText(args.hintText),0<args.maxLength&&$.textField.setMaxLength(args.maxLength),exports.setHintText=function(text){$.textField.setHintText(text)},exports.setBottom=function(value){$.viewMain.setBottom(value)},exports.setPasswordMask=function(bool){$.textField.setPasswordMask(bool)},exports.setReturnKeyType=function(text){var type=Titanium.UI.RETURNKEY_DEFAULT;"next"==text?type=Titanium.UI.RETURNKEY_NEXT:"go"==text?type=Titanium.UI.RETURNKEY_GO:"send"==text?type=Titanium.UI.RETURNKEY_SEND:"continue"==text?type=Titanium.UI.RETURNKEY_CONTINUE:"done"==text&&(type=Titanium.UI.RETURNKEY_DONE),$.textField.setReturnKeyType(type)},exports.setKeyboardType=function(text){var type=Titanium.UI.KEYBOARD_TYPE_DEFAULT;"email"==text?type=Titanium.UI.KEYBOARD_TYPE_EMAIL:"number"==text?type=Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION:"user"==text?type=Titanium.UI.KEYBOARD_TYPE_ASCII:"phone"==text?type=Titanium.UI.KEYBOARD_TYPE_PHONE_PAD:"decimal"==text&&(type=Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD),$.textField.setKeyboardType(type)},exports.setValue=function(text){$.textField.setValue(text)},exports.getValue=function(){return $.textField.getValue()},exports.setColorLabel=function(value){$.lbHeader.setColor(value)},exports.setColorBorder=function(value){$.viewTf.setBorderColor(value)},exports.setBackgroundColor=function(value){$.viewTf.setBackgroundColor(value)},exports.setEditable=function(bool){$.textField.setEditable(bool)},exports.blur=function(){$.textField.blur()},exports.hide=function(){$.viewMain.hide()},exports.show=function(){$.viewMain.show()},

$.textField.addEventListener("change",function(e){
var v=e.value;
v=v.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g,"$1.$2.$3-$4"),

args.maxLength&&(
$.textField.getValue()!=v&&$.textField.setValue(v),
$.textField.setSelection($.textField.getValue().length,$.textField.getValue().length));

}),





__defers["$.__views.viewMain!click!click"]&&$.addListener($.__views.viewMain,"click",click),__defers["$.__views.viewTf!click!click"]&&$.addListener($.__views.viewTf,"click",click),



_.extend($,exports);
}

module.exports=Controller;