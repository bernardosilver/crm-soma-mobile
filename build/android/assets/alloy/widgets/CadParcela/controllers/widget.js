var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"CadParcela/"+s:
s.substring(0,index)+"/CadParcela/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){
var Widget=new(require("/alloy/widget"))("CadParcela");




if(this.__widgetId="CadParcela",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var
__parentSymbol=__processArg(arguments[0],"__parentSymbol"),
$model=__processArg(arguments[0],"$model"),
__itemTemplate=__processArg(arguments[0],"__itemTemplate");var

$=this,
exports={},
__defers={};







$.__views.viewMain=Ti.UI.createView(
{id:"viewMain",width:Ti.UI.FILL,height:"45dp"}),

$.__views.viewMain&&$.addTopLevelView($.__views.viewMain),
$.__views.viewBtApagar=Ti.UI.createView(
{id:"viewBtApagar",height:Ti.UI.FILL,width:"80dp",right:0}),

$.__views.viewMain.add($.__views.viewBtApagar),
$.__views.btApagar=Ti.UI.createButton(
{backgroundColor:Alloy.Globals.RED_COLOR,id:"btApagar",height:Ti.UI.FILL,width:Ti.UI.FILL,title:"Apagar",color:"white"}),

$.__views.viewBtApagar.add($.__views.btApagar),
$.__views.viewContents=Ti.UI.createView(
{id:"viewContents",height:Ti.UI.FILL,width:"100%",backgroundColor:"white"}),

$.__views.viewMain.add($.__views.viewContents),
$.__views.__alloyId20=Ti.UI.createImageView(
{touchEnabled:!1,image:"/images/exit.png",height:"25dp",width:"25dp",left:"130dp",id:"__alloyId20"}),

$.__views.viewContents.add($.__views.__alloyId20),
$.__views.tfParcela1=Ti.UI.createTextField(
{font:{fontSize:"17dp"},color:"black",height:"45dp",backgroundColor:"white",right:0,borderRadius:5,paddingLeft:10,paddingRight:0,hintTextColor:"gray",bubbleParent:!0,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfParcela1",left:"50dp",bottom:0,hintText:"Clique para selecionar a data da parcela",editable:!1}),

$.__views.viewContents.add($.__views.tfParcela1),
$.__views.picMenos=Ti.UI.createView(
{id:"picMenos",width:"50dp",left:0,bubbleParent:!1,height:Ti.UI.FILL}),

$.__views.viewContents.add($.__views.picMenos),
$.__views.__alloyId21=Ti.UI.createImageView(
{width:"30dp",height:"30dp",left:"10dp",image:"/images/removeRed.png",id:"__alloyId21"}),

$.__views.picMenos.add($.__views.__alloyId21),
$.__views.__alloyId22=Ti.UI.createView(
{width:"95%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId22"}),

$.__views.viewMain.add($.__views.__alloyId22),
exports.destroy=function(){},




_.extend($,$.__views);var







actionApagar,args=arguments[0]||{},data=args.data||"",editable=args.editable||!1,mask=require("Mask"),moment=require("alloy/moment");


$.tfParcela1.setHintText("Data da parcela"),
data&&$.tfParcela1.setValue(data),

$.tfParcela1.setEditable(editable);

const formatData=dia=>{var
arr=dia.split("/"),
dt=arr[2]+arr[1]+arr[0];
return dt;
};

var ultima=moment(formatData(args.ultima)).add(1,"d");

const showPicker=()=>{var
anoAtual=moment(ultima).year(),
mesAtual=moment(ultima).month(),
diaAtual=moment(ultima).date(),


maxDate=moment().add(3,"d");
0==maxDate.day()?
maxDate=moment().add(4,"d"):

6==maxDate.day()&&(
maxDate=moment().add(5,"d")),


maxDate=moment().add(65,"d");var
maxAno=moment(maxDate).year(),
maxMes=moment(maxDate).month(),
maxDia=moment(maxDate).date(),

value=moment(ultima).add(1,"days"),
valueAno=moment(value).year(),
valueMes=moment(value).month(),
valueDia=moment(value).date(),

picker=Ti.UI.createPicker({
type:Ti.UI.PICKER_TYPE_DATE,
locale:"pt_BR",
minDate:new Date(anoAtual,mesAtual,diaAtual),
maxDate:new Date(maxAno,maxMes,maxDia),
value:new Date(valueAno,valueMes,valueDia)});


picker.showDatePickerDialog({
value:new Date(valueAno,valueMes,valueDia),
callback:function(e){
e.cancel?
Ti.API.info("User canceled dialog"):(

Ti.API.info("User selected date: "+moment(e.value).format("DD/MM/YYYY")),
$.tfParcela1.setValue(moment(e.value).format("DD/MM/YYYY")));

}});

};


exports.setFocus=function(){!1,




$.viewMain.setHeight("1dp"),
$.viewMain.animate({height:"45dp",duration:200},function(){});
},

exports.getValues=function(){
return{
parcela:$.tfParcela1.getValue()};

},

exports.setActionApagar=function(action){
actionApagar=action;
},

$.picMenos.addEventListener("click",function(e){return!
editable||void
$.viewContents.animate({left:"-80dp",duration:200},function(){});
}),

$.btApagar.addEventListener("click",function(e){return!
editable||void(
setTimeout(function(){actionApagar()},200),
$.viewMain.animate({height:"1dp",duration:200},function(){}));




}),

$.tfParcela1.addEventListener("focus",function(e){
showPicker();
}),

$.tfParcela1.addEventListener("click",function(e){
showPicker();
}),









_.extend($,exports);
}

module.exports=Controller;