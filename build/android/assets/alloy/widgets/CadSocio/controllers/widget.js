var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"CadSocio/"+s:
s.substring(0,index)+"/CadSocio/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){































































































function validaCPF(value){var
copy=value.replace(".","").replace(".","").replace("-",""),
num1=parseInt(copy[0]),
num2=parseInt(copy[1]),
num3=parseInt(copy[2]),
num4=parseInt(copy[3]),
num5=parseInt(copy[4]),
num6=parseInt(copy[5]),
num7=parseInt(copy[6]),
num8=parseInt(copy[7]),
num9=parseInt(copy[8]),
num10=parseInt(copy[9]),
num11=parseInt(copy[10]);

if(num1==num2&&num2==num3&&num3==num4&&num4==num5&&num5==num6&&num6==num7&&num7==num8&&num8==num9&&num9==num10&&num10==num11)return!1;var
soma1=10*num1+9*num2+8*num3+7*num4+6*num5+5*num6+4*num7+3*num8+2*num9,
resto1=10*soma1%11;
if(10==resto1||11==resto1)resto1=0;else
if(resto1!=num10)return!1;var

soma2=11*num1+10*num2+9*num3+8*num4+7*num5+6*num6+5*num7+4*num8+3*num9+2*num10,
resto2=10*soma2%11;
if(10==resto2||11==resto2)resto2=0;else
if(resto2!=num11)return!1;

return!0;
}var Widget=new(require("/alloy/widget"))("CadSocio");if(this.__widgetId="CadSocio",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.viewMain=Ti.UI.createView({id:"viewMain",width:Ti.UI.FILL,height:"100dp"}),$.__views.viewMain&&$.addTopLevelView($.__views.viewMain),$.__views.viewBtApagar=Ti.UI.createView({id:"viewBtApagar",height:Ti.UI.FILL,width:"80dp",right:0,backgroundColor:"yellow"}),$.__views.viewMain.add($.__views.viewBtApagar),$.__views.btApagar=Ti.UI.createButton({backgroundColor:Alloy.Globals.RED_COLOR,id:"btApagar",height:Ti.UI.FILL,width:Ti.UI.FILL,title:"Apagar",color:"white"}),$.__views.viewBtApagar.add($.__views.btApagar),$.__views.viewContents=Ti.UI.createView({id:"viewContents",height:Ti.UI.FILL,width:"100%",backgroundColor:"white"}),$.__views.viewMain.add($.__views.viewContents),$.__views.__alloyId4=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"0dp",left:"50dp",id:"__alloyId4"}),$.__views.viewContents.add($.__views.__alloyId4),$.__views.tfNome=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",backgroundColor:"white",right:"5%",borderRadius:5,paddingLeft:5,paddingRight:5,hintTextColor:"gray",bubbleParent:!0,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfNome",maxLength:30,width:Ti.UI.FILL,hintText:"Nome completo",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS}),$.__views.__alloyId4.add($.__views.tfNome),$.__views.__alloyId5=Ti.UI.createView({width:"100%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId5"}),$.__views.__alloyId4.add($.__views.__alloyId5),$.__views.__alloyId6=Ti.UI.createView({width:Ti.UI.FILL,height:"45dp",top:"45dp",left:"50dp",id:"__alloyId6"}),$.__views.viewContents.add($.__views.__alloyId6),$.__views.tfCpf=Ti.UI.createTextField({font:{fontSize:"17dp"},color:"black",height:"45dp",backgroundColor:"white",right:"5%",borderRadius:5,paddingLeft:5,paddingRight:5,hintTextColor:"gray",bubbleParent:!0,returnKeyType:Titanium.UI.RETURNKEY_NEXT,id:"tfCpf",width:Ti.UI.FILL,hintText:"CPF - somente numeros",autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_WORDS,keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),$.__views.__alloyId6.add($.__views.tfCpf),$.__views.__alloyId7=Ti.UI.createView({width:"100%",height:"1px",right:0,backgroundColor:"#d0d2d2",bottom:0,id:"__alloyId7"}),$.__views.__alloyId6.add($.__views.__alloyId7),$.__views.lbId=Ti.UI.createLabel({id:"lbId",visible:!1}),$.__views.viewContents.add($.__views.lbId),$.__views.picMenos=Ti.UI.createView({id:"picMenos",width:"40dp",left:0,bubbleParent:!1,height:Ti.UI.FILL}),$.__views.viewContents.add($.__views.picMenos),$.__views.__alloyId8=Ti.UI.createImageView({width:"30dp",height:"30dp",left:"10dp",image:"/images/removeRed.png",id:"__alloyId8"}),$.__views.picMenos.add($.__views.__alloyId8),exports.destroy=function(){},_.extend($,$.__views);var actionApagar,args=arguments[0]||{},socio=args.socio||"",editable=args.editable||!1,mask=require("Mask");socio&&($.tfNome.setValue(socio.AO_NOMINS),$.tfCpf.setValue(mask.cnpj(socio.AO_CGC)),$.lbId.setText(socio.ID)),$.tfNome.setEditable(editable),$.tfCpf.setEditable(editable),

exports.setFocus=function(){!1,




$.viewMain.setHeight("1dp"),
$.viewMain.animate({height:"100dp",duration:200},function(){});
},

exports.getValues=function(){
return{
AO_NOMINS:$.tfNome.getValue(),
AO_CGC:$.tfCpf.getValue(),
ID:$.lbId.getText()};

},

exports.setActionApagar=function(action){return!
editable||void(
actionApagar=action);
},

$.picMenos.addEventListener("click",function(e){return!
editable||void
$.viewContents.animate({left:"-80dp",duration:200},function(){});
}),

$.btApagar.addEventListener("click",function(e){return!
editable||void(
setTimeout(function(){actionApagar()},200),
$.viewMain.animate({height:"1dp",duration:200},function(){}));




}),


$.tfCpf.addEventListener("change",function(e){
var v="";
v=mask.cpf(e.value),
$.tfCpf.setColor(validaCPF(v)?"blue":"red"),
$.tfCpf.getValue()!=v&&$.tfCpf.setValue(v);
}),









_.extend($,exports);
}

module.exports=Controller;