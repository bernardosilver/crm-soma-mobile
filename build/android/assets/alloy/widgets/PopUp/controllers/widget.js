var Alloy=require("/alloy"),
Backbone=Alloy.Backbone,
_=Alloy._;


function WPATH(s){var
index=s.lastIndexOf("/"),
path=-1===index?
"PopUp/"+s:
s.substring(0,index)+"/PopUp/"+s.substring(index+1);

return 0===path.indexOf("/")?path:"/"+path;
}

function __processArg(obj,key){
var arg=null;



return obj&&(arg=obj[key]||null),arg;
}

function Controller(){














































function calcPreco(desc,prcIni){return(
desc?
(parseFloat(prcIni).toFixed(2)*parseFloat(desc).toFixed(2)/100+parseFloat(prcIni)).toFixed(2):

prcIni);
}

function createViewLabelWithTag(options,callback,title,callBackPedir){var
view=Ti.UI.createScrollView({
layout:"vertical",
backgroundColor:"white",
bottom:"25dp",
top:"25dp",
height:Ti.UI.SIZE}),

lbTitle=Ti.UI.createLabel({
color:"gray",
top:1?"15dp":callBackPedir?"15dp":"0dp",
bottom:"15dp",
font:{
fontSize:14},

textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
height:Ti.UI.SIZE,
text:title}),

viewLine=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"15dp",
right:"15dp",
backgroundColor:Alloy.Globals.BACKGROUND_COLOR});









for(var i in view.add(lbTitle),view.add(viewLine),options)
view.add(itemLabelWithTag(options[i],callback,i));




return view.add(Ti.UI.createView({height:"25dp"})),view;
}


function itemLabelWithTag(option,callback,i){var

valor,
tag,
array=option.split(": ");

2==array.length?(
valor=array[1],
tag=array[0]):(

valor=option,
tag="");var


view=Ti.UI.createView({
height:"65dp",
width:Ti.UI.FILL,
left:0,
bubbleParent:!callback}),


viewLine=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
bottom:0,
left:"15dp",
right:"15dp",
backgroundColor:Alloy.Globals.BACKGROUND_COLOR}),


view2=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.SIZE,
layout:"vertical",
left:"10dp"}),


etiqueta=Ti.UI.createLabel({
text:tag,
color:Alloy.Globals.BLUE_COLOR,
font:{
fontSize:12},

left:"10dp",
top:"5dp",
textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT}),


label=Ti.UI.createLabel({
text:valor,
color:lbColor,
height:"32dp",
font:{
fontSize:14,
fontWeight:"bold"},

left:"10dp",
textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT}),


imgCTA=Ti.UI.createImageView({
image:"/images/disclosure.png",
height:"20dp",
width:"20dp",
right:"15dp"});














return view.addEventListener("click",function(e){callback&&(callback(valor,i),hide())}),view.add(viewLine),view.add(view2),view.add(imgCTA),2==array.length&&view2.add(etiqueta),view2.add(label),view;
}

function createPopUpAddItem(prod,info,callback){var
desconto=OrderManager.getDesconto(),
view=Ti.UI.createScrollView({
layout:"vertical",
backgroundColor:"white",
bottom:"25dp",
top:"25dp",
height:Ti.UI.SIZE}),


lbTitle=Ti.UI.createLabel({
color:"#205D33",
top:"15dp",
bottom:"15dp",
font:{
fontSize:18,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
height:Ti.UI.SIZE,
text:prod.DESCR+" - R$"+parseFloat(info.prcVnd).toFixed(2)}),


viewLine=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"15dp",
right:"15dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#CCC"}),


viewSeparator1=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"25dp",
right:"25dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#EEE"}),


viewSeparator2=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"25dp",
right:"25dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#4C5454"}),


viewSeparator3=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"25dp",
right:"25dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#4C5454"}),


lbQnt=Ti.UI.createLabel({
color:"#28a745",
top:"5dp",
bottom:"9dp",
left:"10dp",
font:{
fontSize:17,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
height:Ti.UI.SIZE,
text:"Quantidade: "}),


lbDesc=Ti.UI.createLabel({
color:"#28a745",
top:"5dp",
bottom:"9dp",
left:"10dp",
font:{
fontSize:17,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
height:Ti.UI.SIZE,
text:"Desconto (%): "}),


lbValor=Ti.UI.createLabel({
color:"#28a745",
top:"5dp",
bottom:"9dp",
left:"10dp",
font:{
fontSize:17,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
height:Ti.UI.SIZE,
text:"Pre\xE7o (R$): "}),


viewQnt=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"0dp",
bottom:"15dp"}),


viewDesc=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"0dp",
bottom:"15dp"}),


viewValor=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"0dp",
bottom:"15dp"}),


imgRemovQnt=Ti.UI.createImageView({
image:"/images/removeRed.png",
height:"31dp",
width:"31dp",
right:"20dp"}),


imgRemovDesc=Ti.UI.createImageView({
image:"/images/removeRed.png",
height:"31dp",
width:"31dp",
right:"20dp"}),


txtQnt=Ti.UI.createTextField({
height:"40dp",
width:"100dp",
color:"#205D33",
backgroundColor:"#B2B2B2",
font:{
fontSize:18,
fontWeight:"bold"},

hintText:"0",
value:prod.CK_QTDVEN?prod.CK_QTDVEN:0,
borderRadius:4,
keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),


txtDesc=Ti.UI.createTextField({
height:"40dp",
width:"100dp",
color:"#205D33",
backgroundColor:"#B2B2B2",
font:{
fontSize:18,
fontWeight:"bold"},

hintText:"0",
value:prod.CK_DESCONT?prod.CK_DESCONT:desconto,
borderRadius:4,
keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),


txtValor=Ti.UI.createTextField({
height:"40dp",
width:"100dp",

color:"#205D33",
backgroundColor:"#B2B2B2",
font:{
fontSize:18,
fontWeight:"bold"},

hintText:"0",
value:prod.CK_PRUNIT?parseFloat(prod.CK_PRUNIT).toFixed(2):calcPreco(desconto,info.prcVnd),
borderRadius:4,
keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),


imgAddQnt=Ti.UI.createImageView({
image:"/images/addGreen.png",
height:"40dp",
width:"40dp",
right:"65dp"}),


imgAddDesc=Ti.UI.createImageView({
image:"/images/addGreen.png",
height:"40dp",
width:"40dp",
right:"65dp"}),


viewBtns=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"15dp",
bottom:"15dp"}),



btnConfirm=Ti.UI.createButton({
title:"Confirmar",
left:"35dp",
backgroundColor:"#28a745",
borderRadius:4,
font:{
fontSize:17}}),



btnCancel=Ti.UI.createButton({
title:"Cancelar",
right:"35dp",
backgroundColor:"#FF4B4B",
borderRadius:4,
font:{
fontSize:17}});





























































































return btnConfirm.addEventListener("click",function(){if(0==txtQnt.getValue()||0==txtValor.getValue())return Alloy.Globals.showAlert("Ops!","\xC9 necess\xE1rio inserir a quantidade e o valor do item!");let prcUni=parseFloat(txtValor.getValue()).toFixed(2);callback({qnt:parseInt(txtQnt.getValue()),val:prcUni,desc:parseFloat(txtDesc.getValue()).toFixed(2)},prod)}),btnCancel.addEventListener("click",function(){1?dialog&&dialog.hide():hide()}),imgRemovQnt.addEventListener("click",function(){var value=txtQnt.getValue()?parseFloat(txtQnt.getValue()).toFixed(2):0;value=0>=value?0:value-1,txtQnt.setValue(value)}),imgRemovDesc.addEventListener("click",function(){var value=txtDesc.getValue()?parseFloat(txtDesc.getValue()).toFixed(2):0;--value,txtDesc.setValue(value);let prcUni=calcPreco(parseFloat(txtDesc.getValue()),info.prcVnd);txtValor.setValue(prcUni)}),imgAddQnt.addEventListener("click",function(){var value=txtQnt.getValue()?parseFloat(txtQnt.getValue()).toFixed(2):0;++value,txtQnt.setValue(value)}),imgAddDesc.addEventListener("click",function(){var value=txtDesc.getValue()?parseFloat(txtDesc.getValue()).toFixed(2):0;++value,txtDesc.setValue(value);let prcUni=calcPreco(parseFloat(txtDesc.getValue()),info.prcVnd);txtValor.setValue(prcUni)}),txtQnt.addEventListener("change",function(e){txtQnt.setValue(Mask.int(e.source.value))}),txtDesc.addEventListener("change",function(e){if(!txtDesc.getValue())return txtValor.setValue(parseFloat(info.prcVnd));txtDesc.setValue(Mask.float(e.source.value));let prcUni=calcPreco(parseFloat(txtDesc.getValue()),info.prcVnd);txtValor.setValue(prcUni)}),txtValor.addEventListener("change",function(e){txtValor.setValue(Mask.float(e.source.value));let desc=((parseFloat(txtValor.getValue()).toFixed(2)-parseFloat(info.prcVnd).toFixed(2))/(parseFloat(info.prcVnd).toFixed(2)/100)).toFixed(2);txtDesc.setValue(desc)}),view.add(lbTitle),view.add(viewLine),viewQnt.add(lbQnt),viewQnt.add(imgRemovQnt),viewQnt.add(txtQnt),viewQnt.add(imgAddQnt),view.add(viewQnt),viewValor.add(lbValor),viewValor.add(txtValor),view.add(viewValor),viewDesc.add(lbDesc),viewDesc.add(imgRemovDesc),viewDesc.add(txtDesc),viewDesc.add(imgAddDesc),view.add(viewDesc),view.add(viewSeparator3),view.add(viewBtns),viewBtns.add(btnConfirm),viewBtns.add(btnCancel),view;
}

function createPopUpDesconto(callback){var
view=Ti.UI.createScrollView({
layout:"vertical",
backgroundColor:"white",
bottom:"25dp",
top:"25dp",
height:Ti.UI.SIZE}),


lbTitle=Ti.UI.createLabel({
color:"#205D33",
top:"15dp",
bottom:"15dp",
font:{
fontSize:18,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
height:Ti.UI.SIZE,
text:"DESCONTO GERAL"}),


viewLine=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"15dp",
right:"15dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#CCC"}),


viewSeparator3=Ti.UI.createView({
height:1,
width:Ti.UI.FILL,
left:"25dp",
right:"25dp",
top:"15dp",
bottom:"15dp",
backgroundColor:"#4C5454"}),


lbDesc=Ti.UI.createLabel({
color:"#28a745",
top:"5dp",
bottom:"9dp",
left:"10dp",
font:{
fontSize:17,
fontWeight:"bold"},

textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
height:Ti.UI.SIZE,
text:"Desconto (%): "}),


viewDesc=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"0dp",
bottom:"15dp"}),


imgRemovDesc=Ti.UI.createImageView({
image:"/images/removeRed.png",
height:"31dp",
width:"31dp",
right:"20dp"}),


txtDesc=Ti.UI.createTextField({
height:"40dp",
width:"100dp",
color:"#205D33",
backgroundColor:"#B2B2B2",
font:{
fontSize:18,
fontWeight:"bold"},

hintText:"0",
value:0,
borderRadius:4,
keyboardType:Titanium.UI.KEYBOARD_TYPE_NUMBER_PAD}),


imgAddDesc=Ti.UI.createImageView({
image:"/images/addGreen.png",
height:"40dp",
width:"40dp",
right:"65dp"}),


viewBtns=Ti.UI.createView({
height:Ti.UI.SIZE,
width:Ti.UI.FILL,
top:"15dp",
bottom:"15dp"}),



btnConfirm=Ti.UI.createButton({
title:"Confirmar",
left:"35dp",
backgroundColor:"#28a745",
borderRadius:4,
font:{
fontSize:17}}),



btnCancel=Ti.UI.createButton({
title:"Cancelar",
right:"35dp",
backgroundColor:"#FF4B4B",
borderRadius:4,
font:{
fontSize:17}});














































return btnConfirm.addEventListener("click",function(){callback(parseFloat(txtDesc.getValue()).toFixed(2))}),btnCancel.addEventListener("click",function(){1?dialog&&dialog.hide():hide()}),imgRemovDesc.addEventListener("click",function(){var value=txtDesc.getValue()?parseFloat(txtDesc.getValue()).toFixed(2):0;--value,txtDesc.setValue(value)}),imgAddDesc.addEventListener("click",function(){var value=txtDesc.getValue()?parseFloat(txtDesc.getValue()).toFixed(2):0;++value,txtDesc.setValue(value)}),txtDesc.addEventListener("change",function(e){txtDesc.setValue(Mask.float(e.source.value))}),view.add(lbTitle),view.add(viewLine),viewDesc.add(lbDesc),viewDesc.add(imgRemovDesc),viewDesc.add(txtDesc),viewDesc.add(imgAddDesc),view.add(viewDesc),view.add(viewSeparator3),view.add(viewBtns),viewBtns.add(btnConfirm),viewBtns.add(btnCancel),view;
}














































function onClick(){
isDisplay&&
hide();
}

function show(){
$.main.touchEnabled=!0,
$.main.visible=!0,
$.main.animate({
duration:120,
opacity:1},
function(){
isDisplay=!0;
});
}

function hide(){
$.main.touchEnabled=!1,
isDisplay=!1,
$.main.animate({
duration:120,
opacity:0},
function(){
$.main.visible=!1;
});
}var Widget=new(require("/alloy/widget"))("PopUp");if(this.__widgetId="PopUp",require("/alloy/controllers/BaseController").apply(this,Array.prototype.slice.call(arguments)),this.__controllerPath="widget",this.args=arguments[0]||{},arguments[0])var __parentSymbol=__processArg(arguments[0],"__parentSymbol"),$model=__processArg(arguments[0],"$model"),__itemTemplate=__processArg(arguments[0],"__itemTemplate");var $=this,exports={},__defers={};$.__views.main=Ti.UI.createView({touchEnabled:!1,id:"main",width:Ti.UI.FILL,height:Ti.UI.FILL,opacity:0,visible:!1}),$.__views.main&&$.addTopLevelView($.__views.main),onClick?$.addListener($.__views.main,"click",onClick):__defers["$.__views.main!click!onClick"]=!0,$.__views.background=Ti.UI.createView({id:"background",backgroundColor:"black",opacity:.3,width:Ti.UI.FILL,height:Ti.UI.FILL}),$.__views.main.add($.__views.background),$.__views.content=Ti.UI.createScrollView({id:"content",top:"50dp",bottom:"50dp",width:"85%",height:Ti.UI.SIZE,borderRadius:4,backgroundColor:"white",elevation:10,contentWidth:Ti.UI.FILL,showVerticalScrollIndicator:!0}),$.__views.main.add($.__views.content),exports.destroy=function(){},_.extend($,$.__views);var Mask=require("Mask"),isDisplay=!1,lbColor=Alloy.Globals.DARK_GRAY_COLOR,dialog=null,OrderManager=require("OrderManager");exports.showLabelWithTag=function(options,callback,title,callBackPedir){1?(dialog=Ti.UI.createOptionDialog(),dialog.setAndroidView(createViewLabelWithTag(options,callback,title,callBackPedir)),dialog.show()):($.content.removeAllChildren(),$.content.add(createViewLabelWithTag(options,callback,title,callBackPedir)),show())},exports.showPopUpAddItem=function(prod,info,callback){1?(dialog=Ti.UI.createOptionDialog(),dialog.setAndroidView(createPopUpAddItem(prod,info,callback)),dialog.show()):($.content.removeAllChildren(),$.content.add(createPopUpAddItem(prod,info,callback)),show())},exports.showPopUpDesconto=function(callback){1?(dialog=Ti.UI.createOptionDialog(),dialog.setAndroidView(createPopUpDesconto(callback)),dialog.show()):($.content.removeAllChildren(),$.content.add(createPopUpDesconto(callback)),show())},exports.hide=function(){1?dialog&&dialog.hide():hide()},





__defers["$.__views.main!click!onClick"]&&$.addListener($.__views.main,"click",onClick),



_.extend($,exports);
}

module.exports=Controller;