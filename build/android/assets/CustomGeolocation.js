var httpClient=null,
warnning=!1;

function getCurrentCoordinatesiOS(action){

Ti.Geolocation.purpose="Localiza\xE7\xE3o Atual via GPS",
Ti.Geolocation.accuracy=Ti.Geolocation.ACCURACY_BEST,
Ti.Geolocation.distanceFilter=0,
Ti.Geolocation.preferredProvider=Ti.Geolocation.PROVIDER_GPS;

var locationCallBack=function(e){(
!e.success||e.error)&&
!1===warnning&&(
warnning=!0),



action&&action(e.coords),

Ti.Geolocation.removeEventListener("location",locationCallBack);
};
Ti.Geolocation.addEventListener("location",locationCallBack);
}

function getCurrentCoordinatesAndroid(action){
Ti.Geolocation.locationServicesEnabled?
Titanium.Geolocation.getCurrentPosition(function(e){
e.error?
Ti.API.error("Error: "+e.error):(

action(e.coords),
Ti.API.info(e.coords));

}):(

console.log("DESATIVADO"),
alert("Please enable location services"));

}

function translateErrorCode(code){
if(null===code)
return null;

switch(code){
case Ti.Geolocation.ERROR_LOCATION_UNKNOWN:
return"Localiza\xE7\xE3o desconhecida";
break;
case Ti.Geolocation.ERROR_DENIED:
return"Acesso negado";
break;
case Ti.Geolocation.ERROR_NETWORK:
return"Erro de conex\xE3o";
break;
case Ti.Geolocation.ERROR_HEADING_FAILURE:
return"Falha em detectar posi\xE7\xE3o";
break;
case Ti.Geolocation.ERROR_REGION_MONITORING_DENIED:
return"Acesso negado ao monitoramento de regi\xE3o";
break;
case Ti.Geolocation.ERROR_REGION_MONITORING_FAILURE:
return"Falha no acesso ao monitoramento de regi\xE3o";
break;
case Ti.Geolocation.ERROR_REGION_MONITORING_DELAYED:
return"Instala\xE7\xE3o de monitoramento regi\xE3o adiada";
break;
default:
return"Erro desconhecido";}


}

function getCurrentCoordinates(action){!0?

getCurrentCoordinatesAndroid(action):

getCurrentCoordinatesiOS(action);

}

exports.hasPermission=function(){
return Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE);
},

exports.getCoordinates=function(action){
var warnning=!1;
Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)?
getCurrentCoordinates(action):

Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE,function(e){
Ti.API.info(JSON.stringify(e)),
!0===e.success?
getCurrentCoordinates(action):(

Ti.API.error("Access denied, error: "+e.error),
action(null));

});

},

exports.getDistance=function(lat1,lon1,lat2,lon2){var _Mathsqrt=





Math.sqrt,_Mathcos=Math.cos,_Mathsin=Math.sin,_MathPI=Math.PI,R=6371,dLat=(lat2-lat1)*_MathPI/180,dLon=(lon2-lon1)*_MathPI/180,a=_Mathsin(dLat/2)*_Mathsin(dLat/2)+_Mathcos(lat1*_MathPI/180)*_Mathcos(lat2*_MathPI/180)*_Mathsin(dLon/2)*_Mathsin(dLon/2),c=2*Math.atan2(_Mathsqrt(a),_Mathsqrt(1-a)),
d=6371*c;

return d;
};

function checkComponent(components){
var types=[{input:"route",result:"logradouro"},{input:"street_number",result:"numero"},{input:"administrative_area_level_2",result:"cidade"},{input:"administrative_area_level_1",result:"estado"},{input:"locality",result:"cidade"},{input:"postal_code",result:"cep"},{input:"sublocality_level_1",result:"bairro"}];

for(var j in types)
if(-1<components.types.indexOf(types[j].input))
return{key:types[j].result,value:components.long_name};


}

function getAddressComponets(components){
var address={};
for(var i in components){
var result=checkComponent(components[i]);
result&&(address[result.key]=result.value);
}
return address;
}

exports.getCoordinatesByAddress=function(params){var
address=params.address||"",
input=params.input,
latitude=params.latitude,
longitude=params.longitude,
successCallBack=params.successCallBack,
errorCallBack=params.errorCallBack;

httpClient&&(
Ti.API.info("ABORT"),
httpClient.abort(),
httpClient=null),

Ti.API.info("GET ADDRESSES"),
httpClient=Titanium.Network.createHTTPClient({
timeout:2e4});var


key="AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE",

body="address="+encodeURI(address);
body=body+"&key="+"AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE";
var url="https://maps.googleapis.com/maps/api/geocode/json?"+body;

httpClient.open("GET",url),
httpClient.setRequestHeader("Cache-Control","no-cache, no-store, must-revalidate"),

httpClient.onload=function(){
Ti.API.info("onload called, readyState = "+this.readyState+" Response: "+this.responseText+" Length: "+this.responseData.length);
try{
var json=JSON.parse(this.responseText);
if(json){
var address={};
json.results&&json.results.length&&(
address=getAddressComponets(json.results[0].address_components),
address.latitude=json.results[0].geometry.location.lat,
address.longitude=json.results[0].geometry.location.lng),


successCallBack(address);
}else
errorCallBack();

}catch(e){
Ti.API.error("Erro: "+e.message+" Response: "+this.responseText);
}
},

httpClient.onerror=function(e){
Ti.API.error("onerror called, readyState = "+this.readyState+" error = "+e.error);
},

httpClient.send();
};

function getAddressesList(predictions){
var addresses=[];
for(var i in predictions)
if(10>addresses.length)addresses.push({address:predictions[i].description});else return addresses;

return addresses;
}

exports.getAddresses=function(params){var
input=params.input,
latitude=params.latitude,
longitude=params.longitude,
successCallBack=params.successCallBack,
errorCallBack=params.errorCallBack;

httpClient&&(
Ti.API.info("ABORT"),
httpClient.abort(),
httpClient=null),

Ti.API.info("GET ADDRESSES"),
httpClient=Titanium.Network.createHTTPClient({
timeout:2e4});var


key="AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE",

body="input="+encodeURI(input);
body+="&language=pt_BR",

latitude&&longitude&&(body=body+"&location="+latitude+","+longitude),

body=body+"&key="+"AIzaSyBBe7WVvZg4WhWUvPkkojd77yMztW6sTxE",

httpClient.open("GET","https://maps.googleapis.com/maps/api/place/autocomplete/json?"+body),
httpClient.setRequestHeader("Cache-Control","no-cache, no-store, must-revalidate"),

httpClient.onload=function(){
Ti.API.info("onload called, readyState = "+this.readyState+" Response: "+this.responseText+" Length: "+this.responseData.length);
try{
var json=JSON.parse(this.responseText);
successCallBack(getAddressesList(json.predictions));
}catch(e){
Ti.API.error("Erro: "+e.message+" Response: "+this.responseText);
}
},

httpClient.onerror=function(e){
Ti.API.error("onerror called, readyState = "+this.readyState+" error = "+e.error);
},

httpClient.send();
};