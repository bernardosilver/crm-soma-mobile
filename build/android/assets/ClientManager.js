var moment=require("alloy/moment"),

cliente={
socios:[],
refCom:[],
refBanc:[]},


DADOS_COMPLETO=!1,
ENTREGA_COMPLETO=!1,
COBRANCA_COMPLETO=!1,
SOCIOS_COMPLETO=!1,
COMERCIAL_COMPLETO=!1,
BANCARIA_COMPLETO=!1,
CREDITO_COMPLETO=!1;

exports.init=function(){
cliente.socios=[],
cliente.refCom=[],
cliente.refBanc=[];
},

exports.isActive=function(){
return!!(cliente.A1_NOME||cliente.A1_LC||cliente.A1_END||cliente.A1_ENDCOB||cliente.socios&&cliente.socios.length||cliente.refCom&&cliente.refCom.length||cliente.refBanc&&cliente.refBanc.length);
},

exports.delete=function(){
cliente={},
DADOS_COMPLETO=!1,
ENTREGA_COMPLETO=!1,
COBRANCA_COMPLETO=!1,
SOCIOS_COMPLETO=!1,
COMERCIAL_COMPLETO=!1,
BANCARIA_COMPLETO=!1,
CREDITO_COMPLETO=!1;
},

exports.setDadosCompleto=function(){
DADOS_COMPLETO=!0;
},
exports.setEntregaCompleto=function(){
ENTREGA_COMPLETO=!0;
},
exports.setCobrancaCompleto=function(){
COBRANCA_COMPLETO=!0;
},
exports.setSociosCompleto=function(){
SOCIOS_COMPLETO=!0;
},
exports.setComercialCompleto=function(){
COMERCIAL_COMPLETO=!0;
},
exports.setBancariaCompleto=function(){
BANCARIA_COMPLETO=!0;
},
exports.setCreditoCompleto=function(){
CREDITO_COMPLETO=!0;
},

exports.setDadosPessoais=function(dados){
cliente.A1_NOME=dados.A1_NOME,
cliente.A1_NREDUZ=dados.A1_NREDUZ,
cliente.A1_DDD=dados.A1_DDD,
cliente.A1_TEL=dados.A1_TEL,
cliente.A1_TEL2=dados.A1_TEL2,
cliente.A1_TEL3=dados.A1_TEL3,
cliente.A1_TEL4=dados.A1_TEL4,
cliente.A1_EMAIL=dados.A1_EMAIL,
cliente.A1_PESSOA=dados.A1_PESSOA,
cliente.A1_CGC=dados.A1_CGC,
cliente.A1_INSCR=dados.A1_INSCR,
cliente.A1_PFISICA=dados.A1_PFISICA,
cliente.A1_DTNASC=dados.A1_DTNASC,
cliente.A1_ME=dados.A1_ME,
cliente.A1_OBSRC=dados.A1_OBSRC;
},

exports.setEndereco=function(dados){
cliente.A1_END=dados.logradouro,
cliente.A1_COMPLEM=dados.complemento,
cliente.A1_BAIRRO=dados.bairro,
cliente.A1_EST=dados.uf,
cliente.COD_UF=dados.COD_UF,
cliente.A1_MUN=dados.municipio,
cliente.A1_CODMUN=dados.CODMUN,
cliente.A1_CEP=dados.cep,
cliente.A1_INFOROT=dados.referencia,
cliente.A1_LAT=dados.latitude,
cliente.A1_LONG=dados.longitude;
},

exports.setEnderecoCobranca=function(dados){
cliente.A1_ENDCOB=dados.logradouro,
cliente.A1_BAIRROC=dados.bairro,
cliente.A1_ESTC=dados.uf,
cliente.CODC_UF=dados.CODC_UF,
cliente.A1_MUNC=dados.municipio,
cliente.COD_MC=dados.COD_MC,
cliente.A1_CEPC=dados.cep;
},

exports.setSocios=function(dados){

for(var i in cliente.socios=[],dados)
cliente.socios.push({
ID:dados[i].ID,
AO_FILIAL:"",
AO_CLIENTE:dados[i].AO_CLIENTE||"",
AO_LOJA:dados[i].AO_LOJA||"",
AO_TIPO:"1",
AO_NOMINS:dados[i].AO_NOMINS||"",
AO_DATA:dados[i].AO_DATA||"",
AO_SOCIO:dados[i].AO_SOCIO||"",
AO_CGC:dados[i].AO_CGC||"",
AO_TELEFON:dados[i].AO_TELEFON||"",
AO_CONTATO:dados[i].AO_CONTATO||"",
AO_OBSERV:dados[i].AO_OBSERV||"",
AO_DTALT:dados[i].AO_DTALT||"",
ID_CLIENTE:dados[i].ID_CLIENTE||"",
RECNO:"",
DEL:""});


},

exports.setReferenciasCom=function(dados){

for(var i in cliente.refCom=[],dados)
cliente.refCom.push({
ID:dados[i].ID,
AO_FILIAL:"",
AO_CLIENTE:dados[i].AO_CLIENTE||"",
AO_LOJA:dados[i].AO_LOJA||"",
AO_TIPO:"2",
AO_NOMINS:dados[i].AO_NOMINS||"",
AO_DATA:dados[i].AO_DATA||"",
AO_SOCIO:dados[i].AO_SOCIO||"",
AO_CGC:dados[i].AO_CGC||"",
AO_TELEFON:dados[i].AO_TELEFON||"",
AO_CONTATO:dados[i].AO_CONTATO||"",
AO_OBSERV:dados[i].AO_OBSERV||"",
AO_DTALT:dados[i].AO_DTALT||"",
ID_CLIENTE:dados[i].ID_CLIENTE||"",
RECNO:"",
DEL:""});


},

exports.setReferenciasBanc=function(dados){

for(var i in cliente.refBanc=[],dados)
cliente.refBanc.push({
ID:dados[i].ID,
AO_FILIAL:"",
AO_CLIENTE:dados[i].AO_CLIENTE||"",
AO_LOJA:dados[i].AO_LOJA||"",
AO_TIPO:"3",
AO_NOMINS:dados[i].AO_NOMINS||"",
AO_DATA:dados[i].AO_DATA||"",
AO_SOCIO:dados[i].AO_SOCIO||"",
AO_CGC:dados[i].AO_CGC||"",
AO_TELEFON:dados[i].AO_TELEFON||"",
AO_CONTATO:dados[i].AO_CONTATO||"",
AO_OBSERV:dados[i].AO_OBSERV||"",
AO_DTALT:dados[i].AO_DTALT||"",
ID_CLIENTE:dados[i].ID_CLIENTE||"",
RECNO:"",
DEL:""});


},

exports.setLimiteCredito=function(limite){
cliente.A1_LC=limite;
},

exports.getStatus=function(){return(
DADOS_COMPLETO?ENTREGA_COMPLETO?COBRANCA_COMPLETO?SOCIOS_COMPLETO?COMERCIAL_COMPLETO?BANCARIA_COMPLETO?CREDITO_COMPLETO?0:7:6:5:4:3:2:1);
},

exports.getDadosPessoais=function(){

return{
A1_NOME:cliente.A1_NOME||"",
A1_NREDUZ:cliente.A1_NREDUZ||"",
A1_DDD:cliente.A1_DDD||"",
A1_TEL:cliente.A1_TEL||"",
A1_TEL2:cliente.A1_TEL2||"",
A1_TEL3:cliente.A1_TEL3||"",
A1_TEL4:cliente.A1_TEL4||"",
A1_EMAIL:cliente.A1_EMAIL||"",
A1_PESSOA:cliente.A1_PESSOA||"",
A1_CGC:cliente.A1_CGC||"",
A1_INSCR:cliente.A1_INSCR||"",
A1_PFISICA:cliente.A1_PFISICA||"",
A1_DTNASC:cliente.A1_DTNASC||"",
A1_ME:cliente.A1_ME||"",
A1_OBSRC:cliente.A1_OBSRC||""};

},

exports.getEndereco=function(){

return{
A1_END:cliente.A1_END,
A1_COMPLEM:cliente.A1_COMPLEM,
A1_BAIRRO:cliente.A1_BAIRRO,
A1_EST:cliente.A1_EST,
COD_UF:cliente.COD_UF,
A1_MUN:cliente.A1_MUN,
A1_CODMUN:cliente.A1_CODMUN,
A1_CEP:cliente.A1_CEP,
A1_INFOROT:cliente.A1_INFOROT,
A1_LAT:cliente.A1_LAT,
A1_LONG:cliente.A1_LONG};

},

exports.getEnderecoCobranca=function(){

return{
A1_ENDCOB:cliente.A1_ENDCOB||"",
A1_BAIRROC:cliente.A1_BAIRROC||"",
A1_ESTC:cliente.A1_ESTC||"",
CODC_UF:cliente.CODC_UF||"",
A1_MUNC:cliente.A1_MUNC||"",
A1_CEPC:cliente.A1_CEPC||""};

},

exports.getSocios=function(){
var socios=[];
if(cliente.socios&&cliente.socios.length){
var socio={};
for(var i in cliente.socios)
socio=cliente.socios[i],
"1"==socio.AO_TIPO&&socios.push(socio);

}
return socios;
},

exports.getReferenciasCom=function(){
var referencias=[];
if(cliente.refCom.length){
var ref={};
for(var i in cliente.refCom)
ref=cliente.refCom[i],
"2"==ref.AO_TIPO&&referencias.push(ref);

}
return referencias;
},

exports.getReferenciasBanc=function(){
var referencias=[];
if(cliente.refBanc.length){
var ref={};
for(var i in cliente.refBanc)
ref=cliente.refBanc[i],
"3"==ref.AO_TIPO&&referencias.push(ref);

}
return referencias;
},

exports.getLimiteCredito=function(){

return cliente.A1_LC;
},

exports.getCliente=function(){

return cliente;
},

exports.getStatusCli=function(blq,titvenc,risco,observ,tlc,sld){var
result={cod:"",desc:"",color:"",block:""},
credR=parseFloat(tlc)-parseFloat(sld);




























return"1"==blq?(result.cod="1",result.color="black",result.desc="CLIENTE BLOQUEADO",result.status=2):0<titvenc&&"A"!==risco?(result.cod="1",result.color=Alloy.Globals.RED_COLOR,result.block="!",result.desc="CLIENTE POSSUI T\xCDTULOS VENCIDOS",result.status=5):"CADASTRO WEB"==observ&&"A"!==risco||"CADASTRO MOBILE"==observ&&"A"!==risco?(result.cod="1",result.color=Alloy.Globals.RED_COLOR,result.desc="CADASTRO DO CLIENTE AGUARDANDO LIBERA\xC7\xC3O",result.status=3):0>=credR&&"A"!==risco?(result.cod="1",result.color=Alloy.Globals.RED_COLOR,result.block="\uFF0D",result.desc="CR\xC9DITO INSUFICIENTE",result.status=4):(result.cod="2",result.color=Alloy.Globals.MAIN_COLOR,result.desc="CLIENTE LIBERADO",result.status=1),result;
},

exports.generateCliente=function(){var
usuario=Alloy.Globals.getLocalDataUsuario().data,
new_cliente={
A1_NOME:cliente.A1_NOME,
A1_NREDUZ:cliente.A1_NREDUZ,
A1_DDD:cliente.A1_DDD,
A1_TEL:cliente.A1_TEL,
A1_TEL2:cliente.A1_TEL2,
A1_TEL3:cliente.A1_TEL3,
A1_TEL4:cliente.A1_TEL4,
A1_EMAIL:cliente.A1_EMAIL,
A1_PESSOA:cliente.A1_PESSOA,
A1_CGC:cliente.A1_CGC,
A1_INSCR:cliente.A1_INSCR,
A1_PFISICA:cliente.A1_PFISICA,
A1_DTNASC:cliente.A1_DTNASC,
A1_ME:cliente.A1_ME,
A1_OBSRC:cliente.A1_OBSRC,

A1_END:cliente.A1_END,
A1_COMPLEM:cliente.A1_COMPLEM,
A1_BAIRRO:cliente.A1_BAIRRO,
A1_EST:cliente.A1_EST,
A1_MUN:cliente.A1_MUN,
A1_CODMUN:cliente.A1_CODMUN,
A1_CEP:cliente.A1_CEP,
A1_INFOROT:cliente.A1_INFOROT,
A1_LAT:cliente.A1_LAT,
A1_LONG:cliente.A1_LONG,

A1_ENDCOB:cliente.A1_ENDCOB,
A1_BAIRROC:cliente.A1_BAIRROC,
A1_ESTC:cliente.A1_ESTC,
A1_MUNC:cliente.A1_MUNC,
A1_CEPC:cliente.A1_CEPC,

A1_LC:cliente.A1_LC,
A1_MSALDO:0,
A1_MCOMPRA:0,
A1_METR:0,
A1_MATR:0,
A1_TITPROT:0,
A1_CHQDEVO:0,
A1_MOBILE:1,
A1_DTULCHQ:"",
A1_PRICOM:"",
A1_DTCADAS:moment().format("YYYYMMDD"),
A1_FILIAL:"",
A1_LOJA:"01",
A1_OBSERV:"CADASTRO MOBILE",
A1_PAIS:"105",
A1_CODPAIS:"01058",
A1_MSBLQL:"1",
A1_EXPORTA:"N",
A1_MOEDALC:"1",
A1_CONTA:"11201001",
A1_TPFRET:"C",
A1_RISCO:"C",
A1_LIBEDIT:"1",
A1_DTALT:"",
SALDO:0,
A1_DTULTIT:"",
DTSYNC:"1900-01-01 00:00:00",
A1_VEND:"",
A1_VEND2:"",
A1_COND:"",
A1_COND2:"",
A1_SUPER:""};





return new_cliente.A1_VEND2=200<=parseInt(usuario.USR_LOGIN)&&299>=parseInt(usuario.USR_LOGIN)?usuario.USR_LOGIN:usuario.USR_LOGIN,new_cliente;
},
exports.generateRefs=function(id){
var new_refs=[];




for(var i in console.log("REF COM: ",cliente.refCom),console.log("REF BANC: ",cliente.refBanc),console.log("SOCIOS: ",cliente.socios),cliente.refCom)
cliente.refCom[i].ID_CLIENTE=id,
new_refs.push(cliente.refCom[i]);

for(var i in cliente.refBanc)
cliente.refBanc[i].ID_CLIENTE=id,
new_refs.push(cliente.refBanc[i]);

for(var i in cliente.socios)
cliente.socios[i].ID_CLIENTE=id,
new_refs.push(cliente.socios[i]);

return new_refs;
},

exports.validateCGC=function(cgc,tipo){
if("soc"==tipo)
return cliente.A1_CGC==cgc;return(
"dado"==tipo?
_.find(cliente.socios,function(soc){
return soc.AO_CGC==cgc;
}):void 0);

};