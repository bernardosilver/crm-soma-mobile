var moment = require('alloy/moment');

var cliente = {
socios: [],
refCom: [],
refBanc: [],
condPag: {}
}

var DADOS_COMPLETO      = false;
var ENTREGA_COMPLETO    = false;
var COBRANCA_COMPLETO   = false;
var SOCIOS_COMPLETO     = false;
var COMERCIAL_COMPLETO  = false;
var BANCARIA_COMPLETO   = false;
var CONDPAG_COMPLETO    = false;
var CREDITO_COMPLETO    = false;

exports.init = function() {
    cliente.socios = [];
    cliente.refCom = [];
    cliente.refBanc = [];
}

exports.isActive = function(){
    return cliente.A1_NOME || cliente.A1_LC || cliente.A1_END || cliente.A1_ENDCOB || (cliente.socios && cliente.socios.length) || (cliente.refCom && cliente.refCom.length) || (cliente.refBanc && cliente.refBanc.length) ? true : false;
}

exports.delete = function(){
    cliente = {};
    DADOS_COMPLETO      = false;
    ENTREGA_COMPLETO    = false;
    COBRANCA_COMPLETO   = false;
    SOCIOS_COMPLETO     = false;
    COMERCIAL_COMPLETO  = false;
    BANCARIA_COMPLETO   = false;
    CONDPAG_COMPLETO    = false;
    CREDITO_COMPLETO    = false;
}

exports.setDadosCompleto = function() {
    DADOS_COMPLETO = true;
}
exports.setEntregaCompleto = function() {
    ENTREGA_COMPLETO = true;
}
exports.setCobrancaCompleto = function() {
    COBRANCA_COMPLETO = true;
}
exports.setSociosCompleto = function() {
    SOCIOS_COMPLETO = true;
}
exports.setComercialCompleto = function() {
    COMERCIAL_COMPLETO = true;
}
exports.setBancariaCompleto = function() {
    BANCARIA_COMPLETO = true;
}
exports.setCondPagCompleto = function() {
    CONDPAG_COMPLETO = true;
}
exports.setCreditoCompleto = function() {
    CREDITO_COMPLETO = true;
}

exports.setDadosPessoais = function(dados) {
    cliente.A1_NOME 	= dados.A1_NOME;
	cliente.A1_NREDUZ	= dados.A1_NREDUZ;
	cliente.A1_DDD 		= dados.A1_DDD;
	cliente.A1_TEL 		= dados.A1_TEL;
	cliente.A1_TEL2		= dados.A1_TEL2;
	cliente.A1_TEL3		= dados.A1_TEL3;
	cliente.A1_TEL4		= dados.A1_TEL4;
	cliente.A1_EMAIL	= dados.A1_EMAIL;
	cliente.A1_PESSOA 	= dados.A1_PESSOA; // F ou J
	cliente.A1_CGC		= dados.A1_CGC;
	cliente.A1_INSCR	= dados.A1_INSCR;
	cliente.A1_PFISICA	= dados.A1_PFISICA;
	cliente.A1_DTNASC	= dados.A1_DTNASC;
	cliente.A1_ME 		= dados.A1_ME;
	cliente.A1_OBSRC	= dados.A1_OBSRC;
}

exports.setEndereco = function(dados) {
    cliente.A1_END      = dados.logradouro;
    cliente.A1_COMPLEM  = dados.complemento;
    cliente.A1_BAIRRO   = dados.bairro;
    cliente.A1_EST      = dados.uf;
    cliente.COD_UF      = dados.COD_UF;
    cliente.A1_MUN      = dados.municipio;
    cliente.A1_CODMUN   = dados.CODMUN;
    cliente.A1_CEP      = dados.cep;
    cliente.A1_INFOROT  = dados.referencia;
    cliente.A1_LAT      = dados.latitude;
    cliente.A1_LONG     = dados.longitude;
}

exports.setEnderecoCobranca = function(dados) {
    cliente.A1_ENDCOB   = dados.logradouro;
    cliente.A1_BAIRROC  = dados.bairro;
    cliente.A1_ESTC     = dados.uf;
    cliente.CODC_UF     = dados.CODC_UF;
    cliente.A1_MUNC     = dados.municipio;
    cliente.COD_MC      = dados.COD_MC;
    cliente.A1_CEPC     = dados.cep;
}

exports.setSocios = function(dados) {
    cliente.socios = [];
    for(var i in dados) {
        cliente.socios.push({
            ID          : dados[i].ID,
            AO_FILIAL   : '',
            AO_CLIENTE  : dados[i].AO_CLIENTE || '',
            AO_LOJA     : dados[i].AO_LOJA    || '',
            AO_TIPO     : '1', //1=Socios;2=Comerciais;3=Bancos
            AO_NOMINS   : dados[i].AO_NOMINS  || '',
            AO_DATA     : dados[i].AO_DATA    || '',
            AO_SOCIO    : dados[i].AO_SOCIO   || '',
            AO_CGC      : dados[i].AO_CGC     || '',
            AO_TELEFON  : dados[i].AO_TELEFON || '',
            AO_CONTATO  : dados[i].AO_CONTATO || '',
            AO_OBSERV   : dados[i].AO_OBSERV  || '',
            AO_DTALT    : dados[i].AO_DTALT   || '1900-01-01 00:00:00',
            ID_CLIENTE  : dados[i].ID_CLIENTE || '',
            RECNO       : 0,
            DEL         : ''
        });
    }
}

exports.setReferenciasCom = function(dados) {
    cliente.refCom = [];
    for(var i in dados) {
        cliente.refCom.push({
            ID          : dados[i].ID,
            AO_FILIAL   : '',
            AO_CLIENTE  : dados[i].AO_CLIENTE   || '',
            AO_LOJA     : dados[i].AO_LOJA      || '',
            AO_TIPO     : '2',
            AO_NOMINS   : dados[i].AO_NOMINS    || '',
            AO_DATA     : dados[i].AO_DATA      || '',
            AO_SOCIO    : dados[i].AO_SOCIO     || '',
            AO_CGC      : dados[i].AO_CGC       || '',
            AO_TELEFON  : dados[i].AO_TELEFON   || '',
            AO_CONTATO  : dados[i].AO_CONTATO   || '',
            AO_OBSERV   : dados[i].AO_OBSERV    || '',
            AO_DTALT    : dados[i].AO_DTALT     || '1900-01-01 00:00:00',
            ID_CLIENTE  : dados[i].ID_CLIENTE   || '',
            RECNO       : 0,
            DEL         : ''
        });
    }
}

exports.setReferenciasBanc = function(dados) {
    cliente.refBanc = [];
    for(var i in dados) {
        cliente.refBanc.push({
            ID          : dados[i].ID,
            AO_FILIAL   : '',
            AO_CLIENTE  : dados[i].AO_CLIENTE   || '',
            AO_LOJA     : dados[i].AO_LOJA      || '',
            AO_TIPO     : '3',
            AO_NOMINS   : dados[i].AO_NOMINS    || '',
            AO_DATA     : dados[i].AO_DATA      || '',
            AO_SOCIO    : dados[i].AO_SOCIO     || '',
            AO_CGC      : dados[i].AO_CGC       || '',
            AO_TELEFON  : dados[i].AO_TELEFON   || '',
            AO_CONTATO  : dados[i].AO_CONTATO   || '',
            AO_OBSERV   : dados[i].AO_OBSERV    || '',
            AO_DTALT    : dados[i].AO_DTALT     || '1900-01-01 00:00:00',
            ID_CLIENTE  : dados[i].ID_CLIENTE   || '',
            RECNO       : 0,
            DEL         : ''
        });
    }
}

exports.setCondPag = function(cond) {
    cliente.condPag = {
        ID          : cond.ID,
        CODCLI      : cond.CODCLI || '',
        LOJA        : '01',
        GRPVEN      : '',
        CONDPG      : cond.CONDPG,
        TIPO        : cond.TIPO,
        COND        : cond.COND,
        DESCRI      : cond.DESCRI,
        FATORVD     : cond.FATORVD,
        E4_ACRSVEN  : cond.E4_ACRSVEN,
        E4_DESCVEN  : cond.E4_DESCVEN,
        DATDE       : '',
        DTSYNC      : '1900-01-01 00:00:00',
        RECNO       : 0
    };
}

exports.setLimiteCredito = function(limite) {
    cliente.A1_LC = limite;
}

exports.getStatus = function() {
    if(!DADOS_COMPLETO) return 1;
    else if(!ENTREGA_COMPLETO) return 2;
    else if(!COBRANCA_COMPLETO) return 3;
    else if(cliente.A1_PESSOA === 'J' && !SOCIOS_COMPLETO) return 4
    else if(!COMERCIAL_COMPLETO) return 5;
    else if(!BANCARIA_COMPLETO) return 6;
    else if(!CONDPAG_COMPLETO) return 7;
    else if(!CREDITO_COMPLETO) return 8;
    else return 0;
}

exports.getDadosPessoais = function() {
    
    return {    
        A1_NOME     : cliente.A1_NOME || '',
        A1_NREDUZ   : cliente.A1_NREDUZ || '',
        A1_DDD      : cliente.A1_DDD || '',
        A1_TEL      : cliente.A1_TEL || '',
        A1_TEL2     : cliente.A1_TEL2 || '',
        A1_TEL3     : cliente.A1_TEL3 || '',
        A1_TEL4     : cliente.A1_TEL4 || '',
        A1_EMAIL    : cliente.A1_EMAIL || '',
        A1_PESSOA   : cliente.A1_PESSOA || '',
        A1_CGC      : cliente.A1_CGC || '',
        A1_INSCR    : cliente.A1_INSCR || '',
        A1_PFISICA  : cliente.A1_PFISICA || '',
        A1_DTNASC   : cliente.A1_DTNASC || '',
        A1_ME       : cliente.A1_ME || '',
        A1_OBSRC    : cliente.A1_OBSRC || ''
    };
}

exports.getEndereco = function() {

	return {
        A1_END      : cliente.A1_END,
        A1_COMPLEM  : cliente.A1_COMPLEM,
        A1_BAIRRO   : cliente.A1_BAIRRO,
        A1_EST      : cliente.A1_EST,
        COD_UF      : cliente.COD_UF,
        A1_MUN      : cliente.A1_MUN,
        A1_CODMUN   : cliente.A1_CODMUN,
        A1_CEP      : cliente.A1_CEP,
        A1_INFOROT  : cliente.A1_INFOROT,
        A1_LAT      : cliente.A1_LAT,
        A1_LONG     : cliente.A1_LONG
    };
}

exports.getEnderecoCobranca = function() {
    
    return {
        A1_ENDCOB   : cliente.A1_ENDCOB || '',
        A1_BAIRROC  : cliente.A1_BAIRROC || '',
        A1_ESTC     : cliente.A1_ESTC || '',
        CODC_UF     : cliente.CODC_UF || '',
        A1_MUNC     : cliente.A1_MUNC || '',
        A1_CEPC     : cliente.A1_CEPC || ''
    }
}

exports.getSocios = function() {
    var socios = [];
    if(cliente.socios && cliente.socios.length) {
        var socio = {};
        for(var i in cliente.socios) {
            socio = cliente.socios[i];
            if(socio.AO_TIPO == '1') socios.push(socio);
        }
    }
    return socios;
}

exports.getReferenciasCom = function() {
    var referencias = [];
    if(cliente.refCom.length) {
        var ref = {};
        for(var i in cliente.refCom) {
            ref = cliente.refCom[i];
            if(ref.AO_TIPO == '2') referencias.push(ref);
        }
    }
    return referencias;
}

exports.getReferenciasBanc = function() {
    var referencias = [];
    if(cliente.refBanc.length) {
        var ref = {};
        for(var i in cliente.refBanc) {
            ref = cliente.refBanc[i];
            if(ref.AO_TIPO == '3') referencias.push(ref);
        }
    }
    return referencias;
}

exports.getCondPag = function() {
    var condPag = cliente.condPag;
    return condPag;
}

exports.getLimiteCredito = function() {
    
    return cliente.A1_LC;
}

exports.getCliente = function() {

    return cliente;
}

exports.getStatusCli = function(cliente) {
    // exports.getStatusCli = function(blq, titvenc, risco, observ, tlc, credr) {
    // (cliente.A1_MSBLQL, cliente.A1_TITVENC, cliente.A1_RISCO, cliente.A1_OBSERV, cliente.A1_TLC, cliente.CREDR)
    var result = {cod: '', desc: '', color: '', block: ''};
    //var credR = parseFloat(tlc) - parseFloat(credr);
    var credR = parseFloat(cliente.CREDR);
	if(cliente.A1_MSBLQL == "1"){
		result.cod = "1";
		result.color = 'black'; 
        result.desc = 'CLIENTE BLOQUEADO';
        result.status = 2;
	}
	else if(cliente.A1_TITVENC > 0 && cliente.A1_RISCO !== "A" && cliente.DIASVENC > 5){
		result.cod = "1";
		result.color = Alloy.Globals.RED_COLOR;
        result.block = '!';
        result.desc = 'CLIENTE POSSUI TÍTULOS VENCIDOS';
        result.status = 5;
    }
    else if(cliente.A1_TITVENC > 0 && cliente.A1_RISCO !== "A" && (cliente.NROPAG < 3 || cliente.METR > 5)) {
        result.cod = "1";
		result.color = Alloy.Globals.RED_COLOR;
        result.block = '!';
        result.desc = 'CLIENTE POSSUI TÍTULOS VENCIDOS';
        result.status = 5;
    }
	else if((cliente.A1_OBSERV == "CADASTRO WEB" && cliente.A1_RISCO !== "A") || (cliente.A1_OBSERV == "CADASTRO MOBILE" && cliente.A1_RISCO !== "A")){
		result.cod = "1";
		result.color = Alloy.Globals.RED_COLOR;
        result.desc = 'CADASTRO DO CLIENTE AGUARDANDO LIBERAÇÃO';
        result.status = 3;
	}
	else if(credR <= 0 && cliente.A1_RISCO !== "A"){
		result.cod = "1";
		result.color = Alloy.Globals.RED_COLOR;
		result.block = '－';
        result.desc = 'CRÉDITO INSUFICIENTE';
        result.status = 4;
	}
	else{
		result.cod = "2";
		result.color = Alloy.Globals.MAIN_COLOR;
        result.desc = 'CLIENTE LIBERADO';
        result.status = 1;
	}
	return result;
}

exports.generateCliente = function(){
    var usuario = Alloy.Globals.getLocalDataUsuario().data;
    var new_cliente = {
        A1_NOME     : cliente.A1_NOME,
        A1_NREDUZ   : cliente.A1_NREDUZ,
        A1_DDD      : cliente.A1_DDD,
        A1_TEL      : cliente.A1_TEL,
        A1_TEL2     : cliente.A1_TEL2 || '',
        A1_TEL3     : cliente.A1_TEL3 || '',
        A1_TEL4     : cliente.A1_TEL4 || '',
        A1_EMAIL    : cliente.A1_EMAIL,
        A1_PESSOA   : cliente.A1_PESSOA,
        A1_CGC      : cliente.A1_CGC,
        A1_INSCR    : cliente.A1_INSCR,
        A1_PFISICA  : cliente.A1_PFISICA,
        A1_DTNASC   : cliente.A1_DTNASC,
        A1_ME       : cliente.A1_ME,
        A1_OBSRC    : cliente.A1_OBSRC,
        //ENDERECO DE ENTREGA
        A1_END      : cliente.A1_END,
        A1_COMPLEM  : cliente.A1_COMPLEM,
        A1_BAIRRO   : cliente.A1_BAIRRO,
        A1_EST      : cliente.A1_EST,
        A1_MUN      : cliente.A1_MUN,
        A1_CODMUN   : cliente.A1_CODMUN,
        A1_CEP      : cliente.A1_CEP,
        A1_INFOROT  : cliente.A1_INFOROT,
        A1_LAT      : toString(cliente.A1_LAT),
        A1_LONG     : toString(cliente.A1_LONG),
        //ENDERECO DE COBRANCA
        A1_ENDCOB   : cliente.A1_ENDCOB,
        A1_BAIRROC  : cliente.A1_BAIRROC,
        A1_ESTC     : cliente.A1_ESTC,
        A1_MUNC     : cliente.A1_MUNC,
        A1_CEPC     : cliente.A1_CEPC,
        //LIMITE DE CREDITO
        A1_LC       : cliente.A1_LC, // float
        CREDR       : cliente.A1_LC, // float
        A1_MSALDO   : 0,
        A1_MCOMPRA  : 0,
        METR        : 0,
        A1_MATR     : 0,
        A1_TITPROT  : 0,
        A1_CHQDEVO  : 0,
        A1_MOBILE   : 1,
        A1_DTULCHQ  : '',
        A1_PRICOM   : '',
        A1_DTCADAS  : moment().format('YYYYMMDD'), // DATA DE CADASTRO
        A1_FILIAL   : '', // FILIAL
        A1_LOJA     : '01', // LOJA
        A1_OBSERV   : 'CADASTRO MOBILE', // OBSERVACOES
        A1_PAIS     : '105', // PAIS
        A1_CODPAIS  : '01058', // CODIGO DO PAIS
        A1_MSBLQL   : '1',
        A1_EXPORTA  : 'N', // EXPORTA
        A1_MOEDALC  : '1',
        A1_CONTA    : '11201001', // CONTA CONTABIL
        A1_TPFRET   : 'C', // TIPO DE FRETE CIF/FOB
        A1_RISCO    : 'C', // GRAU DE RISCO
        A1_LIBEDIT  : '1', // LIBERA CAD PARA ALTERACAO 1=SIM; 2=NAO
        A1_DTALT    : '1900-01-01 00:00:00', // DATA E HORA DA ALTERACAO
        SALDO       : 0, // SALDO DE PEDIDOS EM ABERTO NO SITE
        A1_DTULTIT  : '19000101', // DATA ULTIMO PROTESTO
        DTSYNC      : '1900-01-01 00:00:00',
        A1_VEND     : '', // VENDEDOR 1 - VENDEDOR INSUMO
        A1_VEND2    : '', // VENDEDOR 2 - VENDEDOR PET ENTRE 000200 E 000299
        A1_COND     : '', // COND PAGAMENTO VEND 1 - INSUMO
        A1_COND2    : '', // COND PAGAMENTO VEND 2 - PET
        A1_SUPER    : '', // CODIGO DO SUPERVISOR G.O.
        NROPAG      : 1
    }
    if(parseInt(usuario.USR_LOGIN) >= 200 && parseInt(usuario.USR_LOGIN) <= 299){
        new_cliente.A1_VEND2 = usuario.USR_LOGIN;
    }
    else {
        new_cliente.A1_VEND2 = usuario.USR_LOGIN;
    }
    return new_cliente;
}
exports.generateRefs = function(id) {
    var new_refs = [];

    for(var i in cliente.refCom) {
        cliente.refCom[i].ID_CLIENTE = id;
        new_refs.push(cliente.refCom[i]);
    }
    for(var i in cliente.refBanc) {
        cliente.refBanc[i].ID_CLIENTE = id;
        new_refs.push(cliente.refBanc[i]);
    }
    for(var i in cliente.socios) {
        cliente.socios[i].ID_CLIENTE = id;
        new_refs.push(cliente.socios[i]);
    }
    return new_refs;
}

exports.generateCondPag = function(id) {
    cliente.condPag.CODCLI = id;
    return cliente.condPag;
}

exports.validateCGC = function(cgc, tipo) {
    if(tipo == 'soc') {
        return cliente.A1_CGC == cgc ? true : false;
    }
    else if(tipo == 'dado') {
        return _.find(cliente.socios, function(soc) {
            return soc.AO_CGC == cgc;
        });
    }
}