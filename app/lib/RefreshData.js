var moment = require("alloy/moment");

//LOGIN
exports.sendLogin = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "login";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.post(params);
};

exports.sendToken = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "token";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.post(params);
};


exports.updateUsuario = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "usuario";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.put(params);
};

//======= ESTADOS =======
exports.getEstado = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "estado";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

//======= MUNICIPIOS =======
exports.getMnc = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "municipio";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

//======= CLIENTES =======
exports.getCliente = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "cliente";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

exports.getCredGrpVen = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "cliente_grp";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

exports.cadCliente = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();

	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "cliente";
	params.headers = [{ key: "Content-Type", value: "application/json" }];

	Ti.API.info(JSON.stringify(params));

	httpUtil.post(params);
};

exports.cadCondPag = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();

	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "condPag";
	params.headers = [{ key: "Content-Type", value: "application/json" }];

	Ti.API.info(JSON.stringify(params));

	httpUtil.post(params);
};

exports.cadReferencia = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();

	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "referencia";
	params.headers = [{ key: "Content-Type", value: "application/json" }];

	Ti.API.info(JSON.stringify(params));

	httpUtil.post(params);
};

exports.cadCabecalho = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();
	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "cabecalho";
	params.headers = [{ key: "Content-Type", value: "application/json" }];
	Ti.API.info(JSON.stringify(params));

    httpUtil.post(params);
};

exports.cadItensPed = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();

	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "itemsPed";
	params.headers = [{ key: "Content-Type", value: "application/json" }];

	Ti.API.info(JSON.stringify(params));

	httpUtil.post(params);
};

exports.cadCabecalhoOrcamento = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();
	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "cabOrc";
	params.headers = [{ key: "Content-Type", value: "application/json" }];
	Ti.API.info(JSON.stringify(params));

    httpUtil.post(params);
};

exports.cadItensOrcamento = function (_params) {
	var HttpUtil = require("HttpUtil");
	var httpUtil = new HttpUtil();

	var params = _params ? _params : {};

	httpUtil.setSuccessCallBack(params.successCallBack);
	httpUtil.setErrorCallBack(params.errorCallBack);

	params.url = Alloy.Globals.URL_API + "itemsOrc";
	params.headers = [{ key: "Content-Type", value: "application/json" }];

	Ti.API.info(JSON.stringify(params));

	httpUtil.post(params);
};

//======= REFERENCIAS =======
exports.getReferencia = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "referencia";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

//======= REFERENCIAS =======
exports.getTabelas = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "tabela";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

//======= PRODUTOS =======
exports.getProduto = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "produto";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};

//======= CONDICOES DE PAGAMENTO =======
exports.getCondPag = function (_params) {

    var HttpUtil = require("HttpUtil");
    var httpUtil = new HttpUtil();

    var params = _params ? _params : {};

    httpUtil.setSuccessCallBack(params.successCallBack);
    httpUtil.setErrorCallBack(params.errorCallBack);

    params.url = Alloy.Globals.URL_API + "condPag";
    params.headers = [{ key: "Content-Type", value: "application/json" }];

    Ti.API.info(JSON.stringify(params));

    httpUtil.get(params);
};
