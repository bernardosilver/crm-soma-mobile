var moment = require('alloy/moment');

// First add languages that support your application
var languages = ['pt-br'];
// Full language list:  https://github.com/appcelerator/alloy/tree/master/Alloy/builtins/moment/lang

// You must explicit way to require language files in order to compiler saw their. 
require('alloy/moment/lang/pt-br');

function selectLanguage(lang) {
    // Not all locales with country code has corresponding file in the alloy/moment. 
    // So we will use first 2-letter language code as insurance.
    [lang.split('-')[0], lang.toLowerCase()].forEach(function (lang) {
        if (languages.indexOf(lang) !== -1) {
            moment.lang(lang);
        }
    });
}

exports.selectLanguage = selectLanguage;