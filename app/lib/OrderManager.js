let moment    = require('alloy/moment');
let LocalData = require('LocalData');
/*

CJ_TIPOCLI
TPPED
TPVENDA
DTEMISS
*/
const formatData = ((dia) =>{
    let arr = dia.split('/');
    let dt = arr[2] + arr[1] + arr[0];
    return dt;
});

let pedido = {
    orcamento: false,
    desconto: 0,
    cabecalho: {},
    items: []
};

let produtos  = [];

pedido.cabecalho.CJ_FRTBAST = 0;

//VARIAVEIS QUE INDICARAO SE SERA PEDIDO OU ORCAMENTO
let statusCli   = 1 //TRUE - PEDIDO | FALSE - ORCAMENTO
let statusCred  = 1 //TRUE - PEDIDO | FALSE - ORCAMENTO
let statusDesc  = 1 //TRUE - PEDIDO | FALSE - ORCAMENTO

let total       = 0;
let totalProd   = 0;
let pesoTotal   = 0;
let valorFret   = 0;

var CLIENTE     = false;
var TABELA      = false;
var PAGAMENTO   = false;
var PARCELA     = false;
var FRETE       = false;

const setStatusCli = (bool => {
    statusCli = bool;
});

const setStatusCred = (bool => {
    statusCred = bool;
});

const setStatusDesc = (bool => {
    statusDesc = bool;
});

const getStatus = (() => {
    if(!CLIENTE) return 1;
    else if(CLIENTE && !TABELA) return 2;
    else if(TABELA && !PAGAMENTO) return 3;
    else if(PAGAMENTO && !PARCELA) return 4
    else if(!FRETE) return 5;
    else return 0;
});

const deletePedido = (() => {
    pedido = {
        cabecalho: {},
        items: []
    };
    produtos = [];
    setNumPpd();
    setCliente();
    setTabela();
    setPagamento();
    setParcela();
    setDesconto();
    setFrete();
    setObs();
    removeProdutos();
    calcTotalPedido();
});

const isActive = ( () => {
    const status = getStatus();
    if(status != 0) return true;
    else return false;
});

const setNumPpd = (numppd => {
    pedido.cabecalho.numppd = numppd || '';
});

const setCliente = (cliente => {
    if(cliente) {
        pedido.cabecalho.CLIENTE_ID = cliente.ID;
        pedido.cabecalho.CJ_CLIENTE = cliente.A1_COD;
        pedido.cabecalho.CJ_CLIENT  = cliente.A1_COD;
        pedido.cabecalho.GRPVEN     = cliente.A1_GRPVEN;
        pedido.cabecalho.CJ_LOJA    = cliente.A1_LOJA,
        pedido.cabecalho.CJ_LOJAENT = cliente.A1_LOJA;
        pedido.cabecalho.CREDR      = cliente.CREDR;
        pedido.cabecalho.CJ_PESSOA  = cliente.A1_PESSOA;
        pedido.cabecalho.CJ_CGC     = cliente.A1_CGC;
        pedido.cabecalho.CJ_NOME    = cliente.A1_NOME;
        pedido.cabecalho.CJ_MUN     = cliente.A1_MUN;
        pedido.cabecalho.CJ_DDD     = cliente.A1_DDD;
        pedido.cabecalho.CJ_TEL     = cliente.A1_TEL;
        pedido.cabecalho.CJ_NREDUZ  = cliente.A1_NREDUZ;
        pedido.cabecalho.CJ_TIPOCLI = cliente.A1_TIPO;
        CLIENTE = true;
    }
    else {
        pedido.cabecalho.CLIENTE_ID = null;
        pedido.cabecalho.CJ_CLIENTE = null;
        pedido.cabecalho.CJ_CLIENT  = null;
        pedido.cabecalho.GRPVEN     = null;
        pedido.cabecalho.CJ_LOJA    = null;
        pedido.cabecalho.CJ_LOJAENT = null;
        pedido.cabecalho.CREDR      = null;
        pedido.cabecalho.CJ_PESSOA  = null;
        pedido.cabecalho.CJ_CGC     = null;
        pedido.cabecalho.CJ_NOME    = null;
        pedido.cabecalho.CJ_MUN     = null;
        pedido.cabecalho.CJ_DDD     = null;
        pedido.cabecalho.CJ_TEL     = null;
        pedido.cabecalho.CJ_NREDUZ  = null;
        pedido.cabecalho.CJ_TIPOCLI = null;
        CLIENTE = false;
    }
    setTabela();
    setPagamento();
    setParcela();
    removeProdutos();
});

const setTabela = (tabela => {
    pedido.cabecalho.CJ_TABELA  = tabela ? tabela.CodTab : '';
    pedido.cabecalho.CJ_DTTAB   = tabela ? tabela.DtTab  : '';
    pedido.cabecalho.CJ_FILIAL  = tabela ? tabela.Filial : '';
    pedido.cabecalho.CJ_DESCTAB = tabela ? tabela.Descr  : '';
    pedido.cabecalho.TPVEND     = tabela ? tabela.TpVnd  : '';
    TABELA = true;
    if(!tabela) TABELA = false;

    setPagamento();
    setParcela();
    removeProdutos();
});

const calcFatorDtFix = ((parcela) => {
    let dtPrev = moment().hours(0).minutes(0).seconds(0).milliseconds(0).add(6, 'days');
    let qntPrc = parcela && parcela.length ? parcela.length : 0;
    let fc     = 0;
    let p      = 0;
    let diff   = 0;
    let media  = 0;
    if(qntPrc > 0) {
        for(let prc of parcela){
            const dt = moment(formatData(prc));
            // DIFF = DT VENC - DT PREV
            diff = dt.diff(dtPrev, 'days');
            fc += ((diff-7) * (0.0007)); 
            p += 1;
        }
        media = fc/p
    }
    pedido.cabecalho.FATORCONDPG = media.toFixed(4);

});

const setPagamento = (pagamento => {
    if(pedido.cabecalho.CLIENTE_ID) {
        pedido.cabecalho.CJ_CONDPAG = pagamento ? pagamento.CONDPG : '';
        pedido.cabecalho.ACRSVEN = pagamento ? pagamento.ACRSVEN : '';
        pedido.cabecalho.DESCVEN = pagamento ? pagamento.DESCVEN : '';
        pedido.cabecalho.DESCPAG = pagamento ? pagamento.DESCRI : '';

        if(pagamento && pagamento.ACRSVEN > 0) {
            pedido.cabecalho.FATORCONDPG = pagamento.ACRSVEN;
        }
        else if(pagamento && pagamento.DESCVEN < 0) {
            pedido.cabecalho.FATORCONDPG = pagamento.DESCVEN;
        }
        else if (!pagamento || (!pagamento.ACRSVEN && !pagamento.DESCVEN)) {
            pedido.cabecalho.FATORCONDPG = 0;
        }
    }
    else {
        pedido.cabecalho.CJ_CONDPAG = pagamento ? pagamento.CONDPG : '';
        pedido.cabecalho.ACRSVEN = pagamento ? pagamento.ACRSVEN : '';
        pedido.cabecalho.DESCVEN = pagamento ? pagamento.DESCVEN : '';
        pedido.cabecalho.DESCPAG = pagamento ? pagamento.DESCRI : '';

        if(pagamento && pagamento.ACRSVEN > 0) {
            pedido.cabecalho.FATORCONDPG = pagamento.ACRSVEN;
        }
        else if(pagamento && pagamento.DESCVEN < 0) {
            pedido.cabecalho.FATORCONDPG = pagamento.DESCVEN;
        }
        else if (!pagamento || (!pagamento.ACRSVEN && !pagamento.DESCVEN)) {
            pedido.cabecalho.FATORCONDPG = 0;
        }
    }
    PAGAMENTO = true;

    if(pagamento && pagamento.CONDPG == '003') {
        PARCELA = false;
    }
    else if(pagamento && pagamento.CONDPG != '003') {
        PARCELA = true;
    }
    else if(!pagamento) {
        PAGAMENTO = false;
    }
    setParcela();
    removeProdutos();
});

const setParcela = (parcela => {
    pedido.cabecalho.CJ_PARC1 = '';
    pedido.cabecalho.CJ_DATA1 = parcela && parcela[0] ? formatData(parcela[0]) : '';
    
    pedido.cabecalho.CJ_PARC2 = '';
    pedido.cabecalho.CJ_DATA2 = parcela && parcela[1] ? formatData(parcela[1]) : '';
    
    pedido.cabecalho.CJ_PARC3 = '';
    pedido.cabecalho.CJ_DATA3 = parcela && parcela[2] ? formatData(parcela[2]) : '';
    
    pedido.cabecalho.CJ_PARC4 = '';
    pedido.cabecalho.CJ_DATA4 = parcela && parcela[3] ? formatData(parcela[3]) : '';
    
    pedido.cabecalho.CJ_PARC5 = '';
    pedido.cabecalho.CJ_DATA5 = parcela && parcela[4] ? formatData(parcela[4]) : '';
    
    pedido.cabecalho.CJ_PARC6 = '';
    pedido.cabecalho.CJ_DATA6 = parcela && parcela[5] ? formatData(parcela[5]) : '';
    
    pedido.cabecalho.CJ_PARC7 = '';
    pedido.cabecalho.CJ_DATA7 = parcela && parcela[6] ? formatData(parcela[6]) : '';
    
    pedido.cabecalho.CJ_PARC8 = '';
    pedido.cabecalho.CJ_DATA8 = parcela && parcela[7] ? formatData(parcela[7]) : '';
    
    pedido.cabecalho.CJ_PARC9 = '';
    pedido.cabecalho.CJ_DATA9 = parcela && parcela[8] ? formatData(parcela[8]) : '';

    pedido.cabecalho.QNTPARC = parcela ? parcela.length : 0;

    if(parcela && parcela.length) calcFatorDtFix(parcela);
    
    PARCELA = true;
    if(!parcela && pedido.cabecalho.CJ_CONDPAG == '003') PARCELA = false;
    removeProdutos();
});

const setFrete = (frete => {
    pedido.cabecalho.CJ_TPFRETE = frete ? frete.CJ_TPFRETE : '';
    pedido.cabecalho.CJ_FRTBAST = frete ? parseFloat(frete.CJ_FRTBAST).toFixed(2) : 0;
    FRETE = true;
    if(frete && frete.CJ_TPFRETE == 'C'){
        calcTotalPedido();
    }
});

const setDesconto = (desc => {
    pedido.desconto = desc ? desc : 0;
});

const setObs = (obs => {
    pedido.cabecalho.CJ_OBS = obs;
});

// CODTAB
// CODPRO
// DESCR
// UN
// PESO
// COMIS
// ARMZ
// PRVEN
// GRUPO
// DA1_ESTADO
// B1_IPI
// B1_VLR_ICM
// ALIQIN
// ALIQEX
// MVA

const removeProdutos = (() => {
    produtos = [];
    calcTotalPedido();
});

const insertItems = ((values, produto) => {
    const tot = parseFloat(values.val).toFixed(2) * parseFloat(values.qnt).toFixed(2);
    const peso_tot = parseFloat(produto.PESO).toFixed(2) * parseFloat(values.qnt).toFixed(2);
    produto.indice      = produtos.length;
    produto.CK_QTDVEN   = values.qnt;
    produto.CK_DESCONT  = values.desc;
    produto.CK_VALOR    = parseFloat(tot).toFixed(2);
    produto.CK_PRUNIT   = values.val;
    produto.TPESO       = parseFloat(peso_tot).toFixed(2);
    produtos.push(produto);
    calcTotalPedido();
});

const updateItem = ((values, produto) => {
    var pos = parseInt(produto.indice);
    produtos[pos].CK_QTDVEN   = values.qnt;
    produtos[pos].CK_DESCONT  = values.desc;
    produtos[pos].CK_VALOR    = values.val * values.qnt;
    produtos[pos].CK_PRUNIT   = values.val;
    produtos[pos].TPESO       = parseFloat(produto.PESO) * parseFloat(values.qnt);
    calcTotalPedido();
})

const excluirProduto = ((produto) => {
    var pos = parseInt(produto.indice);
    if(produtos.length == 1) {
        produtos = [];
    }
    else {
        produtos.splice(pos,1);
    }
    LocalData.deleteItensPed({cab_id: produto.ID_PROD})
    calcTotalPedido();
    reorderProds();
})

const reorderProds = (() => {
    let count = 0;
    for(let prod of produtos) {
        prod.indice = count;
        count+= 1;
    }
})

const setItem = ((produto) => {
// NAO PODE ALTERAR O VALOR UNIT DE PROD PET
    let valorFrt    = 0;
    let totalFret   = 0;
    let totalProd   = 0;
    const valid     = moment().add(15, 'days').format('YYYYMMDD');
    let pos       = produto.indice + 1;
    if(pos < 10) {
        pos = '0' + pos.toString();
    }
    else {
        pos = pos.toString();
    }
    valorFrt = (parseFloat(produto.PESO).toFixed(2)/1000) * parseFloat(pedido.cabecalho.CJ_FRTBAST).toFixed(2);
    totalFret = valorFrt * parseFloat(produto.CK_QTDVEN);
    totalProd = pedido.cabecalho.CJ_TPFRETE === 'C' ? parseFloat(totalFret) + parseFloat(produto.CK_VALOR) : produto.CK_VALOR;

    const prod = {
        CK_NUM      : '', // NUMERO GERADO NA HORA DE GRAVAR NO MYSQL - CONSULTAR O ULTIMO P GERAR UM NOVO
        CK_FILIAL   : pedido.cabecalho.CJ_FILIAL,
        CK_ITEM     : pos, // ORDEM DO ITEM NO PEDIDO, por ex item 1, item 2...
        CK_PRODUTO  : produto.CODPRO,
        CK_DESCRI   : produto.DESCR,
        CK_UM       : produto.UN,
        CK_LOCAL    : produto.ARMZ, // ARMAZEM DO PRODUTO, VEM DO PRODUTO
        PRCTAB      : produto.PRVEN, // PRECO DA TABELA QUE SAI DA FABRICA SEM IMPOSTO
        CK_PRCVEN   : parseFloat(produto.CK_PRUNIT).toFixed(2), // PRECO DE VENDA, PRECO DA FABRICA - DESCONTO + IMPOSTO
        CK_PRUNIT   : parseFloat(produto.CK_PRUNIT).toFixed(2), // PRECO DA FABRICA COM DESCONTO
        CK_COMIS1   : produto.COMIS, // COMISSAO EM PORCENTAGEM QUE VEM DO PRODUTO PELA TABELA DE PRECO
        CALCIPI     : 0, // VALOR DO IPI CALCULADO
        CALCST      : 0, // VALOR DO CST CALCULADO
        CK_QTDVEN   : produto.CK_QTDVEN, // QUANTIDADE TOTAL VENDIDA
        CK_DESCONT  : parseFloat(produto.CK_DESCONT).toFixed(2), // DESCONTO EM % POR ITEM
        CK_VALOR    : produto.CK_VALOR, // CK_PREUNIT * CK_QTDVEN
        CK_CLIENTE  : pedido.cabecalho.CJ_CLIENTE, // COD DO CLIENTE
        CK_LOJA     : pedido.cabecalho.CJ_LOJA, // LOJA DO CLIENTE
        CK_ENTREG   : moment().add(5, 'days').format('YYYYMMDD'), // PREVISAO DE ENTREGA
        CK_FILVEN   : '01', // FILIAL DE VENDA
        CK_FILENT   : '01', // FILIAL DE ENTREGA
        CK_DT1VEN   : valid, // DATA DE VALIDADE
        CK_YOPER    : '01', // TIPO DE OPERACAO SOMA
        CK_DTALT    : '1900-01-01 00:00:00', // DATA E HORA DE ALTERACAO
        GRUPO       : produto.GRUPO, // GRUPO DO PRODUTO
        PESOUNI     : parseFloat(produto.PESO).toFixed(2), // PESO UNITARIO DO PRODUTO
        TPESO       : produto.TPESO, // PESO TOTAL DO ITEM
        TVLRFRT     : parseFloat(totalProd).toFixed(2), // TOTAL FRETE DO ITEM - FRTPROD * QNT
        FRTPROD     : valorFrt, // FRETE DO ITEM (PRECO COM IMPOSTO)
        ID          : '',
        CJID        : '',
        RECNO       : 0,
        DTSYNC      : '1900-01-01 00:00:00',
        CK_COTCLI   : pedido.cabecalho.numppd || '',
        ID_PROD      : produto.ID_PROD || ''
    }

    pedido.items.push(prod);
});

const calcTotalPedido = (() => {
    const reducerValor = (accumulator, currentValue) => accumulator + parseFloat(currentValue.CK_VALOR);
    const reducerPeso = (accumulator, currentValue) => accumulator + parseFloat(currentValue.TPESO);
    const valorProds = produtos.reduce(reducerValor, 0);
    valorFret = (produtos.reduce(reducerPeso, 0)/1000) * pedido.cabecalho.CJ_FRTBAST;
    pesoTotal  = produtos.reduce(reducerPeso, 0);

    total = pedido.cabecalho.CJ_TPFRETE == 'C' ? valorProds + valorFret : valorProds;
    totalProd = valorProds;
});

const getNumPpd = (() => {
    return pedido.cabecalho.numppd;
})

const getCliente = (() => {
    return {
        CLIENTE_ID: pedido.cabecalho.CLIENTE_ID,
        A1_COD: pedido.cabecalho.CJ_CLIENTE,
        A1_LOJA: pedido.cabecalho.CJ_LOJA,
        CJ_LOJAENT: pedido.cabecalho.CJ_LOJAENT,
        CREDR: pedido.cabecalho.CREDR,
        A1_PESSOA: pedido.cabecalho.CJ_PESSOA,
        A1_CGC: pedido.cabecalho.CJ_CGC,
        A1_NOME: pedido.cabecalho.CJ_NOME,
        A1_NREDUZ: pedido.cabecalho.CJ_NREDUZ,
        A1_GRPVEN : pedido.cabecalho.GRPVEN
    };
});

const getSaldo = (() => {
    var calcSaldo = pedido.cabecalho.CJ_TPFRETE == 'C' ? (parseFloat(pedido.cabecalho.CREDR) - parseFloat(total)).toFixed(2) : (parseFloat(pedido.cabecalho.CREDR) - parseFloat(totalProd)).toFixed(2);
    return {
        saldo: pedido.cabecalho.CREDR,
        restante: calcSaldo
    }
});

const getTabela = (() => {
    return {
        CJ_TABELA: pedido.cabecalho.CJ_TABELA,
        CJ_DTTAB: pedido.cabecalho.CJ_DTTAB,
        CJ_FILIAL: pedido.cabecalho.CJ_FILIAL,
        CJ_DESCTAB: pedido.cabecalho.CJ_DESCTAB
        // Descr : 
        // Filial : 
    };
});

const getPagamento = (() => {
    return {
        CJ_CONDPAG: pedido.cabecalho.CJ_CONDPAG,
        ACRSVEN: pedido.cabecalho.ACRSVEN,
        DESCVEN: pedido.cabecalho.DESCVEN,  
        DESCPAG: pedido.cabecalho.DESCPAG,
        FATORCONDPG : pedido.cabecalho.FATORCONDPG  
    }
});

const getParcela = (() => {
    return {
        CJ_PARC1: pedido.cabecalho.CJ_PARC1,
        CJ_DATA1: pedido.cabecalho.CJ_DATA1,
        
        CJ_PARC2: pedido.cabecalho.CJ_PARC2,
        CJ_DATA2: pedido.cabecalho.CJ_DATA2,
        
        CJ_PARC3: pedido.cabecalho.CJ_PARC3,
        CJ_DATA3: pedido.cabecalho.CJ_DATA3,
        
        CJ_PARC4: pedido.cabecalho.CJ_PARC4,
        CJ_DATA4: pedido.cabecalho.CJ_DATA4,
        
        CJ_PARC5: pedido.cabecalho.CJ_PARC5,
        CJ_DATA5: pedido.cabecalho.CJ_DATA5,
        
        CJ_PARC6: pedido.cabecalho.CJ_PARC6,
        CJ_DATA6: pedido.cabecalho.CJ_DATA6,
        
        CJ_PARC7: pedido.cabecalho.CJ_PARC7,
        CJ_DATA7: pedido.cabecalho.CJ_DATA7,
        
        CJ_PARC8: pedido.cabecalho.CJ_PARC8,
        CJ_DATA8: pedido.cabecalho.CJ_DATA8,
        
        CJ_PARC9: pedido.cabecalho.CJ_PARC9,
        CJ_DATA9: pedido.cabecalho.CJ_DATA9,

        QNTPARC: pedido.cabecalho.QNTPARC
    }
});

const getFrete = (() => {
    return {
        CJ_TPFRETE: pedido.cabecalho.CJ_TPFRETE,
        CJ_FRTBAST: pedido.cabecalho.CJ_FRTBAST
    };
});

const getDesconto = (() => {
    return pedido.desconto;
});

const getObs = (() => {
    return pedido.cabecalho.CJ_OBS;
});

const getItems = (() => {
    return produtos;
});

const getPedido = (() => {
    return pedido;
});

const getTotalPedido = (() => {
    return {valor: total, totalProd: totalProd, qntItens: produtos.length, pesoTotal: pesoTotal, valorFret: valorFret};
});

const validateOrcamento = (() => {
    //SE TRUE É PEDIDO | SE FALSE É ORCAMENTO
    // if(statusCli && statusCred && statusDesc) { -- CONSIDERA DESCONTO ACIMA OU ABAIXO DO PERMITIDO
    if(statusCli && statusCred) {
        return 1;
    }
    else {
        return 0;
    }
});

const orderGenerate = (() => {
    const prev    = moment().add(6, 'days').format('YYYYMMDD');
    const valid   = moment().add(15, 'days').format('YYYYMMDD');
    const orcamento = validateOrcamento(); //SE TRUE É PEDIDO | SE FALSE É ORCAMENTO
    calcParcelas();
    const cabecalho = {
        CJ_FILIAL    : pedido.cabecalho.CJ_FILIAL,
        CJ_NUM       : '',
        CJ_EMISSAO   : moment().format('YYYYMMDD'),
        CJ_CLIENTE   : pedido.cabecalho.CJ_CLIENTE, // OK
        CLIENTE_ID   : pedido.cabecalho.CLIENTE_ID, // OK
        CJ_NOME      : pedido.cabecalho.CJ_NOME,
        CJ_MUN       : pedido.cabecalho.CJ_MUN,
        CJ_DDD       : pedido.cabecalho.CJ_DDD,
        CJ_TEL       : pedido.cabecalho.CJ_TEL,
        CJ_CGC       : pedido.cabecalho.CJ_CGC,
        CJ_LOJA      : pedido.cabecalho.CJ_LOJA, // OK
        CJ_TIPOCLI   : pedido.cabecalho.CJ_TIPOCLI || '', // OK
        CJ_CLIENT    : pedido.cabecalho.CJ_CLIENT, // OK
        CJ_LOJAENT   : pedido.cabecalho.CJ_LOJAENT, // OK
        CJ_CONDPAG   : pedido.cabecalho.CJ_CONDPAG, // OK
        CJ_TABELA    : pedido.cabecalho.CJ_TABELA, // OK
        CJ_DTTAB     : pedido.cabecalho.CJ_DTTAB, // OK
        CJ_STATUS    : 'A', // OK
        CJ_VALIDA    : valid,
        CJ_MOEDA     : '1',
        CJ_TIPLIB    : '2',
        CJ_TPCARGA   : '1',
        CJ_TXMOEDA   : '1',
        CJ_ROTEIRO   : '',
        CJ_TPFRETE   : pedido.cabecalho.CJ_TPFRETE || 'F', // OK
        CJ_FRTBAST   : pedido.cabecalho.CJ_FRTBAST, // OK
        CJ_VEND      : Alloy.Globals.getLocalDataUsuario().data.USR_LOGIN, // CODIGO DO VENDEDOR< DEVERA BUSCAR DO USUARIO LOGADO
        CJ_OBS       : pedido.cabecalho.CJ_OBS || '', // OK
        CJ_PARC1     : pedido.cabecalho.CJ_PARC1 || 0, // OK
        CJ_DATA1     : pedido.cabecalho.CJ_DATA1 || '', // OK
        CJ_PARC2     : pedido.cabecalho.CJ_PARC2 || 0, // OK
        CJ_DATA2     : pedido.cabecalho.CJ_DATA2 || '', // OK
        CJ_PARC3     : pedido.cabecalho.CJ_PARC3 || 0, // OK
        CJ_DATA3     : pedido.cabecalho.CJ_DATA3 || '', // OK
        CJ_PARC4     : pedido.cabecalho.CJ_PARC4 || 0, // OK
        CJ_DATA4     : pedido.cabecalho.CJ_DATA4 || '', // OK
        CJ_PARC5     : pedido.cabecalho.CJ_PARC5 || 0, // OK
        CJ_DATA5     : pedido.cabecalho.CJ_DATA5 || '', // OK
        CJ_PARC6     : pedido.cabecalho.CJ_PARC6 || 0, // OK
        CJ_DATA6     : pedido.cabecalho.CJ_DATA6 || '', // OK
        CJ_PARC7     : pedido.cabecalho.CJ_PARC7 || 0, // OK
        CJ_DATA7     : pedido.cabecalho.CJ_DATA7 || '', // OK
        CJ_PARC8     : pedido.cabecalho.CJ_PARC8 || 0, // OK
        CJ_DATA8     : pedido.cabecalho.CJ_DATA8 || '', // OK
        CJ_PARC9     : pedido.cabecalho.CJ_PARC9 || 0, // OK
        CJ_DATA9     : pedido.cabecalho.CJ_DATA9 || '', // OK
        CJ_DTALT     : moment().format('YYYY-MM-DD hh:mm:ss'),
        TPPED        : !orcamento ? '' : 'VND',
        GRPVEN       : pedido.cabecalho.GRPVEN || '',
        FATORCONDPG  : pedido.cabecalho.FATORCONDPG,
        DTPREV       : prev,
        ID           : '',
        ORC_ID       : null,
        RECNO        : 0,
        DTSYNC       : '1900-01-01 00:00:00',
        DEL          : '',
        DTEMISS      : moment().format('YYYY-MM-DD hh:mm:ss'),
        CJ_ORC       : orcamento
    }
    const array  = getItems();
    let prods = [];
    for(prod of array){
        setItem(prod);
    }
    return {cabecalho: cabecalho, produtos: pedido.items}
});

const calcParcelas = (() => {
    let qnt  = pedido.cabecalho.QNTPARC;
    const tot  = getTotalPedido();
    let parc = 0;
    parc = (tot.valor/qnt).toFixed(2);
    for(var i in pedido.cabecalho) {
        if(i.indexOf('CJ_PARC') > -1 && qnt > 0) {
            pedido.cabecalho[i] = parseFloat(parc).toFixed(2);
            qnt -= 1;
        }
    }
});

const itemsGenerate = ((order_id) => {
    for(prod of pedido.items) {
        prod.CK_NUM = order_id;
    }
    let prods = pedido.items;
    return prods;
});

const generateOrcamento = ((cab) => {
    const cabecalho = {
        OR_FILIAL    : cab.CJ_FILIAL,
        OR_EMISSAO   : cab.CJ_EMISSAO,
        OR_CLIENTE   : cab.CJ_CLIENTE,
        OR_LOJA      : cab.CJ_LOJA,
        OR_CONDPAG   : cab.CJ_CONDPAG,
        OR_TABELA    : cab.CJ_TABELA,
        OR_DTTAB     : cab.CJ_DTTAB,
        OR_STATUS    : cab.CJ_STATUS,
        OR_VALIDA    : cab.CJ_VALIDA,       
        OR_TPFRETE   : cab.CJ_TPFRETE || 'F',
        OR_FRTBAST   : cab.CJ_FRTBAST,
        OR_VEND      : cab.CJ_VEND,
        OR_OBS       : cab.CJ_OBS || ' ',
        OR_PARC1     : cab.CJ_PARC1 || 0,
        OR_DATA1     : cab.CJ_DATA1 || ' ',
        OR_PARC2     : cab.CJ_PARC2 || 0,
        OR_DATA2     : cab.CJ_DATA2 || ' ',
        OR_PARC3     : cab.CJ_PARC3 || 0,
        OR_DATA3     : cab.CJ_DATA3 || ' ',
        OR_PARC4     : cab.CJ_PARC4 || 0,
        OR_DATA4     : cab.CJ_DATA4 || ' ',
        OR_PARC5     : cab.CJ_PARC5 || 0,
        OR_DATA5     : cab.CJ_DATA5 || ' ',
        OR_PARC6     : cab.CJ_PARC6 || 0,
        OR_DATA6     : cab.CJ_DATA6 || ' ',
        OR_PARC7     : cab.CJ_PARC7 || 0,
        OR_DATA7     : cab.CJ_DATA7 || ' ',
        OR_PARC8     : cab.CJ_PARC8 || 0,
        OR_DATA8     : cab.CJ_DATA8 || ' ',
        OR_PARC9     : cab.CJ_PARC9 || 0,
        OR_DATA9     : cab.CJ_DATA9 || ' ',
        OR_GRPVEN    : cab.GRPVEN || ' ',
        OR_FATORCONDPG  : cab.FATORCONDPG,
        OR_DTPREV    : cab.DTPREV,
        ID           : '',
        OR_TPPED     : 'VND'
    }
    return cabecalho;
});

const generateItensOrcamento = ((array, id_cab) => {
    const orcProds = [];
    for(let prod of array){
        orcProds.push({
            IOR_FILIAL  : prod.CK_FILIAL,
            IOR_ITEM    : prod.CK_ITEM,
            IOR_PRODUTO : prod.CK_PRODUTO,
            IOR_DESCRI  : prod.CK_DESCRI,
            IOR_UM      : prod.CK_UM,
            IOR_LOCAL   : prod.CK_LOCAL,
            IOR_PRCTAB  : prod.PRCTAB,
            IOR_PRCVEN  : prod.CK_PRCVEN,
            IOR_PRUNIT  : prod.CK_PRUNIT,
            IOR_COMIS1  : prod.CK_COMIS1,
            CALCIPI     : prod.CALCIPI || 0,
            CALCST      : prod.CALCST || 0,
            IOR_QTDVEN  : prod.CK_QTDVEN,
            IOR_DESCONT : prod.CK_DESCONT,
            IOR_VALOR   : prod.CK_VALOR,
            IOR_ENTREG  : prod.CK_ENTREG,
            IOR_DT1VEN  : prod.CK_DT1VEN,
            IOR_YOPER   : prod.CK_YOPER,
            IOR_GRUPO   : prod.GRUPO,
            IOR_PESOUNI : prod.PESOUNI,
            IOR_TPESO   : prod.TPESO,
            IOR_TVLRFRT : prod.TVLRFRT,
            IOR_FRTPROD : prod.FRTPROD,
            OR_ID       : id_cab,
            ID_LOCAL    : prod.ID,
        })
    }
    return orcProds;
});
 
exports.setStatusCli  = setStatusCli;
exports.setStatusCred = setStatusCred;
exports.setStatusDesc = setStatusDesc;
exports.getStatus     = getStatus;
exports.isActive      = isActive;
exports.delete        = deletePedido;
exports.setNumPpd     = setNumPpd;
exports.setCliente    = setCliente;
exports.setTabela     = setTabela;
exports.setPagamento  = setPagamento;
exports.setParcela    = setParcela;
exports.setFrete      = setFrete;
exports.setDesconto   = setDesconto;
exports.setObs        = setObs;
exports.setItem       = setItem;
exports.insertItems   = insertItems;
exports.updateItem    = updateItem;
exports.excluirProduto= excluirProduto;
exports.getNumPpd     = getNumPpd;
exports.getCliente    = getCliente;
exports.getSaldo      = getSaldo;
exports.getTabela     = getTabela;
exports.getPagamento  = getPagamento;
exports.getParcela    = getParcela;
exports.getFrete      = getFrete;
exports.getDesconto   = getDesconto;
exports.getObs        = getObs;
exports.getItems      = getItems;
exports.getPedido     = getPedido;
exports.getTotalPedido= getTotalPedido;
exports.orderGenerate = orderGenerate;
exports.itemsGenerate = itemsGenerate;
exports.generateOrcamento      = generateOrcamento;
exports.generateItensOrcamento = generateItensOrcamento;

// CJ_FILIAL   , CJ_NUM    , CJ_EMISSAO    , CJ_CLIENTE    , CJ_LOJA   , CJ_CLIENT     , CJ_LOJAENT, CJ_CONDPAG    , CJ_TABELA     , CJ_DTTAB      , CJ_STATUS     , CJ_TPFRETE    , CJ_FRTBAST    , CJ_VALIDA , CJ_TIPLIB     , CJ_TPCARGA    , CJ_MOEDA  , CJ_TXMOEDA, CJ_VEND   , CJ_OBS    , CJ_DTALT  , TPPED     , TPVENDA   , GRPVEN    , FATORCONDPG   , DTPREV    , ORC_ID    , DTSYNC                , DEL   $pcampos
// '$CJ_FILIAL', '$codped' , '$CJ_EMISSAO' , '$CJ_CLIENTE' , '$CJ_LOJA', '$CJ_CLIENTE' , '$CJ_LOJA', '$CJ_CONDPAG' , '$CJ_TABELA'  , '$CJ_DTTAB'   , '$CJ_STATUS'  , '$CJ_TPFRETE' , '$CJ_FRTBAST' , '$dtvalid', '$CJ_TIPLIB'  , '$CJ_TPCARGA' , '1'       ,       '1' , '$CJ_VEND', '$CJ_OBS' , '$dthr'   , '$tpped'  , '$tpvenda', '$grpven' , '$fatorcondpg', '$dtprev' , '$idorc'  , '1900-01-01 00:00:00' , ''    $pvals