var moment = require("alloy/moment");

//CRIA TABELA PARA CADASTRAR CLIENTES
exports.createTableCadCliente = function() {
    try{
        var db = Ti.Database.open('somaCRMDB');
        db.execute(`DROP TABLE IF EXISTS cadCliente`);
        db.execute(`CREATE TABLE IF NOT EXISTS cadCliente (ID INTEGER PRIMARY KEY, A1_FILIAL TEXT, A1_COD TEXT, A1_LOJA TEXT, A1_PESSOA TEXT, A1_CGC TEXT, A1_NOME TEXT, A1_NREDUZ TEXT, A1_ME TEXT, A1_END TEXT, A1_COMPLEM TEXT, A1_BAIRRO TEXT, A1_EST TEXT, A1_MUN TEXT, A1_CODMUN TEXT, A1_CEP TEXT, A1_LAT TEXT, A1_LONG TEXT, A1_DDD TEXT, A1_PAIS TEXT, A1_TEL TEXT, A1_TEL2 TEXT, A1_TEL3 TEXT, A1_TEL4 TEXT, A1_FAX TEXT, A1_CEL1 TEXT, A1_CEL2 TEXT, A1_ENDCOB TEXT, A1_BAIRROC TEXT, A1_MUNC TEXT, A1_ESTC TEXT, A1_CEPC TEXT, A1_INSCR TEXT, A1_PFISICA TEXT, A1_VEND TEXT, A1_VEND2 TEXT, A1_TPFRET TEXT, A1_RISCO TEXT, A1_LC REAL, A1_CODPAIS TEXT, A1_MSBLQL TEXT, A1_EXPORTA TEXT, A1_MOEDALC TEXT, A1_CONTA TEXT, A1_SUPER TEXT, A1_OBSERV TEXT, A1_EMAIL TEXT, A1_DTCADAS TEXT, A1_DTNASC TEXT, A1_DTALT TEXT, A1_LIBEDIT TEXT, SALDO REAL, A1_INFOROT INTEGER, A1_OBSRC INTEGER, A1_DTULTIT TEXT, A1_COND TEXT, A1_COND2 TEXT, DTSYNC TEXT)`);
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.insertCadCliente = function(params) {
    var cliente = params.cliente || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!cliente) return error('Cliente não informado para cadastrar');
    var db = Ti.Database.open('somaCRMDB');
    try {
        db.execute(
            `INSERT INTO cliente` + 
            `(A1_FILIAL  , A1_LOJA   , A1_PESSOA , A1_CGC    , A1_NOME   , A1_DTNASC , ` +
            `A1_NREDUZ   , A1_ME     , A1_END    , A1_COMPLEM, A1_BAIRRO , A1_EST, ` +
            `A1_MUN      , A1_CODMUN , A1_CEP    , A1_LAT    , A1_LONG   , A1_DDD, ` +
            `A1_TEL      , A1_TEL2   , A1_TEL3   , A1_TEL4   , A1_FAX    , A1_CEL1, ` +
            `A1_CEL2     , A1_ENDCOB , A1_BAIRROC, A1_MUNC   , A1_ESTC   , A1_CEPC, ` +
            `A1_INSCR    , A1_PFISICA, A1_VEND   , A1_VEND2  , A1_TPFRET , A1_RISCO, ` +
            `A1_LC       , A1_CODPAIS, A1_MSBLQL , A1_EXPORTA, A1_MOEDALC, A1_CONTA, ` +
            `A1_SUPER    , A1_OBSERV , A1_EMAIL  , A1_DTCADAS, A1_DTALT  , A1_LIBEDIT, ` +
            `SALDO       , A1_INFOROT, A1_OBSRC  , A1_DTULTIT, A1_COND   , A1_COND2, ` +
            `CREDR       , DTSYNC    , A1_PAIS   , A1_MOBILE)` +
            `VALUES ` +
            `(?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` + 
            `?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?)`,
            cliente.A1_FILIAL , cliente.A1_LOJA   , cliente.A1_PESSOA , cliente.A1_CGC      , cliente.A1_NOME   , cliente.A1_DTNASC,
            cliente.A1_NREDUZ , cliente.A1_ME     , cliente.A1_END    , cliente.A1_COMPLEM  , cliente.A1_BAIRRO , cliente.A1_EST,
            cliente.A1_MUN    , cliente.A1_CODMUN , cliente.A1_CEP    , cliente.A1_LAT      , cliente.A1_LONG   , cliente.A1_DDD, 
            cliente.A1_TEL    , cliente.A1_TEL2   , cliente.A1_TEL3   , cliente.A1_TEL4     , cliente.A1_FAX    , cliente.A1_CEL1, 
            cliente.A1_CEL2   , cliente.A1_ENDCOB , cliente.A1_BAIRROC, cliente.A1_MUNC     , cliente.A1_ESTC   , cliente.A1_CEPC, 
            cliente.A1_INSCR  , cliente.A1_PFISICA, cliente.A1_VEND   , cliente.A1_VEND2    , cliente.A1_TPFRET , cliente.A1_RISCO, 
            cliente.A1_LC     , cliente.A1_CODPAIS, cliente.A1_MSBLQL , cliente.A1_EXPORTA  , cliente.A1_MOEDALC, cliente.A1_CONTA,
            cliente.A1_SUPER  , cliente.A1_OBSERV , cliente.A1_EMAIL  , cliente.A1_DTCADAS  , cliente.A1_DTALT  , cliente.A1_LIBEDIT,
            cliente.SALDO     , cliente.A1_INFOROT, cliente.A1_OBSRC  , cliente.A1_DTULTIT  , cliente.A1_COND   , cliente.A1_COND2,
            cliente.CREDR     , cliente.DTSYNC    , cliente.A1_PAIS   , cliente.A1_MOBILE
        );

        const id = db.lastInsertRowId;
        success(id);
        db.close();
    }
    catch(err) {
        error(err);
    }
}

exports.updateCadCliente = function(params) {
    var data    = params.data || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    var id      = data.id;
    var cliente = data.cliente;
    if(!cliente) return error('Cliente não informado para cadastrar');
    var db = Ti.Database.open('somaCRMDB');
    try {
        db.execute(
            `UPDATE cliente SET ` + 
            `A1_FILIAL = ? , A1_LOJA = ?     , A1_PESSOA = ?   , A1_CGC = ?      , A1_NOME = ?     , A1_DTNASC = ? , ` +
            `A1_NREDUZ = ?  , A1_ME = ?       , A1_END = ?      , A1_COMPLEM = ?  , A1_BAIRRO = ?   , A1_EST = ?    , ` +
            `A1_MUN = ?     , A1_CODMUN = ?   , A1_CEP = ?      , A1_LAT = ?      , A1_LONG = ?     , A1_DDD = ?    , ` +
            `A1_TEL = ?     , A1_TEL2 = ?     , A1_TEL3 = ?     , A1_TEL4 = ?     , A1_FAX = ?      , A1_CEL1 = ?   , ` +
            `A1_CEL2 = ?    , A1_ENDCOB = ?   , A1_BAIRROC = ?  , A1_MUNC = ?     , A1_ESTC = ?     , A1_CEPC = ?   , ` +
            `A1_INSCR = ?   , A1_PFISICA = ?  , A1_VEND = ?     , A1_VEND2 = ?    , A1_TPFRET = ?   , A1_RISCO = ?  , ` +
            `A1_LC = ?      , A1_CODPAIS = ?  , A1_MSBLQL = ?   , A1_EXPORTA = ?  , A1_MOEDALC = ?  , A1_CONTA = ?  , ` +
            `A1_SUPER = ?   , A1_OBSERV = ?   , A1_EMAIL = ?    , A1_DTCADAS = ?  , A1_DTALT = ?    , A1_LIBEDIT = ?, ` +
            `SALDO = ?      , A1_INFOROT = ?  , A1_OBSRC = ?    , A1_DTULTIT = ?  , A1_COND = ?     , A1_COND2 = ?  , ` +
            `CREDR = ?      , DTSYNC = ?      , A1_PAIS = ?     , A1_MOBILE = ? ` +
            `WHERE ID = '${id}' `,
            cliente.A1_FILIAL , cliente.A1_LOJA   , cliente.A1_PESSOA , cliente.A1_CGC      , cliente.A1_NOME   , cliente.A1_DTNASC,
            cliente.A1_NREDUZ , cliente.A1_ME     , cliente.A1_END    , cliente.A1_COMPLEM  , cliente.A1_BAIRRO , cliente.A1_EST,
            cliente.A1_MUN    , cliente.A1_CODMUN , cliente.A1_CEP    , cliente.A1_LAT      , cliente.A1_LONG   , cliente.A1_DDD, 
            cliente.A1_TEL    , cliente.A1_TEL2   , cliente.A1_TEL3   , cliente.A1_TEL4     , cliente.A1_FAX    , cliente.A1_CEL1, 
            cliente.A1_CEL2   , cliente.A1_ENDCOB , cliente.A1_BAIRROC, cliente.A1_MUNC     , cliente.A1_ESTC   , cliente.A1_CEPC, 
            cliente.A1_INSCR  , cliente.A1_PFISICA, cliente.A1_VEND   , cliente.A1_VEND2    , cliente.A1_TPFRET , cliente.A1_RISCO, 
            cliente.A1_LC     , cliente.A1_CODPAIS, cliente.A1_MSBLQL , cliente.A1_EXPORTA  , cliente.A1_MOEDALC, cliente.A1_CONTA,
            cliente.A1_SUPER  , cliente.A1_OBSERV , cliente.A1_EMAIL  , cliente.A1_DTCADAS  , cliente.A1_DTALT  , cliente.A1_LIBEDIT,
            cliente.SALDO     , cliente.A1_INFOROT, cliente.A1_OBSRC  , cliente.A1_DTULTIT  , cliente.A1_COND   , cliente.A1_COND2,
            cliente.CREDR     , cliente.DTSYNC    , cliente.A1_PAIS   , cliente.A1_MOBILE
        );
        success(db.lastInsertRowId);
        db.close();
    }
    catch(err) {
        db.close();
        error(err);
    }
}

//CRIA TABELA PARA CADASTRAR REFERENCIAS
exports.createTableCadRef = function() {
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS cadRef`);
    db.execute(`CREATE TABLE IF NOT EXISTS cadRef (ID INTEGER PRIMARY KEY, AO_FILIAL TEXT, AO_CLIENTE TEXT, AO_LOJA TEXT, AO_TIPO TEXT, AO_NOMINS TEXT, AO_DATA TEXT, AO_SOCIO TEXT, AO_CGC TEXT, AO_TELEFON TEXT, AO_CONTATO TEXT, AO_OBSERV TEXT, AO_DTALT TEXT, DTSYNC TEXT, ID_CLIENTE INTEGER, RECNO INTEGER, DEL TEXT)`);
    db.close();
}

exports.insertCadRef = function(params) {
    var ref     = params.ref || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!ref) return error('Referência não informada para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `INSERT INTO cadRef` + 
            `(AO_FILIAL  , AO_CLIENTE, AO_LOJA  , AO_TIPO, ` +
            `AO_NOMINS   , AO_DATA   , AO_SOCIO , AO_CGC, ` +
            `AO_TELEFON  , AO_CONTATO, AO_OBSERV, AO_DTALT, ` +
            `DTSYNC      , ID_CLIENTE, RECNO    , DEL) ` + 
            `VALUES ` +
            `(?, ?, ?, ?, ` +
            `?, ?, ?, ?, ` +
            `?, ?, ?, ?, ` +
            `?, ?, ?, ?) `,
            ref.AO_FILIAL   , ref.AO_CLIENTE, ref.AO_LOJA   , ref.AO_TIPO, 
            ref.AO_NOMINS   , ref.AO_DATA   , ref.AO_SOCIO  , ref.AO_CGC, 
            ref.AO_TELEFON  , ref.AO_CONTATO, ref.AO_OBSERV , ref.AO_DTALT, 
            ref.DTSYNC      , ref.ID_CLIENTE, ref.RECNO     , ref.DEL
        );
        success({ref: ref.AO_NOMINS, id: db.lastInsertRowId});
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.updateCadRef = function(params) {
    var data    = params.data || null;
    var id      = data.id; 
    var ref     = data.ref || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('REFERENCIA SEM ID: ', ref);
    if(!ref) return error('Referência não informada para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cadRef SET ` + 
            `AO_FILIAL = ?   , AO_CLIENTE = ?, AO_LOJA = ?  , AO_TIPO = ? , ` +
            `AO_NOMINS = ?   , AO_DATA = ?   , AO_SOCIO = ? , AO_CGC = ?  , ` +
            `AO_TELEFON = ?  , AO_CONTATO = ?, AO_OBSERV = ?, AO_DTALT = ?, ` +
            `DTSYNC = ?      , ID_CLIENTE = ?, RECNO = ?    , DEL = ? ` + 
            `WHERE ID = '${id}' `,
            ref.AO_FILIAL   , ref.AO_CLIENTE, ref.AO_LOJA   , ref.AO_TIPO, 
            ref.AO_NOMINS   , ref.AO_DATA   , ref.AO_SOCIO  , ref.AO_CGC, 
            ref.AO_TELEFON  , ref.AO_CONTATO, ref.AO_OBSERV , ref.AO_DTALT, 
            ref.DTSYNC      , ref.ID_CLIENTE, ref.RECNO     , ref.DEL
        );
        success({ref: ref.AO_NOMINS, id: id});
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.updateRef = function(params) {
    var data      = params.data || null;
    var id_antigo = data.id_cliLocal;
    var id_novo   = data.ID || null;
    var A1_COD    = data.A1_COD || null;
    var success   = params.success || function(){};
    var error     = params.error || function(){};
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cadRef SET AO_CLIENTE = ?, ID_CLIENTE = ? WHERE ID_CLIENTE = '${id_antigo}'`,
            A1_COD, id_novo
        );
        success();
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.getCadRef = function(params) {
    var id      = params.cliente_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    var where   = id ? `WHERE ID_CLIENTE = '${id}'` : '';
    var refs    = [];
    try {
        var db      = Ti.Database.open('somaCRMDB');
        var result  = db.execute(`SELECT ID, AO_FILIAL, AO_CLIENTE, AO_LOJA, AO_TIPO, AO_NOMINS, AO_DATA, AO_SOCIO, AO_CGC, AO_TELEFON, AO_CONTATO, AO_OBSERV, AO_DTALT, ID_CLIENTE, RECNO, DEL FROM cadRef ${where}`);
        
        while(result.isValidRow()) {
            
            refs.push({
                ID          : result.fieldByName('ID'),
                AO_FILIAL   : result.fieldByName('AO_FILIAL'),
                AO_CLIENTE  : result.fieldByName('AO_CLIENTE'),
                AO_LOJA     : result.fieldByName('AO_LOJA'),
                AO_TIPO     : result.fieldByName('AO_TIPO'),
                AO_NOMINS   : result.fieldByName('AO_NOMINS'),
                AO_DATA     : result.fieldByName('AO_DATA'),
                AO_SOCIO    : result.fieldByName('AO_SOCIO'),
                AO_CGC      : result.fieldByName('AO_CGC'),
                AO_TELEFON  : result.fieldByName('AO_TELEFON'),
                AO_CONTATO  : result.fieldByName('AO_CONTATO'),
                AO_OBSERV   : result.fieldByName('AO_OBSERV'),
                AO_DTALT    : result.fieldByName('AO_DTALT'),
                ID_CLIENTE  : result.fieldByName('ID_CLIENTE'),
                RECNO       : result.fieldByName('RECNO'),
                DEL         : result.fieldByName('DEL'),
            });
            result.next();
        }
        result.close();
        db.close();
        success(refs)
    }
    catch(err){
        error(err);
    }
}

exports.deleteCadRef = function(params){
    var id      = params.ref_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('ID da referência inválida');
    var where   = `WHERE ID = '${id}'`;
    var db      = Ti.Database.open('somaCRMDB');
    db.execute(`DELETE FROM cadRef ${where}`)
    db.close();
}

//CRIA TABELA PARA CADASTRAR REFERENCIAS
exports.createTableCadCondPag = function() {
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS cadCondPag`);
    db.execute(`CREATE TABLE IF NOT EXISTS cadCondPag (ID INTEGER PRIMARY KEY, CODCLI TEXT, `+ 
    `LOJA TEXT, GRPVEN TEXT, CONDPG TEXT, TIPO TEXT, COND TEXT,DESCRI TEXT, ` +
    `FATORVD REAL, E4_DESCVEN REAL,  E4_ACRSVEN REAL, DATDE TEXT, DTSYNC TEXT, RECNO INTEGER)`);
    db.close();
}

exports.insertCadCondPag = function(params) {
    var condPag = params.condPag || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!condPag) return error('Referência não informada para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `INSERT INTO cadCondPag` + 
            `(CODCLI, LOJA, GRPVEN, CONDPG, TIPO, COND, DESCRI, ` +
            `FATORVD, E4_DESCVEN, E4_ACRSVEN, DATDE, DTSYNC, RECNO) ` +
            `VALUES ` +
            `(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) `,
            condPag.CODCLI, condPag.LOJA, condPag.GRPVEN, condPag.CONDPG, condPag.TIPO, condPag.COND, condPag.DESCRI, 
            condPag.FATORVD, condPag.E4_DESCVEN, condPag.E4_ACRSVEN, condPag.DATDE, condPag.DTSYNC, condPag.RECNO
        );
        success({condPag: condPag, id: db.lastInsertRowId});
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.updateCadCondPag = function(params) {
    var data    = params.data || null;
    var condPag = data.new_condPag || null;
    var id      = condPag.CODCLI; 
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('CONDIÇÃO SEM ID: ', condPag);
    if(!condPag) return error('Condição não informada para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cadCondPag SET ` + 
            `CODCLI = ?         , LOJA = ?          , GRPVEN = ?    , CONDPG = ? , ` +
            `TIPO = ?           , COND = ?          , DESCRI = ?    , FATORVD = ?, ` +
            `E4_DESCVEN = ?     , E4_ACRSVEN = ?    , DATDE = ?     , DTSYNC = ? , RECNO = ? ` +
            `WHERE CODCLI = '${id}' `,
            condPag.CODCLI      , condPag.LOJA      , condPag.GRPVEN, condPag.CONDPG, 
            condPag.TIPO        , condPag.COND      , condPag.DESCRI, condPag.FATORVD, 
            condPag.E4_DESCVEN  , condPag.E4_ACRSVEN, condPag.DATDE , condPag.DTSYNC, condPag.RECNO 
        );
        success({condPag: condPag.DESCRI, id: id});
        db.close();
    }
    catch(err){
        error(err);
    }
}


exports.updateCondPag = function(params) {
    var data      = params.data || null;
    var id_antigo = data.id_cliLocal;
    var A1_COD    = data.A1_COD || null;
    var success   = params.success || function(){};
    var error     = params.error || function(){};
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cadCondPag SET CODCLI = ? WHERE CODCLI = '${id_antigo}'`,
            A1_COD
        );
        success();
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.getCadCondPag = function(params) {
    var id      = params.cliente_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    var where   = id ? `WHERE CODCLI = '${id}'` : '';
    var condPag    = {};
    try {
        var db      = Ti.Database.open('somaCRMDB');
        var result  = db.execute(`SELECT ID, CODCLI, LOJA, GRPVEN, CONDPG, TIPO, COND, DESCRI, FATORVD, E4_DESCVEN, E4_ACRSVEN, DATDE, DTSYNC, RECNO FROM cadCondPag ${where}`);
        
        while(result.isValidRow()) {
            
            condPag = {
                ID          : result.fieldByName('ID'),
                CODCLI      : result.fieldByName('CODCLI'),
                LOJA        : result.fieldByName('LOJA'),
                GRPVEN      : result.fieldByName('GRPVEN'),
                CONDPG      : result.fieldByName('CONDPG'),
                TIPO        : result.fieldByName('TIPO'),
                COND        : result.fieldByName('COND'),
                DESCRI      : result.fieldByName('DESCRI'),
                FATORVD     : result.fieldByName('FATORVD'),
                E4_DESCVEN  : result.fieldByName('E4_DESCVEN'),
                E4_ACRSVEN  : result.fieldByName('E4_ACRSVEN'),
                DATDE       : result.fieldByName('DATDE'),
                DTSYNC      : result.fieldByName('DTSYNC'),
                RECNO       : result.fieldByName('RECNO'),
            };
            result.next();
        }
        result.close();
        db.close();
        success(condPag)
    }
    catch(err){
        error(err);
    }
}

exports.deleteCadCondPag = function(params){
    var id      = params.cliente_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('ID da condição inválida');
    var where   = `WHERE CODCLI = '${id}'`;
    var db      = Ti.Database.open('somaCRMDB');
    db.execute(`DELETE FROM cadCondPag ${where}`)
    db.close();
}

//CRIA TABELA DE CLIENTES PARA SINCRONIZAR
exports.createTableCliente = function(){
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS cliente`);
    db.execute(`CREATE TABLE IF NOT EXISTS cliente ( ` +
        `ID INTEGER PRIMARY KEY, ` +
        `A1_FILIAL TEXT, ` +
        `A1_COD TEXT, ` +
        `A1_MOBILE INTEGER, ` +
        `A1_LOJA TEXT, ` +
        `A1_PESSOA TEXT, ` +
        `A1_CGC TEXT, ` +
        `A1_NOME TEXT, ` +
        `A1_NREDUZ TEXT, ` +
        `A1_TIPO TEXT, ` +
        `A1_ME TEXT, ` +
        `A1_END TEXT, ` +
        `A1_COMPLEM TEXT, ` +
        `A1_BAIRRO TEXT, ` +
        `A1_EST TEXT, ` +
        `A1_MUN TEXT, ` +
        `A1_CODMUN TEXT, ` +
        `A1_CEP TEXT, ` +
        `A1_LAT TEXT, ` +
        `A1_LONG TEXT, ` +
        `A1_DDD TEXT, ` +
        `A1_TEL TEXT, ` +
        `A1_TEL2 TEXT, ` +
        `A1_TEL3 TEXT, ` +
        `A1_TEL4 TEXT, ` +
        `A1_FAX TEXT, ` +
        `A1_CEL1 TEXT, ` +
        `A1_CEL2 TEXT, ` +
        `A1_ENDCOB TEXT, ` +
        `A1_BAIRROC TEXT, ` +
        `A1_MUNC TEXT, ` +
        `A1_ESTC TEXT, ` +
        `A1_CEPC TEXT, ` +
        `A1_INSCR TEXT, ` +
        `A1_PFISICA TEXT, ` +
        `A1_PAIS TEXT, ` +
        `A1_CODPAIS TEXT, ` +
        `A1_EXPORTA TEXT, ` +
        `A1_MOEDALC TEXT, ` +
        `A1_CONTA TEXT, ` +
        `A1_VEND TEXT, ` +
        `A1_VEND2 TEXT, ` +
        `A1_TPFRET TEXT, ` +
        `A1_RISCO TEXT, ` +
        `A1_LC REAL, ` +
        `A1_GRPTRIB TEXT, ` +
        `A1_MSBLQL TEXT, ` +
        `A1_SUPER TEXT, ` +
        `A1_OBSERV TEXT, ` +
        `A1_EMAIL TEXT, ` +
        `A1_DTALT TEXT, ` +
        `A1_DTCADAS TEXT, ` +
        `A1_DTNASC TEXT, ` +
        `A1_LIBEDIT TEXT, ` +
        `A1_GRPVEN TEXT, ` +
        `A1_TITVENC REAL, ` +
        `A1_TLC REAL, ` +
        `DIASVENC INTEGER, ` +
        `NROPAG INTEGER, ` +
        `CREDR REAL, ` +
        `SALDO REAL, ` +
        `A1_INFOROT TEXT, ` +
        `A1_OBSRC TEXT, ` +
        `A1_ULTCOM TEXT, ` +
        `A1_MSALDO REAL, ` +
        `A1_MCOMPRA REAL, ` +
        `METR REAL, ` +
        `A1_MATR REAL, ` +
        `A1_TITPROT REAL, ` +
        `A1_DTULTIT TEXT, ` +
        `A1_CHQDEVO REAL, ` +
        `A1_DTULCHQ TEXT, ` +
        `A1_PRICOM TEXT, ` +
        `DTSYNC TEXT, ` +
        `A1_COND TEXT,  ` +
        `A1_COND2 TEXT,  ` +
        `E4_DESCRI TEXT, ` +
        `E4_DESCRI2 TEXT )`
    );
    db.close();
}

exports.insertClientes = function(array) {
    var db = Ti.Database.open('somaCRMDB');
    db.execute('BEGIM');
    for (var i in array) {
        // if(!array[i].A1_MOBILE) {
        //     array[i].A1_MOBILE = 0;
        // }
        db.execute(`INSERT INTO cliente (` +
            `A1_FILIAL, ` +
            `A1_COD, ` +
            `A1_MOBILE, ` +
            `A1_LOJA, ` +
            `A1_PESSOA, ` +
            `A1_CGC, ` +
            `A1_NOME, ` +
            `A1_NREDUZ, ` +
            `A1_TIPO, ` +
            `A1_ME, ` +
            `A1_END, ` +
            `A1_COMPLEM, ` +
            `A1_BAIRRO, ` +
            `A1_EST, ` +
            `A1_MUN, ` +
            `A1_CODMUN, ` +
            `A1_CEP, ` +
            `A1_LAT, ` +
            `A1_LONG, ` +
            `A1_DDD, ` +
            `A1_TEL, ` +
            `A1_TEL2, ` +
            `A1_TEL3, ` +
            `A1_TEL4, ` +
            `A1_FAX, ` +
            `A1_CEL1, ` +
            `A1_CEL2, ` +
            `A1_ENDCOB, ` +
            `A1_BAIRROC, ` +
            `A1_MUNC, ` +
            `A1_ESTC, ` +
            `A1_CEPC, ` +
            `A1_INSCR, ` +
            `A1_PFISICA, ` +
            `A1_PAIS, ` +
            `A1_CODPAIS, ` +
            `A1_VEND, ` +
            `A1_VEND2, ` +
            `A1_TPFRET, ` +
            `A1_RISCO, ` +
            `A1_LC, ` +
            `A1_GRPTRIB, ` +
            `A1_MSBLQL, ` +
            `A1_SUPER, ` +
            `A1_OBSERV, ` +
            `A1_EMAIL, ` +
            `A1_DTALT, ` +
            `A1_DTCADAS, ` +
            `A1_DTNASC, ` +
            `A1_LIBEDIT, ` +
            `A1_GRPVEN, ` +
            `A1_TITVENC, ` +
            `A1_TLC, ` +
            `DIASVENC, ` +
            `NROPAG, ` +
            `CREDR, ` +
            `A1_INFOROT, ` +
            `A1_OBSRC, ` +
            `A1_ULTCOM, ` +
            `A1_MSALDO, ` +
            `A1_MCOMPRA, ` +
            `METR, ` +
            `A1_MATR, ` +
            `A1_TITPROT, ` +
            `A1_DTULTIT, ` +
            `A1_CHQDEVO, ` +
            `A1_DTULCHQ, ` +
            `A1_PRICOM, ` +
            `DTSYNC, ` +
            `A1_COND,  ` +
            `A1_COND2,  ` +
            `E4_DESCRI, ` +
            `E4_DESCRI2) ` +
            `VALUES ( ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ` +
            `)`,
            array[i].A1_FILIAL, 
            array[i].A1_COD, 
            array[i].A1_MOBILE,
            array[i].A1_LOJA, 
            array[i].A1_PESSOA, 
            array[i].A1_CGC, 
            array[i].A1_NOME, 
            array[i].A1_NREDUZ, 
            array[i].A1_TIPO, 
            array[i].A1_ME, 
            array[i].A1_END, 
            array[i].A1_COMPLEM, 
            array[i].A1_BAIRRO, 
            array[i].A1_EST, 
            array[i].A1_MUN, 
            array[i].A1_CODMUN, 
            array[i].A1_CEP, 
            array[i].A1_LAT, 
            array[i].A1_LONG, 
            array[i].A1_DDD, 
            array[i].A1_TEL, 
            array[i].A1_TEL2, 
            array[i].A1_TEL3, 
            array[i].A1_TEL4, 
            array[i].A1_FAX, 
            array[i].A1_CEL1, 
            array[i].A1_CEL2, 
            array[i].A1_ENDCOB, 
            array[i].A1_BAIRROC, 
            array[i].A1_MUNC, 
            array[i].A1_ESTC, 
            array[i].A1_CEPC, 
            array[i].A1_INSCR, 
            array[i].A1_PFISICA, 
            array[i].A1_PAIS, 
            array[i].A1_CODPAIS, 
            array[i].A1_VEND, 
            array[i].A1_VEND2, 
            array[i].A1_TPFRET, 
            array[i].A1_RISCO, 
            array[i].A1_LC, 
            array[i].A1_GRPTRIB, 
            array[i].A1_MSBLQL, 
            array[i].A1_SUPER, 
            array[i].A1_OBSERV, 
            array[i].A1_EMAIL, 
            array[i].A1_DTALT, 
            array[i].A1_DTCADAS, 
            array[i].A1_DTNASC, 
            array[i].A1_LIBEDIT, 
            array[i].A1_GRPVEN, 
            array[i].A1_TITVENC, 
            array[i].A1_TLC, 
            array[i].DIASVENC,
            array[i].NROPAG, 
            array[i].CREDR, 
            array[i].A1_INFOROT, 
            array[i].A1_OBSRC, 
            array[i].A1_ULTCOM, 
            array[i].A1_MSALDO, 
            array[i].A1_MCOMPRA, 
            array[i].METR, 
            array[i].A1_MATR, 
            array[i].A1_TITPROT, 
            array[i].A1_DTULTIT, 
            array[i].A1_CHQDEVO, 
            array[i].A1_DTULCHQ, 
            array[i].A1_PRICOM,  
            array[i].DTSYNC, 
            array[i].A1_COND,  
            array[i].A1_COND2,  
            array[i].E4_DESCRI, 
            array[i].E4_DESCRI2
        );
    }
    db.execute('COMMIT');
    db.close();
}

exports.countCadCli = function(params){
    var success  = params.success || function(){};
    var error    = params.error || function(){};
    var count    = 0;
    var db = Ti.Database.open('somaCRMDB');
    try {
        var result = db.execute(`SELECT COUNT(*) AS QNT FROM cliente WHERE A1_MOBILE = '1'`);
        count = result.fieldByName('QNT');
        result.close();
        db.close();
        success(count);
    }
    catch(err) {
        error(err);
    }
}

exports.getClientes = function(params){
    var id       = params.cliente_id || null;
    var success  = params.success || function(){};
    var error    = params.error || function(){};
    var clientes = [];
    var where    = '';

    if(id) {
        where = `WHERE A1_COD = '${id}'`
    }

    var db = Ti.Database.open('somaCRMDB');
    try {
        var result = db.execute(`SELECT ` +
            `ID, ` +
            `A1_COD, ` +
            `A1_MOBILE, ` +
            `A1_PESSOA, ` +
            `A1_CGC, ` +
            `A1_NOME, ` +
            `A1_NREDUZ, ` +
            `A1_MUN, ` +
            `A1_LOJA, ` +
            `A1_DDD, ` +
            `A1_TEL, ` +
            `A1_RISCO, ` +
            `A1_MSBLQL, ` +
            `A1_OBSERV, ` +
            `A1_GRPVEN, ` +
            `A1_TITVENC, ` +
            `A1_TLC, ` +
            `A1_TIPO, ` +
            `DIASVENC, ` +
            `NROPAG, ` +
            `CREDR, ` +
            `A1_ULTCOM ` +
            `FROM cliente ${where}`
        );
        while(result.isValidRow()) {
            clientes.push({
                ID          : result.fieldByName('ID'),
                A1_COD      : result.fieldByName('A1_COD'),
                A1_MOBILE   : result.fieldByName('A1_MOBILE'),
                A1_PESSOA   : result.fieldByName('A1_PESSOA'),
                A1_CGC      : result.fieldByName('A1_CGC'),
                A1_NOME     : result.fieldByName('A1_NOME'),
                A1_NREDUZ   : result.fieldByName('A1_NREDUZ'),
                A1_MUN      : result.fieldByName('A1_MUN'),
                A1_LOJA     : result.fieldByName('A1_LOJA'),
                A1_DDD      : result.fieldByName('A1_DDD'),
                A1_TEL      : result.fieldByName('A1_TEL'),
                A1_RISCO    : result.fieldByName('A1_RISCO'),
                A1_MSBLQL   : result.fieldByName('A1_MSBLQL'),
                A1_OBSERV   : result.fieldByName('A1_OBSERV'),
                A1_GRPVEN   : result.fieldByName('A1_GRPVEN'),
                A1_TITVENC  : result.fieldByName('A1_TITVENC'),
                A1_TLC      : result.fieldByName('A1_TLC'),
                A1_TIPO     : result.fieldByName('A1_TIPO'),
                DIASVENC    : result.fieldByName('DIASVENC'),
                NROPAG      : result.fieldByName('NROPAG'),
                CREDR       : result.fieldByName('CREDR'),
                A1_ULTCOM   : result.fieldByName('A1_ULTCOM'),
            });
            result.next();
        }
        result.close();
        db.close();
        success(clientes);
    }
    catch(err) {
        error(err);
    }
}

exports.getClienteDetail = function(params){
    var id       = params.id || null;
    var success  = params.success || function(){};
    var error    = params.error || function(){};
    var where    = '';
    var clientes = [];
    var db = Ti.Database.open('somaCRMDB');
    if(id.ID) {
        where = `WHERE ID = '${id.ID}'`;
    }
    else if(id.A1_COD) {
        where = `WHERE A1_COD = '${id.A1_COD}'`;
    }
    else if(id.A1_MOBILE) {
        where = `WHERE A1_MOBILE = '${id.A1_MOBILE}'`
    }

    try {
        var result = db.execute(`SELECT ` +
            `ID, ` +
            `A1_FILIAL, ` +
            `A1_COD, ` +
            `A1_MOBILE, ` +
            `A1_LOJA, ` +
            `A1_PESSOA, ` +
            `A1_CGC, ` +
            `A1_NOME, ` +
            `A1_NREDUZ, ` +
            `A1_TIPO, ` +
            `A1_ME, ` +
            `A1_END, ` +
            `A1_COMPLEM, ` +
            `A1_BAIRRO, ` +
            `A1_EST, ` +
            `A1_MUN, ` +
            `A1_CODMUN, ` +
            `A1_CEP, ` +
            `A1_DDD, ` +
            `A1_LAT, ` +
            `A1_LONG, ` +
            `A1_TEL, ` +
            `A1_TEL2, ` +
            `A1_TEL3, ` +
            `A1_TEL4, ` +
            `A1_FAX, ` +
            `A1_CEL1, ` +
            `A1_CEL2, ` +
            `A1_ENDCOB, ` +
            `A1_BAIRROC, ` +
            `A1_MUNC, ` +
            `A1_ESTC, ` +
            `A1_CEPC, ` +
            `A1_INSCR, ` +
            `A1_PFISICA, ` +
            `A1_PAIS, ` +
            `A1_CODPAIS, ` +
            `A1_VEND, ` +
            `A1_VEND2, ` +
            `A1_TPFRET, ` +
            `A1_RISCO, ` +
            `A1_LC, ` +
            `A1_GRPTRIB, ` +
            `A1_MSBLQL, ` +
            `A1_SUPER, ` +
            `A1_OBSERV, ` +
            `A1_EMAIL, ` +
            `A1_DTALT, ` +
            `A1_DTCADAS, ` +
            `A1_DTNASC, ` +
            `A1_LIBEDIT, ` +
            `A1_GRPVEN, ` +
            `A1_TITVENC, ` +
            `A1_TLC, ` +
            `DIASVENC, ` +
            `NROPAG, ` +
            `CREDR, ` +
            `A1_INFOROT, ` +
            `A1_OBSRC, ` +
            `A1_ULTCOM, ` +
            `A1_MSALDO, ` +
            `A1_MCOMPRA, ` +
            `METR, ` +
            `A1_MATR, ` +
            `A1_TITPROT, ` +
            `A1_DTULTIT, ` +
            `A1_CHQDEVO, ` +
            `A1_DTULCHQ, ` +
            `A1_PRICOM, ` +
            `DTSYNC, ` +
            `A1_COND,  ` +
            `A1_COND2,  ` +
            `E4_DESCRI, ` +
            `E4_DESCRI2 ` +
            `FROM cliente ` + 
            `${where}`
        );
        while(result.isValidRow()) {
            clientes.push({
                ID          : result.fieldByName('ID'),
                A1_FILIAL   : result.fieldByName('A1_FILIAL'),
                A1_COD      : result.fieldByName('A1_COD'),
                A1_MOBILE   : result.fieldByName('A1_MOBILE'),
                A1_LOJA     : result.fieldByName('A1_LOJA'),
                A1_PESSOA   : result.fieldByName('A1_PESSOA'),
                A1_CGC      : result.fieldByName('A1_CGC'),
                A1_NOME     : result.fieldByName('A1_NOME'),
                A1_NREDUZ   : result.fieldByName('A1_NREDUZ'),
                A1_TIPO     : result.fieldByName('A1_TIPO'),
                A1_ME       : result.fieldByName('A1_ME'),
                A1_END      : result.fieldByName('A1_END'),
                A1_COMPLEM  : result.fieldByName('A1_COMPLEM'),
                A1_BAIRRO   : result.fieldByName('A1_BAIRRO'),
                A1_EST      : result.fieldByName('A1_EST'),
                A1_MUN      : result.fieldByName('A1_MUN'),
                A1_CODMUN   : result.fieldByName('A1_CODMUN'),
                A1_CEP      : result.fieldByName('A1_CEP'),
                A1_DDD      : result.fieldByName('A1_DDD'),
                A1_LAT      : result.fieldByName('A1_LAT'),
                A1_LONG     : result.fieldByName('A1_LONG'),
                A1_TEL      : result.fieldByName('A1_TEL'),
                A1_TEL2     : result.fieldByName('A1_TEL2'),
                A1_TEL3     : result.fieldByName('A1_TEL3'),
                A1_TEL4     : result.fieldByName('A1_TEL4'),
                A1_FAX      : result.fieldByName('A1_FAX'),
                A1_CEL1     : result.fieldByName('A1_CEL1'),
                A1_CEL2     : result.fieldByName('A1_CEL2'),
                A1_ENDCOB   : result.fieldByName('A1_ENDCOB'),
                A1_BAIRROC  : result.fieldByName('A1_BAIRROC'),
                A1_MUNC     : result.fieldByName('A1_MUNC'),
                A1_ESTC     : result.fieldByName('A1_ESTC'),
                A1_CEPC     : result.fieldByName('A1_CEPC'),
                A1_INSCR    : result.fieldByName('A1_INSCR'),
                A1_PFISICA  : result.fieldByName('A1_PFISICA'),
                A1_PAIS     : result.fieldByName('A1_PAIS'),
                A1_CODPAIS  : result.fieldByName('A1_CODPAIS'),
                A1_VEND     : result.fieldByName('A1_VEND'),
                A1_VEND2    : result.fieldByName('A1_VEND2'),
                A1_TPFRET   : result.fieldByName('A1_TPFRET'),
                A1_RISCO    : result.fieldByName('A1_RISCO'),
                A1_LC       : result.fieldByName('A1_LC'),
                A1_GRPTRIB  : result.fieldByName('A1_GRPTRIB'),
                A1_MSBLQL   : result.fieldByName('A1_MSBLQL'),
                A1_SUPER    : result.fieldByName('A1_SUPER'),
                A1_OBSERV   : result.fieldByName('A1_OBSERV'),
                A1_EMAIL    : result.fieldByName('A1_EMAIL'),
                A1_DTALT    : result.fieldByName('A1_DTALT'),
                A1_DTCADAS  : result.fieldByName('A1_DTCADAS'),
                A1_DTNASC   : result.fieldByName('A1_DTNASC'),
                A1_LIBEDIT  : result.fieldByName('A1_LIBEDIT'),
                A1_GRPVEN   : result.fieldByName('A1_GRPVEN'),
                A1_TITVENC  : result.fieldByName('A1_TITVENC'),
                A1_TLC      : result.fieldByName('A1_TLC'),
                DIASVENC    : result.fieldByName('DIASVENC'),
                NROPAG      : result.fieldByName('NROPAG'),
                CREDR       : result.fieldByName('CREDR'),
                A1_INFOROT  : result.fieldByName('A1_INFOROT'),
                A1_OBSRC    : result.fieldByName('A1_OBSRC'),
                A1_ULTCOM   : result.fieldByName('A1_ULTCOM'),
                A1_MSALDO   : result.fieldByName('A1_MSALDO'),
                A1_MCOMPRA  : result.fieldByName('A1_MCOMPRA'),
                METR        : result.fieldByName('METR'),
                A1_MATR     : result.fieldByName('A1_MATR'),
                A1_TITPROT  : result.fieldByName('A1_TITPROT'),
                A1_DTULTIT  : result.fieldByName('A1_DTULTIT'),
                A1_CHQDEVO  : result.fieldByName('A1_CHQDEVO'),
                A1_DTULCHQ  : result.fieldByName('A1_DTULCHQ'),
                A1_PRICOM   : result.fieldByName('A1_PRICOM'),
                DTSYNC      : result.fieldByName('DTSYNC'),
                A1_COND     : result.fieldByName('A1_COND'),
                A1_COND2    : result.fieldByName('A1_COND2'),
                E4_DESCRI   : result.fieldByName('E4_DESCRI'),
                E4_DESCRI2  : result.fieldByName('E4_DESCRI2'),
            });
            result.next();
        }
        result.close();
        db.close();
        success(clientes);
    }
    catch(err) {
        error(err);
    }
}

//CRIA TABELA DE REFERENCIAS PARA SINCRONIZAR
exports.createTableReferencia = function(){
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS referencia`);
    db.execute(`CREATE TABLE IF NOT EXISTS referencia (` + 
    `ID INTEGER PRIMARY KEY, ` + 
    `AO_FILIAL TEXT, ` + 
    `AO_CLIENTE TEXT, ` + 
    `AO_LOJA TEXT, ` + 
    `AO_TIPO TEXT, ` +
    `AO_NOMINS TEXT, ` + 
    `AO_DATA TEXT, ` + 
    `AO_SOCIO TEXT, ` +
    `AO_CGC TEXT, ` +
    `AO_TELEFON TEXT, ` + 
    `AO_CONTATO TEXT, ` +
    `AO_OBSERV TEXT, ` +
    `AO_DTALT TEXT, ` +
    `DTSYNC TEXT, ` +
    `ID_CLIENTE INTEGER, ` +
    `RECNO INTEGER, `+
    `DEL TEXT)`);
    db.close();
}

exports.insertReferencia = function(array) {
    var db = Ti.Database.open('somaCRMDB');
    db.execute('BEGIM');
    for (var i in array) {
        db.execute(`INSERT INTO referencia (` +
            `AO_FILIAL, ` + 
            `AO_CLIENTE, ` + 
            `AO_LOJA, ` + 
            `AO_TIPO, ` +
            `AO_NOMINS, ` + 
            `AO_DATA, ` + 
            `AO_SOCIO, ` +
            `AO_CGC, ` +
            `AO_TELEFON, ` + 
            `AO_CONTATO, ` +
            `AO_OBSERV, ` +
            `AO_DTALT, ` +
            `DTSYNC, ` +
            `ID_CLIENTE, ` +
            `RECNO, `+
            `DEL ` +
            `) ` +
            `VALUES ( ` +
                `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?` +
            `)`,
            array[i].AO_FILIAL, 
            array[i].AO_CLIENTE, 
            array[i].AO_LOJA, 
            array[i].AO_TIPO, 
            array[i].AO_NOMINS, 
            array[i].AO_DATA, 
            array[i].AO_SOCIO, 
            array[i].AO_CGC, 
            array[i].AO_TELEFON, 
            array[i].AO_CONTATO, 
            array[i].AO_OBSERV, 
            array[i].AO_DTALT, 
            array[i].DTSYNC, 
            array[i].ID_CLIENTE, 
            array[i].RECNO, 
            array[i].DEL
        );
    }
    db.execute('COMMIT');
    db.close();
}

exports.getReferencia = function(params){
    var success  = params.success   || function(){};
    var error    = params.error     || function(){};
    var cod_cli  = params.cod_cli   || null;
    var refs     = [];
    var db       = Ti.Database.open('somaCRMDB');
    var where    = ``;
    if(cod_cli) where = `WHERE AO_CLIENTE = '${cod_cli}'`;
    try {
        var result = db.execute(`SELECT ` +
            `ID, ` +
            `AO_FILIAL, ` +
            `AO_CLIENTE, ` +
            `AO_LOJA, ` +
            `AO_TIPO, ` +
            `AO_NOMINS, ` +
            `AO_DATA, ` +
            `AO_SOCIO, ` +
            `AO_CGC, ` +
            `AO_TELEFON, ` +
            `AO_CONTATO, ` +
            `AO_OBSERV, ` +
            `AO_DTALT, ` +
            `DTSYNC, ` +
            `ID_CLIENTE, ` +
            `RECNO, ` +
            `DEL  ` +
            `FROM referencia ` +
            `${where}`
        );
        while(result.isValidRow()) {
            refs.push({
                ID          : result.fieldByName('ID'),
                AO_FILIAL   : result.fieldByName('AO_FILIAL'),
                AO_CLIENTE  : result.fieldByName('AO_CLIENTE'),
                AO_LOJA     : result.fieldByName('AO_LOJA'),
                AO_TIPO     : result.fieldByName('AO_TIPO'),
                AO_NOMINS   : result.fieldByName('AO_NOMINS'),
                AO_DATA     : result.fieldByName('AO_DATA'),
                AO_SOCIO    : result.fieldByName('AO_SOCIO'),
                AO_CGC      : result.fieldByName('AO_CGC'),
                AO_TELEFON  : result.fieldByName('AO_TELEFON'),
                AO_CONTATO  : result.fieldByName('AO_CONTATO'),
                AO_OBSERV   : result.fieldByName('AO_OBSERV'),
                AO_DTALT    : result.fieldByName('AO_DTALT'),
                DTSYNC      : result.fieldByName('DTSYNC'),
                ID_CLIENTE  : result.fieldByName('ID_CLIENTE'),
                RECNO       : result.fieldByName('RECNO'),
                DEL         : result.fieldByName('DEL'),
            });
            result.next();
        }
        result.close();
        db.close();
        success(refs);
    }
    catch(err) {
        error(err);
    }
}

exports.createTableUF = function() {
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(`DROP TABLE IF EXISTS uf`);
        db.execute(`CREATE TABLE IF NOT EXISTS uf (ID INTEGER PRIMARY KEY, UF_COD TEXT, UF_NOME TEXT, UF_SIGLA TEXT)`);
        db.close();
    }
    catch(err) {
        return err;
    }
}

exports.createTableMnc = function(){
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(`DROP TABLE IF EXISTS mnc`);
        db.execute(`CREATE TABLE IF NOT EXISTS mnc (MNC_CODMC INTEGER PRIMARY KEY, MNC_CODUF TEXT, MNC_CODMUN TEXT, MNC_MUN TEXT)`);
        db.close();
    }
    catch(err) {
        return err;
    }  
}

exports.insertUF = function(array) {
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute('BEGIN');
        for(var i in array){
            db.execute(
                `INSERT INTO uf` + 
                `(UF_COD, UF_NOME, UF_SIGLA) ` + 
                `VALUES ` +
                `(?, ?, ?) `,
                array[i].UF_COD, array[i].UF_NOME, array[i].UF_SIGLA
            );
        }
        db.execute('COMMIT');
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.insertMnc = function(array){
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute('BEGIN');
        for(var i in array){
            db.execute(
                `INSERT INTO mnc` + 
                `(MNC_CODMC  , MNC_CODUF, MNC_CODMUN  , MNC_MUN) ` + 
                `VALUES ` +
                `(?, ?, ?, ?) `,
                array[i].MNC_CODMC, array[i].MNC_CODUF, array[i].MNC_CODMUN, array[i].MNC_MUN
            );
        }
        db.execute('COMMIT');
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.getUF = function(id){
    var where = '';
    var ufs   = [];
    if(id) where = `WHERE ID = ${id}`;
    try {
        var db = Ti.Database.open('somaCRMDB');
        var result = db.execute(`SELECT ` +
            `ID, UF_COD, UF_NOME, UF_SIGLA FROM uf ${where}`
        );
        while(result.isValidRow()) {
            ufs.push({
                ID        : result.fieldByName('ID'),
                UF_COD    : result.fieldByName('UF_COD'),
                UF_NOME   : result.fieldByName('UF_NOME'),
                UF_SIGLA  : result.fieldByName('UF_SIGLA')
            });
            result.next();
        }
        result.close();
        db.close();
        return ufs;
    }
    catch(err) {
        return err;
    }
}

exports.getMnc = function(params) {
    var where = '';
    var mncs = [];
    if(params && params.codmc) where = `WHERE MNC_CODMC = ${params.codmc}`;
    else if(params && params.coduf) where = `WHERE MNC_CODUF = ${params.coduf}`;
    else if(params && params.codmun) where = `WHERE MNC_CODMUN = ${params.codmun}`;

    try {
        var db = Ti.Database.open('somaCRMDB');
        var result = db.execute(`SELECT ` +
            `MNC_CODMC, MNC_CODUF, MNC_CODMUN, MNC_MUN FROM mnc ${where}`
        );
        while(result.isValidRow()) {
            mncs.push({
                MNC_CODMC  : result.fieldByName('MNC_CODMC'),
                MNC_CODUF  : result.fieldByName('MNC_CODUF'),
                MNC_CODMUN : result.fieldByName('MNC_CODMUN'),
                MNC_MUN    : result.fieldByName('MNC_MUN')
            });
            result.next();
        }
        result.close();
        db.close();
        return mncs;
    }
    catch(err) {
        return err;
    }

}

//CRIA TABELA DE PREÇOS PARA SINCRONIZAR
exports.createTableTabelas = function(){
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS tabelaPreco`);
    db.execute(`CREATE TABLE IF NOT EXISTS tabelaPreco (` + 
    `ID INTEGER PRIMARY KEY, ` + 
    `Filial TEXT, ` + 
    `CodTab TEXT, ` + 
    `Descr TEXT, ` + 
    `DtTab TEXT, ` +
    `TpVnd TEXT, ` +
    `Vend TEXT)`);
    db.close();
}

exports.insertTabelas = function(array, vend){
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute('BEGIN');
        for(var i in array){
            array[i].Vend = vend;
            db.execute(
                `INSERT INTO tabelaPreco` + 
                `(Filial, CodTab, Descr, DtTab, TpVnd, Vend) ` + 
                `VALUES ` +
                `(?, ?, ?, ?, ?, ?) `,
                array[i].Filial, array[i].CodTab, array[i].Descr, array[i].DtTab, array[i].TpVnd, array[i].Vend
            );
        }
        db.execute('COMMIT');
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.getTabelas = function(params) {
    var vend    = params.vend || null;
    var tab     = params.tab || null;
    var where   = '';
    var tabelas = [];
    if(vend) where += `WHERE Vend = '${vend}'`;
    if(tab) where += where ? ` AND CodTab = '${tab}'` : `WHERE CobTab = '${tab}'`;
    try {
        var db = Ti.Database.open('somaCRMDB');
        var result = db.execute(`SELECT ` +
            `Filial, CodTab, Descr, DtTab, TpVnd, Vend FROM tabelaPreco ${where}`
        );
        while(result.isValidRow()) {
            tabelas.push({
                Filial  : result.fieldByName('Filial'),
                CodTab  : result.fieldByName('CodTab'),
                Descr   : result.fieldByName('Descr'),
                DtTab   : result.fieldByName('DtTab'),
                TpVnd   : result.fieldByName('TpVnd'),
                Vend    : result.fieldByName('Vend')
            });
            result.next();
        }
        result.close();
        db.close();
        return tabelas;
    }
    catch(err) {
        return err;
    }

}

//CRIA TABELA DE PREÇOS PARA SINCRONIZAR
exports.createTableCondPag = function(){
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS condPag`);
    db.execute(`CREATE TABLE IF NOT EXISTS condPag (` + 
    `ID INTEGER PRIMARY KEY, ` + 
    `CODCLI INTEGER, ` + 
    `GRPVEN INTEGER, ` + 
    `CONDPG TEXT, ` + 
    `TIPO TEXT, ` + 
    `COND TEXT, ` + 
    `DESCRI TEXT, ` +
    `ACRSVEN REAL, ` +
    `DESCVEN REAL)`);
    db.close();
}

exports.insertCondPag = function(array){
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute('BEGIN');
        for(var i in array){
            db.execute(
                `INSERT INTO condPag` + 
                `(CODCLI, GRPVEN, CONDPG, TIPO, COND, DESCRI, ACRSVEN, DESCVEN) ` + 
                `VALUES ` +
                `(?, ?, ?, ?, ?, ?, ?, ?) `,
                array[i].CODCLI, array[i].GRPVEN, array[i].CONDPG, array[i].TIPO, array[i].COND, array[i].DESCRI, array[i].ACRSVEN, array[i].DESCVEN
            );
        }
        db.execute('COMMIT');
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.getCondPag = function(params) {
    var where = '';
    var cli_id  = params.cliente_id || null;
    var cliente = params.A1_COD || null;
    var grpven = params.GRPVEN || null;
    var condpag = params.CONDPAG || null;
    var condPag = [];
    if(cliente) where = `WHERE CODCLI = '${cliente}'`;
    if(condpag) where += where ? ` AND CONDPG = '${condpag}'` : `WHERE CONDPG = '${condpag}'`;
    if(grpven) where += where ? ` AND GRPVEN = '${grpven}'` : `WHERE GRPVEN = '${grpven}'`;
    try {
        var db = Ti.Database.open('somaCRMDB');
        var result = db.execute(`SELECT DISTINCT ` +
            `CODCLI, GRPVEN, CONDPG, TIPO, COND, DESCRI, ACRSVEN, DESCVEN FROM condPag ${where}`
        );
        while(result.isValidRow()) {
            condPag.push({
                CODCLI  : result.fieldByName('CODCLI'),
                GRPVEN  : result.fieldByName('GRPVEN'),
                CONDPG  : result.fieldByName('CONDPG'),
                TIPO    : result.fieldByName('TIPO'),
                COND    : result.fieldByName('COND'),
                DESCRI  : result.fieldByName('DESCRI'),
                ACRSVEN : result.fieldByName('ACRSVEN'),
                DESCVEN : result.fieldByName('DESCVEN')
            });
            result.next();
        }
        result.close();
        db.close();
        return condPag;
    }
    catch(err) {
        return err;
    }
}

//CRIA TABELA DE PREÇOS PARA SINCRONIZAR
exports.createTableProd = function(){
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`DROP TABLE IF EXISTS produto`);
    db.execute(`CREATE TABLE IF NOT EXISTS produto (` + 
    `ID INTEGER PRIMARY KEY, ` + 
    `CODTAB TEXT, ` + 
    `CATEGO TEXT, ` + 
    `DESCCAT TEXT, ` + 
    `CODPRO TEXT, ` + 
    `DESCR TEXT, ` + 
    `UN TEXT, ` + 
    `PESO REAL, ` +
    `COMIS REAL, ` +
    `ARMZ TEXT, ` +
    `PRVEN REAL, ` +
    `GRUPO TEXT, ` +
    `DA1_ESTADO TEXT, ` +
    `B1_IPI REAL, ` +
    `B1_VLR_ICM REAL, ` +
    `ALIQIN REAL, ` +
    `ALIQEX REAL, ` +
    `MVA REAL)`);
    db.close();  
}

exports.insertProds = function(array){
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute('BEGIN');
        for(var i in array){
            db.execute(
                `INSERT INTO produto` +
                `(CODTAB, ` + 
                `CATEGO, ` + 
                `DESCCAT, ` + 
                `CODPRO, ` + 
                `DESCR, ` + 
                `UN, ` + 
                `PESO, ` +
                `COMIS, ` +
                `ARMZ, ` +
                `PRVEN, ` +
                `GRUPO, ` +
                `DA1_ESTADO, ` +
                `B1_IPI, ` +
                `B1_VLR_ICM, ` +
                `ALIQIN, ` +
                `ALIQEX, ` +
                `MVA) ` +
                `VALUES ` +
                `(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) `,
                array[i].CODTAB, array[i].CATEGO, array[i].DESCCAT, array[i].CODPRO, array[i].DESCR, 
                array[i].UN, array[i].PESO, array[i].COMIS, 
                array[i].ARMZ, array[i].PRVEN, array[i].GRUPO, 
                array[i].DA1_ESTADO, array[i].B1_IPI, array[i].B1_VLR_ICM, 
                array[i].ALIQIN, array[i].ALIQEX, array[i].MVA
            );
        }
        db.execute('COMMIT');
        db.close();
    }
    catch(err){
        return err;
    }
}

exports.getProds = function(params) {
    var where = '';
    var prods = [];
    var CODTAB  = params.CODTAB || null;
    var CODPRO = params.CODPRO || null;

    if(CODTAB) where = `WHERE CODTAB = '${CODTAB}'`;
    if(CODPRO) where += where? ` AND CODPRO = '${CODPRO}'` : `WHERE CODPRO = '${CODPRO}'`;

    try {
        var db = Ti.Database.open('somaCRMDB');
        var result = db.execute(`SELECT ` +
            `CODTAB, ` + 
            `CATEGO, ` + 
            `DESCCAT, ` + 
            `CODPRO, ` + 
            `DESCR, ` + 
            `UN, ` + 
            `PESO, ` +
            `COMIS, ` +
            `ARMZ, ` +
            `PRVEN, ` +
            `GRUPO, ` +
            `DA1_ESTADO, ` +
            `B1_IPI, ` +
            `B1_VLR_ICM, ` +
            `ALIQIN, ` +
            `ALIQEX, ` +
            `MVA FROM produto ${where}`
        );
        while(result.isValidRow()) {
            prods.push({
                CODTAB      : result.fieldByName('CODTAB'),
                CATEGO      : result.fieldByName('CATEGO'),
                DESCCAT     : result.fieldByName('DESCCAT'),
                CODPRO      : result.fieldByName('CODPRO'),
                DESCR       : result.fieldByName('DESCR'),
                UN          : result.fieldByName('UN'),
                PESO        : result.fieldByName('PESO'),
                COMIS       : result.fieldByName('COMIS'),
                ARMZ        : result.fieldByName('ARMZ'),
                PRVEN       : result.fieldByName('PRVEN'),
                GRUPO       : result.fieldByName('GRUPO'),
                DA1_ESTADO  : result.fieldByName('DA1_ESTADO'),
                B1_IPI      : result.fieldByName('B1_IPI'),
                B1_VLR_ICM  : result.fieldByName('B1_VLR_ICM'),
                ALIQIN      : result.fieldByName('ALIQIN'),
                ALIQEX      : result.fieldByName('ALIQEX'),
                MVA         : result.fieldByName('MVA')
            });
            result.next();
        }
        result.close();
        db.close();
        return prods;
    }
    catch(err) {
        return err;
    }
}

//CRIA TABELA PARA CADASTRAR CABECALHO DO PEDIDO
exports.createTableCabecalho = function() {
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`CREATE TABLE IF NOT EXISTS cabecalho (`+
    `CJ_FILIAL TEXT, CJ_NUM TEXT, CJ_EMISSAO TEXT, CJ_CLIENTE TEXT, CLIENTE_ID INTEGER, CJ_NOME TEXT, CJ_MUN TEXT, CJ_DDD TEXT, CJ_TEL TEXT, CJ_CGC, ` + 
    `CJ_LOJA TEXT, CJ_CLIENT TEXT, CJ_LOJAENT TEXT, CJ_CONDPAG TEXT, CJ_TABELA TEXT, CJ_DTTAB TEXT, CJ_STATUS TEXT, `+
    `CJ_VALIDA TEXT, CJ_MOEDA INTEGER, CJ_TIPLIB TEXT, CJ_TPCARGA TEXT, CJ_TXMOEDA REAL, CJ_ROTEIRO TEXT, CJ_TPFRETE TEXT, CJ_FRTBAST REAL, CJ_VEND TEXT, CJ_OBS TEXT, `+
    `CJ_PARC1 REAL, CJ_DATA1 TEXT, CJ_PARC2 REAL, CJ_DATA2 TEXT, CJ_PARC3 REAL, CJ_DATA3 TEXT, CJ_PARC4 REAL, CJ_DATA4 TEXT, CJ_PARC5 REAL, CJ_DATA5 TEXT, CJ_PARC6 REAL, `+ 
    `CJ_DATA6 TEXT, CJ_PARC7 REAL, CJ_DATA7 TEXT, CJ_PARC8 REAL, CJ_DATA8 TEXT, CJ_PARC9 REAL, CJ_DATA9 TEXT, CJ_DTALT TEXT, GRPVEN TEXT, FATORCONDPG REAL, DTPREV TEXT, `+
    `ID INTEGER PRIMARY KEY, ORC_ID INTEGER, RECNO INTEGER, DTSYNC TEXT, DEL TEXT, CJ_ORC INTEGER)`);
    db.close();
}

exports.insertCabecalho = function(params) {
    var data    = params.data || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!data) return error('Cabeçalho não informado para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `INSERT INTO cabecalho ` + 
            `(CJ_FILIAL, CJ_NUM, CJ_EMISSAO, CJ_CLIENTE, CLIENTE_ID, CJ_NOME, CJ_MUN, CJ_DDD, CJ_TEL, CJ_CGC, ` +
            `CJ_LOJA, CJ_CLIENT, CJ_LOJAENT, CJ_CONDPAG, CJ_TABELA, CJ_DTTAB, CJ_STATUS, `+
            `CJ_VALIDA, CJ_MOEDA, CJ_TIPLIB, CJ_TPCARGA, CJ_TXMOEDA, CJ_ROTEIRO, CJ_TPFRETE, CJ_FRTBAST, CJ_VEND, CJ_OBS, `+
            `CJ_PARC1, CJ_DATA1, CJ_PARC2, CJ_DATA2, CJ_PARC3, CJ_DATA3, CJ_PARC4, CJ_DATA4, CJ_PARC5, CJ_DATA5, CJ_PARC6, `+ 
            `CJ_DATA6, CJ_PARC7, CJ_DATA7, CJ_PARC8, CJ_DATA8, CJ_PARC9, CJ_DATA9, CJ_DTALT, GRPVEN, FATORCONDPG, DTPREV, `+
            `ORC_ID, RECNO, DTSYNC, DEL, CJ_ORC) ` +
            `VALUES ` +
            `(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,//47
            data.CJ_FILIAL, data.CJ_NUM, data.CJ_EMISSAO, data.CJ_CLIENTE, data.CLIENTE_ID, data.CJ_NOME, data.CJ_MUN, data.CJ_DDD, data.CJ_TEL,
            data.CJ_CGC, data.CJ_LOJA, data.CJ_CLIENT, data.CJ_LOJAENT,
            data.CJ_CONDPAG, data.CJ_TABELA, data.CJ_DTTAB, data.CJ_STATUS, data.CJ_VALIDA, data.CJ_MOEDA, data.CJ_TIPLIB,
            data.CJ_TPCARGA, data.CJ_TXMOEDA, data.CJ_ROTEIRO, data.CJ_TPFRETE, data.CJ_FRTBAST, data.CJ_VEND, data.CJ_OBS,
            data.CJ_PARC1, data.CJ_DATA1, data.CJ_PARC2, data.CJ_DATA2, data.CJ_PARC3, data.CJ_DATA3, data.CJ_PARC4, data.CJ_DATA4,
            data.CJ_PARC5, data.CJ_DATA5, data.CJ_PARC6, data.CJ_DATA6, data.CJ_PARC7, data.CJ_DATA7, data.CJ_PARC8, data.CJ_DATA8,
            data.CJ_PARC9, data.CJ_DATA9, data.CJ_DTALT, data.GRPVEN, data.FATORCONDPG, data.DTPREV, data.ORC_ID, data.RECNO,
            data.DTSYNC, data.DEL, data.CJ_ORC
        );
        success({cabecalho: data.CJ_CLIENTE, id: db.lastInsertRowId});
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.updateCabecalho = function(params) {
    var data        = params.data || null;
    var id          = data.ID; 
    var success     = params.success || function(){};
    var error       = params.error || function(){};
    if(!id) return error('CABECALHO SEM ID: ', data);
    if(!data) return error('Cabeçalho não informado para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cabecalho SET ` + 
            `CJ_FILIAL = ?, CJ_NUM = ?, CJ_EMISSAO = ?, CJ_CLIENTE = ?, CLIENTE_ID = ?, CJ_NOME = ?, CJ_MUN = ?, CJ_DDD = ?, CJ_TEL = ?, CJ_CGC = ?, ` +
            `CJ_LOJA = ?, CJ_CLIENT = ?, CJ_LOJAENT = ?, CJ_CONDPAG = ?, CJ_TABELA = ?, CJ_DTTAB = ?, CJ_STATUS = ?, `+
            `CJ_VALIDA = ?, CJ_MOEDA = ?, CJ_TIPLIB = ?, CJ_TPCARGA = ?, CJ_TXMOEDA = ?, CJ_ROTEIRO = ?, CJ_TPFRETE = ?, CJ_FRTBAST = ?, CJ_VEND = ?, CJ_OBS = ?, `+
            `CJ_PARC1 = ?, CJ_DATA1 = ?, CJ_PARC2 = ?, CJ_DATA2 = ?, CJ_PARC3 = ?, CJ_DATA3 = ?, CJ_PARC4 = ?, CJ_DATA4 = ?, CJ_PARC5 = ?, CJ_DATA5 = ?, CJ_PARC6 = ?, `+ 
            `CJ_DATA6 = ?, CJ_PARC7 = ?, CJ_DATA7 = ?, CJ_PARC8 = ?, CJ_DATA8 = ?, CJ_PARC9 = ?, CJ_DATA9 = ?, CJ_DTALT = ?, GRPVEN = ?, FATORCONDPG = ?, DTPREV = ?, `+
            `ORC_ID = ?, RECNO = ?, DTSYNC = ?, CJ_ORC = ? ` +
            `WHERE ID = '${id}' `,
            data.CJ_FILIAL, data.CJ_NUM, data.CJ_EMISSAO, data.CJ_CLIENTE, data.CLIENTE_ID, data.CJ_NOME, data.CJ_MUN, data.CJ_DDD, data.CJ_TEL, 
            data.CJ_CGC, data.CJ_LOJA, data.CJ_CLIENT, data.CJ_LOJAENT, data.CJ_CONDPAG, data.CJ_TABELA, data.CJ_DTTAB, data.CJ_STATUS, 
            data.CJ_VALIDA, data.CJ_MOEDA, data.CJ_TIPLIB, data.CJ_TPCARGA, data.CJ_TXMOEDA, data.CJ_ROTEIRO, data.CJ_TPFRETE, data.CJ_FRTBAST, data.CJ_VEND, data.CJ_OBS,
            data.CJ_PARC1, data.CJ_DATA1, data.CJ_PARC2, data.CJ_DATA2, data.CJ_PARC3, data.CJ_DATA3, data.CJ_PARC4, data.CJ_DATA4, data.CJ_PARC5, data.CJ_DATA5, data.CJ_PARC6,
            data.CJ_DATA6, data.CJ_PARC7, data.CJ_DATA7, data.CJ_PARC8, data.CJ_DATA8, data.CJ_PARC9, data.CJ_DATA9, data.CJ_DTALT, data.GRPVEN, data.FATORCONDPG, data.DTPREV, 
            data.ORC_ID, data.RECNO, data.DTSYNC, data.CJ_ORC
        );
        success(id);
        db.close();
    }
    catch(err){
        error(err);
    }
}


exports.updateNumCabecalho = function(params) {
    var data      = params.data || null;
    var id_antigo = data.id_cabLocal;
    var CJ_NUM    = data.CJ_NUM || null;
    var success   = params.success || function(){};
    var error     = params.error || function(){};
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE cabecalho SET CJ_NUM = ? WHERE ID = '${id_antigo}'`,
            CJ_NUM
        );
        success();
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.countPedidos = function(params){
    var success  = params.success || function(){};
    var error    = params.error || function(){};
    var count    = 0;
    var db = Ti.Database.open('somaCRMDB');
    try {
        var result = db.execute(`SELECT COUNT(*) AS QNT FROM cabecalho`);
        count = result.fieldByName('QNT');
        result.close();
        db.close();
        success(count);
    }
    catch(err) {
        error(err);
    }
}

exports.getCabecalho = function(params) {
    var id      = params.cab_id || null;
    var cli_id  = params.cli_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    var where   = '';
    if(id) where = `WHERE ID = '${id}'`;
    else if(cli_id) where = `WHERE CLIENTE_ID = ${cli_id}`
    var cabecalho    = [];
    try {
        var db      = Ti.Database.open('somaCRMDB');
        var result  = db.execute(`SELECT ID, CJ_FILIAL, CJ_NUM, CJ_EMISSAO, CJ_CLIENTE, CLIENTE_ID, CJ_NOME, CJ_MUN, CJ_DDD, CJ_TEL, CJ_CGC, ` +
                                `CJ_LOJA, CJ_CLIENT, CJ_LOJAENT, CJ_CONDPAG, CJ_TABELA, ` +
                                `CJ_DTTAB, CJ_STATUS, CJ_VALIDA, CJ_MOEDA, CJ_TIPLIB, CJ_TPCARGA, CJ_TXMOEDA, CJ_ROTEIRO, CJ_TPFRETE, CJ_FRTBAST, ` + 
                                `CJ_VEND, CJ_OBS, CJ_PARC1, CJ_DATA1, CJ_PARC2, CJ_DATA2, CJ_PARC3, CJ_DATA3, CJ_PARC4, CJ_DATA4, CJ_PARC5, CJ_DATA5, ` +
                                `CJ_PARC6, CJ_DATA6, CJ_PARC7, CJ_DATA7, CJ_PARC8, CJ_DATA8, CJ_PARC9, CJ_DATA9, CJ_DTALT, GRPVEN, FATORCONDPG, DTPREV, ` +
                                `ORC_ID, RECNO, DTSYNC, DEL, CJ_ORC FROM cabecalho ${where}`);
        
        while(result.isValidRow()) {
            cabecalho.push({
                ID          : result.fieldByName('ID'),
                CJ_FILIAL   : result.fieldByName('CJ_FILIAL'),
                CJ_NUM      : result.fieldByName('CJ_NUM'),
                CJ_EMISSAO  : result.fieldByName('CJ_EMISSAO'),
                CJ_CLIENTE  : result.fieldByName('CJ_CLIENTE'),
                CLIENTE_ID  : result.fieldByName('CLIENTE_ID'),
                CJ_NOME     : result.fieldByName('CJ_NOME'),
                CJ_MUN      : result.fieldByName('CJ_MUN'),
                CJ_DDD      : result.fieldByName('CJ_DDD'),
                CJ_TEL      : result.fieldByName('CJ_TEL'),
                CJ_CGC      : result.fieldByName('CJ_CGC'),
                CJ_LOJA     : result.fieldByName('CJ_LOJA'),
                CJ_CLIENT   : result.fieldByName('CJ_CLIENT'),
                CJ_LOJAENT  : result.fieldByName('CJ_LOJAENT'),
                CJ_CONDPAG  : result.fieldByName('CJ_CONDPAG'),
                CJ_TABELA   : result.fieldByName('CJ_TABELA'),
                CJ_DTTAB    : result.fieldByName('CJ_DTTAB'),
                CJ_STATUS   : result.fieldByName('CJ_STATUS'),
                CJ_VALIDA   : result.fieldByName('CJ_VALIDA'),
                CJ_MOEDA    : result.fieldByName('CJ_MOEDA'),
                CJ_TIPLIB   : result.fieldByName('CJ_TIPLIB'),
                CJ_TPCARGA  : result.fieldByName('CJ_TPCARGA'),
                CJ_TXMOEDA  : result.fieldByName('CJ_TXMOEDA'),
                CJ_ROTEIRO  : result.fieldByName('CJ_ROTEIRO'),
                CJ_TPFRETE  : result.fieldByName('CJ_TPFRETE'),
                CJ_FRTBAST  : result.fieldByName('CJ_FRTBAST'),
                CJ_VEND     : result.fieldByName('CJ_VEND'),
                CJ_OBS      : result.fieldByName('CJ_OBS'),
                CJ_PARC1    : result.fieldByName('CJ_PARC1'),
                CJ_DATA1    : result.fieldByName('CJ_DATA1'),
                CJ_PARC2    : result.fieldByName('CJ_PARC2'),
                CJ_DATA2    : result.fieldByName('CJ_DATA2'),
                CJ_PARC3    : result.fieldByName('CJ_PARC3'),
                CJ_DATA3    : result.fieldByName('CJ_DATA3'),
                CJ_PARC4    : result.fieldByName('CJ_PARC4'),
                CJ_DATA4    : result.fieldByName('CJ_DATA4'),
                CJ_PARC5    : result.fieldByName('CJ_PARC5'),
                CJ_DATA5    : result.fieldByName('CJ_DATA5'),
                CJ_PARC6    : result.fieldByName('CJ_PARC6'),
                CJ_DATA6    : result.fieldByName('CJ_DATA6'),
                CJ_PARC7    : result.fieldByName('CJ_PARC7'),
                CJ_DATA7    : result.fieldByName('CJ_DATA7'),
                CJ_PARC8    : result.fieldByName('CJ_PARC8'),
                CJ_DATA8    : result.fieldByName('CJ_DATA8'),
                CJ_PARC9    : result.fieldByName('CJ_PARC9'),
                CJ_DATA9    : result.fieldByName('CJ_DATA9'),
                CJ_DTALT    : result.fieldByName('CJ_DTALT'),
                GRPVEN      : result.fieldByName('GRPVEN'),
                FATORCONDPG : result.fieldByName('FATORCONDPG'),
                DTPREV      : result.fieldByName('DTPREV'),
                ID          : result.fieldByName('ID'),
                ORC_ID      : result.fieldByName('ORC_ID'),
                RECNO       : result.fieldByName('RECNO'),
                DTSYNC      : result.fieldByName('DTSYNC'),
                DEL         : result.fieldByName('DEL'),
                CJ_ORC      : result.fieldByName('CJ_ORC')
            });
            result.next();
        }
        result.close();
        db.close();
        success(cabecalho)
    }
    catch(err){
        error(err);
    }
}

exports.deleteCabecalho = function(params){
    var id      = params.cab_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('ID do cabecalho inválida');
    var where   = `WHERE ID = '${id}'`;
    var db      = Ti.Database.open('somaCRMDB');
    db.execute(`DELETE FROM cabecalho ${where}`)
    db.close();
}

//CRIA TABELA PARA CADASTRAR ITENS DO PEDIDO
exports.createTableItensPed = function() {
    var db = Ti.Database.open('somaCRMDB');
    db.execute(`CREATE TABLE IF NOT EXISTS itensPed (`+
    `ID INTEGER PRIMARY KEY, CK_NUM TEXT, CK_FILIAL TEXT, CK_ITEM TEXT, CK_PRODUTO TEXT, CK_DESCRI TEXT, CK_UM TEXT, CK_LOCAL TEXT, PRCTAB REAL, ` +
    `CK_PRCVEN REAL, CK_PRUNIT REAL, CK_COMIS1 REAL, CALCIPI REAL, CALCST REAL, CK_QTDVEN INTEGER, CK_DESCONT REAL, CK_VALOR REAL, ` +
    `CK_CLIENTE TEXT, CK_LOJA TEXT, CK_ENTREG TEXT, CK_FILVEN TEXT, CK_FILENT TEXT, CK_DT1VEN TEXT, CK_YOPER TEXT, CK_DTALT TEXT, ` +
    `GRUPO TEXT, PESOUNI REAL, TPESO REAL, TVLRFRT REAL, FRTPROD REAL, CJID INTEGER, RECNO INTEGER, DTSYNC TEXT, CK_COTCLI TEXT)`);
    db.close();
}

exports.insertItemPed = function(params) {
    var data    = params.data || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!data) return error('Cabeçalho não informado para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `INSERT INTO itensPed` + 
            `(CK_NUM, CK_FILIAL, CK_ITEM, CK_PRODUTO, CK_DESCRI, CK_UM, CK_LOCAL, PRCTAB, CK_PRCVEN, CK_PRUNIT, ` +
            `CK_COMIS1, CALCIPI, CALCST, CK_QTDVEN, CK_DESCONT, CK_VALOR, CK_CLIENTE, CK_LOJA, CK_ENTREG, CK_FILVEN, ` + 
            `CK_FILENT, CK_DT1VEN, CK_YOPER, CK_DTALT, GRUPO, PESOUNI, TPESO, TVLRFRT, FRTPROD, CJID, RECNO, DTSYNC, CK_COTCLI) ` +
            `VALUES ` +
            `(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ` +
            `?, ?, ?, ?, ?, ?, ?)`,//34
            data.CK_NUM, data.CK_FILIAL, data.CK_ITEM, data.CK_PRODUTO, data.CK_DESCRI, data.CK_UM, data.CK_LOCAL, data.PRCTAB, data.CK_PRCVEN, data.CK_PRUNIT,
            data.CK_COMIS1, data.CALCIPI, data.CALCST, data.CK_QTDVEN, data.CK_DESCONT, data.CK_VALOR, data.CK_CLIENTE, data.CK_LOJA, data.CK_ENTREG, data.CK_FILVEN, 
            data.CK_FILENT, data.CK_DT1VEN, data.CK_YOPER, data.CK_DTALT, data.GRUPO, data.PESOUNI, data.TPESO, data.TVLRFRT, data.FRTPROD, data.CJID, data.RECNO, data.DTSYNC, data.CK_COTCLI
        );
        success({item: data.CK_DESCRI, id: db.lastInsertRowId});
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.updateItemPed = function(params) {
    var data        = params.data || null;
    var id          = data.ID;
    var id_prod     = data.ID_PROD;
    var where       = '';
    var success     = params.success || function(){};
    var error       = params.error || function(){};
    if(id) {
        where = `ID = '${id}'`;
    }
    else if(id_prod){
        where = `ID = '${id_prod}'`;
    }
    else return error('ITEM SEM ID: ', data);
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE itensPed SET ` + 
            `CK_NUM = ?, CK_FILIAL = ?, CK_ITEM = ?, CK_PRODUTO = ?, CK_DESCRI = ?, CK_UM = ?, CK_LOCAL = ?, PRCTAB = ?, CK_PRCVEN = ?, CK_PRUNIT = ?, ` +
            `CK_COMIS1 = ?, CALCIPI = ?, CALCST = ?, CK_QTDVEN = ?, CK_DESCONT = ?, CK_VALOR = ?, CK_CLIENTE = ?, CK_LOJA = ?, CK_ENTREG = ?, CK_FILVEN = ?, ` + 
            `CK_FILENT = ?, CK_DT1VEN = ?, CK_YOPER = ?, CK_DTALT = ?, GRUPO = ?, PESOUNI = ?, TPESO = ?, TVLRFRT = ?, FRTPROD = ?, CJID = ?, RECNO = ?, DTSYNC = ?, CK_COTCLI = ? ` +
            `WHERE ${where} `,
            data.CK_NUM, data.CK_FILIAL, data.CK_ITEM, data.CK_PRODUTO, data.CK_DESCRI, data.CK_UM, data.CK_LOCAL, data.PRCTAB, data.CK_PRCVEN, data.CK_PRUNIT,
            data.CK_COMIS1, data.CALCIPI, data.CALCST, data.CK_QTDVEN, data.CK_DESCONT, data.CK_VALOR, data.CK_CLIENTE, data.CK_LOJA, data.CK_ENTREG, data.CK_FILVEN, 
            data.CK_FILENT, data.CK_DT1VEN, data.CK_YOPER, data.CK_DTALT, data.GRUPO, data.PESOUNI, data.TPESO, data.TVLRFRT, data.FRTPROD, data.ID, data.RECNO, data.DTSYNC, data.CK_COTCLI
        );
        success(data);
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.setNumProds = function(params) {
    var CK_NUM      = params.CK_NUM || null;
    var id_cabLocal = params.id_cabLocal || null; 
    var CJID        = params.CJID || null; 
    var success     = params.success || function(){};
    var error       = params.error || function(){};
    if(!CK_NUM) return error('CK_NUM não informado para cadastrar');
    try {
        var db = Ti.Database.open('somaCRMDB');
        db.execute(
            `UPDATE itensPed SET CK_NUM = ?, CJID = ? WHERE CK_NUM = '${id_cabLocal}' `,
            CK_NUM, CJID
        );
        success();
        db.close();
    }
    catch(err){
        error(err);
    }
}

exports.getItensPed = function(params) {
    var id      = params.cab_id || null;
    var ck_num  = params.ck_num || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    var where   = '';
    var itens   = [];

    if(id){
        where = `WHERE ID = ${id}`;
    }
    else if (ck_num) {
        where = `WHERE CK_NUM = ${ck_num}`;
    }
    try {
        var db      = Ti.Database.open('somaCRMDB');
        var result  = db.execute(`SELECT ID, CK_NUM, CK_FILIAL, CK_ITEM, CK_PRODUTO, CK_DESCRI, CK_UM, CK_LOCAL, PRCTAB, CK_PRCVEN, CK_PRUNIT, ` +
                                `CK_COMIS1, CALCIPI, CALCST, CK_QTDVEN, CK_DESCONT, CK_VALOR, CK_CLIENTE, CK_LOJA, CK_ENTREG, CK_FILVEN, ` + 
                                `CK_FILENT, CK_DT1VEN, CK_YOPER, CK_DTALT, GRUPO, PESOUNI, TPESO, TVLRFRT, FRTPROD, CJID, RECNO, DTSYNC, CK_COTCLI FROM itensPed ${where}`);
        while(result.isValidRow()) {
            itens.push({
                ID          : result.fieldByName('ID'),
                CK_NUM      : result.fieldByName('CK_NUM'),
                CK_FILIAL   : result.fieldByName('CK_FILIAL'),
                CK_ITEM     : result.fieldByName('CK_ITEM'),
                CK_PRODUTO  : result.fieldByName('CK_PRODUTO'),
                CK_DESCRI   : result.fieldByName('CK_DESCRI'),
                CK_UM       : result.fieldByName('CK_UM'),
                CK_LOCAL    : result.fieldByName('CK_LOCAL'),
                PRCTAB      : result.fieldByName('PRCTAB'),
                CK_PRCVEN   : result.fieldByName('CK_PRCVEN'),
                CK_PRUNIT   : result.fieldByName('CK_PRUNIT'),
                CK_COMIS1   : result.fieldByName('CK_COMIS1'),
                CALCIPI     : result.fieldByName('CALCIPI'),
                CALCST      : result.fieldByName('CALCST'),
                CK_QTDVEN   : result.fieldByName('CK_QTDVEN'),
                CK_DESCONT  : result.fieldByName('CK_DESCONT'),
                CK_VALOR    : result.fieldByName('CK_VALOR'),
                CK_CLIENTE  : result.fieldByName('CK_CLIENTE'),
                CK_LOJA     : result.fieldByName('CK_LOJA'),
                CK_ENTREG   : result.fieldByName('CK_ENTREG'),
                CK_FILVEN   : result.fieldByName('CK_FILVEN'),
                CK_FILENT   : result.fieldByName('CK_FILENT'),
                CK_DT1VEN   : result.fieldByName('CK_DT1VEN'),
                CK_YOPER    : result.fieldByName('CK_YOPER'),
                CK_DTALT    : result.fieldByName('CK_DTALT'),
                GRUPO       : result.fieldByName('GRUPO'),
                PESOUNI     : result.fieldByName('PESOUNI'),
                TPESO       : result.fieldByName('TPESO'),
                TVLRFRT     : result.fieldByName('TVLRFRT'),
                FRTPROD     : result.fieldByName('FRTPROD'),
                CJID        : result.fieldByName('CJID'),
                RECNO       : result.fieldByName('RECNO'),
                DTSYNC      : result.fieldByName('DTSYNC'),
                CK_COTCLI   : result.fieldByName('CK_COTCLI')
            });
            result.next();
        }
        result.close();
        db.close();
        success(itens)
    }
    catch(err){
        error(err);
    }
}

exports.deleteItensPed = function(params){
    var id      = params.cab_id || null;
    var success = params.success || function(){};
    var error   = params.error || function(){};
    if(!id) return error('ID do item inválido');
    var where   = `WHERE ID = '${id}'`;
    var db      = Ti.Database.open('somaCRMDB');
    db.execute(`DELETE FROM itensPed ${where}`)
    db.close();
}



//APAGA OS DADODS DE UMA DETERMINADA TABELA PASSADA POR PARAMETRO
exports.clearTable = function(params) {
    var table = params.table;
    var where = params.where || '';
    var db    = Ti.Database.open('somaCRMDB');
    try{
        db.execute(`DELETE FROM '${table}' ${where}`);
        db.close();
    }
    catch(err) {
    }
}
//EXCLUI UMA DETERMINADA TABELA PASSADA POR PARAMETRO
exports.dropTable = function(params) {
    var table = params.table;
    var db    = Ti.Database.open('somaCRMDB');
    try{
        db.execute(`DROP TABLE IF EXISTS ${table}`);
        db.close();
    }
    catch(err) {
    }
}