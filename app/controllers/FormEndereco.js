// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var ClientManager 	  = require('ClientManager');
var mask 		 	  = require('Mask');
var customGeolocation = require("CustomGeolocation");
var args 		 = $.args;
var new_end 	 = {};
var cobranca 	 = args.cobranca 	 || false;
var callback 	 = args.callback  	 || function(){};
var end_entrega  = args.end_entrega || null;
var end_cobranca = args.end_cobranca || null;
var end_ent		 = ClientManager.getEndereco();

function doClose(e){
	$.mainWindow.close();
}

function doSave(e){
	$.activityIndicator.show();
	var message = validate();
	if(message){
		Alloy.Globals.showAlert("Ops!","É necessário informar o "+message+"!");
		$.activityIndicator.hide();
		return false;
    }
	getValues();
}

function copyValues(end){
	$.tfLogradouro.value	= end.A1_END
	$.lbUF.text				= end.A1_EST
	$.lbMunicipio.text		= end.A1_MUN
	$.tfBairro.value		= end.A1_BAIRRO
	$.tfCEP.value			= end.A1_CEP
	$.lbUF.setColor('black');
	$.lbMunicipio.setColor('black');
	new_end.CODC_UF = end.COD_UF;
}

function setValues(){
	if(cobranca && !end_cobranca) {
		var endereco = ClientManager.getEnderecoCobranca();
		$.tfLogradouro.value 	= endereco.A1_ENDCOB;
		$.tfBairro.value 		= endereco.A1_BAIRROC;
		$.lbUF.text 			= endereco.A1_ESTC || 'UF';
		$.lbMunicipio.text		= endereco.A1_MUNC || 'Município';
		$.tfCEP.value 			= endereco.A1_CEPC;
		new_end.CODC_UF 		= endereco.CODC_UF;
		$.viewGeo.setHeight('0dp');
		$.viewGeo.setVisible(false);
		$.lbUF.setColor(!endereco.A1_ESTC ? 'gray' : 'black');
		$.lbMunicipio.setColor(!endereco.A1_MUNC ? 'gray' : 'black');
	}
	else if(!cobranca && !end_cobranca && !end_entrega) {
		var endereco = ClientManager.getEndereco();
		$.tfLogradouro.value	= endereco.A1_END;
		$.tfComplemento.value	= endereco.A1_COMPLEM;
		$.lbUF.text				= endereco.A1_EST || 'UF';
		$.lbMunicipio.text		= endereco.A1_MUN || 'Município';
		$.tfBairro.value		= endereco.A1_BAIRRO;
		$.tfCEP.value			= endereco.A1_CEP;
		$.tfReferencia.value	= endereco.A1_INFOROT;
		new_end.COD_UF 			= endereco.COD_UF;
		$.viewGeo.setHeight('0dp');
		$.viewGeo.setVisible(false);
		$.lbUF.setColor(!endereco.A1_EST ? 'gray' : 'black');
		$.lbMunicipio.setColor(!endereco.A1_MUN ? 'gray' : 'black');
	}
	else if(end_cobranca) {
		$.tfLogradouro.value 	= end_cobranca.A1_ENDCOB;
		$.tfBairro.value 		= end_cobranca.A1_BAIRROC;
		$.lbUF.text 			= end_cobranca.A1_ESTC || 'UF';
		$.lbMunicipio.text		= end_cobranca.A1_MUNC || 'Município';
		$.tfCEP.value 			= end_cobranca.A1_CEPC;
		$.viewGeo.setHeight('0dp');
		$.viewGeo.setVisible(false);
		$.lbUF.setColor(!end_cobranca.A1_ESTC ? 'gray' : 'black');
		$.lbMunicipio.setColor(!end_cobranca.A1_MUNC ? 'gray' : 'black');
	}
	else if(end_entrega) {
		$.tfLogradouro.value	= end_entrega.A1_END;
		$.tfComplemento.value	= end_entrega.A1_COMPLEM;
		$.lbUF.text				= end_entrega.A1_EST || 'UF';
		$.lbMunicipio.text		= end_entrega.A1_MUN || 'Município';
		$.tfBairro.value		= end_entrega.A1_BAIRRO;
		$.tfCEP.value			= end_entrega.A1_CEP;
		$.tfReferencia.value	= end_entrega.A1_INFOROT;
		$.lbUF.setColor(!end_entrega.A1_EST ? 'gray' : 'black');
		$.lbMunicipio.setColor(!end_entrega.A1_MUN ? 'gray' : 'black');
		if(end_entrega.A1_LAT && end_entrega.A1_LAT != '' && end_entrega.A1_LONG && end_entrega.A1_LONG != '') {
			$.tfGeo.text = 'Clique para visualizar no mapa';
		}
		else {
			$.viewGeo.setHeight('0dp');
			$.viewGeo.setVisible(false);
		}
	}
}

function createViewGeolocalizacao(lat,long){
	if(lat && long){
		var coord = lat + ',' + long;
		var url_google = "http://maps.apple.com/?q=" + coord + "&z=14&t=k";
		var url_waze   = "https://waze.com/ul?q=" + coord + "&navigate=yes&zoom=17";
		var dialog = Ti.UI.createAlertDialog({
            title: "Mapa",
            message: "Deseja abrir a localização por qual aplicativo?",
            buttonNames: ["Google Maps","Cancelar", "Waze"]
        });
        dialog.addEventListener("click", function (e) {
            if (e.index == 0) result = Ti.Platform.openURL(url_google);
            else if (e.index == 2) result = Ti.Platform.openURL(url_waze);
            else return false;
		});
		dialog.show();
	}
}

function getValues(){
	new_end.logradouro 	= mask.removeCaracter($.tfLogradouro.value).toUpperCase();
	new_end.complemento = mask.removeCaracter($.tfComplemento.value || '').toUpperCase();
	new_end.uf 			= mask.removeCaracter($.lbUF.text).toUpperCase();
	new_end.municipio	= mask.removeCaracter($.lbMunicipio.text).toUpperCase();
	new_end.bairro 		= mask.removeCaracter($.tfBairro.value).toUpperCase();
	new_end.cep			= mask.removeCepMask($.tfCEP.value).toUpperCase();
	new_end.referencia 	= mask.removeCaracter($.tfReferencia.value || '').toUpperCase();
	if(cobranca) {
		ClientManager.setEnderecoCobranca(new_end);
		callback(ClientManager.getEnderecoCobranca());
		$.activityIndicator.hide();
		doClose();
	}
	else {
		customGeolocation.getCoordinates(function(coords){
			if(coords) {
				if(!new_end.CODMUN) {
					$.lbMunicipio.text = 'Município';
					$.lbMunicipio.setColor('gray');
					$.activityIndicator.hide();
					return Alloy.Globals.showAlert('Ops!', 'É necessário selecionar a cidade novamente!');
				}
				new_end.latitude  = coords.latitude;
				new_end.longitude = coords.longitude;
				ClientManager.setEndereco(new_end);
				callback(ClientManager.getEndereco());
				$.activityIndicator.hide();
				doClose();
			}
			else {
				Alloy.Globals.showAlert('Ops!', 'É necessário permitir a geolocalização para cadastrar o cliente!');
				$.activityIndicator.hide();
			}
		});
	}
}

function validate(){
	for(var i in fields){
		if(!fields[i].field.getValue() || mask.removeCaracter(fields[i].field.getValue().trim()) == "") return fields[i].name;
	}
	if($.lbUF.text == 'UF') return 'UF';
	if($.lbMunicipio.text == 'Município') return 'Município';
	return null;
}

function createMenuAndroid(){
	if(OS_ANDROID) {
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", doSave);
		};
		activity.invalidateOptionsMenu();
	}
}

function activeCopy(bool) {
	if(bool && end_ent.A1_END) {
		$.copiarEndereco.setVisible(true);
		$.copiarEndereco.setOpacity(1);
		$.copiarEndereco.setTop('20dp');
		$.copiarEndereco.setHeight(Ti.UI.SIZE);
	}
	else if(bool && !end_ent.A1_END) {
		$.copiarEndereco.setVisible(true);
		$.copiarEndereco.setOpacity(0.5);
		$.copiarEndereco.setTop('20dp');
		$.copiarEndereco.setHeight(Ti.UI.SIZE);
	}
	else {
		$.copiarEndereco.setVisible(false);
		$.copiarEndereco.setTop('0dp');
		$.copiarEndereco.setHeight('0dp');
	}
}

function getMaps(q1) { //OK
	var url = "http://maps.google.com?q=" + q1 + "&zoom=14";
	result = Ti.Platform.openURL(url);    
}

function setEditable(bool) {
	$.tfLogradouro.setEditable(bool);
	$.tfComplemento.setEditable(bool);
	$.tfBairro.setEditable(bool);
	$.tfCEP.setEditable(bool);
	$.tfReferencia.setEditable(bool);
}

var fields = [];
fields.push({field : $.tfLogradouro, name : "Rua, Avenida, Praça, etc..."});
fields.push({field : $.tfBairro, name : "Bairro"});
fields.push({field : $.tfCEP, name : "CEP"});

$.tfCEP.addEventListener('change', function(e){
	var v = mask.cep(e.value);
    if ($.tfCEP.getValue() != v) $.tfCEP.setValue(v);
});

$.copiarEndereco.addEventListener('click', function(e){
	if(end_ent.A1_END) copyValues(end_ent);
});

$.viewGeo.addEventListener('click', function(e){
	createViewGeolocalizacao(end_entrega.A1_LAT,end_entrega.A1_LONG);
});

$.viewUF.addEventListener('click', function(e){
	if(end_entrega || end_cobranca) return false;
	Alloy.Globals.openWindow('CadCliente/ListUF', {
		callback: function(uf){
			$.lbUF.text = mask.removeCaracter(uf.UF_SIGLA).toUpperCase();
			$.lbMunicipio.text = 'Município';
			new_end.MNC_CODMC = null;
			$.lbUF.setColor('black');
			$.lbMunicipio.setColor('gray');
			if(cobranca) new_end.CODC_UF = uf.UF_COD;
			else new_end.COD_UF = uf.UF_COD;
			
		}
	});
});

$.viewCidade.addEventListener('click', function(e){
	if(end_entrega || end_cobranca) return false;
	else if(!new_end.COD_UF && !new_end.CODC_UF) return Alloy.Globals.showAlert("Ops!","É necessário informar o UF para selecionar uma cidade!");
	Alloy.Globals.openWindow('CadCliente/ListCity', {
		uf_id: cobranca ? new_end.CODC_UF : new_end.COD_UF,
		callback: function(cidade){
			$.lbMunicipio.text = mask.removeCaracter(cidade.MNC_MUN).toUpperCase();
			$.lbMunicipio.setColor('black');
			new_end.CODMUN = cidade.MNC_CODMC;
		}
	});
});

$.viewReferencia.setVisible(!cobranca);
$.viewComplemento.setVisible(!cobranca);
$.viewComplemento.setHeight(cobranca ? '0dp' : '45dp');


$.mainWindow.addEventListener("open",function(e){
	if(!end_entrega && !end_cobranca){
		setValues();
		activeCopy(cobranca);
		setEditable(true);
		createMenuAndroid();
	}
	else {
		activeCopy(false);
		setValues();
		setEditable(false);
	}
});