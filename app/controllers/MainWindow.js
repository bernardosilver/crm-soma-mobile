// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var LocalData = require('LocalData');
var moment    = require('alloy/moment');
var usuario   = Alloy.Globals.getLocalDataUsuario();;
var menu      = null;
var actionBar = null;

function createMenu() {
    var leftView  =  Alloy.createController('Menu').getView(); //$.getView('teste').__views.teste;
    // var centerView = Ti.UI.createView({ backgroundColor:'yellow' });
    // var rightView = Ti.UI.createView({ backgroundColor:'orange' });

    var drawer = Ti.UI.Android.createDrawerLayout({
        leftView: leftView,
        // centerView: centerView,
        // rightView: rightView
    });
    // var btn = Ti.UI.createButton({ title: 'RIGHT' });

    // btn.addEventListener('click', function() {
    //     drawer.toggleRight();
    // });

    // centerView.add(btn);
    return drawer;
}

function doClose() {
    $.mainWindow.close();
}

function checkSync() {
    var dtSync  = Alloy.Globals.getSync();
    var dtAtual = moment().hours(0).minutes(0).seconds(0).milliseconds(0);
    var diffDay = dtSync ? dtAtual.diff(moment(dtSync),'days') : null;
    var ultSync = moment(dtSync);
    if(!dtSync || (dtSync && diffDay >= 1)) {
        Alloy.Globals.openWindow("Sync", {download: true});
    }
    countCadCli();
    countPedidos();
}

function countCadCli() {
    LocalData.countCadCli({
        success	: function(count){
            if(count == 0){
                $.labelCli.setText('Não existem clientes para serem enviados!');
            }
            if(count > 0) {
                Alloy.Globals.setCountCadCli(count);
                $.labelCli.setText(`Atenção! Existe(m) ${count} cliente(s) para ser(em) enviado(s)!`);
           }
        },
        error	: function(err){
            console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
        }
    });
}

function countPedidos() {
    LocalData.countPedidos({
        success	: function(count){
            if(count == 0){
                $.labelPed.setText('Não existem pedidos para serem enviados!');
            }
            if(count > 0) {
                Alloy.Globals.setCountCadCli(count);
                $.labelPed.setText(`Atenção! Existe(m) ${count} pedido(s) para ser(em) enviado(s)!`);
           }
        },
        error	: function(err){
            console.log('DEU ERRADO AO CONTAR PEDIDOS: ', err);
        }
    });
}

$.mainWindow.open();

$.mainWindow.addEventListener('focus', function(){
    usuario = Alloy.Globals.getLocalDataUsuario();
    if (!usuario){
        doClose();
        Alloy.Globals.openWindow("Login", { navigation: true });
    }
    var activity = $.mainWindow.getActivity(),
        actionBar = activity.getActionBar();

    if (actionBar) {
        actionBar.displayHomeAsUp = true;
       
    }
    if(menu) {
        $.mainWindow.remove(menu);
    }
    if(usuario) {
        menu = createMenu();
        $.mainWindow.add(menu);
        actionBar.onHomeIconItemSelected = function() {
            menu.toggleLeft();
        };
    }
    checkSync();
});

$.mainWindow.addEventListener('open', function(){
    
});