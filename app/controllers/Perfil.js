// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args         = $.args;
var mask         = require("Mask");
var sha512       = require("sha512");
var refreshData  = require("RefreshData");
var PhotoManager = require("PhotoManager");
var usuario      = Alloy.Globals.getLocalDataUsuario().data;
var refTextField = null;

function doClose() {
    $.mainWindow.close();
}

function createScrollView() {
    $.txtName.setValue(usuario.USR_NOME);
    $.txtEmail.setValue(usuario.USR_EMAIL);
    $.txtApelido.setValue(usuario.USR_LOGIN);
    $.txtPhone.setValue(mask.telefone(usuario.USR_MOBILE));
    // showPhoto(usuario);
}

function allertLogOut() {
    Alloy.Globals.showAlert({
        title: 'Deseja encerrar sua sessão?',
        buttons: ['Sim', 'Não, não fazer nada'],
        listener: function (e) {
            if (e.index == 0) {
                logout();
            }
        }
    });
}

function logout() {
    doClose();
    Alloy.Globals.removeLocalDataUsuario();
    Alloy.Globals.setSync(null);
    Alloy.Globals.openWindow("Login", { navigation: true });
}

function showPhoto(user) {
    if (user.USR_FOTO.trim()){
        $.iconProfile.setImage(user.USR_FOTO);
        $.editProfile.setText("Alterar");
        $.viewLabel.setLeft("13dp");
    }
    else{
        $.editProfile.setText("Cadastrar");
        $.viewLabel.setLeft("5dp");
    }
   
    if (OS_IOS){
        $.profile.setViewShadowRadius(4);
        $.profile.setViewShadowOffset({
            y: 3,
        });
        $.profile.setViewShadowColor("#434343");
    }
    else if(OS_ANDROID){
        $.profile.setElevation(8);
    }
    
}

function doTakePhoto() {
    PhotoManager.showOptions(function (previewImage) {
        Alloy.Globals.openWindowModal("EditPhoto", {
            previewImage    : previewImage,
            usuario         : Alloy.Globals.getLocalDataUsuario(),
            errorCallBack: function () {
                Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao enviar a foto. Verifique sua conexão e tente novamente.");
            },
            successCallBack : function(){
                setInfo();
            }
        });
    });
}

function salvar () {
    if(!Ti.Network.online) return Alloy.Globals.showAlert("Erro!", "É necessário estar conectado à internet para alterar a senha!");
    else if (!validateForm()) return Alloy.Globals.showAlert("Erro!", "Favor preencher corretamente os campos Nome, Email e Telefone!");
    else if (!validateSenha()) return Alloy.Globals.showAlert("Erro!", "Favor preencher corretamente os campos de alteração de senha!");
    else {
        updateUsuario();
    }
}

const validateForm = function() {
    var nome  = mask.removeCaracter($.txtName.getValue());
    if(!!!nome.match(/[A-Z][a-z]* [A-Z][a-z]*/) || nome.length < 4){
		return false;
    }
    else if (!validaEmail($.txtEmail.getValue())) {
		return false;
    }
    else if(mask.removeTelMask($.txtPhone.getValue()).length < 10){
		return false;
	}
    else return true;
}

const validaEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

const validateSenha = function(){
    var nova = $.txtNovaSenha.getValue();
    var confirm = $.txtRepetirSenha.getValue();
    if(!nova && !confirm) return true; 
    else if(!nova && confirm) return false;
    else if(nova && !confirm) return false;
    else if (nova != confirm) return false;
    else if (nova == confirm) return true;
    else return false;
}

function updateUsuario() {
    $.activityIndicator.show();
    // var nome    = mask.removeCaracter($.txtName.getValue()).toUpperCase();
    var email   = $.txtEmail.getValue().toLowerCase();
    var phone   = mask.removeTelMask($.txtPhone.getValue());
    var senha   = $.txtNovaSenha.getValue().trim();
    var user = {
        USR_ID: usuario.USR_ID,
        // USR_NOME: nome,
        USR_EMAIL: email,
        USR_MOBILE: phone,
        USR_PSWDAPP: senha
    }

    refreshData.updateUsuario({
        data            : user,
        successCallBack : function(response){
            Alloy.Globals.showAlert("Sucesso!","Dados atualizados!");
            refreshLocalData(user);
        },
        errorCallBack   : function(response){
            Alloy.Globals.showAlert("Erro!","Ocorreu um erro ao atualizar os dados. Tente novamente!");
            $.activityIndicator.hide();
        }
    });
    
}

function refreshLocalData(novo) {
    var usr = Alloy.Globals.getLocalDataUsuario().data;
    var token = Alloy.Globals.getLocalDataUsuario().token;
    // usr.USR_NOME    = novo.USR_NOME;
    usr.USR_EMAIL   = novo.USR_EMAIL;
    usr.USR_MOBILE  = novo.USR_MOBILE;
    Alloy.Globals.setLocalDataUsuario({data: usr, token: token});
    $.activityIndicator.hide();
    doClose();
} 

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/exit.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
            allertLogOut();
        });
    };
    activity.invalidateOptionsMenu();
}

$.txtName.setEditable(false);
$.txtApelido.setEditable(false);

$.txtPhone.addEventListener("change", function (e) {
    var v = mask.telefone(e.value);
    if ($.txtPhone.getValue() != v) $.txtPhone.setValue(v);
    $.txtPhone.setSelection($.txtPhone.getValue().length, $.txtPhone.getValue().length);
});

// $.profile.addEventListener("click", function () {
//     doTakePhoto();
// });

$.mainWindow.addEventListener("open", function (e) {
    createScrollView();
});

$.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
});