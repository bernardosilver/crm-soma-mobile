var args        = arguments[0] || {};
var RefreshData = require("RefreshData");

$.widgetInputUser.setHintText('Nome de usuário');
$.widgetInputUser.setKeyboardType('user');
$.widgetInputUser.setReturnKeyType('next');

$.widgetInputSenha.setHintText('Senha');
$.widgetInputSenha.setPasswordMask(true);
$.widgetInputSenha.setReturnKeyType('done');

$.btEntrar.addEventListener('click', function (e) {
    var data = {};
    data.login = $.widgetInputUser.getValue();
    data.senha = $.widgetInputSenha.getValue();

    $.activityIndicator.show();
    $.activityIndicator.setTouch(true);
    RefreshData.sendLogin({
        successCallBack: function (response) {
            $.activityIndicator.hide();
            var usuario = response;
            Alloy.Globals.setLocalDataUsuario(usuario);
            $.widgetInputUser.setValue("");
            $.widgetInputSenha.setValue("");
            Alloy.Globals.openWindow("MainWindow");
            doClose();
        },
        errorCallBack: function (response) {
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao fazer login. " + response.error);
            $.activityIndicator.hide();
        },
        data: data
    });
});

function doClose() {
    $.mainWindow.close();
}