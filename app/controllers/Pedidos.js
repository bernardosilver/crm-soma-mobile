// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args          = $.args;
var moment        = require("alloy/moment");
var optionsFilter = [];
var LocalData 	  = require('LocalData');

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar pedido",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar pedido",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

function doClose() {
    $.mainWindow.close();
}

function onFilter(){
	$.popup.showLabelWithTag(optionsFilter,function(categoria,index){
		var itemIndex 		= OS_ANDROID ? (firstVisibleSectionIndex < index ? 0 : 0) : 0;
		var sectionIndex 	= parseInt(index);
		scrollTo(sectionIndex, itemIndex);
		$.popup.hide();
	},"Toque para filtrar");
}

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/add.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
            var WindowTabGroup = Alloy.createController('FormPedido').getView();
            if (OS_IOS)
                WindowTabGroup.open();
            else
                WindowTabGroup.open({ modal: false });
        });
    };
    activity.invalidateOptionsMenu();
}

function createListView() {
    var section = Ti.UI.createListSection();
    var listPedidos = [];
    var list        = [];
    LocalData.getCabecalho({
        success: function(response) {
            list = response;
        },
        error: function(err) {

        }
    })
    var pedidos = _.sortBy(list, function(ped) {return ped.CJ_EMISSAO;}).reverse();
    for (var i in pedidos) {
        listPedidos.push(createItem(pedidos[i]));
    }
    listPedidos.push(setEmptyBottom());
    section.setItems(listPedidos);
    $.listView.setSections([section]);
}

function createItem(pedido) {
    var searchableText = pedido.CJ_NOME + pedido.CJ_CLIENTE + pedido.ID + pedido.CJ_MUN + pedido.CJ_CGC + pedido.CJ_DDD + pedido.CJ_TEL + moment(pedido.CJ_EMISSAO).format('DDMMYYYY') + moment(pedido.DTPREV).format('DDMMYYYY');
    return {
        template: 'templatePedido',
        lbCod       : {
            text: 'Pedido nº: ' + pedido.ID
        },
        lbNome      : {
            attributedString: Alloy.Globals.setNameWithCode(pedido.CJ_NOME, pedido.CJ_CLIENTE),
        },
        lbMunicipio : {
            text: pedido.CJ_MUN,
        },
        lbDtEmissao : {
            attributedString: Alloy.Globals.setNameWithCode(moment(pedido.CJ_EMISSAO).format('DD/MM/YYYY'), 'Data Emissao'),
        },
        lbDtCarga  : {
            attributedString: Alloy.Globals.setNameWithCode(moment(pedido.DTPREV).format('DD/MM/YYYY'), 'Prev. Carga'),
        },
        status  : {
            backgroundColor : pedido.CJ_ORC ? 'green' : 'red'
        },
        properties  : {
            data: pedido,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function setEmptyBottom(){
	return {
		template 		: "templateEmpty",
		properties 		: {
			accessoryType 			: Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}


async function excluirPedido(cab) {
    var id = cab.ID
    $.activityIndicator.show();
    await LocalData.clearTable({table: 'cabecalho', where: ` WHERE ID = '${id}'`});
    await LocalData.clearTable({table: 'itensPed', where: ` WHERE CK_NUM = '${id}'`});
    createListView();
    $.activityIndicator.hide();
}

$.listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    var data = item.properties.data;
    if(data.ID) {
        Alloy.Globals.showAlert({
            title: 'ATENÇÃO',
            message: 'Qual opção deseja executar?',
            // buttons: ['Cancelar','Excluir', 'Editar'],
            buttons: ['Excluir', 'Cancelar', 'Editar',],
            // buttons: [0,1, 2],
            listener: function (e) {
                if(e.index == 0) {
                    // EXCLUIR
                    excluirPedido(data);
                }
                else if(e.index == 2) {
                    // EDITAR
                    Alloy.Globals.openWindow('FormPedido', {pedido:data});
                }
            }

        })
    }
});

$.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
});

$.mainWindow.addEventListener("focus", function () {
    createListView();
});

