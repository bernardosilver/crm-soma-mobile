var args        = arguments[0] || {};
var Draggable   = require('ti.draggable');
var refreshData = require("RefreshData");

var previewImage    = args.previewImage;
var usuario         = args.usuario || null;
var successCallBack = args.successCallBack || function () { };
var errorCallBack   = args.errorCallBack || function () { };

var leftCrop    = 0;
var topCrop     = 0;

$.activityIndicator.setTouch(true);

function createCropArea(size, axis, min, max) {
    Ti.API.error("CROP AREA");

    var draggableView = Draggable.createView({
        width       : size,
        height      : size,
        top         : 0,
        left        : 0,
        borderColor : Alloy.Globals.RED_COLOR,
        borderWidth : 4,
        draggableConfig : {
            axis    : axis,
            minTop  : min,
            maxTop  : max,
            minLeft : min,
            maxLeft : max
        }
    });

    $.contentPreview.add(draggableView);

    draggableView.addEventListener("end", function (e) {
        topCrop     = OS_IOS ? e.top : Alloy.Globals.PixelsToDPUnits(e.top);
        leftCrop    = OS_IOS ? e.left : Alloy.Globals.PixelsToDPUnits(e.left);
    });
}

function saveFoto(arquivo) {
    if(usuario){
        var fotoAntiga = usuario.imagem || null;
        // refreshData.atualizarUsuario({
        //     data    : {
        //         id      : usuario.id,
        //         imagem  : arquivo,
        //     },
        //     successCallBack: function (response) {
        //         if (fotoAntiga) removeProfileImage(fotoAntiga);
        //         $.activityIndicator.hide();
        //         usuario.imagem = response.imagem;
        //         Alloy.Globals.setLocalDataUsuario(usuario);
        //         successCallBack();
        //         doClose();
        //     },
        //     errorCallBack: function (response) {
        //         $.activityIndicator.hide();
        //         Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao atualizar! " + response.error);
        //     }
        // });
    }
}

function sendArquivo(arquivo) {
    $.activityIndicator.show();
    if (usuario){
        // refreshData.sendProfile({
        //     data: { file: arquivo },
        //     successCallBack: function (response) {
        //         Ti.API.error("FOTO FOI", response);
        //         if(usuario) saveFoto(response.arquivo);
        //         else{
        //             $.activityIndicator.hide();
        //             doClose();
        //             successCallBack(response.arquivo);
        //         }
        //     },
        //     errorCallBack: function (response) {
        //         $.activityIndicator.hide();
        //         if (OS_IOS)
        //             $.mainWindow.close();
        //         errorCallBack();
        //     }
        // });
    }
}

function removeProfileImage(imagem) {
    // refreshData.removeProfile({
    //     data: { id: usuario.id, imagem: imagem, update: true },
    //     successCallBack: function (response) {
    //     },
    //     errorCallBack: function (response) {
          
    //     }
    // });
}


function doClose() {
    $.mainWindow.close();
}

function doSave() {

    $.activityIndicator.show();

    var leftCropOriginal    = Math.floor((leftCrop * previewImage.width) / $.preview.size.width);
    var topCropOriginal     = Math.floor((topCrop * previewImage.height) / $.preview.size.height);
    var cropSize            = null;

    if (previewImage.width < previewImage.height) {
        cropSize = Math.floor(previewImage.width);
        topCropOriginal = (cropSize + topCropOriginal) > previewImage.height ? previewImage.height - cropSize : topCropOriginal;
    } else {
        cropSize            = Math.floor(previewImage.height);
        leftCropOriginal    = (cropSize + leftCropOriginal) > previewImage.width ? previewImage.width - cropSize : leftCropOriginal;
    }

    var imageCropped = cropPreviewImage(cropSize, leftCropOriginal, topCropOriginal);

    sendArquivo(imageCropped);
}

function cropPreviewImage(cropSize, leftCrop, topCrop) {
    var imageCropped = previewImage.imageAsResized(previewImage.width, previewImage.height).imageAsCropped({
        width   : cropSize,
        height  : cropSize,
        x       : leftCrop,
        y       : topCrop
    });
    return imageCropped.imageAsResized(640, 640).imageAsCompressed(0.9);
}

function showPreviewImagem(width, height) {
    var cropSize    = (width < height) ? width : height;
    var min         = 0;
    var max         = (width < height) ? height - cropSize : width - cropSize;
    var axis        = (width < height) ? "y" : "x";

    createCropArea(cropSize, axis, min, max);
}

$.preview.addEventListener("load", function (e) {
    var width   = $.preview.size.width;
    var height  = $.preview.size.height;
    showPreviewImagem(width, height);
});

$.preview.image = (previewImage.width > 1080 || previewImage.height > 1080) ? previewImage.imageAsResized(previewImage.width * 0.75, previewImage.height * 0.75) : previewImage;


