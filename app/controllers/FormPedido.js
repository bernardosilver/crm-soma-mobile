// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args 	   	  = $.args;
var click_pedido  = args.pedido || null;
var OrderManager  = require('OrderManager');
var ClientManager = require('ClientManager');
var LocalData 	  = require('LocalData');
let moment 		  = require('alloy/moment');
var Mask          = require("Mask");
var usuario   	  = Alloy.Globals.getLocalDataUsuario().data;
var actionItem 	  = {};
var pedido   	  = {};
var sections 	  = [];
var status		  = null;
var listManager   = null;
var sectionParcela = null;


function doClose(){
	if(click_pedido) {
		OrderManager.delete();
	}
	$.mainWindow.close();
}

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/save.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
			validatePedido();
        });
    };
    activity.invalidateOptionsMenu();
}

async function getDados() {
	let cab = await getCabecalho(click_pedido.ID);
	let prods = await getProdutosPedido(click_pedido.ID);
	

	createObject(cab, prods);

}

const getCabecalho = (id) => new Promise((resolve, reject) =>{
	LocalData.getCabecalho({
		cab_id: id,
		success: function(response) {
			resolve(response[0]);
		},
		error: function(err) {
			reject(err);
		}
	});
});

const getProdutosPedido = (id) => new Promise((resolve, reject) =>{
	LocalData.getItensPed({
		ck_num: id,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			console.log('ERRO BUSCAR PROD: ', err);
			reject(err);
		}
	});
});

async function createObject(cabecalho, produtos){
	var obj 	  = {};
	// COTACAO DO CLIENTE. É ARMAZENADO NO PRODUTO (CK_COTCLI)
	obj.numppd    = produtos[0].CK_COTCLI;
	listManager.setNumPpd(obj.numppd);
	OrderManager.setNumPpd(obj.numppd);
//------------------------------------------------------------
	await LocalData.getClientes({
        cliente_id : cabecalho.CJ_CLIENTE,
		success	: function(array){
			obj.cliente = array[0];
		},
		error	: function(err){
			console.log('DEU ERRADO AO CADASTRAR CLIENTE: ', err);
		}
	});
	listManager.setCliente(obj.cliente);
	OrderManager.setCliente(obj.cliente);
	listManager.setTabela();
	listManager.setPagamento();
	if(sectionParcela) {
		listManager.setParcela();
		sectionParcela = null;
		$.listView.replaceSectionAt(4, sectionParcela);
	}
//------------------------------------------------------------
	const tabela = await LocalData.getTabelas({vend: usuario.USR_LOGIN, tab: cabecalho.CJ_TABELA});
	obj.tabela    = tabela[0];
	OrderManager.setTabela(obj.tabela);
	listManager.setTabela(OrderManager.getTabela());
	listManager.setPagamento();
	if(sectionParcela) {
		listManager.setParcela();
		sectionParcela = null;
		$.listView.replaceSectionAt(4, sectionParcela);
	}
//------------------------------------------------------------
	var condPag = await LocalData.getCondPag({A1_COD: cabecalho.CJ_CLIENTE, CONDPAG: cabecalho.CJ_CONDPAG});
	obj.pagamento = condPag[0];
	OrderManager.setPagamento(obj.pagamento);
	listManager.setPagamento(OrderManager.getPagamento());
	if(sectionParcela) {
		listManager.setParcela();
		sectionParcela = null;
		$.listView.replaceSectionAt(4, sectionParcela);
	}
	if(obj.pagamento.CONDPG == '003') {
		sectionParcela = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Parcela(s)"}).getView()
		});
		sectionParcela.setItems(refreshParcela());
		$.listView.insertSectionAt(4,sectionParcela);
	}
	
	else if(sectionParcela) {
		sectionParcela = null;
		$.listView.replaceSectionAt(4, sectionParcela);
	}
//------------------------------------------------------------
	obj.parcela = [];
	for(var i in cabecalho){
		if(i.indexOf('CJ_DATA') > -1 && cabecalho[i] != ''){
			obj.parcela.push(moment(cabecalho[i]).format('DD/MM/YYYY'));
		}

	}
	if(obj.parcela.length){
		OrderManager.setParcela(obj.parcela);
		listManager.setParcela(OrderManager.getParcela());
	}
//------------------------------------------------------------
	obj.frete 	  = {
		CJ_TPFRETE: cabecalho.CJ_TPFRETE,
		CJ_FRTBAST: cabecalho.CJ_FRTBAST
	}
	OrderManager.setFrete(obj.frete);
	listManager.setFrete(OrderManager.getFrete());

//------------------------------------------------------------
	//  NAO PREENCHE
	// obj.desconto  = OrderManager.getDesconto();
	// listManager.setDesconto(value);
	// OrderManager.setDesconto(value);

//------------------------------------------------------------
	obj.obs 	  = cabecalho.CJ_OBS;
	listManager.setObservacao(obj.obs);
	OrderManager.setObs(obj.obs);

//------------------------------------------------------------
	//  SETAR TODOS OS ITENS NO ORDERMANAGER
	let prod = [];
	let aux = {};
	let values = {};
	for(produto of produtos){
		// BUSCAR O PRODUTO PASSANDO O ID E TABELA
		aux = LocalData.getProds({CODPRO: produto.CK_PRODUTO, CODTAB: cabecalho.CJ_TABELA});
		aux[0].ID_PROD = produto.ID;
		// ENVIAR O PRODUTO PARA O ORDERMANAGER;
		values = {
			qnt: produto.CK_QTDVEN,
			desc: produto.CK_DESCONT,
			val: produto.CK_PRUNIT
		}
		OrderManager.insertItems(values, aux[0]);
	}
	obj.cart 	  = OrderManager.getTotalPedido();
	listManager.setProdutos(obj.cart);
	// refreshListView(obj);
}


function createListView() {
	sections = [];

	var sectionNumPpd = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Num. Pedido Compra"}).getView()
	});
	sectionNumPpd.setItems(refreshNumPpd());
	sections.push(sectionNumPpd);

	var sectionCliente = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Cliente"}).getView()
	});
	sectionCliente.setItems(refreshCliente());
	sections.push(sectionCliente);

	var sectionTabela = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Tabela de preços"}).getView()
	});
	sectionTabela.setItems(refreshTabela());
	sections.push(sectionTabela);

	var sectionPagamento = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Forma de pagamento"}).getView()
	});
	sectionPagamento.setItems(refreshPagamento());
	sections.push(sectionPagamento);

	
	sections.push(sectionParcela);

	var sectionFrete = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Frete"}).getView()
	});
	sectionFrete.setItems(refreshFrete());
	sections.push(sectionFrete);

	var sectionDesconto = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Desconto"}).getView()
	});
	sectionDesconto.setItems(refreshDesconto());
	sections.push(sectionDesconto);

	var sectionObservacao = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Observação"}).getView()
	});
	sectionObservacao.setItems(refreshObservacao());
	sections.push(sectionObservacao);

	var sectionProdutos = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Produtos"}).getView()
	});
	sectionProdutos.setItems(refreshProdutos());
	sections.push(sectionProdutos);

	$.listView.setSections(sections);

	return {
		setNumPpd: function(numppd) {
			sectionNumPpd.setItems(refreshNumPpd(numppd));
		},
		setCliente: function(cliente) {
			sectionCliente.setItems(refreshCliente(cliente));
		},
		setTabela: function(tabela) {
			sectionTabela.setItems(refreshTabela(tabela));
		},
		setPagamento: function(condicao) {
			sectionPagamento.setItems(refreshPagamento(condicao));
		},
		setParcela: function(parcelas) {
			sectionParcela.setItems(refreshParcela(parcelas));
		},
		setFrete: function(frete) {
			sectionFrete.setItems(refreshFrete(frete));
		},
		setDesconto: function(desc) {
			sectionDesconto.setItems(refreshDesconto(desc));
		},
		setObservacao: function(obs) {
			sectionObservacao.setItems(refreshObservacao(obs));
		},
		setProdutos: function(cart) {
			sectionProdutos.setItems(refreshProdutos(cart));
		}
	};
}

function refreshListView(obj){
	var numppd  	= obj.numppd || null;
	var cliente  	= obj.cliente || null;
	var tabela   	= obj.tabela || null;
	var pagamento 	= obj.pagamento || null;
	var parcela 	= obj.parcela || null;
	var frete 	 	= obj.frete || null;
	var desconto	= obj.desconto || null;
	var obs	 		= obj.obs || null;
	var cart	  	= obj.cart || null;
	listManager.setNumPpd(numppd);
	listManager.setCliente(cliente);
	listManager.setTabela(tabela);
	listManager.setPagamento(pagamento);
	if(parcela.CJ_DATA1){
		sectionParcela = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Parcela(s)"}).getView()
		});
		sectionParcela.setItems(refreshParcela());
		$.listView.insertSectionAt(4,sectionParcela);
		listManager.setParcela(parcela);
	}
	listManager.setFrete(frete);
	listManager.setDesconto(desconto);
	listManager.setObservacao(obs);
	listManager.setProdutos(cart);
	$.activityIndicator.hide();
}

function refreshNumPpd(numppd) {
	var itemNumPpd = numppd ? createItemNumPpd(numppd) : createItemListEscolher('numppd','Clique para inserir');
	return [itemNumPpd];
}

function refreshCliente(cliente) {
	var itemCliente = cliente && cliente.A1_NOME ? createItemCliente(Alloy.Globals.createTextCliente(cliente)) : createItemListEscolher('cliente','Clique para selecionar');
	return [itemCliente];
}

function refreshTabela(tabela) {
	var itemTabela = tabela && tabela.CJ_TABELA ? createItemTabela(tabela) : createItemListEscolher('tabela','Clique para selecionar');
	return [itemTabela];
}

function refreshPagamento(pagamento) {
	var itemPagamento = pagamento && pagamento.CJ_CONDPAG ? createItemPagamento(pagamento) : createItemListEscolher('pagamento','Clique para selecionar');
	return [itemPagamento];
}

function refreshParcela(parcela) {
	var itemParcela = parcela && parcela.CJ_DATA1 ? createItemParcela(parcela) : createItemListEscolher('parcela','Clique para selecionar');
	return [itemParcela];
}

function refreshFrete(frete) {
	var itemFrete = frete && frete.CJ_FRTBAST ? createItemFrete(frete) : createItemListEscolher('frete','Clique para adicionar');
	return [itemFrete];
}

function refreshDesconto(desc) {
	var itemDesconto = desc ? createItemDesconto(desc) : createItemListEscolher('desconto','Clique para adicionar');
	return [itemDesconto];
}

function refreshObservacao(obs) {
	var itemObs = obs ? createItemObs(obs) : createItemListEscolher('obs','Observações para entrega');
	return [itemObs];
}

function refreshProdutos(cart) {
	var itemProds = cart ? createItemProds(cart) : createItemListEscolher('prods','Produtos do pedido');
	return [itemProds];
}

function createItemListEscolher(type, label){
	return {
		template 	: 'templateEscolher',
		label 		: {
			text 	: label || 'Escolher'
		},
		labelEscolher 	: {
			text 		: type == 'obs' || type== 'frete' ? 'Adicionar' : 'Escolher'
		},
		properties 		: {
			type 					: type,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemNumPpd(numppd) {
	return {
		template 	: "templateNumPpd",
		lbNome 		: {
			text 	: numppd
		},
		properties 		: {
			type 					: 'numppd',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemCliente(cliente) {
	return {
		template 	: "templateCliente",
		lbNome 		: {
			text 	: cliente
		},
		properties 		: {
			type 					: 'cliente',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemTabela(tabela) {
	return {
		template 	: "templateTabela",
		lbNome 		: {
			text 	: tabela.CJ_DESCTAB + ' - Filial: ' + tabela.CJ_FILIAL
		},
		properties 		: {
			type 					: 'tabela',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemPagamento(pagamento) {
	return {
		template 	: "templatePagamento",
		lbNome 		: {
			text 	: pagamento.DESCPAG
		},
		properties 		: {
			type 					: 'pagamento',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemParcela(parcela) {
	return {
		template 	: "templateParcela",
		lbNome 		: {
			text 	: parcela.QNTPARC + ' Parcela(s)'
		},
		properties 		: {
			type 					: 'parcela',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemFrete(frete) {
	return {
		template 	: "templateFrete",
		lbNome 		: {
			text 	: frete.CJ_TPFRETE == 'F' ? 'FOB - R$' +  frete.CJ_FRTBAST : 'CIF - R$' +  frete.CJ_FRTBAST
		},
		properties 		: {
			type 					: 'frete',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemDesconto(desc) {
	return {
		template 	: "templateDesc",
		lbNome 		: {
			text 	: desc + '%'
		},
		properties 		: {
			type 					: 'desconto',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemObs(obs) {
	return {
		template 	: "templateObservacao",
		lbObservacao 		: {
			text 	: obs
		},
		properties 		: {
			type 					: 'obs',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemProds(cart) {
	return {
		template 	: "templateProdutos",
		lbNome 		: {
			text 	: `${cart.qntItens} Produto(s) no pedido. Valor total s/ frete: R$${Mask.real(parseFloat(cart.totalProd).toFixed(2))}`
		},
		properties 		: {
			type 					: 'prods',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function checkStatus(){
	status = OrderManager.getStatus();
	var text = '';
	if(status == 1) $.btCheckOut.setText('Selecionar cliente'); 
	else if(status == 2) $.btCheckOut.setText('Selecionar tabela de preços');
	else if(status == 3) $.btCheckOut.setText('Selecionar forma de pagamento');
	else if(status == 4) $.btCheckOut.setText('Selecionar parcela');
	else if(status == 5) $.btCheckOut.setText('Inserir frete');
	else $.btCheckOut.setText('Selecionar produtos');
	$.activityIndicator.hide();
}
function validatePedido() {
	var cart = OrderManager.getTotalPedido();
	if(cart.qntItens === 0) {
		Alloy.Globals.showAlert('Ops!','É necessário completar o pedido para salvar!');
	}
	else{
		Alloy.Globals.showAlert({
			title: 'ATENÇÃO',
			message: 'Deseja salvar o pedido?',
			// buttons: ['Cancelar','Excluir', 'Editar'],
			buttons: ['Sim', 'Cancelar'],
			// buttons: [0,1, 2],
			listener: function (e) {
				if(e.index == 0) {
					insertPedido();
				}
			}
		});
	}
}

async function insertPedido() {
	$.activityIndicator.show();
	let pedido_id = 0;
	let new_dados = OrderManager.orderGenerate();
	if(click_pedido && click_pedido.ID) {
		new_dados.cabecalho.ID = click_pedido.ID;
		await LocalData.updateCabecalho({
			data: new_dados.cabecalho,
			success	: function(id){
				pedido_id = id;
			},
			error	: function(err){
				console.log('DEU ERRO AO ATUALIZAR PEDIDO: ', err);
			}
		});
		const new_items  = await OrderManager.itemsGenerate(click_pedido.ID);
		for(const new_item of new_items){
			if(new_item.ID_PROD) {
				await LocalData.updateItemPed({
					data: new_item,
					success : function(prod) {
						console.log('ITEM ATUALIZADO: ', prod);
					},
					error	: function(err) {
						console.log('DEU ERRO AO ATUALIZAR ITEM: ', err);
					}
				})
			}
			else {
				await LocalData.insertItemPed({
					data 	: new_item,
					success : function(response) {
						console.log('PRODUTO INSERIDO: ', response);
					},
					error	: function(err) {
						console.log('DEU ERRO AO INSERIR PRODUTOS: ', err);
					}
				})
			}
		}
		OrderManager.delete();
		$.activityIndicator.hide();
		doClose();
	}
	else {
		await LocalData.createTableCabecalho();
		await LocalData.insertCabecalho({
			data : new_dados.cabecalho,
			success	: function(response){
				pedido_id = response.id;
			},
			error	: function(err){
				console.log('DEU ERRO AO CADASTRAR PEDIDO: ', err);
			}
		});
		const new_items  = await OrderManager.itemsGenerate(pedido_id);
		await LocalData.createTableItensPed();
		for(const new_item of new_items){
			await LocalData.insertItemPed({
				data 	: new_item,
				success : function(response) {
					console.log('PRODUTOS INSERIDOS: ', response);
				},
				error	: function(err) {
					console.log('DEU ERRO AO INSERIR PRODUTOS: ', err);
				}
			})
		}
		OrderManager.delete();
		doClose();
		$.activityIndicator.hide();
	}
}

actionItem.numppd = function(e) {
	Alloy.Globals.showAlert({
		title: 'Num. Ped. Compra',
		hasTextField: true,
		message: 'Digite o número do pedido de compra',
		buttons: ['OK', 'Cancelar'],
		listener: function(e) {
			if(e.index == 0) {
				const num = Mask.removeCaracter(e.text)
				listManager.setNumPpd(num);
				OrderManager.setNumPpd(num);
			}
		}
	})
};

actionItem.cliente = function(e) {
	Alloy.Globals.openWindow('Clientes', {pedido: true, callback: function(cliente){
		var status = ClientManager.getStatusCli(cliente.A1_MSBLQL, cliente.A1_TITVENC, cliente.A1_RISCO, cliente.A1_OBSERV, cliente.A1_TLC, cliente.CREDR);
		listManager.setCliente(cliente);
		OrderManager.setCliente(cliente);
		listManager.setTabela();
		listManager.setPagamento();
		if(sectionParcela) {
			listManager.setParcela();
			sectionParcela = null;
			$.listView.replaceSectionAt(4, sectionParcela);
		}
	}});
};

actionItem.tabela = function(e) {
	if(status != 0 && status < 2) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar um cliente primeiro!');
	Alloy.Globals.openWindow('CadPedido/TabelaPreco', {pedido: true, callback: function(tabela){
		OrderManager.setTabela(tabela);
		listManager.setTabela(OrderManager.getTabela());
		listManager.setPagamento();
		if(sectionParcela) {
			listManager.setParcela();
			sectionParcela = null;
			$.listView.replaceSectionAt(4, sectionParcela);
		}
		
	}});
};

actionItem.pagamento = function(e) {
	if(status != 0 && status < 3) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a tabela de preço primeiro!');
    Alloy.Globals.openWindow('CadPedido/CondPagamento', {pedido: true, callback: function(cond){
		OrderManager.setPagamento(cond);
		listManager.setPagamento(OrderManager.getPagamento());
		if(sectionParcela) {
			listManager.setParcela();
			sectionParcela = null;
			$.listView.replaceSectionAt(4, sectionParcela);
		}
		
		if(cond.CONDPG == '003') {
			sectionParcela = Ti.UI.createListSection({
				headerView: Alloy.createWidget("HeaderList",{title: "Parcela(s)"}).getView()
			});
			sectionParcela.setItems(refreshParcela());
			$.listView.insertSectionAt(4,sectionParcela);
		}
		else if(sectionParcela) {
			sectionParcela = null;
			$.listView.replaceSectionAt(4, sectionParcela);
		}
	}});
};

actionItem.parcela = function(e) {
	if(status != 0 && status < 4) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a forma de pagamento primeiro!');
	Alloy.Globals.openWindow('CadPedido/Parcela', {callback: function(parcelas){
		OrderManager.setParcela(parcelas);
		listManager.setParcela(OrderManager.getParcela());
		
	}});
};

actionItem.frete = function(e) {
	Alloy.Globals.openWindow('CadPedido/Frete', {callback: function(frete){
		const frt = frete.CJ_FRTBAST.replace(".", "").replace(",", ".");
		frete.CJ_FRTBAST = frt;
		OrderManager.setFrete(frete);
		listManager.setFrete(OrderManager.getFrete());
		
	}});
};

actionItem.desconto = function(e) {
	$.popup.showPopUpDesconto(function(value){
		if(value < -4 || value > 6) {
			Alloy.Globals.showAlert('Atenção', 'Desconto além do permitido!');
			OrderManager.setStatusDesc(0);
		}
		else OrderManager.setStatusDesc(1);
        $.popup.hide();
        listManager.setDesconto(value);
		OrderManager.setDesconto(value);
    });
};

actionItem.obs = function(e) {
	Alloy.Globals.openWindow('CadPedido/Observacao', {callback: function(obs){
		listManager.setObservacao(obs);
		OrderManager.setObs(obs);
	}});
};

actionItem.prods = function(e) {
	if(sectionParcela && status < 0 && status <=4) return Alloy.Globals.showAlert('Atenção!', 'É necessário inserir as parcelas primeiro!');
	else if(status != 0 && status < 4) return Alloy.Globals.showAlert('Atenção!', 'É necessário selecionar a forma de pagamento primeiro!');
	Alloy.Globals.openWindow('Produtos', {callback: function(prod){
	}});
};


listManager = createListView();

$.listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    var type = item.properties.type;
    if(actionItem[type])
        actionItem[type](e);
});

$.viewPanel.addEventListener('click', function(e){
	if(status == 0) actionItem.prods()
	else if(status == 1) actionItem.cliente();
	else if(status == 2) actionItem.tabela();
	else if(status == 3) actionItem.pagamento();
	else if(status == 4) actionItem.parcela();
	else if(status == 5) actionItem.frete();
});

$.mainWindow.addEventListener('focus', function(e) {
	if(listManager) {
		var cart = OrderManager.getTotalPedido();
		listManager.setProdutos(cart);
	}
	checkStatus();
});

$.mainWindow.addEventListener('open', function(e) {
	$.activityIndicator.show();
	refreshMenuActionBar();
	if(click_pedido && click_pedido.ID) {
		getDados();
	}
	else{
		var obj 	  = {};
		obj.numppd    = OrderManager.getNumPpd();
		obj.cliente   = OrderManager.getCliente();
		obj.tabela    = OrderManager.getTabela();
		obj.pagamento = OrderManager.getPagamento();
		obj.parcela   = OrderManager.getParcela();
		obj.frete 	  = OrderManager.getFrete();
		obj.desconto  = OrderManager.getDesconto();
		obj.obs 	  = OrderManager.getObs();
		obj.cart 	  = OrderManager.getTotalPedido();
		refreshListView(obj);
	}
	checkStatus();
});