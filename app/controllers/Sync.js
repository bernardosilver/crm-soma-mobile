var args        = $.args;
var download    = args.download || false;
var upload      = args.upload || false;
var moment      = require('alloy/moment');
var RefreshData = require("RefreshData");
var OrderManager= require('OrderManager');
var LocalData   = require("LocalData");
var usuario     = Alloy.Globals.getLocalDataUsuario().data;

function doClose() {
    $.mainWindow.close();
}

function showErrorMessage() {
    $.message.setVisible(true);
    $.message.setTextAlign(Ti.UI.TEXT_ALIGNMENT_CENTER);
    $.message.setText("Ocorreu um erro ao sincronizar os dados! Verifique a conexão com a internet e tente novamente!");
    $.message.setBtTitle("Tentar Novamente");
    $.message.setBtAction(function () {
        downloadDados();
        $.message.setVisible(false);
    });
    $.activityIndicator.hide();
}

async function uploadDados() {
    let insertCli   = {};
    let insertCab   = {};
    let id_cliLocal = 0;
    let id_cabLocal = 0;
    let condPag     = {};
    let refs        = [];
    let cabecalhos  = [];
    let produtos    = [];
    const qnt       = await countCadCli();
   
    $.activityIndicator.show();
    $.activityIndicator.setText(`... fazendo upload dos clientess. 1 de ${qnt} ...`);
    
    var clientes = await getCadCli() || [];
    if(clientes.length){
        for(var i = 0; i < clientes.length; i++) {
            $.activityIndicator.setText(`... fazendo upload dos clientes. '${i+1} de ${qnt}' ...`);
            id_cliLocal = clientes[i].ID;
            insertCli = await uploadCli(clientes[i]); // COLOCAR COD_CLI NO AO_CLIENTE DAS REFS
            insertCli.id_cliLocal = id_cliLocal;
            await updateCondPag(insertCli);
            // ATUALIZAR CONDICAO DE PAGAMENTO NO PEDIDO
            condPag = await getLocalCondPag(insertCli.A1_COD);
            await uploadLocalCondPag(condPag);
            await updateReferencias(insertCli);
            refs = await getCadRef(insertCli.ID);
            await uploadReferencias(refs);
            cabecalhos = await getCabecalho(insertCli.id_cliLocal);
            if(cabecalhos.length) {
                for(let cab of cabecalhos) {
                    await updateCabecalho(cab, insertCli.A1_COD);
                    produtos  = await getItensPed(cab.ID);
                    for(let prod of produtos) {
                        prod.CK_CLIENTE = insertCli.A1_COD;
                        await updateProduto(prod);
                    }
                }
            }
        }
    }
    cabecalhos = await getCabecalho();
    $.activityIndicator.setText(`... fazendo upload dos pedidos ...`);
    for(let cab of cabecalhos) {
        id_cabLocal = cab.ID;
        if(cab.CJ_ORC) { // É UM PEDIDO
            insertCab = await uploadCabecalho(cab); // COLOCAR CJ_NUM DO CAB NO CK_NUM DOS ITENS
            insertCab.id_cabLocal = id_cabLocal;
            await updateItensPed(insertCab.id_cabLocal, insertCab.CJ_NUM, insertCab.ID) // ATUALIZAR CK_NUM DO ITEM COM CJ_NUM
            produtos = await getItensPed(insertCab.CJ_NUM);
            await uploadItensPed(produtos);
            
        }
        else {
            //CADASTRO ORCAMENTO
            //RECEBO ID DO CADASTRO COMO RETORNO
            //PREENCHO OR_ID DO ITEM COM O ID DO ORCAMENTO
            const cabOrc = OrderManager.generateOrcamento(cab);
            insertCab = await uploadCabecalhoOrcamento(cabOrc, id_cabLocal);
            insertCab.id_cabLocal = id_cabLocal;
            produtos = await getItensOrc(id_cabLocal);
            const prodsOrc = OrderManager.generateItensOrcamento(produtos, insertCab.ID);
            await uploadItensOrc(prodsOrc);

        }
    }
    try{
        LocalData.clearTable({
            table: 'cabecalho',
        });
        LocalData.clearTable({
            table: 'itensPed',
        });
        $.activityIndicator.hide();
        await downloadDados();
        doClose();
    }
    catch{
        $.activityIndicator.hide();
        // await downloadDados();
        doClose();
    }
    
}

async function downloadDados() {
    try {
        var clientes = [];
        var tabelas  = [];
        $.activityIndicator.show();
        $.activityIndicator.setText('... fazendo download da lista de estados ...');
        await downloadEstados();
        $.activityIndicator.setText('... concluído! ...');
        $.activityIndicator.setText('... fazendo download da lista de cidades ...');
        await downloadMunicipios();
        $.activityIndicator.setText('... concluído! ...');
        $.activityIndicator.setText('... fazendo download da lista de clientes ...');
        clientes = await downloadClientes();
        $.activityIndicator.setText('... concluído! ...');
        $.activityIndicator.setText('... fazendo download das condições de pagamento ...');
        await downloadCondPag(clientes);
        $.activityIndicator.setText('... concluído! ...');
        $.activityIndicator.setText('... fazendo download das tabelas de preços ...');
        tabelas = await downloadTabelas();
        $.activityIndicator.setText('... concluído! ...');
        $.activityIndicator.setText('... fazendo download dos produtos ...');
        await downloadProdutos(tabelas);
        Alloy.Globals.setSync(moment().hours(0).minutes(0).seconds(0).milliseconds(0));
        $.activityIndicator.hide();
        doClose();
    }
    catch {
        showErrorMessage();  
    }
}

async function downloadEstados() {
    try {
        let estados = [];
        await LocalData.createTableUF();
        await LocalData.clearTable({table: 'uf'});
        estados = await getUf();
        await LocalData.insertUF(estados);
    }
    catch (err) {
        Alloy.Globals.showAlert("Ops!", "Ocorreu um erro no download dos estados. " + err);
    }
}

const getUf = () => new Promise((resolve, reject) =>{
    RefreshData.getEstado({
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            }
            else 
                showErrorMessage();
        }
    })
});

async function downloadMunicipios() {
    try {
        let mnc = [];
        await LocalData.createTableMnc();
        await LocalData.clearTable({table: 'mnc'});
        mnc = await getMnc();
        await LocalData.insertMnc(mnc);
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de cidades! ', err);
        showErrorMessage();
    }
}

const getMnc = () => new Promise((resolve, reject) =>{
    RefreshData.getMnc({
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            }
            else
                showErrorMessage();
        }
    })
});

async function downloadClientes() {
    try {
        let clientes = [];
        await LocalData.createTableCliente();
        await LocalData.clearTable({table: 'cliente', where: ' WHERE A1_MOBILE !=1 '});// where: ' WHERE A1_MOBILE !=1 '
        // await LocalData.clearTable({table: 'cadRef'});
        clientes = await getClientes();
        for(var i in clientes){
            if(clientes[i].A1_GRPVEN && clientes[i].A1_GRPVEN.trim() !== '') {
                var obj = await getCredGrpVen(clientes[i].A1_GRPVEN);
                clientes[i].A1_TITVENC = obj[0].TITVENC;
                clientes[i].DIASVENC   = obj[0].DIASVENC;
                clientes[i].NROPAG     = obj[0].NROPAG;
                clientes[i].METR       = obj[0].METR;
                clientes[i].A1_LC      = obj[0].LC;
                clientes[i].CREDR      = obj[0].CREDR;

            }
        }
        await LocalData.insertClientes(clientes);
        return clientes;
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de clientes! ', err);
        showErrorMessage();
    }
}

const getClientes = () => new Promise((resolve, reject) =>{
    var data = {}
    data.cod = usuario.USR_LOGIN
    RefreshData.getCliente({
        data: data,
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            } 
            else
                showErrorMessage();
        }
    })
});

const getCredGrpVen = (grp) => new Promise((resolve, reject) =>{
    var data = {}
    data.A1_GRPVEN = grp
    RefreshData.getCredGrpVen({
        data: data,
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            } 
            else
                showErrorMessage();
        }
    })
});

async function downloadCondPag(clientes) {
    try {
        let cond = [];
        await LocalData.clearTable({table: 'condPag'});
        await LocalData.createTableCondPag();
        for(var i in clientes) {
            cond = await getCondPag({cod: clientes[i].A1_COD, grupo: clientes[i].A1_GRPVEN});
            await LocalData.insertCondPag(cond);
        }
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de condições de pagamento! ', err);
        showErrorMessage();
    }
}

const getCondPag = (data) => new Promise((resolve, reject) =>{

    RefreshData.getCondPag({
        data: data,
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            } 
            else
                showErrorMessage();
        }
    })
});

async function downloadReferencias() {
    try {
        let refs = [];
        await LocalData.createTableReferencia();
        await LocalData.clearTable({table: 'referencia'});
        refs = await getReferencias();
        await LocalData.insertReferencia(refs);
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de referências! ', err);
        showErrorMessage();
    }
}

const getReferencias = () => new Promise((resolve, reject) =>{
    RefreshData.getReferencia({
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            } 
            else {
                showErrorMessage();
            }
        }
    })
});

async function downloadTabelas() {
    try {
        let tabelas = [];
        await LocalData.createTableTabelas();
        await LocalData.clearTable({table: 'tabelaPreco'});
        await LocalData.createTableProd();
        await LocalData.clearTable({table: 'produto'});
        tabelas = await getTabelas();
        await LocalData.insertTabelas(tabelas, usuario.USR_LOGIN);
        return tabelas;
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de tabelas de preços! ', err);
        showErrorMessage();
    }
}

async function downloadProdutos(tabelas) {
    try {
        let prods   = [];
        for(var i in tabelas) {
            prods = await getProduto(tabelas[i]);
            await LocalData.insertProds(prods);
        }
    }
    catch (err) {
        Alloy.Globals.showAlert('Erro ao realizar download de produtos! ', err);
        showErrorMessage();
    }
}

const getTabelas = () => new Promise((resolve, reject) =>{
    RefreshData.getTabelas({
        data: {user: usuario.USR_LOGIN},
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            }
            else
                showErrorMessage();
        }
    })
});

const getProduto = (tabela) => new Promise((resolve, reject) =>{
    RefreshData.getProduto({
        data: tabela,
        successCallBack: function(response){
            resolve(response.data);
        },
        errorCallBack: function(err) {
            if(err.code == 401){
                Alloy.Globals.openWindow("Login");
                doClose();
            }
            else
                showErrorMessage();
        }
    })
});

const countCadCli = () => new Promise((resolve,reject) => {
    LocalData.countCadCli({
        success	: function(count){
            resolve(count);
        },
        error	: function(err){
            reject(err);
            showErrorMessage();
        }
    });
});

const getCadCli = () => new Promise((resolve,reject) => {
    LocalData.getClienteDetail({
        id: {A1_MOBILE: '1'},
        success	: function(clientes){
            resolve (clientes);
        },
        error	: function(err){
            reject(err);
            showErrorMessage();
        }
    });
});

const uploadCli = (cli) => new Promise((resolve,reject) => {
    var id = cli.ID;
    delete cli.ID;
    delete cli.A1_MOBILE;
    delete cli.CREDR;
    delete cli.E4_DESCRI;
    delete cli.E4_DESCRI2;
    cli.A1_MSALDO = 0;
    cli.A1_MCOMPRA = 0;
    cli.METR = 0;
    cli.A1_MATR = 0;
    cli.A1_TITPROT = 0;
    cli.A1_CHQDEVO = 0;
    cli.A1_DTULCHQ = '';
    cli.A1_PRICOM = '';
    cli.NROPAG = 0;
    RefreshData.cadCliente({
        successCallBack: function (response) {
            LocalData.clearTable({
                table: 'cliente',
                where: `WHERE ID = '${id}'`
            })
            resolve(response.data);
        },
        errorCallBack: function (response) {
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cliente. " + response.error);
        },
        data: cli
    });
});

const updateCondPag = (obj) => new Promise((resolve,reject) => {
    LocalData.updateCondPag({
        data: obj,
        success	: function(response){
            resolve ();
        },
        error	: function(err){
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao atualizar condição de pagamento. " + err);
            reject(err);
        }
    });
});

const updateReferencias = (obj) => new Promise((resolve,reject) => {
    LocalData.updateRef({
        data: obj,
        success	: function(response){
            resolve ();
        },
        error	: function(err){
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao atualizar referências " + err);
            reject(err);
        }
    });
});

const updateCabecalho = (cab,id_cli) => new Promise((resolve,reject) => {
    cab.CJ_CLIENTE = id_cli;
    cab.CJ_CLIENT  = id_cli;
    LocalData.updateCabecalho({
        data: cab,
        success	: function(response){
            resolve ();
        },
        error	: function(err){
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao atualizar referências " + err);
            reject(err);
        }
    });
});

const updateProduto = (prod) => new Promise((resolve,reject) => {
    LocalData.updateItemPed({
        data: prod,
        success : function(prod) {
            resolve(prod);
        },
        error	: function(err) {
            console.log('DEU ERRO AO ATUALIZAR ITEM: ', err);
        }
    })
});

const uploadCabecalho = (cab) => new Promise((resolve,reject) => {
    var id = cab.ID;
    delete cab.ID;
    delete cab.CLIENTE_ID;
    delete cab.CJ_NOME;
    delete cab.CJ_MUN;
    delete cab.CJ_DDD;
    delete cab.CJ_TEL;
    delete cab.CJ_CGC;
    delete cab.CJ_ORC;
    RefreshData.cadCabecalho({
        successCallBack: function (response) {
            LocalData.clearTable({
                table: 'cabecalho',
                where: `WHERE ID = '${id}'`
            });
            resolve(response.data);
        },
        errorCallBack: function (response) {
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cabeçalho do pedido. " + response.error);
        },
        data: cab
    });
});

const uploadCabecalhoOrcamento = (cab, id_local) => new Promise((resolve,reject) => {
    var id = id_local;
    RefreshData.cadCabecalhoOrcamento({
        successCallBack: function (response) {
            LocalData.clearTable({
                table: 'cabecalho',
                where: `WHERE ID = '${id}'`
            });
            resolve(response.data);
        },
        errorCallBack: function (response) {
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cabeçalho do pedido. " + response.error);
        },
        data: cab
    });
});

const updateItensPed = (id_cabLocal, CJ_NUM, CJID) => new Promise((resolve,reject) => {
    LocalData.setNumProds({
        CK_NUM: CJ_NUM,
        id_cabLocal: id_cabLocal,
        CJID: CJID, 
        success : function(response) {
            resolve();
        },
        error	: function(err) {
            console.log('DEU ERRO AO ATUALIZAR ITEM: ', err);
            reject(err);
        }
    })
});

async function uploadItensPed(array){
    for(var i in array){
        id_itemLocal = array[i].ID;
        delete array[i].ID;
        await RefreshData.cadItensPed({
            successCallBack: function (response) {
                LocalData.clearTable({
                    table: 'itensPed',
                    where: `WHERE ID = '${id_itemLocal}'`
                })
            },
            errorCallBack: function (response) {
                console.log('DEU ERRO AO CAD ITEM: ', response);
                Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar item do pedido. " + response.error);
                return false;
            },
            data: array[i]
        });
    }
}

async function uploadItensOrc(array){
    for(var i in array){
        id_itemLocal = array[i].ID_LOCAL;
        delete array[i].ID_LOCAL;
        await RefreshData.cadItensOrcamento({
            successCallBack: function (response) {
                LocalData.clearTable({
                    table: 'itensPed',
                    where: `WHERE ID = '${id_itemLocal}'`
                })
            },
            errorCallBack: function (response) {
                console.log('DEU ERRO AO CAD ITEM: ', response);
                Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar item do orçamento. " + response.error);
                return false;
            },
            data: array[i]
        });
    }
}

const getCabecalho = (id_cli) => new Promise((resolve,reject) => {
    LocalData.getCabecalho({
        cli_id : id_cli,
        success: function(response) {
            resolve(response);
        },
        error: function(err) {

        }
    })
});

const getItensPed = (cab_id) => new Promise((resolve,reject) => {
    LocalData.getItensPed({
        ck_num : cab_id,
        success: function(response) {
            resolve(response);
        },
        error: function(err) {

        }
    })
});

const getItensOrc = (cab_id) => new Promise((resolve,reject) => {
    LocalData.getItensPed({
		ck_num: cab_id,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			console.log('ERRO BUSCAR PROD: ', err);
			reject(err);
		}
	});
});

const getLocalCondPag = (id) => new Promise((resolve,reject) => {
    LocalData.getCadCondPag({
		cliente_id: id,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao buscar condição de pagamento. " + err);
			reject(err);
		}
	});
});

const getCadRef = (id) => new Promise((resolve,reject) => {
    LocalData.getCadRef({
		cod_cli: id,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao buscar referências. " + err);
			reject(err);
		}
	});
});

async function uploadLocalCondPag(condPag){
    let id_condPagLocal = condPag.ID;
    delete condPag.ID;
    await RefreshData.cadCondPag({
        successCallBack: function (response) {
            LocalData.clearTable({
                table: 'cadCondPag',
                where: `WHERE ID = '${id_condPagLocal}'`
            })
        },
        errorCallBack: function (response) {
            Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar cond. pagamento. " + response.error);
            return false;
        },
        data: condPag
    });
}

async function uploadReferencias(refs){
    let id_refLocal = 0;
    for(var i in refs){
        id_refLocal = refs[i].ID;
        delete refs[i].ID;
        await RefreshData.cadReferencia({
            successCallBack: function (response) {
                LocalData.clearTable({
                    table: 'cadRef',
                    where: `WHERE ID = '${id_refLocal}'`
                })
            },
            errorCallBack: function (response) {
                Alloy.Globals.showAlert("Ops!", "Ocorreu um erro ao cadastrar referências. " + response.error);
                return false;
            },
            data: refs[i]
        });
    }
}


$.mainWindow.addEventListener('open', function(){
    if(download) downloadDados();
    else if(upload) uploadDados();
});