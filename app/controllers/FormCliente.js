// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args 		  = $.args;
var click_cliente = args.cliente || null;
var Mask          = require("Mask");
var actionItem    = {};
var cliente    	  = {};
var status		  = null;
var ClientManager = require('ClientManager');
var LocalData 	  = require('LocalData');
var listManager   = null;

function doClose(){
	$.mainWindow.close();
}

function createListView(obj) {
	var sections = [];

	var sectionDadosPessoais = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Dados Pessoais"}).getView()
	});
	sectionDadosPessoais.setItems(refreshDadosPessoais());
	sections.push(sectionDadosPessoais);

	var sectionEndereco = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Endereço de entrega"}).getView()
	});
	sectionEndereco.setItems(refreshEndereco());
	sections.push(sectionEndereco);

	var sectionCobranca = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Endereço de cobrança"}).getView()
	});
	sectionCobranca.setItems(refreshCobranca());
	sections.push(sectionCobranca);
	if(!click_cliente || click_cliente.A1_MOBILE) {
		var sectionSocios = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Sócios"}).getView()
		});
		sectionSocios.setItems(refreshSocios());
		sections.push(sectionSocios);

		var sectionComerciais = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Referências Comerciais"}).getView()
		});
		sectionComerciais.setItems(refreshComerciais());
		sections.push(sectionComerciais);

		var sectionBancarias = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Referências Bancárias"}).getView()
		});
		sectionBancarias.setItems(refreshBancarias());
		sections.push(sectionBancarias);

		var sectionCondPag = Ti.UI.createListSection({
			headerView: Alloy.createWidget("HeaderList",{title: "Condição de Pagamento"}).getView()
		});
		sectionCondPag.setItems(refreshCondPag());
		sections.push(sectionCondPag);
	}

	var sectionCredito = Ti.UI.createListSection({
		headerView: Alloy.createWidget("HeaderList",{title: "Limite de Crédito"}).getView()
	});
	sectionCredito.setItems(refreshCredito());
	sections.push(sectionCredito);

	$.listView.setSections(sections);

	return {
		setDadosPessoais: function(dados) {
			sectionDadosPessoais.setItems(refreshDadosPessoais(dados));
		},
		setEndereco: function(end) {
			sectionEndereco.setItems(refreshEndereco(end));
		},
		setCobranca: function(end) {
			sectionCobranca.setItems(refreshCobranca(end));
		},
		setSocios: function(socios) {
			sectionSocios.setItems(refreshSocios(socios));
		},
		setComerciais: function(referencias) {
			sectionComerciais.setItems(refreshComerciais(referencias));
		},
		setBancarias: function(referencias) {
			sectionBancarias.setItems(refreshBancarias(referencias));
		},
		setCondPag: function(condPag) {
			sectionCondPag.setItems(refreshCondPag(condPag));
		},
		setCredito: function(credito, saldo) {
			sectionCredito.setItems(refreshCredito(credito, saldo));
		}
	};
}

function refreshListView(obj){
	var dados    = obj.dados || null;
	var entrega  = obj.entrega || null;
	var cobranca = obj.cobranca || null;
	var socios 	 = obj.socios || null;
	var refCom 	 = obj.refCom || null;
	var refBan	 = obj.refBan || null;
	var condPag	 = obj.condPag || null;
	var cred 	 = obj.cred || null;
	var saldo 	 = obj.saldo || null;
	listManager.setDadosPessoais(dados);
	listManager.setEndereco(entrega);
	listManager.setCobranca(cobranca);
	listManager.setCredito(cred, saldo);
	if(!click_cliente || click_cliente.A1_MOBILE) {
		listManager.setSocios(socios);
		listManager.setComerciais(refCom);
		listManager.setBancarias(refBan);
		listManager.setCondPag(condPag);
	}
	$.activityIndicator.hide();
}

function refreshDadosPessoais(dados) {
	var itemDadosPessoais = dados && dados.A1_NOME ? createItemDadosPessoais(Alloy.Globals.createTextCliente(dados)) : createItemListEscolher('dados','Clique para inserir as informações.');
	return [itemDadosPessoais];
}

function refreshEndereco(endereco) {
	var itemEndereco = endereco && endereco.A1_END ? createItemEndereco(Alloy.Globals.createTextEndereco(endereco)) : createItemListEscolher('endereco','Clique para inserir as informações.');
	return [itemEndereco];
}

function refreshCobranca(endereco) {
	var itemCobranca = endereco && endereco.A1_ENDCOB ? createItemCobranca(Alloy.Globals.createTextEnderecoCob(endereco)) : createItemListEscolher('cobranca','Clique para inserir as informações.');
	return [itemCobranca];
}

function refreshSocios(socios) {
	var itemSocios = socios && socios.length ? createItemSocios(Alloy.Globals.createTextSocios(socios)) : createItemListEscolher('socios','Clique para inserir as informações.');
	return [itemSocios];
}

function refreshComerciais(referencias) {
	var itemComerciais = referencias && referencias.length ? createItemComerciais(Alloy.Globals.createTextRefCom(referencias)) : createItemListEscolher('comerciais','Clique para inserir as informações.');
	return [itemComerciais];
}

function refreshBancarias(bancos) {
	var itemBancarias = bancos && bancos.length ? createItemBancarias(Alloy.Globals.createTextRefBanc(bancos)) : createItemListEscolher('bancarias','Clique para inserir as informações.');
	return [itemBancarias];
}

function refreshCondPag(condPag) {
	var itemCondPag = condPag ? createCondPag(condPag.DESCRI) : createItemListEscolher('condPag','Clique para inserir.');
	return [itemCondPag];
}

function refreshCredito(credito, saldo) {
	var itemCredito = credito ? createItemCredito(credito, saldo) : createItemListEscolher('credito','Clique para inserir.');
	return [itemCredito];
}

function createItemListEscolher(type, label){
	return {
		template 	: 'templateEscolher',
		label 		: {
			text 	: label || 'Escolher'
		},
		labelEscolher 	: {
			text 		: 'Inserir'
		},
		properties 		: {
			type 					: type,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemDadosPessoais(dados) {
	return {
		template: "templateDados",
		lbNome 	: {
			text: dados
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties : {
            data                    : dados,
			type 					: 'dados',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemEndereco(endereco) {
	return {
		template 	: "templateEndereco",
		lbNome 		: {
			text 	: endereco
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : endereco,
			type 					: 'endereco',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemCobranca(endereco) {
	return {
		template 	: "templateCobranca",
		lbNome 		: {
			text 	: endereco
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : endereco,
			type 					: 'cobranca',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemSocios(socios) {
	return {
		template 	: "templateSocios",
		lbNome 		: {
			text 	: socios
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : socios,
			type 					: 'socios',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemComerciais(text) {
	return {
		template 	: "templateComerciais",
		lbNome 		: {
			text 	: text
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : text,
			type 					: 'comerciais',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemBancarias(text) {
	return {
		template 	: "templateBancarias",
		lbNome 		: {
			text 	: text
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : text,
			type 					: 'bancarias',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createCondPag(condPag) {
	return {
		template 	: "templateCondPag",
		lbNome 		: {
			text 	: condPag,
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : condPag,
			type 					: 'condPag',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function createItemCredito(cred, saldo) {
	return {
		template 	: "templateCredito",
		lbNome 		: {
			text 	: saldo ? 'R$ ' + Mask.real(cred) + ' / Disponível: R$ ' + Mask.real(saldo): 'R$ ' + Mask.real(cred),
		},
		labelAlterar : {
			visible : click_cliente ? false : true,
		},
		properties 		: {
            data                    : cred,
			type 					: 'credito',
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

async function getDados() {
	var cli  	 = {};
	var condPag  = {};
	var refs 	 = [];
	$.activityIndicator.show();
	if(click_cliente.A1_COD) {
		cli = await getCliente({A1_COD: click_cliente.A1_COD});
		await createCliente(cli,refs);
		checkStatus();
	}
	else if(click_cliente.A1_MOBILE){
		cli = await getCliente({ID: click_cliente.ID});
		condPag = await getCondPag(click_cliente.ID);
		refs = await getReferencias(click_cliente.ID);
		createManager(cli, condPag, refs);
	}
}

const getCliente = (id) => new Promise((resolve, reject) =>{
	LocalData.getClienteDetail({
		id,
		success: function(response) {
			resolve(response[0]);
		},
		error: function(err) {
			reject(err);
		}
	});
})

const getCondPag = (cod_cli) => new Promise((resolve, reject) =>{
	LocalData.getCadCondPag({
		cliente_id: cod_cli,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			reject(err);
		}
	});
})

const getReferencias = (cod_cli) => new Promise((resolve, reject) =>{
	LocalData.getCadRef({
		cliente_id: cod_cli,
		success: function(response) {
			resolve(response);
		},
		error: function(err) {
			reject(err);
		}
	});
})

function createCliente(cli, refs){
	cliente.socios = [];
	cliente.refCom = [];
	cliente.refBan = [];

	// DADOS PESSOAIS
	cliente.dados = {
		A1_NOME   	: cli.A1_NOME,
		A1_NREDUZ 	: cli.A1_NREDUZ,
		A1_DDD    	: cli.A1_DDD.trim(),
		A1_TEL    	: cli.A1_TEL,
		A1_TEL2   	: cli.A1_TEL2,
		A1_TEL3   	: cli.A1_TEL3,
		A1_TEL4   	: cli.A1_TEL4,
		A1_EMAIL  	: cli.A1_EMAIL,
		A1_PESSOA 	: cli.A1_PESSOA,
		A1_CGC    	: cli.A1_CGC,
		A1_INSCR  	: cli.A1_INSCR,
		A1_PFISICA	: cli.A1_PFISICA,
		A1_DTNASC   : cli.A1_DTNASC,
		A1_ME   	: cli.A1_ME,
		A1_OBSRC   	: cli.A1_OBSRC
	}

	//ENDERECO ENTREGA
	cliente.entrega		= {
		A1_END      : cli.A1_END,
		A1_COMPLEM  : cli.A1_COMPLEM,
		A1_BAIRRO   : cli.A1_BAIRRO,
		A1_EST      : cli.A1_EST,
		A1_MUN      : cli.A1_MUN,
		A1_CODMUN   : cli.A1_CODMUN,
		A1_CEP      : cli.A1_CEP,
		A1_INFOROT  : cli.A1_INFOROT,
		A1_LAT      : cli.A1_LAT,
		A1_LONG     : cli.A1_LONG
	}

	//LIMITE DE CREDITO
	cliente.cred = parseFloat(cli.A1_LC).toFixed(2);
	cliente.saldo = parseFloat(cli.CREDR).toFixed(2);

	//ENDERECO COBRANCA
	cliente.cobranca	= {
		A1_ENDCOB   : cli.A1_ENDCOB,
		A1_BAIRROC  : cli.A1_BAIRROC,
		A1_ESTC     : cli.A1_ESTC,
		A1_MUNC     : cli.A1_MUNC,
		A1_CEPC     : cli.A1_CEPC
	}
	if(refs.length){
		for(const ref of refs) {
			if(ref.AO_TIPO == '1') cliente.socios.push(ref);
			if(ref.AO_TIPO == '2') cliente.refCom.push(ref);
			if(ref.AO_TIPO == '3') cliente.refBan.push(ref);
		}
	}

	refreshListView(cliente);
}

function checkStatus(){
	status = ClientManager.getStatus();
	var text = '';
	if(status == 1) $.btCheckOut.setText('Inserir dados pessoais'); 
	else if(status == 2) $.btCheckOut.setText('Inserir endereço de entrega');
	else if(status == 3) $.btCheckOut.setText('Inserir endereço de cobrança');
	else if(status == 4) $.btCheckOut.setText('Inserir sócio');
	else if(status == 5) $.btCheckOut.setText('Inserir referências comereciais');
	else if(status == 6) $.btCheckOut.setText('Inserir referências bancárias');
	else if(status == 7) $.btCheckOut.setText('Inserir Cond. de Pagamento');
	else if(status == 8) $.btCheckOut.setText('Inserir limite de crédito');
	else $.btCheckOut.setText('Salvar cliente');
	$.activityIndicator.hide();
}

async function insertCliente() {
	$.activityIndicator.show();
	var cliente_id = 0;
	const new_dados = await ClientManager.generateCliente();
	// await LocalData.createTableCadCliente();
	if(click_cliente && click_cliente.ID) {
		await LocalData.updateCadCliente({
			data: {
				id 		: click_cliente.ID,
				cliente : new_dados
			},
			success	: function(id){
				cliente_id = id;
			},
			error	: function(err){
				console.log('DEU ERRO AO ATUALIZAR CLIENTE: ', err);
			}
		});
		const new_condPag  = ClientManager.generateCondPag(click_cliente.ID);
		const new_refs  = ClientManager.generateRefs(click_cliente.ID);
		await LocalData.updateCadCondPag({
			data: {new_condPag},
			success : function(cond) {
				console.log('COND ATUALIZADA: ', cond);
			},
			error : function(err) {
				console.log('DEU ERRO: ', err);
			}
		});
		for(const new_ref of new_refs){
			await LocalData.updateCadRef({
				data: {
					id : new_ref.ID,
					ref: new_ref
				},
				success : function(ref) {
					console.log('REFERENCIA ATUALIZADA: ', ref);
				},
				error	: function(err) {
					console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
				}
			})
		}
		doClose();
		$.activityIndicator.hide();
	}
	else {
		await LocalData.insertCadCliente({
			cliente : new_dados,
			success	: function(id){
				cliente_id = id;
			},
			error	: function(err){
				console.log('DEU ERRO AO CADASTRAR CLIENTE: ', err);
			}
		});
		const new_condPag  = await ClientManager.generateCondPag(cliente_id);
		const new_refs     = await ClientManager.generateRefs(cliente_id);
		await LocalData.createTableCadCondPag();
		await LocalData.insertCadCondPag({
			condPag: new_condPag,
			success : function(cond) {
				console.log('COND CADASTRADA: ', cliente_id, cond);
			},
			error : function(err) {
				console.log('DEU ERRO: ', err);
			}
		});

		await LocalData.createTableCadRef();
		for(const new_ref of new_refs){
			await LocalData.insertCadRef({
				ref 	: new_ref,
				success : function(ref) {
					console.log('REFERENCIA CADASTRADA: ', ref);
				},
				error	: function(err) {
					console.log('DEU ERRO AO CADASTRAR REFERENCIA: ', err);
				}
			})
		}
		doClose();
		$.activityIndicator.hide();
	}
}

function createManager(cli, condPag, refs) {
	var com  = [];
	var banc = [];
	var soc  = [];
	ClientManager.setDadosPessoais(cli);
	ClientManager.setEndereco({
		logradouro 	: cli.A1_END,
		complemento : cli.A1_COMPLEM,
		bairro 		: cli.A1_BAIRRO,
		uf 			: cli.A1_EST,
		COD_UF 		: cli.COD_UF,
		municipio 	: cli.A1_MUN,
		CODMUN 		: cli.A1_CODMUN,
		cep 		: cli.A1_CEP,
		referencia 	: cli.A1_INFOROT,
		latitude 	: cli.A1_LAT,
		longitude 	: cli.A1_LONG,
	});
	ClientManager.setEnderecoCobranca({
		logradouro 	: cli.A1_ENDCOB,
		bairro 		: cli.A1_BAIRROC,
		uf 			: cli.A1_ESTC,
		CODC_UF 	: cli.CODC_UF,
		municipio 	: cli.A1_MUNC,
		COD_MC 		: cli.COD_MC,
		cep 		: cli.A1_CEPC,
	});
	ClientManager.setCondPag(condPag);
	ClientManager.setLimiteCredito(parseFloat(cli.A1_LC).toFixed(2));
	for(var i in refs) {
		if(refs[i].AO_TIPO == '1') {
			soc.push(refs[i]);
		}
		else if(refs[i].AO_TIPO == '2') {
			com.push(refs[i]);
		}
		else if(refs[i].AO_TIPO == '3') {
			banc.push(refs[i]);
		}
	}
	ClientManager.setSocios(soc);
	ClientManager.setReferenciasCom(com);
	ClientManager.setReferenciasBanc(banc);

	var obj 	 = {};
	obj.dados    = ClientManager.getDadosPessoais();
	obj.entrega  = ClientManager.getEndereco();
	obj.cobranca = ClientManager.getEnderecoCobranca();
	obj.socios 	 = ClientManager.getSocios();
	obj.refCom 	 = ClientManager.getReferenciasCom();
	obj.refBan	 = ClientManager.getReferenciasBanc();
	obj.condPag  = ClientManager.getCondPag();
	obj.cred 	 = parseFloat(ClientManager.getLimiteCredito()).toFixed(2);
	refreshListView(obj);

	ClientManager.setDadosCompleto();
	ClientManager.setEntregaCompleto();
	ClientManager.setCobrancaCompleto();
	ClientManager.setSociosCompleto();
	ClientManager.setComercialCompleto();
	ClientManager.setBancariaCompleto();
	ClientManager.setCondPagCompleto();
	ClientManager.setCreditoCompleto();
}

actionItem.dados = function(e) {
    Alloy.Globals.openWindow('CadCliente/DadosPessoais', {dados: cliente.dados, callback: function(dados){
		listManager.setDadosPessoais(dados);
		ClientManager.setDadosCompleto();
	}});
};

actionItem.endereco = function(e) {
	Alloy.Globals.openWindow('FormEndereco', {end_entrega: cliente.entrega, callback: function(endereco){
		listManager.setEndereco(endereco);
		ClientManager.setEntregaCompleto();
	}});
};

actionItem.cobranca = function(e) {
	Alloy.Globals.openWindow('FormEndereco', {end_cobranca: cliente.cobranca, cobranca: true, callback: function(endereco){
		listManager.setCobranca(endereco);
		ClientManager.setCobrancaCompleto();
	}});
};

actionItem.socios = function(e) {
	Alloy.Globals.openWindow('CadCliente/Socios', {socios: cliente.socios, callback: function(socios){
		listManager.setSocios(socios);
		ClientManager.setSociosCompleto();
	}});
};

actionItem.comerciais = function(e) {
    Alloy.Globals.openWindow('CadCliente/RefComercial', {refs: cliente.refCom, callback: function(referencias){
		listManager.setComerciais(referencias);
		ClientManager.setComercialCompleto();
	}});
};

actionItem.bancarias = function(e) {
	Alloy.Globals.openWindow('CadCliente/RefBancaria', {refs: cliente.refBan, callback: function(bancos){
		listManager.setBancarias(bancos);
		ClientManager.setBancariaCompleto();
	}});
};

actionItem.condPag = function(e) {
    Alloy.Globals.openWindow('CadCliente/CondPag', {cred: cliente.condPag, callback: function(valor){
		listManager.setCondPag(valor);
		ClientManager.setCondPagCompleto();
	}});
};

actionItem.credito = function(e) {
    Alloy.Globals.openWindow('CadCliente/Credito', {cred: cliente.cred, callback: function(valor){
		listManager.setCredito(valor);
		ClientManager.setCreditoCompleto();
	}});
};

listManager = createListView();

$.listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    var type = item.properties.type;
    if(actionItem[type])
        actionItem[type](e);
});

$.viewPanel.addEventListener('click', function(e){
	if(status == 0) {
		Alloy.Globals.showAlert({
			title: 'ATENÇÃO',
			message: 'Deseja salvar o cliente?',
			// buttons: ['Cancelar','Excluir', 'Editar'],
			buttons: ['Sim', 'Cancelar'],
			// buttons: [0,1, 2],
			listener: function (e) {
				if(e.index == 0) {
					insertCliente();
				}
			}
		});
	}
	else if(status == 1) actionItem.dados();
	else if(status == 2) actionItem.endereco();
	else if(status == 3) actionItem.cobranca();
	else if(status == 4) actionItem.socios();
	else if(status == 5) actionItem.comerciais();
	else if(status == 6) actionItem.bancarias();
	else if(status == 7) actionItem.condPag();
	else if(status == 8) actionItem.credito();
});

$.mainWindow.addEventListener('focus', function(e) {
	checkStatus();
});
$.mainWindow.addEventListener('open', function(e) {
	$.activityIndicator.show();
	if(click_cliente && click_cliente.A1_COD) {
		$.listView.setBottom('0dp');
		$.viewPanel.setHeight('0dp');
		$.viewPanel.setVisible('false');
		$.viewPanel.setBottom('0dp');
		getDados();
	}
	else if(click_cliente && click_cliente.A1_MOBILE) {
		ClientManager.init();
		getDados();
	}
	else if(ClientManager.isActive()) {
		var obj 	 = {};
		obj.dados    = ClientManager.getDadosPessoais();
		obj.entrega  = ClientManager.getEndereco();
		obj.cobranca = ClientManager.getEnderecoCobranca();
		obj.socios 	 = ClientManager.getSocios();
		obj.refCom 	 = ClientManager.getReferenciasCom();
		obj.refBan	 = ClientManager.getReferenciasBanc();
		obj.condPag  = ClientManager.getCondPag();
		obj.cred 	 = ClientManager.getLimiteCredito();
		refreshListView(obj);
	}
	else ClientManager.init();
	checkStatus();
});