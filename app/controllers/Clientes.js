// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args                     = $.args;
var pedido                   = args.pedido || false;
var callback                 = args.callback || (() =>{});
var optionsFilter            = [];
var firstVisibleSectionIndex = 0;
var ClientManager = require('ClientManager');
var OrderManager  = require('OrderManager');
var LocalData 	  = require('LocalData');
var moment        = require("alloy/moment");

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar cliente",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar cliente",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

function doClose() {
    $.mainWindow.close();
}

function onFilter(){
	$.popup.showLabelWithTag(optionsFilter,function(categoria,index){
		var itemIndex 		= OS_ANDROID ? (firstVisibleSectionIndex < index ? 0 : 0) : 0;
		var sectionIndex 	= parseInt(index);
		scrollTo(sectionIndex, itemIndex);
		$.popup.hide();
	},"Toque para filtrar");
}

function scrollTo(sectionIndex, itemIndex){ 
	var options = OS_IOS ? {position: Titanium.UI.iOS.ListViewScrollPosition.TOP, animated:false}  : {animated:false};
	$.listView.scrollToItem(sectionIndex,itemIndex,options);
	// showViewControl(false);
}

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/add.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
            ClientManager.delete();
            var WindowTabGroup = Alloy.createController('FormCliente').getView();
            if (OS_IOS)
                WindowTabGroup.open();
            else
                WindowTabGroup.open({ modal: false });
        });
    };
    activity.invalidateOptionsMenu();
}

function createListView(array) {
    var sections     = [];
    var cadastrados  = [];
    var liberados    = [];
    var bloqueados   = [];
    var aguardando   = [];
    var insuficiente = [];
    var titulos      = [];
    optionsFilter    = [];

    var clientes = _.sortBy(array, function(cli) {return cli.A1_NOME});
    for (var i in clientes) {
        var status = ClientManager.getStatusCli(clientes[i]);
        if(clientes[i].A1_MOBILE) {
            cadastrados.push(createItem(clientes[i], status));
        }
        else if(status.status == 1) {
            liberados.push(createItem(clientes[i], status));
        }
        else if (status.status == 2) {
            bloqueados.push(createItem(clientes[i], status));
        }
        else if (status.status == 3) {
            aguardando.push(createItem(clientes[i], status));
        }
        else if (status.status == 4) {
            insuficiente.push(createItem(clientes[i], status));
        }
        else if (status.status == 5) {
            titulos.push(createItem(clientes[i], status));
        }
    }

    if(liberados.length) {
        optionsFilter.push('LIBERADOS');
        var sectionLiberados = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Liberados') 
        });
        sectionLiberados.setItems(liberados);
        sections.push(sectionLiberados);
    }
    if(bloqueados.length) {
        optionsFilter.push('BLOQUEADOS');
        var sectionBloqueados = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Bloqueados')
        });
        sectionBloqueados.setItems(bloqueados);
        sections.push(sectionBloqueados);
    }
    if(aguardando.length) {
        optionsFilter.push('AGUARD. LIB.');
        var sectionAguardando = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Aguardando liberação')
        });
        sectionAguardando.setItems(aguardando);
        sections.push(sectionAguardando);
    }
    if(insuficiente.length) {
        optionsFilter.push('CRED. INSUF.');
        var sectionInsuficiente = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Crédito insuficiente')
        });
        sectionInsuficiente.setItems(insuficiente);
        sections.push(sectionInsuficiente);
    }
    if(titulos.length) {
        optionsFilter.push('TÍTULOS VENC.');
        var sectionVencidos = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Títulos Vencidos')
        });
        sectionVencidos.setItems(titulos);
        sections.push(sectionVencidos);
    }
    if(cadastrados.length) {
        optionsFilter.push('CADASTRADOS');
        var sectionCad = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Cadastrados')
        });
        sectionCad.setItems(cadastrados);
        sections.push(sectionCad);
    }
    if(sections && sections.length) sections[sections.length - 1].appendItems([setEmptyBottom()]);
    $.listView.setSections(sections);
    $.activityIndicator.hide();
}

function createItem(cliente, status) {
    var searchableText = cliente.A1_NOME + cliente.A1_COD + cliente.A1_MUN + cliente.A1_CGC +  cliente.A1_DDD + cliente.A1_TEL;
    var data = cliente.A1_ULTCOM  && cliente.A1_ULTCOM.trim() ? moment(cliente.A1_ULTCOM).format('DD/MM/YYYY') : null;
    return {
        template: 'templateCliente',
        lbNome: {
            attributedString: Alloy.Globals.setNameWithCode(cliente.A1_NOME, cliente.A1_COD),
        },
        lbMunicipio: {
            text: cliente.A1_MUN,
        },
        lbFone: {
            text: mask.telefone(cliente.A1_DDD.trim() + cliente.A1_TEL.trim()),
        },
        lbDtCompra: {
            attributedString: Alloy.Globals.setNameWithCode(data, 'Ult. compra'),
        },
        status: {
            backgroundColor: status.color
        },
        lbBlock: {
            text: status.block
        },
        properties: {
            data: cliente,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function setEmptyBottom(){
	return {
		template 		: "templateEmpty",
		properties 		: {
			accessoryType 			: Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

async function getClientes(){
    var clientes = [];
    await LocalData.getClientes({
        cliente_id : null,
		success	: function(array){
            clientes = array;
		},
		error	: function(err){
			console.log('DEU ERRADO AO CADASTRAR CLIENTE: ', err);
		}
    });
    await createListView(clientes);
}

async function excluirCliente(cli) {
    var codCli = cli.ID
    $.activityIndicator.show();
    await LocalData.clearTable({table: 'cliente', where: ` WHERE ID = '${codCli}'`});
    await LocalData.clearTable({table: 'cadRef', where: ` WHERE ID_CLIENTE = '${codCli}'`});
    getClientes();
}

$.listView.addEventListener('itemclick', function(e){
    var click = e.section.getItemAt(e.itemIndex);
    var cliente  = click.properties.data;
    if(cliente.A1_MOBILE && !pedido) {
        Alloy.Globals.showAlert({
            title: 'ATENÇÃO',
            message: 'Qual opção deseja executar?',
            // buttons: ['Cancelar','Excluir', 'Editar'],
            buttons: ['Excluir', 'Cancelar', 'Editar',],
            // buttons: [0,1, 2],
            listener: function (e) {
                if(e.index == 0) {
                    // EXCLUIR
                    excluirCliente(cliente);
                }
                else if(e.index == 2) {
                    // EDITAR
                    Alloy.Globals.openWindow('FormCliente', {cliente:cliente});
                }
            }

        })
    }
    else if(pedido) {
        var status = ClientManager.getStatusCli(cliente);
        if(status.status == 1) {
            callback(cliente);
            OrderManager.setStatusCli(true);
            doClose();
        }
        else {
			Alloy.Globals.showAlert({
				title: 'ATENÇÃO',
				message: `Este cliente está bloqueado para pedidos!\nMotivo: ${status.desc}.\nDeseja fazer um orçamento?`,
				// buttons: ['Cancelar','Excluir', 'Editar'],
				buttons: ['Sim', 'Não'],
				// buttons: [0,1, 2],
				listener: function (e) {
					if(e.index == 0) {
                        // ORÇAMENTO
                        callback(cliente);
                        OrderManager.setStatusCli(false);
                        doClose();
					}
					else {
                        // CANCELAR
					}
				}
	
			})
		}
    }
    else {
        Alloy.Globals.openWindow('FormCliente', {cliente:cliente});
    }
});

$.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
});

$.mainWindow.addEventListener("focus", function () {
    $.activityIndicator.show();
    getClientes();
});