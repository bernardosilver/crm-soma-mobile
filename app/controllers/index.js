var refreshData = require("RefreshData");
var LocalData 	= require("LocalData");

Alloy.Globals.openWindowTab = function (screen, params) {
	var controller = Alloy.createController(screen, params);
	if (OS_IOS)
		Alloy.Globals.tabGroup.activeTab.open(controller.getView());
	else
		controller.getView().open({ modal: true });
};

Alloy.Globals.openWindow = function (screen, params) {
	var controller = Alloy.createController(screen, params);
	var currentWindow = null;
	var navigations = Alloy.Globals.Navigations || [];

	var windowNav = (params && params.windowNav) ? params.windowNav : null;
	var navigation = (params && params.navigation) ? params.navigation : null;
	var modal = (params && params.modal) ? params.modal : false;

	if (OS_ANDROID || (!windowNav && !navigation))
		currentWindow = controller.getView();

	if (OS_IOS) {
		if (windowNav) {
			for (var i in navigations) {
				if (navigations[i].controller == windowNav) {
					navigations[i].navigation.openWindow(controller.getView());
					return true;
				}
			}
		} else if (navigation) {
			currentWindow = Titanium.UI.iOS.createNavigationWindow({
				window: controller.getView()
			});
			navigations.push({ navigation: currentWindow, controller: screen });
			Alloy.Globals.Navigations = navigations;
		}
	}

	currentWindow.open({ modal: modal });	
};

Alloy.Globals.openWindowModal = function (screen, params) {
	var controller = Alloy.createController(screen, params);
	var currentWindow = null;
	if (params.navigation == true && OS_IOS) {
		currentWindow = Titanium.UI.iOS.createNavigationWindow({
			window: controller.getView()
		});
		var navigations = Alloy.Globals.Navigations || [];
		navigations.push({ navigation: currentWindow, controller: screen });
		Alloy.Globals.Navigations = navigations;
	} else
		currentWindow = controller.getView();

	currentWindow.open({ modal: true });
};

Alloy.Globals.refreshToken = function () {
	Ti.API.error("[refreshToken]");
	if (Ti.App.Properties.getString("token")) {
		refreshData.sendToken({
			successCallBack: function (data) {
				Ti.API.info("Token cadastrado");
			},
			errorCallBack: function (error) {
				Ti.API.error("Erro ao enviar o token");
			},
			data: {
				usuario_id: Alloy.Globals.isLogged() ? Alloy.Globals.getLocalDataUsuario().id : null,
				token: Ti.App.Properties.getString("token"),
				tipo: OS_ANDROID ? "android" : "ios"
			}
		});
	}
};

function openMainWindow() {
	var MainWindow = Alloy.createController("MainWindow").getView();
	if (OS_IOS)
		MainWindow.open();
	else
		MainWindow.open({ modal: true });
}

Ti.App.addEventListener("uncaughtException", function (e) {
	Ti.API.info(JSON.stringify(e));
});

openMainWindow();
var usuario = Alloy.Globals.getLocalDataUsuario();
if (usuario){
	openMainWindow();
}
else{
	Alloy.Globals.openWindow("Login", { navigation: true });
}