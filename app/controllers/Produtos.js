// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var optionsFilter            = [];
var firstVisibleSectionIndex = 0;
var moment        = require("alloy/moment");
var OrderManager  = require('OrderManager');
var LocalData 	  = require('LocalData');
var Mask 	      = require('Mask');

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar produto",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} 
else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar produto",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

function doClose() {
    $.mainWindow.close();
}

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/cart.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
            Alloy.Globals.openWindow('CadPedido/Cart');
        });
    };
    activity.invalidateOptionsMenu();
}

function onFilter(){
	$.popup.showLabelWithTag(optionsFilter,function(categoria,index){
		var itemIndex 		= OS_ANDROID ? (firstVisibleSectionIndex < index ? 0 : 0) : 0;
		var sectionIndex 	= parseInt(index);
		scrollTo(sectionIndex, itemIndex);
		$.popup.hide();
	},"Toque para filtrar");
}

function scrollTo(sectionIndex, itemIndex){ 
	var options = OS_IOS ? {position: Titanium.UI.iOS.ListViewScrollPosition.TOP, animated:false}  : {animated:false};
	$.listView.scrollToItem(sectionIndex,itemIndex,options);
}

function createListView() {
    var group         = createProdArray();
    var sections      = [];
    var headerSection = null;
    
    for(var i in group) {
        headerSection = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader(group[i][0].DESCCAT)
        });
        headerSection.setItems(createList(group[i]));
        sections.push(headerSection);
        optionsFilter.push(group[i][0].DESCCAT);
    }
    if(sections && sections.length) sections[sections.length - 1].appendItems([setEmptyBottom()]);
    $.listView.setSections(sections);
}

function createProdArray(){
    var tab           = OrderManager.getTabela();
    var condPag       = OrderManager.getPagamento();
    var list          = LocalData.getProds({CODTAB: tab.CJ_TABELA});
    var arrayProd     = [];


    if(condPag.CJ_CONDPAG == '599'){
        arrayProd = [];
        for(var i in list){
            if(list[i].GRUPO.indexOf('EC') > -1){
                arrayProd.push(list[i]);
            }
            else if(list[i].GRUPO.indexOf('PC') > -1){
                arrayProd.push(list[i]);
            }
        }
    }
    else if (condPag.CJ_CONDPAG == '600') {
        arrayProd = [];
        for(var i in list){
            if((list[i].GRUPO.indexOf('EC') == -1) && (list[i].GRUPO.indexOf('PC') == -1)){
                arrayProd.push(list[i]);
            }
        }
    }
    else{
        arrayProd = list;
    }
    
    var produtos = _.sortBy(arrayProd, function(prod) {return prod.DESCR});
    var group    = _.groupBy(produtos, ((prod)=>{return prod.CATEGO}));
    return group;
}

function createList(prods) {
    var array = []
    for(var i in prods) {
        array.push(createItem(prods[i]));
    }
    return array;
}

function createItem(produto) {
    var searchableText = produto.DESCR + produto.CODPRO + produto.DESCCAT;
    return {
        template: 'templateProduto',
        lbProduto: {
            attributedString: Alloy.Globals.setNameWithCode(produto.DESCR, produto.CODPRO),
        },
        properties: {
            data: produto,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function setEmptyBottom(){
	return {
		template 		: "templateEmpty",
		properties 		: {
			accessoryType 			: Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

function generateProdInfo(prod){
    // let prcVnd = 0;
    let aux      = 0;
    let dif      = 0;
    let fc       = 0;
    const condPag  = OrderManager.getPagamento();
    const parcelas = OrderManager.getParcela();
    const dtprv    = moment().hours(0).minutes(0).seconds(0).milliseconds(0)
    dtprv.add(5, 'days');
    let dtvenc   = '';
    let obj = {
        PRVEN : prod.PRVEN,
        CONDPG : condPag.CJ_CONDPAG,
        ACRSVEN : condPag.ACRSVEN,
        DESCVEN : condPag.DESCVEN,
        FATORCONDPG : condPag.FATORCONDPG,
        prcVnd  : 0,
        prcUni  : 0,
        desconto : OrderManager.getDesconto() 

    }
    if(obj.CONDPG == '003') {
        // for(var i in parcelas) {
        //     if(i.indexOf('CJ_DATA') > -1 && parcelas[i]) {
        //         aux += 1;
        //         dtvenc = moment(parcelas[i]);
        //         dif = dtvenc.diff(dtprv,'days'); // diferenca de dias entre a data de carregamento e cada data de vencimento das parcelas.
        //         fc += ((dif-7)*(0.0007));//tabela 7 dias
        //     }
        // }
        // var media = fc/aux;

        obj.prcVnd = (obj.PRVEN * parseFloat(obj.FATORCONDPG)) + obj.PRVEN;
        obj.prcVnd.toFixed(2);
    }
    else{
        if(obj.ACRSVEN) {
            obj.prcVnd = obj.PRVEN + (obj.PRVEN * obj.ACRSVEN/100);
        }
        else if(obj.DESCVEN) {
            obj.prcVnd = obj.PRVEN - (obj.PRVEN * obj.DESCVEN/100);
        }
        else {
            obj.prcVnd = obj.PRVEN;
        }
    }
    obj.prcVnd.toFixed(2);
    return obj; 
}

function updateViewCart() {
    var cart  = OrderManager.getTotalPedido();
    var frete = OrderManager.getFrete();
    var saldo = OrderManager.getSaldo();
    var lbPrd = `${cart.qntItens} Itens`;
    var lbCredito = `Créd. restante: R$${(saldo.restante)}`;
    var lbTotal   = frete.CJ_TPFRETE == 'C' ? `Total pedido + frete: R$${Mask.real(parseFloat(cart.valor).toFixed(2))}` : `Total pedido s/ frete: R$${Mask.real(parseFloat(cart.totalProd).toFixed(2))}`
    $.produtos.setText(lbPrd);
    $.credito.setText(lbCredito);
    $.total.setText(lbTotal);
}

function encontrouProd(produto) {
    const produtos = OrderManager.getItems();
    const encontrou = _.find(produtos, function(prod){ 
        return prod.CODPRO == produto.CODPRO
    });
    if (encontrou) return true;
    else return false;
}

$.mainWindow.addEventListener("focus", function () {
    updateViewCart();
});

$.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
    createListView();
    updateViewCart();
});

$.viewCart.addEventListener("click", function (e) {
    Alloy.Globals.openWindow('CadPedido/Cart');
});

$.listView.addEventListener('itemclick', function(e){
    const click = e.section.getItemAt(e.itemIndex);
    const produto  = click.properties.data;
    const info = generateProdInfo(produto);
    $.popup.showPopUpAddItem(produto, info, function(values, prod){
        if(encontrouProd(produto)) {
            $.popup.hide();
            return Alloy.Globals.showAlert('Ops!', 'Este produto já está inserido no pedido!');
        }
        else{
            OrderManager.insertItems(values, prod);
            if(prod.CODPRO.indexOf('501') == 0 && values.desc != 0) {
                Alloy.Globals.showAlert('Atenção', 'Desconto além do permitido!');
                OrderManager.setStatusDesc(0);
            }
            else if(values.desc < -4 || values.desc > 6) {
                Alloy.Globals.showAlert('Atenção', 'Desconto além do permitido!');
                OrderManager.setStatusDesc(0);
            }
            else OrderManager.setStatusDesc(1);
            if(OrderManager.getSaldo().restante < 0) {
                Alloy.Globals.showAlert('Atenção', 'Crédito do cliente insuficiente. O pedido será inserido como orçamento!');
                OrderManager.setStatusCred(0);
            }
            else OrderManager.setStatusCred(1);
            $.popup.hide();
            updateViewCart();
        }
    });
});