// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args     = $.args;
var callback = args.callback;
var Mask     = require("Mask");
var OrderManager  = require('OrderManager');

const doClose = (() => {
    $.mainWindow.close();
});

const createMenuAndroid = (() => {
	if(OS_ANDROID){
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", (()=> {
                var obs = Mask.removeAcentos($.tfReferencia.getValue().trim().toUpperCase());
                callback(obs);
                doClose();
			}));
		};
		activity.invalidateOptionsMenu();
	}
});

$.mainWindow.addEventListener("open", function(e){
	var obs = OrderManager.getObs();
	if(obs) {
		$.tfReferencia.setValue(obs)
	}
	createMenuAndroid();
});