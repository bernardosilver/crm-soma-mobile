// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args      = $.args;
var pedido    = args.pedido || false;
var callback  = args.callback || (() =>{});
var usuario   = Alloy.Globals.getLocalDataUsuario().data;
var LocalData = require('LocalData');

const doClose = (() => {
    $.mainWindow.close();
});

async function getTabelas(){
    $.activityIndicator.show();
    var tabelas = await LocalData.getTabelas({vend: usuario.USR_LOGIN});
    createListView(tabelas);
    $.activityIndicator.hide();
}

function createListView(array) {
    var section = Ti.UI.createListSection();
    var itens   = [];
    for(var i in array) {
        itens.push(createItemList(array[i]));
    }
    section.setItems(itens);
    $.listView.setSections([section]);
}

function createItemList(item) {
    var searchableText = item.Filial + item.CodTab + item.Descr;
    return {
        template: 'templateTabela',
        lbDesc : {
            text: item.Descr + ' - Filial: ' + item.Filial
        },
        properties: {
            data: item,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    }
}

$.mainWindow.addEventListener("itemclick", function (e) {
    var click = e.section.getItemAt(e.itemIndex);
    var tabela  = click.properties.data;
    if(pedido) {
        callback(tabela);
        doClose();
    }
});

$.mainWindow.addEventListener("focus", function () {
    getTabelas();
});
