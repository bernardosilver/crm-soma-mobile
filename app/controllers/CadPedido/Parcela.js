// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args      = $.args;
var dados 	  = args.dados || null;
var callback 	= args.callback || null;
var parcelas  = [];
var mask 	    = require('Mask');
var moment    = require("alloy/moment");
var OrderManager  = require('OrderManager');


const doClose = (() =>{
 	$.mainWindow.close();
});

const doSave = (() =>{
	var parc = [];
	parc.push($.tfParcela.getValue());
	for(var i in parcelas){
		parc.push(parcelas[i].getValues().parcela);
	}
	callback(parc);
	doClose();
});


const createMenuAndroid = (() =>{
	if(OS_ANDROID) {
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", doSave);
		};
		activity.invalidateOptionsMenu();
	}
});


const clickViewParcela = (data => {
	if(parcelas.length >= 4){
		Alloy.Globals.showAlert("Ops","É possível adicionar apenas 5 parcelas!");
		return false;
  }
  var ult = getUltimaParcela();
  if(!ult) return Alloy.Globals.showAlert('Ops!', 'É necessário inserir uma data para a parcela anterior');
	var parcela = Alloy.createWidget('CadParcela', 'widget', {activityIndicator:$.activityIndicator, ultima: ult, data: data, editable: dados ? false : true});
	$.viewAddParcela.add(parcela.getView());

	parcelas.push(parcela);

	parcela.setFocus();

	parcela.setActionApagar(function(){
		$.viewAddParcela.remove(parcela.getView());
		for(var i in parcelas){
			if(parcelas[i] == parcela){
				parcelas.splice(i,1);
				break;
			}
		}
	});
});

const getUltimaParcela = (() => {
	var tam = parcelas.length;
	var parc = null;
	if(parcelas.length)
		parc = parcelas[tam-1].getValues().parcela;
	else
		parc = $.tfParcela.getValue();
	return parc;
});

const showPicker = (() =>{
	var anoAtual = moment().year();
	var mesAtual = moment().month();
	var diaAtual = moment().add(6, 'days').date();

	// CALCULA PREVISAO DE CARREGAMENTO
	var maxDate  = moment().add(3, 'd');
	if(maxDate.day() == 0) {
		maxDate = moment().add(4, 'd');
	}
	else if(maxDate.day() == 6) {
		maxDate = moment().add(5, 'd');
	}

	maxDate      = moment().add(65, 'd');
	var maxAno   = moment(maxDate).year();
	var maxMes   = moment(maxDate).month();
	var maxDia   = moment(maxDate).date();

	var value    = moment().add(3, 'days');
	var valueAno = moment(value).year();
	var valueMes = moment(value).month();
	var valueDia = moment(value).date();

  	var picker = Ti.UI.createPicker({
		type:Ti.UI.PICKER_TYPE_DATE,
		locale: 'pt_BR',
		minDate:new Date(anoAtual,mesAtual,diaAtual),
		maxDate:new Date(maxAno,maxMes,maxDia),
		value:new Date(valueAno,valueMes,valueDia)
 	 });

  picker.showDatePickerDialog({
		value: new Date(valueAno,valueMes,valueDia),
		callback: function(e) {
			if (e.cancel) {
				Ti.API.info('User canceled dialog');
			} else {
				Ti.API.info('User selected date: ' + moment(e.value).weekday());
				$.tfParcela.setValue(moment(e.value).format('DD/MM/YYYY'));
				removeParcelas();
			}
		}
  });
})

const removeParcelas = (() => {
	for(var i in parcelas) {
		$.viewAddParcela.remove(parcelas[i].getView());
	}
	parcelas = [];
});

const setParcelas = ((parc) => {
	const array = [];
	if(parc.CJ_DATA1){ 
		//array.push(parc.CJ_DATA1);
		$.tfParcela.setValue(moment(parc.CJ_DATA1).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA2){
		array.push(parc.CJ_DATA2);
		clickViewParcela(moment(parc.CJ_DATA2).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA3){
		array.push(parc.CJ_DATA3);
		clickViewParcela(moment(parc.CJ_DATA3).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA4){
		array.push(parc.CJ_DATA4);
		clickViewParcela(moment(parc.CJ_DATA4).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA5){
		array.push(parc.CJ_DATA5);
		clickViewParcela(moment(parc.CJ_DATA5).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA6){
		array.push(parc.CJ_DATA6);
		clickViewParcela(moment(parc.CJ_DATA6).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA7){
		array.push(parc.CJ_DATA7);
		clickViewParcela(moment(parc.CJ_DATA7).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA8){
		array.push(parc.CJ_DATA8);
		clickViewParcela(moment(parc.CJ_DATA8).format('DD/MM/YYYY'));
	}
	if(parc.CJ_DATA9){
		array.push(parc.CJ_DATA9);
		clickViewParcela(moment(parc.CJ_DATA9).format('DD/MM/YYYY'));
	}

});

$.mainWindow.addEventListener("open",function(e){
	const datas = OrderManager.getParcela();
	createMenuAndroid();
	if(datas.CJ_DATA1){
		setParcelas(datas);
	}
  
});

$.addParcela.addEventListener("click", function(e){
	if(dados) return true;
	clickViewParcela();
});

$.tfParcela.addEventListener("focus", function (e) {
    showPicker();
});

$.tfParcela.addEventListener("click", function (e) {
    showPicker();
});
