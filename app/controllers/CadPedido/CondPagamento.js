// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args      = $.args;
var pedido    = args.pedido || false;
var callback  = args.callback || (() =>{});
var usuario   = Alloy.Globals.getLocalDataUsuario().data;
var LocalData = require('LocalData');

var OrderManager  = require('OrderManager');
var cliente       = OrderManager.getCliente();

const doClose = (() => {
    $.mainWindow.close();
});

async function getCondPag(){
    $.activityIndicator.show();
    var condPag = []
    if(!cliente.A1_COD){
        await LocalData.getCadCondPag({
            cliente_id: cliente.CLIENTE_ID,
            success: function(response) {
                condPag.push(response);
            },
            error: function(err){
                console.log('ERRO AO BUSCAR CONDIÇÃO DE PAGAMENTO: ', err);
            }
        });
        // condPag = await LocalData.getCondPag({cliente_id: cliente.CLIENTE_ID});
        // BUSCAR CONDICAO DE PAGAMENTO LOCAL
    }
    else{
        if(cliente.A1_GRPVEN.trim()) {
            condPag = await LocalData.getCondPag({GRPVEN: cliente.A1_GRPVEN});
        }
        else {
            condPag = await LocalData.getCondPag({A1_COD: cliente.A1_COD});
        }
    }
    createListView(condPag);
    $.activityIndicator.hide();
}

function createListView(array) {
    var section = Ti.UI.createListSection();
    var itens   = [];
    for(var i in array) {
        itens.push(createItemList(array[i]));
    }
    section.setItems(itens);
    $.listView.setSections([section]);
}

function createItemList(item) {
    return {
        template: 'templateForma',
        lbDesc : {
            text: item.DESCRI
        },
        properties: {
            data: item,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
        }
    }
}

$.mainWindow.addEventListener("itemclick", function (e) {
    var click = e.section.getItemAt(e.itemIndex);
    var condPag  = click.properties.data;
    if(pedido) {
        callback(condPag);
        doClose();
    }
});

$.mainWindow.addEventListener("focus", function () {
    getCondPag();
});
