// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args 	   = $.args;
var tipo_frete = 'F';
var callback   = args.callback;
var Mask      = require("Mask");

const doClose = (() => {
    $.mainWindow.close();
});

const createMenuAndroid = (() => {
	if(OS_ANDROID){
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", (()=> {
				validateFrete();
			}));
		};
		activity.invalidateOptionsMenu();
	}
});

const validateFrete = (() => {
	var frete = {
		CJ_TPFRETE: tipo_frete,
		CJ_FRTBAST: $.tfFrete.getValue()
	}
	
	callback(frete);
	doClose();
});

const checkFrete = (value => {
	$.checkOnFob.setVisible(value == 'F' ? true : false);
	$.checkOffFob.setVisible(value == 'F' ? false : true);
	$.checkOnCif.setVisible(value == 'F' ? false : true);
	$.checkOffCif.setVisible(value == 'F' ? true : false);

	$.tfFrete.setHintText(value == 'F' ? '0,00' : '0,00');
	// if(value == 'F') {
	// 	$.tfFrete.setEditable(false);
	// }
	// else { 
	// 	$.tfFrete.setEditable(false);
	// }
	$.tfFrete.setValue('0,00');
});

$.viewFob.addEventListener('click', function(e) {
	// if(dados) return true;
	tipo_frete = 'F';
	checkFrete(tipo_frete);
});

$.viewCif.addEventListener('click', function(e) {
	// if(dados) return true;
	tipo_frete = 'C';
	checkFrete(tipo_frete);
});

$.tfFrete.addEventListener("touchend", function(e){
	$.tfFrete.setValue('');
});

$.tfFrete.addEventListener("change", function(e){
	$.tfFrete.setValue(Mask.real(e.source.value));
});

$.mainWindow.addEventListener("open", function(e){
	createMenuAndroid();
});