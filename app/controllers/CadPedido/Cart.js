// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var moment        = require("alloy/moment");
var OrderManager  = require('OrderManager');
var LocalData 	  = require('LocalData');
var Mask 	      = require('Mask');


function doClose() {
    $.mainWindow.close();
}

function createResumo() {
    var frete = OrderManager.getFrete();
    var produtos = OrderManager.getItems();
    var total    = OrderManager.getTotalPedido();

    $.lbQnt.setText(`${total.qntItens} item(s)`);
    $.lbPreco.setText(`R$${Mask.real(parseFloat(total.totalProd).toFixed(2))}`);
    $.lbPeso.setText(`${parseFloat(total.pesoTotal).toFixed(2)}`);
    
    if(frete.CJ_TPFRETE == 'C'){
        $.lbFrete.setText(`CIF R$${Mask.real(parseFloat(total.valorFret).toFixed(2))}`);
        $.lbTotal.setText(`Total pedido + frete: R$${Mask.real(parseFloat(total.valor).toFixed(2))}`);
    }
    else {
        $.lbFrete.setText(`FOB R$${Mask.real(parseFloat(total.valorFret).toFixed(2))}`);
        $.lbTotal.setText(`Total pedido s/ frete: R$${Mask.real(parseFloat(total.totalProd).toFixed(2))}`);
    }

    if(produtos && produtos.length) createListView(produtos);
    else $.listView.deleteSectionAt(0);
}


function createListView(produtos) {
    var sections      = [];
    var headerSection = Ti.UI.createListSection();
    headerSection.setItems(createList(produtos));
    sections.push(headerSection);

    $.listView.replaceSectionAt(0,sections); 
    // $.listView.setSections(sections);
}

function createList(prods) {
    var array = []
    for(var i in prods) {
        array.push(createItem(prods[i]));
    }
    return array;
}

function createItem(produto) {
    return {
        template: 'templateProduto',
        lbNome: {
            attributedString: Alloy.Globals.setNameWithCode(produto.DESCR, produto.CODPRO),
        },
        lbQtd: {
            text: produto.CK_QTDVEN + ' x '
        },
        lbPreco: {
            text: `R$${Mask.real(parseFloat(produto.CK_VALOR).toFixed(2))}`
        },
        properties: {
            data: produto,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_NONE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
        }
    };
}

function generateProdInfo(prod){
    // let prcVnd = 0;
    let aux      = 0;
    let dif      = 0;
    let fc       = 0;
    const condPag  = OrderManager.getPagamento();
    const parcelas = OrderManager.getParcela();
    const dtprv    = moment().hours(0).minutes(0).seconds(0).milliseconds(0)
    dtprv.add(5, 'days');
    let dtvenc   = '';
    let obj = {
        PRVEN : prod.PRVEN,
        CONDPG : condPag.CJ_CONDPAG,
        ACRSVEN : condPag.ACRSVEN,
        DESCVEN : condPag.DESCVEN,
        prcVnd  : 0,
        prcUni  : 0,
        desconto : OrderManager.getDesconto() 

    }
    
    if(obj.CONDPG == '003') {
        for(var i in parcelas) {
            if(i.indexOf('CJ_DATA') > -1 && parcelas[i]) {
                aux += 1;
                dtvenc = moment(parcelas[i]);
                dif = dtvenc.diff(dtprv,'days'); // diferenca de dias entre a data de carregamento e cada data de vencimento das parcelas.
                fc += ((dif-7)*(0.0007));//tabela 7 dias
            }
        }
        var media = fc/aux;
        obj.prcVnd = (obj.PRVEN * parseFloat(media)) + obj.PRVEN;
        obj.prcVnd.toFixed(2);
    }
    else{
        if(obj.ACRSVEN) {
            obj.prcVnd = obj.PRVEN + (obj.PRVEN * obj.ACRSVEN/100);
        }
        else if(obj.DESCVEN) {
            obj.prcVnd = obj.PRVEN - (obj.PRVEN * obj.DESCVEN/100);
        }
        else {
            obj.prcVnd = obj.PRVEN;
        }
    }
    obj.prcVnd.toFixed(2);
    return obj; 
}

$.mainWindow.addEventListener('focus', function(e){
    createResumo();
});

$.listView.addEventListener('itemclick', function(e){
    const click = e.section.getItemAt(e.itemIndex);
    const produto  = click.properties.data;
    const info = generateProdInfo(produto);

    Alloy.Globals.showAlert({
        title: 'ATENÇÃO',
        message: 'Qual opção deseja executar?',
        // buttons: ['Cancelar','Excluir', 'Editar'],
        buttons: ['Excluir', 'Cancelar', 'Alterar',],
        // buttons: [0,1, 2],
        listener: function (e) {
            if(e.index == 0) {
                // EXCLUIR
                OrderManager.excluirProduto(produto);
                createResumo();
            }
            else if(e.index == 2) {
                // ALTERAR
                $.popup.showPopUpAddItem(produto, info, function(values, prod){
                    if(prod.CODPRO.indexOf('501') == 0 && values.desc != 0) {
                        Alloy.Globals.showAlert('Atenção', 'Desconto além do permitido!');
                        OrderManager.setStatusDesc(0);
                    }
                    else if(values.desc < -4 || values.desc > 6) {
                        Alloy.Globals.showAlert('Atenção', 'Desconto além do permitido!');
                        OrderManager.setStatusDesc(0);
                    }
                    else OrderManager.setStatusDesc(1);
                    if(OrderManager.getSaldo().restante < 0) {
                        Alloy.Globals.showAlert('Atenção', 'Crédito do cliente insuficiente. O pedido será inserido como orçamento!');
                        OrderManager.setStatusCred(0);
                    }
                    else OrderManager.setStatusCred(1);
                    $.popup.hide();
                    OrderManager.updateItem(values, prod);
                    // updateViewCart();
                    createResumo();
                });
            }
        }
    })
    
});


