// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args      = $.args;
var condPag    = args.condPag || false;
var callback  = args.callback || (() =>{});
var LocalData = require('LocalData');

var callback = args.callback || function() {};
var cred     = args.cred || null;
var ClientManager = require('ClientManager');

var OrderManager  = require('OrderManager');
var cliente       = OrderManager.getCliente();

const doClose = (() => {
    $.mainWindow.close();
});

async function getCondPag(){
    $.activityIndicator.show();
    // var condPag = await LocalData.getCondPag({A1_COD: cliente.A1_COD});
    var condPag = [
		{
			TIPO       : "1",
			FATORVD    : 0.9951,
			DESCRI     : "DEPOSITO ANTECIP.",
			CONDPG     : "009",
			DTSYNC     : "2019-06-26 16:04:39",
			E4_DESCVEN : 0.49,
			COND       : "0",
			E4_ACRSVEN : 0
		},
        {
          TIPO       : "1",
          FATORVD    : 1,
          DESCRI     : "7",
          CONDPG     : "001",
          DTSYNC     : "2019-02-15 09:31:39",
          E4_DESCVEN : 0,
          COND       : "7",
          E4_ACRSVEN : 0
		},
		{
			TIPO       : "1",
			FATORVD    : 1.0049,
			DESCRI     : "14",
			CONDPG     : "007",
			DTSYNC     : "2019-02-15 09:31:39",
			E4_DESCVEN : 0,
			COND       : "14",
			E4_ACRSVEN : 0.49
		},
		{
			TIPO       : "1",
			FATORVD    : 1.0098,
			DESCRI     : "21",
			CONDPG     : "030",
			DTSYNC     : "2019-02-15 09:31:39",
			E4_DESCVEN : 0,
			COND       : "21",
			E4_ACRSVEN : 0.98
		},
        {
          TIPO       : "1",
          FATORVD    : 1.0147,
          DESCRI     : "28",
          CONDPG     : "002",
          DTSYNC     : "2019-02-15 09:31:39",
          E4_DESCVEN : 0,
          COND       : "28",
          E4_ACRSVEN : 1.47
        },
        {
          TIPO       : "1",
          FATORVD    : 1.0245,
          DESCRI     : "42",
          CONDPG     : "008",
          DTSYNC     : "2019-02-15 09:31:39",
          E4_DESCVEN : 0,
          COND       : "42",
          E4_ACRSVEN : 2.45
        },
        {
          TIPO       : "1",
          FATORVD    : 1.0343,
          DESCRI     : "56",
          CONDPG     : "010",
          DTSYNC     : "2019-02-15 09:31:39",
          E4_DESCVEN : 0,
          COND       : "56",
          E4_ACRSVEN : 3.43
        }
      ]
    createListView(condPag);
    $.activityIndicator.hide();
}

function createListView(array) {
    var section = Ti.UI.createListSection();
    var itens   = [];
    for(var i in array) {
        itens.push(createItemList(array[i]));
    }
    section.setItems(itens);
    $.listView.setSections([section]);
}

function createItemList(item) {
    return {
        template: 'templateForma',
        lbDesc : {
            text: item.DESCRI
        },
        properties: {
            data: item,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
        }
    }
}

$.mainWindow.addEventListener("itemclick", function (e) {
    var click = e.section.getItemAt(e.itemIndex);
    var cond  = click.properties.data;
    if(cond) {
		ClientManager.setCondPag(cond);
		callback(cond);
		doClose();
    }
});

$.mainWindow.addEventListener("focus", function () {
    getCondPag();
});
