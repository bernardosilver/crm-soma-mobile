// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args          = $.args;
var refs          = args.refs;
var callback 	  = args.callback || function(){};
var mask 	      = require('Mask');
var referencias   = [];
var formularios   = [];
var ClientManager = require('ClientManager');


function doClose(e){
	$.mainWindow.close();
}

function doSave(e){
	$.activityIndicator.show();
	var message = validate();
	if(message){
		Alloy.Globals.showAlert("Ops!","É necessário informar "+message.name+"!");
		if(message.field) message.field.focus();
		$.activityIndicator.hide();
		return false;
    }
	getValues();
	ClientManager.setReferenciasCom(referencias);
	callback(referencias);
    doClose();
}

function validate() {
	var nome  	 = mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase();
	var telefone = mask.removeTelMask($.tfTelefone.getValue());
	var cidadeUF = mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase();
	var contato  = mask.removeCaracter($.tfContato.getValue()).toUpperCase();
	if(!!!nome.match(/[aA-zZ]*/) || nome.length < 4){
		return {field: $.tfEmpresa, name: 'o nome da empresa'};
	}
    if(telefone.trim() == '' || telefone.length < 10){
		return {field: $.tfTelefone, name: 'o Telefone'};
	}
    if(cidadeUF.trim() == '' || cidadeUF.length < 5){
		return {field: $.tfCidadeUF, name: 'a Cidade e UF'};
	}
    if(contato.trim() == '' || contato.length < 4){
		return {field: $.tfContato, name: 'o Contato'};
	}
	if(formularios.length < 2) {
		return {name: 'outra referência adicional'};
	}
	else if(formularios.length >= 2){
		var count = 0;
		var obj = {};
		var nome  	 = '';
		var telefone = '';
		var cidadeUF = '';
		var contato  = '';
		for(var i= 0; i<formularios.length; i++){
			var count = parseInt(i);
			var obj = formularios[count].getValues();
			var nome  	 = mask.removeCaracter(obj.nome).toUpperCase();
			var telefone = mask.removeTelMask(obj.telefone);
			var cidadeUF = mask.removeCaracter(obj.cidadeUF).toUpperCase();
			var contato  = mask.removeCaracter(obj.contato).toUpperCase();
			if(!!!nome.match(/[aA-zZ]*/) || nome.length < 4){
				return {name: `nome da empresa da ${count+2}ª referência`};
			}
			if(telefone.trim() == '' || telefone.length < 10){
				return {name: `Telefone da ${count+2}ª referência`};
			}
			if(cidadeUF.trim() == '' || cidadeUF.length < 5){
				return {name: `Cidade e UF da ${count+2}ª referência`};
			}
			if(contato.trim() == '' || contato.length < 5){
				return {name: `Contato da ${count+2}ª referência`};
			}
		}
	}
}

function getValues() {
	referencias = [];
	referencias.push({
		AO_NOMINS	: mask.removeCaracter($.tfEmpresa.getValue()).toUpperCase(),
		AO_TELEFON	: mask.removeTelMask($.tfTelefone.getValue()),
		AO_CONTATO	: mask.removeCaracter($.tfContato.getValue()).toUpperCase(),
		AO_OBSERV 	: mask.removeCaracter($.tfCidadeUF.getValue()).toUpperCase(),
		ID: $.lbId.getText()
	});
	for(var i= 0; i<formularios.length; i++){
		var count = parseInt(i);
		var obj = formularios[count].getValues();
		referencias.push({
			AO_NOMINS	: mask.removeCaracter(obj.nome).toUpperCase(),
			AO_TELEFON	: mask.removeTelMask(obj.telefone),
			AO_CONTATO	: mask.removeCaracter(obj.contato).toUpperCase(),
			AO_OBSERV 	: mask.removeCaracter(obj.cidadeUF).toUpperCase(),
			ID: obj.ID
		});
	}
}

function setValues(obj) {
	referencias = obj ? obj : ClientManager.getReferenciasCom();
	if(referencias.length){
		$.tfEmpresa.setValue(referencias[0].AO_NOMINS);
		$.tfTelefone.setValue(mask.tel(referencias[0].AO_TELEFON));
		$.tfContato.setValue(referencias[0].AO_CONTATO);
		$.tfCidadeUF.setValue(referencias[0].AO_OBSERV);
		$.lbId.setText(referencias[0].ID);
	}
	if(referencias.length > 1) {
		for(var i= 1; i<referencias.length; i++){
			var count = parseInt(i);
			clickViewRefComercial(referencias[count]);
		}
	}
}

function createMenuAndroid(){
	if(OS_ANDROID){
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", doSave);
		};
		activity.invalidateOptionsMenu();
	}
}

function clickViewRefComercial(obj){
	if(formularios.length >= 5){
		Alloy.Globals.showAlert("Ops","É possível adicionar apenas 6 referencias!");
		return false;
	}
	var referencia = Alloy.createWidget('CadRefComercial', 'widget', {
		activityIndicator:$.activityIndicator, 
		obgt: formularios.length > 1 ? false : true,
		ref: obj, 
		editable: refs ? false : true
	});
	$.viewAddReferencia.add(referencia.getView());

	formularios.push(referencia);

	referencia.setFocus();

	referencia.setActionApagar(function(){
		$.viewAddReferencia.remove(referencia.getView());
		for(var i in formularios){
			if(formularios[i] == referencia){
				formularios.splice(i,1);
				break;
			}
		}
	});
}

function setEditable(bool) {
	$.tfEmpresa.setEditable(bool);
	$.tfTelefone.setEditable(bool);
	$.tfContato.setEditable(bool);
	$.tfCidadeUF.setEditable(bool);
}

$.mainWindow.addEventListener("open",function(e){
	// setValues();
	// if(OS_ANDROID)
	// 	createMenuAndroid();

	if(!refs){
		setValues();
		setEditable(true);
		createMenuAndroid();
	}
	else {
		setValues(refs)
		setEditable(false);
	}
});

$.viewReferencia.addEventListener("click", function(e){
	if(refs) return true;
	clickViewRefComercial();
});

$.tfTelefone.addEventListener("change", function (e) {
    var v = mask.tel(e.value);
    if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
    $.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
});
