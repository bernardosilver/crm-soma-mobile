// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args          = $.args;
var uf_id         = args.uf_id;
var callback      = args.callback || function(){};
var mask 		  = require('Mask');
var LocalData     = require('LocalData');
var ClientManager = require('ClientManager');

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar cidade",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar cidade",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

var ufs  = [];
function doClose(e){
	$.mainWindow.close();
}

function createListView(array) {
    var section     = Ti.UI.createListSection();
    var listCidades = [];
    var cidades     = _.sortBy(array, function(uf){
        return uf.MNC_MUN;
    });
    for(var i in cidades) {
        listCidades.push(createItemList(cidades[i]));
    }
    section.setItems(listCidades);
    $.listView.setSections([section]);
}

function createItemList(cidade) {
    var searchableText = cidade.MNC_CODMUN + mask.removeCaracter(cidade.MNC_MUN) + cidade.MNC_CODMC;
    return {
        template: 'templateCidades',
        lbCidade: {
            text: mask.removeCaracter(cidade.MNC_MUN).toUpperCase()
        },
        properties: {
            data: cidade,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function selectCidade(cidade) {
    callback(cidade);
    doClose();
}

async function getMnc(){
    ufs = await LocalData.getMnc({coduf: uf_id});
    await createListView(ufs);
}

$.mainWindow.addEventListener("open",function(e){
    getMnc();
});

$.listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    var data = item.properties.data;
    selectCidade(data);
});

