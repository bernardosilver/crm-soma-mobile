// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args        = $.args;
var dados 	    = args.dados || null;
var cliente 	= null;
var callback 	= args.callback || function(){};
var mask 	    = require('Mask');
var moment      = require("alloy/moment");
var telefones   = [];
var celulares   = [];
var fields 		= [];
var tipo_pessoa = 'F';
var me 			= '1';

var ClientManager = require('ClientManager');

fields.push({field: $.tfNome, name: 'Nome completo'});
fields.push({field: $.tfFantasia, name: 'Nome fantasia'});
fields.push({field: $.tfTelefone, name: 'Telefone'});
fields.push({field: $.tfEmail, name: 'E-mail'});
fields.push({field: $.tfCpfCnpj, name: 'CPF ou CNPJ'});
fields.push({field: $.tfIpr, name: 'Inscrição Estadual'});
fields.push({field: $.tfNasc, name: 'Data de nascimento'});


function doClose(e){
	$.mainWindow.close();
}

function doSave(e){
	$.activityIndicator.show();
	if(ClientManager.validateCGC(mask.removeCPFCNPJMask($.tfCpfCnpj.getValue()), 'dado')){
		Alloy.Globals.showAlert("Ops!","Você não pode cadastrar o mesmo CPF para o cliente e o sócio!");
		$.activityIndicator.hide();
		return false;
	}
	var message = validate();
	if(message){
		Alloy.Globals.showAlert("Ops!","É necessário informar o(a) "+message.name+"!");
		if(message.field) message.field.focus();
		$.activityIndicator.hide();
		return false;
    }
	getValues();
	ClientManager.setDadosPessoais(cliente);
	callback(cliente);
    doClose();
}

function validate() {
	var nome  = mask.removeCaracter($.tfNome.getValue());
	var razao =  mask.removeCaracter($.tfFantasia.getValue());
	var telefone = mask.removeTelMask($.tfTelefone.getValue());
    if(!!!nome.match(/[A-Z][a-z]* [A-Z][a-z]*/) || nome.length < 4){
		return {field: $.tfNome, name: 'Nome'};
    }
    if(!!!razao.match(/[A-Z][a-z]*/) || razao.length < 4){
		return {field: $.tfFantasia, name: 'Nome fantasia'};
	}
    if(telefone.trim() == '' || telefone.length < 10){
		return {field: $.tfTelefone, name: 'Telefone'};
	}
	if(telefones[0] && (mask.removeTelMask(telefones[0].getValues().telefone).length > 0) && (mask.removeTelMask(telefones[0].getValues().telefone).length < 10)){
		return {name: 'segundo telefone'};
	}
	if(telefones[1] && (mask.removeTelMask(telefones[1].getValues().telefone).length > 0) && (mask.removeTelMask(telefones[1].getValues().telefone).length < 10)){
		return {name: 'terceiro telefone'};
	}
	if(telefones[2] && (mask.removeTelMask(telefones[2].getValues().telefone).length > 0) && (mask.removeTelMask(telefones[2].getValues().telefone).length < 10)){
		return {name: 'quarto telefone'};
	}
	if (!validaEmail($.tfEmail.getValue())) {
		return {field: $.tfEmail, name: 'Email'};
	}
	if(tipo_pessoa == 'F' && !validaCPF($.tfCpfCnpj.getValue())) {
		return {field: $.tfCpfCnpj, name: 'CPF'};
	}
	if(tipo_pessoa == 'J' && !validaCNPJ($.tfCpfCnpj.getValue())) {
		return {field: $.tfCpfCnpj, name: 'CNPJ'};
	}
	if(mask.removeCaracter($.tfIpr.getValue()).trim() == '') {
		return {field: $.tfIpr, name: 'Inscrição Estadual'};
	}
	if(!(moment($.tfNasc.getValue(), 'DDMMYYYY').utc()).isValid()){
		return {field: $.tfNasc, name: 'Data de nascimento'};
	}
	
	return null;
}

function getValues() {
	cliente = {};
	cliente.A1_NOME 	= mask.removeCaracter($.tfNome.getValue()).toUpperCase();
	cliente.A1_NREDUZ	= mask.removeCaracter($.tfFantasia.getValue()).toUpperCase();
	cliente.A1_DDD 		= $.tfTelefone.getValue().slice(1,3);
	cliente.A1_TEL 		= $.tfTelefone.getValue().slice(4).replace('-','');
	cliente.A1_TEL2		= telefones.length >= 1 ? mask.removeTelMask(telefones[0].getValues().telefone) : null;
	cliente.A1_TEL3		= telefones.length >= 2 ? mask.removeTelMask(telefones[1].getValues().telefone) : null;
	cliente.A1_TEL4		= telefones.length >= 3 ? mask.removeTelMask(telefones[2].getValues().telefone) : null;
	cliente.A1_EMAIL	= $.tfEmail.getValue().toLowerCase();
	cliente.A1_PESSOA 	= tipo_pessoa; // F ou J
	cliente.A1_CGC		= mask.removeCPFCNPJMask($.tfCpfCnpj.getValue());
	cliente.A1_INSCR	= $.tfIpr.getValue().toUpperCase();
	cliente.A1_PFISICA	= $.tfRG.getValue().toUpperCase();
	cliente.A1_DTNASC	= moment($.tfNasc.getValue(), 'DD/MM/YYYY').format('YYYYMMDD'); //YYYYMMDD
	cliente.A1_ME 		= me;
	cliente.A1_OBSRC	= mask.removeAcentos($.tfObs.getValue().toUpperCase());
}

function setValues(obj) {
	cliente = {};
	cliente = obj ? obj : ClientManager.getDadosPessoais();
	me 		= cliente.A1_ME;
	tipo_pessoa = cliente.A1_PESSOA;

	checkME(me);
	checkPessoa(tipo_pessoa);

	$.tfNome.setValue(cliente.A1_NOME);
	$.tfFantasia.setValue(cliente.A1_NREDUZ);
	$.tfEmail.setValue(cliente.A1_EMAIL);
	$.tfIpr.setValue(cliente.A1_INSCR);
	$.tfRG.setValue(cliente.A1_PFISICA);
	cliente.A1_DTNASC ? $.tfNasc.setValue(moment(cliente.A1_DTNASC, 'YYYYMMDD').format('DD/MM/YYYY')) : ''; //YYYYMMDD
	$.tfObs.setValue(cliente.A1_OBSRC);

	if(cliente.A1_DDD && cliente.A1_TEL) {
		$.tfTelefone.setValue(mask.telefone(cliente.A1_DDD + cliente.A1_TEL));
	}
	if(tipo_pessoa == 'F') {
		$.tfCpfCnpj.setValue(mask.cpf(cliente.A1_CGC));
	}
	else if(tipo_pessoa == 'J') {
		$.tfCpfCnpj.setValue(mask.cnpj(cliente.A1_CGC));
	}
	if(cliente.A1_TEL2 && cliente.A1_TEL2.trim()) clickViewTelefone(cliente.A1_TEL2);
	if(cliente.A1_TEL3 && cliente.A1_TEL3.trim()) clickViewTelefone(cliente.A1_TEL3);
	if(cliente.A1_TEL4 && cliente.A1_TEL4.trim()) clickViewTelefone(cliente.A1_TEL4);
}

function createMenuAndroid(){
	if(OS_ANDROID){
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", doSave);
		};
		activity.invalidateOptionsMenu();
	}
}

function clickViewTelefone(tel){
	if(telefones.length >= 3){
		Alloy.Globals.showAlert("Ops","É possível adicionar apenas 4 telefones!");
		return false;
	}
	var telefone = Alloy.createWidget('CadTelefone', 'widget', {activityIndicator:$.activityIndicator, tipo: 'cel', tel: tel, editable: dados ? false : true});
	$.viewAddTelefone.add(telefone.getView());

	telefones.push(telefone);

	telefone.setFocus();

	telefone.setActionApagar(function(){
		$.viewAddTelefone.remove(telefone.getView());
		for(var i in telefones){
			if(telefones[i] == telefone){
				telefones.splice(i,1);
				break;
			}
		}
	});
}

// function clickViewCel(){
// 	if(celulares.length >= 1){
// 		Alloy.Globals.showAlert("Ops","É possível adicionar apenas 2 celulares!");
// 		return false;
// 	}
// 	var celular = Alloy.createWidget('CadTelefone', 'widget', {activityIndicator:$.activityIndicator, tipo: 'cel'});
// 	$.viewAddCelular.add(celular.getView());

// 	celulares.push(celular);

// 	celular.setFocus();

// 	celular.setActionApagar(function(){
// 		$.viewAddCelular.remove(celular.getView());
// 		for(var i in celulares){
// 			if(celulares[i] == celular){
// 				celulares.splice(i,1);
// 				break;
// 			}
// 		}
// 	});
// }

function validaEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function validaCPF(value) {
	var copy = value.replace(".", "").replace(".", "").replace("-", "");
	var num1 = parseInt(copy[0]);
	var num2 = parseInt(copy[1]);
	var num3 = parseInt(copy[2]);
	var num4 = parseInt(copy[3]);
	var num5 = parseInt(copy[4]);
	var num6 = parseInt(copy[5]);
	var num7 = parseInt(copy[6]);
	var num8 = parseInt(copy[7]);
	var num9 = parseInt(copy[8]);
	var num10 = parseInt(copy[9]);
	var num11 = parseInt(copy[10]);

	if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11) return false;
	var soma1 = (num1 * 10) + (num2 * 9) + (num3 * 8) + (num4 * 7) + (num5 * 6) + (num6 * 5) + (num7 * 4) + (num8 * 3) + (num9 * 2);
	var resto1 = (soma1*10) % 11;
	if (resto1 == 10 || resto1 == 11) resto1 = 0;
	else if (resto1 != num10) return false;

	var soma2 = (num1 * 11) + (num2 * 10) + (num3 * 9) + (num4 * 8) + (num5 * 7) + (num6 * 6) + (num7 * 5) + (num8 * 4) + (num9 * 3) + (num10 * 2);
	var resto2 = (soma2*10) % 11;
	if (resto2 == 10 || resto2 == 11) resto2 = 0;
	else if (resto2 != num11) return false;
	
	return true;
}

function validaCNPJ(value) {
	var copy = value.replace(".", "").replace(".", "").replace("/", "").replace("-", "");
	var num1 = parseInt(copy[0]);
	var num2 = parseInt(copy[1]);
	var num3 = parseInt(copy[2]);
	var num4 = parseInt(copy[3]);
	var num5 = parseInt(copy[4]);
	var num6 = parseInt(copy[5]);
	var num7 = parseInt(copy[6]);
	var num8 = parseInt(copy[7]);
	var num9 = parseInt(copy[8]);
	var num10 = parseInt(copy[9]);
	var num11 = parseInt(copy[10]);
	var num12 = parseInt(copy[11]);
	var num13 = parseInt(copy[12]);
	var num14 = parseInt(copy[13]);

	if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11 && num11 == num12 && num12 == num13 && num13 == num14) return false;

	var soma1 = (num1 * 5) + (num2 * 4) + (num3 * 3) + (num4 * 2) + (num5 * 9) + (num6 * 8) + (num7 * 7) + (num8 * 6) + (num9 * 5) + (num10 * 4) + (num11 * 3) + (num12 * 2);
	var soma2 = (num1 * 6) + (num2 * 5) + (num3 * 4) + (num4 * 3) + (num5 * 2) + (num6 * 9) + (num7 * 8) + (num8 * 7) + (num9 * 6) + (num10 * 5) + (num11 * 4) + (num12 * 3) + (num13 * 2);
	var dig1  = ((soma1) % 11) < 2 ? 0 : (11 - (soma1) % 11);
	var dig2  = ((soma2) % 11) < 2 ? 0 : (11 - (soma2) % 11);
	if(dig1 != num13 || dig2 != num14) return false;
	return true;
}

function checkPessoa(value) {
	$.checkOnFisica.setVisible(value == 'F' ? true : false);
	$.checkOffFisica.setVisible(value == 'F' ? false : true);
	$.checkOnJuridica.setVisible(value == 'F' ? false : true);
	$.checkOffJuridica.setVisible(value == 'F' ? true : false);

	$.tfCpfCnpj.setHintText(value == 'F' ? 'CPF - somente numeros' : 'CNPJ - somente numeros');
	$.tfCpfCnpj.setValue('');
}

function checkME(value) {
	$.checkOnSimME.setVisible(value == '1' ? true : false);
	$.checkOffSimME.setVisible(value == '1' ? false : true);
	$.checkOnNaoME.setVisible(value == '1' ? false : true);
	$.checkOffNaoME.setVisible(value == '1' ? true : false);
}

function setEditable(bool){
	$.tfNome.setEditable(bool);
	$.tfFantasia.setEditable(bool);
	$.tfTelefone.setEditable(bool);
	$.tfEmail.setEditable(bool);
	$.tfCpfCnpj.setEditable(bool);
	$.tfIpr.setEditable(bool);
	$.tfRG.setEditable(bool);
	$.tfNasc.setEditable(bool);
	$.tfObs.setEditable(bool);
}

$.mainWindow.addEventListener("open",function(e){
	
	if(!dados){
		setValues();
		setEditable(true);
		createMenuAndroid();
	}
	else {
		setValues(dados)
		setEditable(false);
	}
	
});

$.viewTelefone.addEventListener("click", function(e){
	if(dados) return true;
	clickViewTelefone();
});

$.tfNome.addEventListener('change', function(e) {
	var v = mask.removeCaracter(e.value);
	if ($.tfNome.getValue() != v) $.tfNome.setValue(v);
	$.tfFantasia.setValue(v.slice(0, v.indexOf(' ')));
});

$.tfTelefone.addEventListener("change", function (e) {
    var v = mask.telefone(e.value);
    if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
    $.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
});

$.tfNasc.addEventListener("change", function (e) {
    var v = mask.date(e.value);
    if ($.tfNasc.getValue() != v) $.tfNasc.setValue(v);
    $.tfNasc.setSelection($.tfNasc.getValue().length, $.tfNasc.getValue().length);
});

$.tfCpfCnpj.addEventListener("change", function (e) {
	var v = '';
	if(tipo_pessoa == 'F') {
		v = mask.cpf(e.value);
		$.tfCpfCnpj.setColor(validaCPF(v) ? 'blue' : 'red');
	}
	else {
		v = mask.cnpj(e.value);
		$.tfCpfCnpj.setColor(validaCNPJ(v) ? 'blue' : 'red');
	}
    if ($.tfCpfCnpj.getValue() != v) $.tfCpfCnpj.setValue(v);
});

$.tfIpr.addEventListener('change', function(e) {
	var v = mask.removeCaracter(e.value);
	if ($.tfIpr.getValue() != v) $.tfIpr.setValue(v);
});

$.tfRG.addEventListener('change', function(e) {
	var v = mask.removeCaracter(e.value);
	if ($.tfRG.getValue() != v) $.tfRG.setValue(v);
});

$.viewFisica.addEventListener('click', function(e) {
	if(dados) return true;
	tipo_pessoa = 'F';
	checkPessoa(tipo_pessoa);
});

$.viewJuridica.addEventListener('click', function(e) {
	if(dados) return true;
	tipo_pessoa = 'J';
	checkPessoa(tipo_pessoa);
});

$.simME.addEventListener('click', function(e) {
	if(dados) return true;
	me = '1';
	checkME(me);
});

$.naoME.addEventListener('click', function(e) {
	if(dados) return true;
	me = '2';
	checkME(me);
});

// $.viewCelular.addEventListener("click", clickViewCel);

// $.tfCel.addEventListener("change", function (e) {
//     var v = mask.telefone(e.value);
//     if ($.tfCel.getValue() != v) $.tfCel.setValue(v);
//     $.tfCel.setSelection($.tfCel.getValue().length, $.tfCel.getValue().length);
// });

// $.tfFax.addEventListener("change", function (e) {
//     var v = mask.tel(e.value);
//     if ($.tfFax.getValue() != v) $.tfFax.setValue(v);
//     $.tfFax.setSelection($.tfFax.getValue().length, $.tfFax.getValue().length);
// });