var args     = arguments[0] || {};
var callback = args.callback || function() {};
var cred     = args.cred || null;
var mask     = require("Mask");
var credito  = '';
var ClientManager = require('ClientManager');

$.credito.value = credito ? 'R$ ' + mask.real(credito) : "R$ 0,00";

if (OS_IOS) {
    var btDone = Titanium.UI.createButton({
        title   : "Salvar",
        color   : "white",
        font    : {
            fontSize    : "13",
            fontWeight  : "bold"
        }
    });
    $.credito.addEventListener('focus', function (e) {
        $.mainWindow.setRightNavButton(btDone);
    });
    $.credito.addEventListener('blur', function (e) {
        $.mainWindow.setRightNavButton(null);
    });
    btDone.addEventListener('click', function (e) {
        $.credito.blur();
        doSave();
    });
}

function doClose(e) {
    $.mainWindow.close();
}

function doSave(e){
    $.activityIndicator.show();
    getValues();
	ClientManager.setLimiteCredito(credito);
	callback(credito);
    doClose();
}

function getValues() {
    credito = $.credito.getValue();
    credito = credito.replace(".", "").replace(",", ".");
    if (credito.indexOf("R$") >= 0) credito = credito.slice(3);
}

function setValues(value) {
    credito = value ? value : ClientManager.getLimiteCredito();
    if(credito)
        $.credito.setValue(mask.real(credito));
}

function createMenuAndroid(){
    if(OS_ANDROID){
        var activity 	= $.mainWindow.activity;
        activity.onCreateOptionsMenu = function(e) {
            var btDone = e.menu.add({
                title 			: "Salvar",
                showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
            });
            btDone.addEventListener("click", doSave);
        };
        activity.invalidateOptionsMenu();
    }
}

function setEditable(bool){
    $.credito.setEditable(bool);
}

$.credito.addEventListener("change", function(e) {
    $.credito.setValue(mask.real(e.source.value));
});

$.credito.addEventListener("focus", function() {
    if(cred) return true;
    $.credito.setValue("");
});

$.mainWindow.addEventListener("open",function(e){
    // setValues();
	// if(OS_ANDROID)
    // 	createMenuAndroid();
    if(!cred){
		setValues();
		setEditable(true);
		createMenuAndroid();
	}
	else {
		setValues(cred)
		setEditable(false);
	}
});