// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args     = $.args;
var mask 	 = require('Mask');
var callback = args.callback || function(){};
var socios 	 = args.socios || null;
var new_socios 	 = [];
var formularios   = [];
var ClientManager = require('ClientManager');
function doClose(e){
	$.mainWindow.close();
}

function doSave(e){
	$.activityIndicator.show();
	if(validadeCPFs()){
		Alloy.Globals.showAlert("Ops!","Você não pode cadastrar o mesmo CPF para o cliente e o sócio!");
		$.activityIndicator.hide();
		return false;
	}
	var message = validate();
	if(message){
		Alloy.Globals.showAlert("Ops!","É necessário informar o "+message.name+"!");
		if(message.field) message.field.focus();
		$.activityIndicator.hide();
		return false;
    }
	getValues();
	ClientManager.setSocios(new_socios);
	callback(new_socios);
    doClose();
}

function validadeCPFs() {
	if(ClientManager.validateCGC(mask.removeCPFCNPJMask($.tfCpf.getValue()), 'soc')){
		return true;
	}
	else if(formularios.length >= 1 && ClientManager.validateCGC(mask.removeCPFCNPJMask(formularios[0].getValues().AO_CGC), 'soc')){
		return true;
	}
}

function validate() {
	var nome  = mask.removeCaracter($.tfNome.getValue());
	if(!!!nome.match(/[aA-zZ]* [aA-zZ]*/) || nome.length < 4){
		return {field: $.tfNome, name: 'Nome'};
	}
	if(!validaCPF($.tfCpf.getValue())) {
		return {field: $.tfCpf, name: 'CPF'};
	}
	if(formularios.length >= 1) {
		if((!validaCPF(formularios[0].getValues().AO_CGC)) || (!!!formularios[0].getValues().AO_NOMINS.match(/[aA-zZ]* [aA-zZ]*/) || formularios[0].getValues().AO_NOMINS.length < 4)){
			return {name: 'segundo sócio'};
		}
	}
}

function getValues() {
	new_socios = [];
	new_socios.push({
		AO_NOMINS: mask.removeCaracter($.tfNome.getValue()).toUpperCase(),
		AO_CGC: mask.removeCPFCNPJMask($.tfCpf.getValue()),
		ID: $.lbId.getText()
	});
	for(var i= 0; i<formularios.length; i++){
		var count = parseInt(i);
		var obj = formularios[count].getValues();
		new_socios.push({
			AO_NOMINS: mask.removeCaracter(obj.AO_NOMINS).toUpperCase(),
			AO_CGC: mask.removeCPFCNPJMask(obj.AO_CGC),
			ID: obj.ID
		});
	}
}

function setValues(obj) {
	new_socios = obj ? obj : ClientManager.getSocios();
	if(new_socios.length){
		$.tfNome.setValue(new_socios[0].AO_NOMINS);
		$.tfCpf.setValue(mask.cnpj(new_socios[0].AO_CGC));
		$.lbId.setText(new_socios[0].ID);
	
		for(var i= 1; i<new_socios.length; i++){
			var count = parseInt(i);
			clickViewSocio(new_socios[count]);
		}
	}
}

function createMenuAndroid(){
	if(OS_ANDROID) {
		var activity 	= $.mainWindow.activity;
		activity.onCreateOptionsMenu = function(e) {
			var btDone = e.menu.add({
				title 			: "Salvar",
				showAsAction 	: Ti.Android.SHOW_AS_ACTION_ALWAYS
			});
			btDone.addEventListener("click", doSave);
		};
		activity.invalidateOptionsMenu();
	}
}

function validaCPF(value) {
	var copy = value.replace(".", "").replace(".", "").replace("-", "");
	var num1 = parseInt(copy[0]);
	var num2 = parseInt(copy[1]);
	var num3 = parseInt(copy[2]);
	var num4 = parseInt(copy[3]);
	var num5 = parseInt(copy[4]);
	var num6 = parseInt(copy[5]);
	var num7 = parseInt(copy[6]);
	var num8 = parseInt(copy[7]);
	var num9 = parseInt(copy[8]);
	var num10 = parseInt(copy[9]);
	var num11 = parseInt(copy[10]);

	if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11) return false;
	var soma1 = (num1 * 10) + (num2 * 9) + (num3 * 8) + (num4 * 7) + (num5 * 6) + (num6 * 5) + (num7 * 4) + (num8 * 3) + (num9 * 2);
	var resto1 = (soma1*10) % 11;
	if (resto1 == 10 || resto1 == 11) resto1 = 0;
	else if (resto1 != num10) return false;

	var soma2 = (num1 * 11) + (num2 * 10) + (num3 * 9) + (num4 * 8) + (num5 * 7) + (num6 * 6) + (num7 * 5) + (num8 * 4) + (num9 * 3) + (num10 * 2);
	var resto2 = (soma2*10) % 11;
	if (resto2 == 10 || resto2 == 11) resto2 = 0;
	else if (resto2 != num11) return false;
	
	return true;
}

function clickViewSocio(obj){
	if(formularios.length >= 1){
		Alloy.Globals.showAlert("Ops","É possível adicionar apenas 2 sócios!");
		return false;
	}
	var socio = Alloy.createWidget('CadSocio', 'widget', {activityIndicator:$.activityIndicator, socio: obj, editable: socios ? false : true});
	$.viewAddSocio.add(socio.getView());

	formularios.push(socio);

	socio.setFocus();

	socio.setActionApagar(function(){
		$.viewAddSocio.remove(socio.getView());
		for(var i in formularios){
			if(formularios[i] == socio){
				formularios.splice(i,1);
				break;
			}
		}
	});
}

function setEditable(bool) {
	$.tfNome.setEditable(bool);
	$.tfCpf.setEditable(bool);
}

$.mainWindow.addEventListener("open",function(e){
	// setValues();
	// if(OS_ANDROID)
	// 	createMenuAndroid();


	if(!socios){
		setValues();
		setEditable(true);
		createMenuAndroid();
	}
	else {
		setValues(socios)
		setEditable(false);
	}
});

$.tfCpf.addEventListener("change", function (e) {
	var v = '';
    v = mask.cpf(e.value);
    $.tfCpf.setColor(validaCPF(v) ? 'blue' : 'red');
    if ($.tfCpf.getValue() != v) $.tfCpf.setValue(v);
});

$.viewSocio.addEventListener("click", function(e){
	if(socios) return true;
	clickViewSocio();
});