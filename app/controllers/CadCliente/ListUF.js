// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args          = $.args;
var callback      = args.callback || function(){};
var LocalData     = require('LocalData');
var ClientManager = require('ClientManager');

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar estado",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar estado",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

var ufs  = [];
function doClose(e){
	$.mainWindow.close();
}

function createListView(array) {
    var section     = Ti.UI.createListSection();
    var listEstados = [];
    var estados     = _.sortBy(array, function(uf){
        return uf.UF_NOME;
    });
    for(var i in estados) {
        listEstados.push(createItemList(estados[i]));
    }
    section.setItems(listEstados);
    $.listView.setSections([section])
}

function createItemList(estado) {
    var searchableText = estado.UF_COD + estado.UF_NOME + estado.UF_SIGLA;
    return {
        template: 'templateEstado',
        lbUF: {
            text: estado.UF_SIGLA + ' - ' + estado.UF_NOME
        },
        properties: {
            data: estado,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function selectUF(estado) {
    callback(estado);
    doClose();
}

async function getUF(){
    ufs = await LocalData.getUF();
    await createListView(ufs);
}

$.mainWindow.addEventListener("open",function(e){
    getUF();
});

$.listView.addEventListener('itemclick', function(e){
    var item = e.section.getItemAt(e.itemIndex);
    var data = item.properties.data;
    selectUF(data);
});

