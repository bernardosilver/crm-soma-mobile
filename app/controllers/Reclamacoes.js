// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var moment                   = require("alloy/moment");
var optionsFilter            = [];
var firstVisibleSectionIndex = 0;

if (OS_IOS) {
    $.listView.setSearchView(Titanium.UI.createSearchBar({
        hintText: "Procurar reclamação",
        barColor: "#e4e8e9",
        borderColor: "#e4e8e9",
        height: "45dp",
    }));
} else if (OS_ANDROID) {
    $.listView.setSearchView(Ti.UI.Android.createSearchView({
        hintText: "Procurar reclamação",
        backgroundColor: "#a5a5a5",
        color: "#205D33"
    }));
}

function doClose() {
    $.mainWindow.close();
}

function onFilter(){
	$.popup.showLabelWithTag(optionsFilter,function(categoria,index){
		var itemIndex 		= OS_ANDROID ? (firstVisibleSectionIndex < index ? 0 : 0) : 0;
		var sectionIndex 	= parseInt(index);
		scrollTo(sectionIndex, itemIndex);
		$.popup.hide();
	},"Toque para filtrar");
}

function scrollTo(sectionIndex, itemIndex){ 
	var options = OS_IOS ? {position: Titanium.UI.iOS.ListViewScrollPosition.TOP, animated:false}  : {animated:false};
	$.listView.scrollToItem(sectionIndex,itemIndex,options);
	// showViewControl(false);
}

function refreshMenuActionBar() {
    var activity = $.mainWindow.getActivity();
    activity.onCreateOptionsMenu = function (e) {
        var menuItemPin = e.menu.add({
            icon: "/images/add.png",
            width:"40dp",
            height: "40dp",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        menuItemPin.addEventListener("click", function (e) {
            var WindowTabGroup = Alloy.createController('FormCliente').getView();
            if (OS_IOS)
                WindowTabGroup.open();
            else
                WindowTabGroup.open({ modal: false });
        });
    };
    activity.invalidateOptionsMenu();
}

function createListView() {
    var sections    = [];
    var registradas = [];
    var encerradas  = [];
    var canceladas  = [];
    var list = [
        {
            descricao: 'BERNARDO SILVEIRA MARTINS SILVEIRA MARTINS',
            setor: 'LOGISTICA',
            roteiro: '999999',
            codigo: '01254438629/2019',
            dt_registro: '20190715',
            dt_ocorrencia: '20190715',
            status: '1'
        },{
            descricao: 'T L VIEIRA AGROPECUARIA',
            setor: 'C.Q.',
            roteiro: '',
            codigo: '31699870000/2019',
            dt_registro: '20190708',
            dt_ocorrencia: '20190708',
            status: '2'
        },{
            descricao: 'BRUNA LACERDA MACHADO',
            setor: 'C.Q.',
            roteiro: '',
            codigo: '01802679650/2019',
            dt_registro: '20190705',
            dt_ocorrencia: '20190620',
            status: '3'
        },{
            descricao: 'DISCOM DISTRIBUICAO E COMERCIO LTDA',
            setor: 'C.Q.',
            roteiro: '',
            codigo: '21712570000/2019',
            dt_registro: '20190704',
            dt_ocorrencia: '20190704',
            status: '3'
        },{
            descricao: 'EDSON MACIEL ALVES FILHO',
            setor: 'LOGISTICA',
            roteiro: '027908',
            codigo: '09083351645/2019',
            dt_registro: '20190625',
            dt_ocorrencia: '20190625',
            status: '2'
        },{
            descricao: 'CV GONCALVES MAGALHAES',
            setor: 'LOGISTICA',
            roteiro: '027907',
            codigo: '33860816000/2019',
            dt_registro: '20190415',
            dt_ocorrencia: '20190315',
            status: '1'
        }
    ];
    
    var reclamacoes = _.sortBy(list, function(recl) {return recl.dt_registro}).reverse();
    // _.sortBy(list, function(ped) {return ped.dt_emissao;}).reverse();
    for (var i in reclamacoes) {
        if(reclamacoes[i].status == '1') {
            registradas.push(createItem(reclamacoes[i]));
        }
        else if (reclamacoes[i].status == '2') {
            encerradas.push(createItem(reclamacoes[i]));
        }
        else if (reclamacoes[i].status == '3') {
            canceladas.push(createItem(reclamacoes[i]));
        }
    }

    if(registradas.length) {
        optionsFilter.push('REGISTRADAS');
        var sectionRegistradas = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Registradas')
        });
        sectionRegistradas.setItems(registradas);
        sections.push(sectionRegistradas);
    }
    if(encerradas.length) {
        optionsFilter.push('ENCERRADAS');
        var sectionEncerradas = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Encerradas')
        });
        sectionEncerradas.setItems(encerradas);
        sections.push(sectionEncerradas);
    }
    if(canceladas.length) {
        optionsFilter.push('CANCELADAS');
        var sectionCanceladas = Ti.UI.createListSection({
            headerView : Alloy.Globals.createSectionHeader('Canceladas')
        });
        sectionCanceladas.setItems(canceladas);
        sections.push(sectionCanceladas);
    }
    if(sections && sections.length) sections[sections.length - 1].appendItems([setEmptyBottom()]);
    $.listView.setSections(sections);

}

function createItem(reclamacao) {
    var searchableText = reclamacao.descricao + reclamacao.codigo + reclamacao.roteiro + reclamacao.dt_registro + reclamacao.dt_ocorrencia;
    return {
        template: 'templateReclamacao',
        lbReclamacao: {
            attributedString: Alloy.Globals.setNameWithCode(reclamacao.descricao, reclamacao.codigo),
        },
        lbSetor: {
            attributedString: Alloy.Globals.setNameWithCode(reclamacao.setor, 'Setor'),
        },
        lbRoteiro: {
            attributedString: reclamacao.roteiro ? Alloy.Globals.setNameWithCode(reclamacao.roteiro, 'Roteiro') : '',
            visible: reclamacao.roteiro ? true : false,
            height: reclamacao.roteiro ? Ti.UI.SIZE : '0dp'
        },
        lbRegistro: {
            attributedString: Alloy.Globals.setNameWithCode(moment(reclamacao.dt_registro).format('DD/MM/YYYY'), 'Data registro'),
        },
        lbOcorrencia: {
            attributedString: Alloy.Globals.setNameWithCode(moment(reclamacao.dt_ocorrencia).format('DD/MM/YYYY'), 'Data ocorrência'),
        },
        status: {
            backgroundColor: reclamacao.status == '1' ? Alloy.Globals.MAIN_COLOR : reclamacao.status == '2' ? 'black' : Alloy.Globals.GRAY_COLOR
        },
        properties: {
            data: reclamacao,
            accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_DISCLOSURE,
            selectedBackgroundColor: Alloy.Globals.defaultBackgroundColor,
            searchableText: searchableText
        }
    };
}

function setEmptyBottom(){
	return {
		template 		: "templateEmpty",
		properties 		: {
			accessoryType 			: Titanium.UI.LIST_ACCESSORY_TYPE_NOME,
			selectedBackgroundColor : Alloy.Globals.BACKGROUND_COLOR
		}
	};
}

$.mainWindow.addEventListener("open", function () {
    refreshMenuActionBar();
    createListView();
});