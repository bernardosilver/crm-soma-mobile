// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var ClientManager = require('ClientManager');
var LocalData     = require('LocalData');
var RefreshData = require("RefreshData");

function createMenu () {
    var usuario  = Alloy.Globals.getLocalDataUsuario().data;

    var sections = [];

    var serctionUsuario  = Ti.UI.createListSection();
    var sectionNovos     = Ti.UI.createListSection();
    var sectionSeparator = Ti.UI.createListSection();
    var sectionConsultas = Ti.UI.createListSection();
    var sectionSync      = Ti.UI.createListSection();

    serctionUsuario.setItems([createOptionUsuario(usuario)]);
    sectionNovos.setItems([
        createOption({opc: 'Novo Pedido', img: '/images/novo_pedido_green.png'}),
        createOption({opc: 'Novo Cliente', img: '/images/novo_cliente_green.png'}),
    ]);
    
    sectionSeparator.setItems([createSeparator()]);
    sectionConsultas.setItems([
        createOption({opc: 'Ver Pedidos', img: '/images/pedidos_green.png'}),
        createOption({opc: 'Ver Clientes', img: '/images/clientes_green.png'}),
    ]);
    // sectionSeparator.setItems([createSeparator()]);
    sectionSync.setItems([
        createSeparator(),
        createOption({opc: 'Baixar dados', img: '/images/download_green.png'}),
        createOption({opc: 'Enviar dados', img: '/images/upload_green.png'}),
    ]);

    sections.push(serctionUsuario);
    sections.push(sectionNovos);
    sections.push(sectionSeparator);
    sections.push(sectionConsultas);
    sections.push(sectionSync);

    $.listView.setSections(sections);
}

function createOptionUsuario (usr) {
    return {
        template: 'templateUsuario',
        lbNome: {
            text: usr.USR_NOME
        },
        lbEmail: {
            text: usr.USR_EMAIL
        },
        viewProfile : {
            image: usr.USR_FOTO.trim() ? usr.USR_FOTO : '/images/perfil_green.png'
        },
        properties: {
            data: usr,
            type: 'perfil'
        },
        events: {
           "itemClick" : function() {
           }, 
           itemClick: function() {
           }, 
        }
    };
}

function createOption (obj) {
    return {
        template: 'templateMenu',
        lbOpcao: {
            text: obj.opc
        },
        imgOpcao: {
            image: obj.img
        },
        properties: {
            data: obj,
            type: 'menu'
        }
    };
}

function createSeparator () {
    return {
        template: 'templateSeparator'
    };
}

function openWindowTabGroup(tab) {
	var WindowTabGroup = Alloy.createController(tab).getView();
	if (OS_IOS)
		WindowTabGroup.open();
	else
		WindowTabGroup.open({ modal: false });
}

createMenu();

$.listView.addEventListener("itemclick", function (e) {
    var item = e.section.getItemAt(e.itemIndex);
    var data = item ? item.properties.data : null;
    var type = item ? item.properties.type : null;

    if(data && data.opc == 'Novo Pedido') {
        Alloy.Globals.openWindow('FormPedido');
    }
    else if(data && data.opc == 'Novo Cliente') {
        ClientManager.delete();
        Alloy.Globals.openWindow('FormCliente');
    }
    else if(data && data.opc == 'Nova Reclamação') {
    }
    else if(data && data.opc == 'Ver Pedidos') {
        Alloy.Globals.openWindow('Pedidos');
    }
    else if(data && data.opc == 'Ver Clientes') {
        Alloy.Globals.openWindow('Clientes');
    }
    else if(data && data.opc == 'Baixar dados') {
        if(Ti.Network.online) {
            Alloy.Globals.openWindow("Sync", {download: true});
        }
        else {
            return Alloy.Globals.showAlert("Erro!", "É necessário estar conectado à internet para sincronizar os dados!");
        }
    }
    else if(data && data.opc == 'Enviar dados') {
        if(!Ti.Network.online) {
            return Alloy.Globals.showAlert("Erro!", "É necessário estar conectado à internet para sincronizar os dados!");
        }
        else {
            var count = 0;
            LocalData.countCadCli({
                success	: function(count){
                    if(!count || count == 0){
                        LocalData.countPedidos({
                            success	: function(count){
                                if(!count || count == 0){
                                    return Alloy.Globals.showAlert("Ops!", "Não existem dados para serem enviados!");
                                }
                                else {
                                    Alloy.Globals.openWindow("Sync", {upload: true});
                                }
                            },
                            error	: function(err){
                                console.log('DEU ERRADO AO CONTAR PEDIDOS: ', err);
                            }
                        });
                    }
                    else {
                        Alloy.Globals.openWindow("Sync", {upload: true});
                    }
                },
                error	: function(err){
                    console.log('DEU ERRADO AO CONTAR CLIENTE: ', err);
                }
            });
        }
    }
    else if(type == 'perfil') {
        Alloy.Globals.openWindow('Perfil');
    }
});