// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.DEBUG = false;


Alloy.Globals.URL_API = "http://200.195.55.221:9020/app/";
// Alloy.Globals.URL_API = "http://192.168.1.52:9020/app/";
//Alloy.Globals.URL_API = "http://192.168.0.241:9010/app/";


Alloy.Globals.BLUE_COLOR        = "#3060CA";
Alloy.Globals.MAIN_COLOR        = "#28a745";
Alloy.Globals.DARK_MAIN_COLOR   = "#205D33";
Alloy.Globals.LIGHT_GRAY_COLOR2 = "#ECF0F1";
Alloy.Globals.GRAY_COLOR        = "#B2B2B2";
Alloy.Globals.DARK_GRAY_COLOR   = "#4C5454";
Alloy.Globals.WHITE_COLOR       = "#FFF";
Alloy.Globals.RED_COLOR         = "#FF4B4B";


Alloy.Globals.defaultBackgroundColor = "#f1f1f1";//cinza claro

var mask = require('Mask');


Alloy.Globals.isLogged = function () {
    if (Ti.App.Properties.getObject("usuario"))
        return true;
    else
        return false;
};

Alloy.Globals.setLocalDataUsuario = function (data) {
    Ti.App.Properties.setObject("usuario", data);
};

Alloy.Globals.removeLocalDataUsuario = function () {
    Ti.App.Properties.removeProperty("usuario");
};

Alloy.Globals.getLocalDataUsuario = function () {
    var usuario = Ti.App.Properties.getObject("usuario", null);
    return usuario ? usuario : null;
};

Alloy.Globals.setSync = function (date) {
    Ti.App.Properties.setString("sync", date)
};

Alloy.Globals.removeSync = function () {
    Ti.App.Properties.removeProperty("sync");
};

Alloy.Globals.getSync = function () {
    var sincronizacao = Ti.App.Properties.getString("sync", null);
    return sincronizacao ? sincronizacao : null;
};


Alloy.Globals.setCountCadCli = function (value) {
    Ti.App.Properties.setInt("cadCli", value)
};

Alloy.Globals.removeCountCadCli = function () {
    Ti.App.Properties.removeProperty("cadCli");
};

Alloy.Globals.getCountCadCli = function () {
    var sincronizacao = Ti.App.Properties.getString("cadCli", null);
    return sincronizacao ? sincronizacao : null;
};

Alloy.Globals.setTitleWithDescription = function (title, text){
    var texto = title + " " + text;
    var attr = Ti.UI.createAttributedString({
        text: texto,
        attributes: [
            {
                type: Ti.UI.ATTRIBUTE_FONT,
                value: { fontSize: 14 },
                range: [texto.indexOf(text), text.length]
            }
        ]
    });
    return attr;
};

// CRIA UMA VIEW PARA SER APLICADA A UM HEADERVIEW
Alloy.Globals.createSectionHeader = function(label, obs){
	return Alloy.createWidget("HeaderList",{title: label, subTitle: obs}).getView()
};

Alloy.Globals.mnum = function (v) {
    v = v.replace(/\D/g, "");
    //Remove tudo o que não é número
    return v;
};

Alloy.Globals.showAlert = function (argsOrTitle, message, buttons, listener) {

    var args = (typeof (argsOrTitle) == "object") ? (argsOrTitle ? argsOrTitle : {}) : {};

    var title = args.title || argsOrTitle || "Alerta";
    var message = args.message || message || "";
    var buttons = args.buttons || buttons || ["Ok"];
    var listener = args.listener || listener || function () { };
    var hasTextField = args.hasTextField || false;
    var defaultText = args.defaultText || "";
    var keyboardType = args.keyboardType || Titanium.UI.KEYBOARD_TYPE_DEFAULT;

    var textField = ((hasTextField == true) && (OS_ANDROID)) ? Ti.UI.createTextField({ keyboardType: keyboardType }) : null;

    if (textField)
        textField.setValue(defaultText);

    var dialog;

    if (OS_ANDROID) {
        dialog = Ti.UI.createAlertDialog({
            title: title,
            androidView: (hasTextField == true) ? textField : null,
            message: message,
            buttonNames: buttons ? buttons : ["Ok"]
        });
    } else {
        dialog = Ti.UI.createAlertDialog({
            title: title,
            style: Titanium.UI.iOS.AlertDialogStyle.DEFAULT,
            message: message,
            keyboardType: keyboardType,
            buttonNames: buttons
        });
    }

    dialog.show();

    if (listener) {
        dialog.addEventListener('click', function (e) {
            var textValue = null;
            if (hasTextField) {
                textValue = (Titanium.Platform.name == "android") ? textField.getValue() : e.text;
            }
            listener({
                index: e.index,
                text: textValue
            });
        });
    }
    return dialog;
};

Alloy.Globals.removeAcento = function (str) {
    var rep = ' ';

    str = str.toLowerCase().replace(/\s+/g, rep); // replace whitespace

    // remove accents, swap ñ for n, etc
    var from = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
    var to = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(
            new RegExp(from.charAt(i), 'g'),
            to.charAt(i)
        );
    }
    // remove invalid chars
    str = str.replace(new RegExp('[^a-z0-9' + rep + ']', "g"), '').replace(/-+/g, rep); // collapse dashes;

    return str;
};

Alloy.Globals.PixelsToDPUnits = function(ThePixels){
  return (ThePixels / (Titanium.Platform.displayCaps.dpi / 160));
};

Alloy.Globals.DPUnitsToPixels = function(TheDPUnits){
  return (TheDPUnits * (Titanium.Platform.displayCaps.dpi / 160));
};

Alloy.Globals.createTextEndereco = function(endereco){
	var text = "";
    text 	 = endereco.A1_END;
    text 	 = (endereco.A1_COMPLEM && endereco.A1_COMPLEM.trim() != "") ? text + " - " + endereco.A1_COMPLEM : text;
    text 	 = text + " - " + endereco.A1_BAIRRO;
    text 	 = text + "\n" + endereco.A1_MUN + " - " + endereco.A1_EST;
    text 	 = (endereco.A1_INFOROT && endereco.A1_INFOROT.trim() != "") ? text + "\nReferência: " + endereco.A1_INFOROT : text;
    return text;
};

Alloy.Globals.createTextEnderecoCob = function(endereco){
	var text = "";
    text 	 = endereco.A1_ENDCOB;
    text 	 = text + " - " + endereco.A1_BAIRROC;
    text 	 = text + "\n" + endereco.A1_MUNC + " - " + endereco.A1_ESTC;
    return text;
};

Alloy.Globals.createTextCliente = function(cliente){
    var cpfcnpj = cliente.A1_PESSOA == 'F' ? 'CPF: ' + mask.cpf(cliente.A1_CGC) : 'CNPJ: ' + mask.cnpj(cliente.A1_CGC);
    var text = "";
    text 	 = cliente.A1_NOME;
    text 	 = (cliente.A1_NREDUZ && cliente.A1_NREDUZ.trim() != "") ? text + " - " + cliente.A1_NREDUZ : text;
    text 	 = text + "\n" + cpfcnpj;
    return text;
};

Alloy.Globals.createTextSocios = function(socios) {
    var text  = `${socios[0].AO_NOMINS} - ${socios[0].AO_CGC}.`
    if(socios.length > 1) text = text + '\nMais 1...';
    return text;
}

Alloy.Globals.createTextRefCom = function(refs) {
    var count = refs.length;
    var text = `${count} de 3 referências cadastradas.`
    return text;
}

Alloy.Globals.createTextRefBanc = function(bancos) {
    var text  = `${bancos[0].AO_NOMINS}.`
    if(bancos.length > 1) text = text + '\nMais 1...';
    return text;
}

Alloy.Globals.setNameWithCode = function (nome, cod){
    var aux     = cod ? cod : '';
    var str     = nome ? nome : 'Não informado.'
    var texto   = aux + ': ' + str;
    var attr    = Ti.UI.createAttributedString({
        text: texto,
        attributes: [
            {
                type: Ti.UI.ATTRIBUTE_FONT,
                value: { fontSize: 14, fontWeight: 'bold' },
                range: [texto.indexOf(cod), aux.length]
            },
        ]
    });
    return attr; 
};
