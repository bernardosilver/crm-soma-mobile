var args = arguments[0] || {};
var tipo = args.tipo 	|| '';
var tel  = args.tel		|| '';
var editable = args.editable || false;
var mask = require('Mask');
var actionApagar;

if(tipo == 'fixo')
    $.tfTelefone1.setHintText('Telefone alternativo');
else if (tipo == 'cel')
    $.tfTelefone1.setHintText('Telefone alternativo');
if(tel) $.tfTelefone1.setValue(tel);

$.tfTelefone1.setEditable(editable);

exports.setFocus = function(){

	if(OS_IOS)
	setTimeout(function(){ $.tfTelefone1.focus(); }, 200);

	$.viewMain.setHeight("1dp");
	$.viewMain.animate({height: "45dp", duration: 200}, function(){});
};

exports.getValues = function(){
	return {
		telefone:	$.tfTelefone1.getValue()
	};
};

exports.setActionApagar = function(action){
	actionApagar = action;
};

$.picMenos.addEventListener("click",function(e){
	if(!editable) return true;
	$.viewContents.animate({left: "-80dp", duration: 200}, function(){});
});

$.btApagar.addEventListener("click",function(e){
	if(!editable) return true;
	setTimeout(function(){ actionApagar(); }, 200);
	if(OS_ANDROID) $.viewMain.animate({height: "1dp", duration: 200}, function(){});
	else if(OS_IOS){
		$.viewBtApagar.animate({height: "0", duration: 200}, function(){});
		$.viewContents.animate({height: "0", duration: 200}, function(){});
	}
});

$.tfTelefone1.addEventListener("change", function (e) {
    var v = tipo == 'fixo' ? mask.tel(e.value) : mask.telefone(e.value);
    if ($.tfTelefone1.getValue() != v) $.tfTelefone1.setValue(v);
    $.tfTelefone1.setSelection($.tfTelefone1.getValue().length, $.tfTelefone1.getValue().length);
});