var args = arguments[0] || {};

if (args.hintText)
    $.textField.setHintText(args.hintText);

if (args.maxLength > 0){
    $.textField.setMaxLength(args.maxLength);
}

exports.setHintText = function (text) {
    $.textField.setHintText(text);
};

exports.setBottom = function (value) {
    $.viewMain.setBottom(value);
};

exports.setPasswordMask = function (bool) {
    $.textField.setPasswordMask(bool);
};

exports.setReturnKeyType = function (text) {
    var type = Titanium.UI.RETURNKEY_DEFAULT;
    if (text == 'next') type = Titanium.UI.RETURNKEY_NEXT;
    else if (text == 'go') type = Titanium.UI.RETURNKEY_GO;
    else if (text == 'send') type = Titanium.UI.RETURNKEY_SEND;
    else if (text == 'continue') type = Titanium.UI.RETURNKEY_CONTINUE;
    else if (text == 'done') type = Titanium.UI.RETURNKEY_DONE;

    $.textField.setReturnKeyType(type);
};

exports.setKeyboardType = function (text) {
    var type = Titanium.UI.KEYBOARD_TYPE_DEFAULT;
    if (text == 'email') type = Titanium.UI.KEYBOARD_TYPE_EMAIL;
    else if (text == 'number') type = Titanium.UI.KEYBOARD_NUMBERS_PUNCTUATION;
    else if (text == 'user') type = Titanium.UI.KEYBOARD_TYPE_ASCII;
    else if (text == 'phone') type = Titanium.UI.KEYBOARD_TYPE_PHONE_PAD;
    else if (text == 'decimal') type = Titanium.UI.KEYBOARD_TYPE_DECIMAL_PAD;

    $.textField.setKeyboardType(type);
};

exports.setValue = function (text) {
    $.textField.setValue(text);
};

exports.getValue = function () {
    return $.textField.getValue();
};

exports.setColorLabel = function (value) {
    $.lbHeader.setColor(value);
};

exports.setColorBorder = function (value) {
    $.viewTf.setBorderColor(value);
};

exports.setBackgroundColor = function (value) {
    $.viewTf.setBackgroundColor(value);
};

exports.setEditable = function (bool) {
    $.textField.setEditable(bool);
};

exports.blur = function () {
    $.textField.blur();
};

exports.hide = function () {
    $.viewMain.hide();
};

exports.show = function () {
    $.viewMain.show();
};

function click(e) {
    $.textField.focus();
}

$.textField.addEventListener('change', function (e) {
    var v = e.value;
    v = v.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");

    if (args.maxLength){
        if ($.textField.getValue() != v) $.textField.setValue(v);
        $.textField.setSelection($.textField.getValue().length, $.textField.getValue().length);
    }
});