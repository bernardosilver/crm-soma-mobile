var args = arguments[0] || {};
var obgt = args.obgt;
var ref  = args.ref || '';
var mask = require('Mask');
var editable = args.editable || false;
var actionApagar;

if(ref) {
	$.tfEmpresa.setValue(ref.AO_NOMINS);
	$.tfTelefone.setValue(mask.tel(ref.AO_TELEFON));
	$.tfCidadeUF.setValue(ref.AO_OBSERV);
	$.tfContato.setValue(ref.AO_CONTATO);
	$.lbId.setText(ref.ID);
}

$.tfEmpresa.setEditable(editable);
$.tfTelefone.setEditable(editable);
$.tfCidadeUF.setEditable(editable);
$.tfContato.setEditable(editable);

exports.setFocus = function(){
	if(OS_IOS)
	setTimeout(function(){ $.tfEmpresa.focus(); }, 200);

	$.viewMain.setHeight("1dp");
	$.viewMain.animate({height: "200dp", duration: 200}, function(){});
};

exports.getValues = function(){
	return {
        nome     : $.tfEmpresa.getValue(),
        telefone : $.tfTelefone.getValue(),
        cidadeUF : $.tfCidadeUF.getValue(),
		contato  : $.tfContato.getValue(),
		ID		 : $.lbId.getText()
	};
};

exports.setActionApagar = function(action){
	actionApagar = action;
};

$.picMenos.addEventListener("click",function(e){
	if(!editable) return true;
	$.viewContents.animate({left: "-80dp", duration: 200}, function(){});
});

$.btApagar.addEventListener("click",function(e){
	if(!editable) return true;
	setTimeout(function(){ actionApagar(); }, 200);
	if(OS_ANDROID) $.viewMain.animate({height: "1dp", duration: 200}, function(){});
	else if(OS_IOS){
		$.viewBtApagar.animate({height: "0", duration: 200}, function(){});
		$.viewContents.animate({height: "0", duration: 200}, function(){});
	}
});

$.tfTelefone.addEventListener("change", function (e) {
    var v = mask.tel(e.value);
    if ($.tfTelefone.getValue() != v) $.tfTelefone.setValue(v);
    $.tfTelefone.setSelection($.tfTelefone.getValue().length, $.tfTelefone.getValue().length);
});

$.lbObgt1.setVisible(obgt);
$.lbObgt2.setVisible(obgt);
$.lbObgt3.setVisible(obgt);
$.lbObgt4.setVisible(obgt);

var arraytf = [$.tfCpf];
// Alloy.Globals.addEventReturn(arraytf, function(){});
