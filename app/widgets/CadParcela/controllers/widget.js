var args     = arguments[0]  || {};
var data     = args.data	 || '';
var editable = args.editable || false;
var mask     = require('Mask');
var moment   = require("alloy/moment");
var actionApagar;


$.tfParcela1.setHintText('Data da parcela');
if(data) $.tfParcela1.setValue(data);

$.tfParcela1.setEditable(editable);

const formatData = ((dia) =>{
    var arr = dia.split('/');
    var dt = arr[2] + arr[1] + arr[0];
    return dt;
});

var ultima   = moment(formatData(args.ultima)).add(1,'d');

const showPicker = (() =>{
    var anoAtual = moment(ultima).year();
    var mesAtual = moment(ultima).month();
    var diaAtual = moment(ultima).date();

    // CALCULA PREVISAO DE CARREGAMENTO
    var maxDate  = moment().add(3, 'd');
    if(maxDate.day() == 0) {
        maxDate = moment().add(4, 'd');
    }
    else if(maxDate.day() == 6) {
        maxDate = moment().add(5, 'd');
    }

    maxDate      = moment().add(65, 'd');
    var maxAno   = moment(maxDate).year();
    var maxMes   = moment(maxDate).month();
    var maxDia   = moment(maxDate).date();

    var value    = moment(ultima).add(1, 'days');
    var valueAno = moment(value).year();
    var valueMes = moment(value).month();
    var valueDia = moment(value).date();

    var picker = Ti.UI.createPicker({
        type:Ti.UI.PICKER_TYPE_DATE,
        locale: 'pt_BR',
        minDate:new Date(anoAtual,mesAtual,diaAtual),
        maxDate:new Date(maxAno,maxMes,maxDia),
        value:new Date(valueAno,valueMes,valueDia)
    });

    picker.showDatePickerDialog({
        value: new Date(valueAno,valueMes,valueDia),
        callback: function(e) {
          if (e.cancel) {
            Ti.API.info('User canceled dialog');
          } else {
            Ti.API.info('User selected date: ' + moment(e.value).format('DD/MM/YYYY'));
            $.tfParcela1.setValue(moment(e.value).format('DD/MM/YYYY'));
          }
        }
    });
})


exports.setFocus = function(){

	if(OS_IOS)
	setTimeout(function(){ $.tfParcela1.focus(); }, 200);

	$.viewMain.setHeight("1dp");
	$.viewMain.animate({height: "45dp", duration: 200}, function(){});
};

exports.getValues = function(){
	return {
		parcela:	$.tfParcela1.getValue()
	};
};

exports.setActionApagar = function(action){
	actionApagar = action;
};

$.picMenos.addEventListener("click",function(e){
	if(!editable) return true;
	$.viewContents.animate({left: "-80dp", duration: 200}, function(){});
});

$.btApagar.addEventListener("click",function(e){
	if(!editable) return true;
	setTimeout(function(){ actionApagar(); }, 200);
	if(OS_ANDROID) $.viewMain.animate({height: "1dp", duration: 200}, function(){});
	else if(OS_IOS){
		$.viewBtApagar.animate({height: "0", duration: 200}, function(){});
		$.viewContents.animate({height: "0", duration: 200}, function(){});
	}
});

$.tfParcela1.addEventListener("focus", function (e) {
    showPicker();
});

$.tfParcela1.addEventListener("click", function (e) {
    showPicker();
});