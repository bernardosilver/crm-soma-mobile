exports.show = function () {
    $.viewMainActivityIndicator.setTop(0);
    $.viewMainActivityIndicator.setLeft(0);
    $.activityIndicator.show();
    $.viewMainActivityIndicator.setVisible(true);
};

exports.hide = function () {
    $.activityIndicator.hide();
    $.viewMainActivityIndicator.setVisible(false);
};

exports.setTouch = function (value) {
    if (value) {
        $.viewMainActivityIndicator.setTouchEnabled(true);
        $.viewBackground.setOpacity(0.3);
    } else {
        $.viewMainActivityIndicator.setTouchEnabled(false);
        $.viewBackground.setOpacity(0);
    }
};

exports.setText = function (txt) {
    $.lbText.setText(txt);
}
 
exports.setBackground = function (color) {
    $.viewMainActivityIndicator.setBackgroundColor(color);
};