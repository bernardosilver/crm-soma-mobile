var args = arguments[0] || {};

var _class = args.class;

for (prop in args) {
    if (prop != "class") {
        $.view[prop] = args[prop];
    } else {
        var style = $.createStyle({
            classes: args[prop],
        });
        $.view.applyProperties(style);
    }
}

var actionButton = function () { };
$.button.setVisible(true);

exports.setVisible = function (bool) {
    $.view.setVisible(bool);
    $.view.setTouchEnabled(bool);
};

exports.setText = function (text) {
    text = text || defaultMessage;
    $.label.setText(text);
};

exports.setTop = function (size) {
    $.view.setTop(size);
};

exports.setTextAlign = function (align) {
    $.label.setTextAlign(align);
};

exports.setBtTitle = function (title) {
    title = title || defaultTitle;
    $.button.setTitle(title);
};

exports.setBtAction = function (action) {
    actionButton = action || function () { };
    if (action)
        $.button.setVisible(true);
    else
        $.button.setVisible(false);
};

function click(e) {
    actionButton();
}
