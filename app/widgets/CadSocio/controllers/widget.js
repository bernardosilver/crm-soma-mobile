var args = arguments[0] || {};
var socio = args.socio || ''
var editable = args.editable || false;
var mask = require('Mask');
var actionApagar;

if(socio) {
	$.tfNome.setValue(socio.AO_NOMINS);
	$.tfCpf.setValue(mask.cnpj(socio.AO_CGC));
	$.lbId.setText(socio.ID);
}

$.tfNome.setEditable(editable);
$.tfCpf.setEditable(editable);

function validaCPF(value) {
	var copy = value.replace(".", "").replace(".", "").replace("-", "");
	var num1 = parseInt(copy[0]);
	var num2 = parseInt(copy[1]);
	var num3 = parseInt(copy[2]);
	var num4 = parseInt(copy[3]);
	var num5 = parseInt(copy[4]);
	var num6 = parseInt(copy[5]);
	var num7 = parseInt(copy[6]);
	var num8 = parseInt(copy[7]);
	var num9 = parseInt(copy[8]);
	var num10 = parseInt(copy[9]);
	var num11 = parseInt(copy[10]);

	if (num1 == num2 && num2 == num3 && num3 == num4 && num4 == num5 && num5 == num6 && num6 == num7 && num7 == num8 && num8 == num9 && num9 == num10 && num10 == num11) return false;
	var soma1 = (num1 * 10) + (num2 * 9) + (num3 * 8) + (num4 * 7) + (num5 * 6) + (num6 * 5) + (num7 * 4) + (num8 * 3) + (num9 * 2);
	var resto1 = (soma1*10) % 11;
	if (resto1 == 10 || resto1 == 11) resto1 = 0;
	else if (resto1 != num10) return false;

	var soma2 = (num1 * 11) + (num2 * 10) + (num3 * 9) + (num4 * 8) + (num5 * 7) + (num6 * 6) + (num7 * 5) + (num8 * 4) + (num9 * 3) + (num10 * 2);
	var resto2 = (soma2*10) % 11;
	if (resto2 == 10 || resto2 == 11) resto2 = 0;
	else if (resto2 != num11) return false;
	
	return true;
}

exports.setFocus = function(){

	if(OS_IOS)
	setTimeout(function(){ $.tfNome.focus(); }, 200);

	$.viewMain.setHeight("1dp");
	$.viewMain.animate({height: "100dp", duration: 200}, function(){});
};

exports.getValues = function(){
	return {
        AO_NOMINS:	$.tfNome.getValue(),
		AO_CGC:    $.tfCpf.getValue(),
		ID: $.lbId.getText()
	};
};

exports.setActionApagar = function(action){
	if(!editable) return true
	actionApagar = action;
};

$.picMenos.addEventListener("click",function(e){
	if(!editable) return true;
	$.viewContents.animate({left: "-80dp", duration: 200}, function(){});
});

$.btApagar.addEventListener("click",function(e){
	if(!editable) return true;
	setTimeout(function(){ actionApagar(); }, 200);
	if(OS_ANDROID) $.viewMain.animate({height: "1dp", duration: 200}, function(){});
	else if(OS_IOS){
		$.viewBtApagar.animate({height: "0", duration: 200}, function(){});
		$.viewContents.animate({height: "0", duration: 200}, function(){});
	}
});


$.tfCpf.addEventListener("change", function (e) {
	var v = '';
    v = mask.cpf(e.value);
    $.tfCpf.setColor(validaCPF(v) ? 'blue' : 'red');
    if ($.tfCpf.getValue() != v) $.tfCpf.setValue(v);
});